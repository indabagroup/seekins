<?php

require_once('app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

$type = 'Filedata';
if(isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
    try {
        $uploader = new Varien_File_Uploader($type);
        $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);
        $path_parts = pathinfo($_FILES[$type]['name']);
        $imageSubDirectories = '/' . substr($path_parts['filename'],0,1) . '/' . substr($path_parts['filename'],1,1) . '/';
        $path = Mage::getBaseDir('media') . '/' . 'customer' . $imageSubDirectories;
        /*if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }*/
        $_filename = $path_parts['filename']. '_' . date('Ymd_His'). '.' .$path_parts['extension'];
        $uploader->save($path, $_filename );
        $filename = $uploader->getUploadedFileName();
        echo $imageSubDirectories . $filename;
    } catch (Exception $e) {
        Mage::logException($e);
    }
}