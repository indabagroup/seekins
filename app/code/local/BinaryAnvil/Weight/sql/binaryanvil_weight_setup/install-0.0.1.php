<?php

$installer =Mage::getModel('eav/entity_setup','core_setup');;
$installer->startSetup();
$installer->addAttribute('catalog_product', 'sp_weight', array(
    'group'             => 'General',
    'type'              => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'label'             => 'SP Weight',
    'input'             => 'text',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required'          => false,
    'user_defined'      => true,
    'visible_on_front'  => true,
    'unique'            => false,
));

$installer->endSetup();
