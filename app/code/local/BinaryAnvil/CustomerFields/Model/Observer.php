<?php

class BinaryAnvil_CustomerFields_Model_Observer {

    public function saveCustomCustomerAttributes($observer){
        $customer = $observer->getCustomer();
        $params = Mage::app()->getRequest()->getParams();
        $customerModel = Mage::getModel('customer/customer')->load($customer->getId());
        if(isset($params['ffl_image_hidden'])){
            Mage::log($params['ffl_image_hidden']);
            $customerModel->setData('ffl_image', $params['ffl_image_hidden']);
        }
        if(isset($params['bl_image_hidden'])){
            Mage::log($params['bl_image_hidden']);
            $customerModel->setData('bl_image', $params['bl_image_hidden']);
        }
        if(isset($params['rs_permit_hidden'])){
            Mage::log($params['rs_permit_hidden']);
            $customerModel->setData('rs_permit', $params['rs_permit_hidden']);
        }
        $customerModel->save();
    }

}