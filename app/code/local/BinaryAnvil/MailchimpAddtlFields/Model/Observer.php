<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/27/14
 * Time: 10:01 AM
 */

class BinaryAnvil_MailchimpAddtlFields_Model_Observer
{
    public function magemonkeyMergevarsAfter(Varien_Event_Observer $observer)
    {
        $merge_vars = $observer->getEvent()->getVars();
        $customer = $observer->getEvent()->getCustomer();
        $blank = $observer->getEvent()->getNewVars();
        $request = Mage::app()->getRequest();
        if( !$customer->getId()){
            if ($request->getPost('firstname') ) {
                $guestFirstName = $request->getPost('firstname');
            } else {
                $guestFirstName = $this->config('guest_name', $customer->getStoreId());
            }

            if($guestFirstName){
                $merge_vars['FNAME'] = $guestFirstName;
            }
        }
        if( !$customer->getId() ){
            if ($request->getPost('lastname')) {
                $guestLastName  = $request->getPost('lastname');
            } else {
                $guestLastName  = $this->config('guest_lastname', $customer->getStoreId());
            }


            if($guestLastName){
                $merge_vars['LNAME'] = $guestLastName;
            }
        }
        if ($request->getControllerName() == 'subscriber' && $request->getModuleName() == 'newsletter') {
            if ($request->getPost('city') ) {
                $city = $request->getPost('city');
                if($city){
                    $merge_vars['CITY'] = $city;
                }
            }

            if ($request->getPost('country_id') ) {
                $country = Mage::getModel('directory/country')->load($request->getPost('country_id'))->getName();
                if($country){
                    $merge_vars['COUNTRY'] = $country;
                }
            }
            $region = $request->getPost('region') != '' ? $request->getPost('region') : '';
            if ($request->getPost('region_id') ) {
                $region = Mage::getSingleton('directory/region')->load($request->getPost('region_id'))->getName();

            }
            if($region){
                $merge_vars['STATE'] = $region;
            }

            if ($request->getPost('zip_code') ) {
                $zip = $request->getPost('zip_code');
                if($zip){
                    $merge_vars['ZIPCODE'] = $zip;
                }
            }
        } else {
            if ($customer->getId()) {
                if($customer->getData('default_billing')) {
                    $address = Mage::getModel('customer/address')->load($customer->getData('default_billing'));
                    if ($address) {
                        $merge_vars['CITY'] = $address->getCity();
                        $merge_vars['COUNTRY'] = Mage::getModel('directory/country')->load($address->getCountryId())->getName();
                        $region = (!$address->getRegion() ? $address->getCity() : $address->getRegion());

                        if ($address->getRegionId()) {
                        }   $region = Mage::getSingleton('directory/region')->load($address->getRegionId())->getName();
                        $merge_vars['STATE'] = $region;
                        $merge_vars['ZIPCODE'] = $address->getPostcode();
                    }
                }
            }
        }
        //Mage::log($request->getControllerName()."-".$request->getModuleName());
        Mage::log($merge_vars);
        $blank[] = $merge_vars;
        Mage::log($blank);
        $observer->getEvent()->setNewVars($merge_vars);
        return $this;
        //return $merge_vars;
    }
}