<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 4/21/14
 * Time: 4:36 PM
 */

$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->updateAttribute ( 'customer', 'sp_disable_as_dealer_locator', 'sort_order', 151 );

$installer->getConnection()->addColumn($installer->getTable('growdevelopment_storelocation'), 'customer_id',
    'int(11) NOT NULL AFTER id'
);

$installer->getConnection()->addColumn($this->getTable('growdevelopment_storelocation'),
    'customer_email',
    'varchar(255) AFTER customer_id'
);

$installer->endSetup();
