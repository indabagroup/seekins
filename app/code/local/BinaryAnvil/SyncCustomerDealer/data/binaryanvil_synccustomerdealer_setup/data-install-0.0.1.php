<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 4/21/14
 * Time: 4:12 PM
 */
$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->addAttribute('customer', 'sp_disable_as_dealer_locator', array(
    'type' => 'int',
    'input' => 'select',
    'label' => 'Disable from displaying as Dealer Locator',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default' => '0',
    'visible_on_front' => 0,
    'is_system' => 0,
    'source' => 'eav/entity_attribute_source_boolean',
));

$defaultUsedInForms = array(
    /*'customer_account_create',
    'customer_account_edit',*/
    'adminhtml_customer'
);

$customer = Mage::getModel('customer/customer');
$attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
$setup->addAttributeToSet('customer', $attrSetId, 'General', 'sp_disable_as_dealer_locator');

if (version_compare(Mage::getVersion(), '1.4.2', '>=')) {
    Mage::getSingleton('eav/config')
        ->getAttribute('customer', 'sp_disable_as_dealer_locator')
        ->setData('used_in_forms', $defaultUsedInForms)
        ->save();
}

$installer->endSetup();
