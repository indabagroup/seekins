<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 4/23/14
 * Time: 5:00 PM
 */

$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->updateAttribute ( 'customer', 'sp_disable_as_dealer_locator', 'default_value', 1 );

$installer->endSetup();