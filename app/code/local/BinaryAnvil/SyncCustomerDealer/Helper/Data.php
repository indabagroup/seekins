<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 4/21/14
 * Time: 3:57 PM
 */

class BinaryAnvil_SyncCustomerDealer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'binaryanvil_synccustomerdealer/settings/enabled';

    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 2;

    const STORE_TYPE_ONLINE             = 1;
    const STORE_TYPE_PHYSICAL           = 2;
    const STORE_TYPE_ONLINE_PHYSICAL    = 3;
    const STORE_TYPE_DEALER             = 4;
    const STORE_TYPE_DISTRIBUTOR        = 5;

    const ZOOM_LEVEL_ONE            = 1;
    const ZOOM_LEVEL_TWO            = 2;
    const ZOOM_LEVEL_THREE          = 3;
    const ZOOM_LEVEL_FOUR           = 4;
    const ZOOM_LEVEL_FIVE           = 5;
    const ZOOM_LEVEL_SIX            = 6;
    const ZOOM_LEVEL_SEVEN          = 7;
    const ZOOM_LEVEL_EIGHT          = 8;
    const ZOOM_LEVEL_NINE           = 9;
    const ZOOM_LEVEL_TEN            = 10;
    const ZOOM_LEVEL_ELEVEN         = 11;
    const ZOOM_LEVEL_TWELVE         = 12;
    const ZOOM_LEVEL_THIRTEEN       = 13;
    const ZOOM_LEVEL_FOURTEEN       = 14;
    const ZOOM_LEVEL_FIFTEEN        = 15;
    const ZOOM_LEVEL_SIXTEEN        = 16;
    const ZOOM_LEVEL_SEVENTEEN      = 17;
    const ZOOM_LEVEL_EIGHTEEN       = 18;
    const ZOOM_LEVEL_NINETEEN       = 19;


    public function isEnabled($storeId)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $storeId);
    }

    public function logError($message)
    {
        if (Mage::getStoreConfig('binaryanvil_synccustomerdealer/settings/general_error')) {
            Mage::log($message, null, 'synccustomerdealer.log', true);
        }

        return $this;
    }
}