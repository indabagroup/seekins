<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 4/21/14
 * Time: 3:39 PM
 */

class BinaryAnvil_SyncCustomerDealer_Model_Observer
{
    public function syncCustomerDealer(Varien_Event_Observer $observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        if (!Mage::helper('binaryanvil_synccustomerdealer')->isEnabled($customer->getData('store_id'))) {
            return false;
        }

        $customerSpDisableAsDealerLocator = $customer->getOrigData('sp_disable_as_dealer_locator');
        if ($customerSpDisableAsDealerLocator == '' || $customerSpDisableAsDealerLocator == 0) {
            $customerSpDisableAsDealerLocator = 0;
        }
        if (Mage::app()->getStore()->isAdmin()) {
            if (!$customer->getOrigData('customer_activated') && $customer->getCustomerActivated()) {
                $this->_createDealerLocator($customer);
            } elseif ($customerSpDisableAsDealerLocator != $customer->getSpDisableAsDealerLocator()) {
                $this->_createDealerLocator($customer);
            }
        }
    }

    public function syncCustomerDealerAddress($observer)
    {
        /** @var $customerAddress Mage_Customer_Model_Address */
        $customerAddress = $observer->getCustomerAddress();
        $customer = $customerAddress->getCustomer();
        if (!Mage::helper('binaryanvil_synccustomerdealer')->isEnabled($customerAddress->getData('store_id'))) {
            return false;
        }

        if (Mage::app()->getStore()->isAdmin()) {
            $orig = $customerAddress->getOrigData();
            unset($orig['is_customer_save_transaction']);
            unset($orig['updated_at']);
            $data = $customerAddress->getData();
            unset($data['updated_at']);
            if (array_diff($orig,$data)) {
                $this->_createDealerLocator($customer);
            }
        } else {
            $orig = $customerAddress->getOrigData();
            unset($orig['is_customer_save_transaction']);
            unset($orig['updated_at']);
            $data = $customerAddress->getData();
            unset($data['is_customer_save_transaction']);
            unset($data['updated_at']);
            unset($data['store_id']);
            //Mage::log($orig);
            //Mage::log($data);
            //Mage::log($customer->getData());
            if ($customer->getData('default_billing') == $customerAddress->getId()) {
                //if (is_array($orig) && is_array($data) && array_diff($orig,$data)) {
                    //Mage::log("frontend: pass here");
                    $this->_createDealerLocator($customerAddress->getCustomer());
                //}
            }
        }
    }

//    public function syncCustomerDealerAddressBefore($observer)
//{
//    /** @var $customerAddress Mage_Customer_Model_Address */
//    $customerAddress = $observer->getCustomerAddress();
//    $customer = $customerAddress->getCustomer();
//    if (!Mage::helper('binaryanvil_synccustomerdealer')->isEnabled($customerAddress->getData('store_id'))) {
//        return false;
//    }
////        $request = Mage::app()->getRequest();
////        Mage::log($request->getParams());
////        Mage::log("start - before");
//    if (!Mage::app()->getStore()->isAdmin()) {
//        $orig = $customerAddress->getOrigData();
//        unset($orig['is_customer_save_transaction']);
//        unset($orig['updated_at']);
//        $data = $customerAddress->getData();
//        unset($data['is_customer_save_transaction']);
//        unset($data['updated_at']);
//        unset($data['store_id']);
//        unset($data['customer_id']);
//        //Mage::log($orig);
//        //Mage::log($data);
//        //Mage::log($customer->getData());
//        if ($customer->getData('default_billing') == $customerAddress->getId()) {
//            if (is_array($orig) && is_array($data) && array_diff($orig,$data)) {
//                Mage::log("frontend  before: pass here");
//                //$this->_createDealerLocator($customerAddress->getCustomer());
//            }
//        }
//    }
//    Mage::log("end - before");
//}

    protected function _createDealerLocator(Mage_Customer_Model_Customer $customer)
    {
        $customerAddress = Mage::getModel('customer/address')->load($customer->getData('default_billing'));
        if ($customer->getData('default_billing')) {
            $region_id = $customerAddress->getData('region_id');
            $regionId = isset($region_id) ? $customerAddress->getData('region_id') : $customerAddress->getData('region');
            $street1 = $customerAddress->getStreet(1);
            $street2 = $customerAddress->getStreet(2);
            $company = $customerAddress->getData('company');
            $street = isset($street2) ? $street1. " ". $street2: $street1;
            $country = Mage::getModel('directory/country')->load($customerAddress->getData('country_id'))->getName();
            $address = $street. "," . $customerAddress->getData('city'). "," .$customerAddress->getData('region'). " ". $customerAddress->getData('postcode'). "," .$country;
            $result = $this->_getLongitudeLatitude($address);

            if ($result['message'] == 'ok') {
                $model = Mage::getModel('storelocation/storelocation')
                    ->setId($this->_checkDealer($customer, $company))
                    ->setStoreName($customerAddress->getData('company'))
                    ->setOwnerName($customer->getName())
                    ->setStreet($street)
                    ->setCity($customerAddress->getData('city'))
                    ->setLocationRegionId($regionId)
                    ->setPostalCode($customerAddress->getData('postcode'))
                    ->setLocationCountryId($customerAddress->getData('country_id'))
                    ->setPhone($customerAddress->getData('phone'))
                    ->setUrl()
                    ->setStoreType(BinaryAnvil_SyncCustomerDealer_Helper_Data::STORE_TYPE_DEALER)
                    ->setDescription()
                    ->setGoogleLatitude($result['latitude'])
                    ->setGoogleLongitude($result['longitude'])
                    ->setGoogleZoomLevel(BinaryAnvil_SyncCustomerDealer_Helper_Data::ZOOM_LEVEL_ONE);
                try {
                    if ($customer->getData('sp_disable_as_dealer_locator') || (!$this->_checkDealer($customer, $company) && $customer->getData('sp_disable_as_dealer_locator'))) {
                        $model->setStatus(BinaryAnvil_SyncCustomerDealer_Helper_Data::STATUS_ENABLED);
                    } else {
                        $model->setStatus(BinaryAnvil_SyncCustomerDealer_Helper_Data::STATUS_DISABLED);
                    }
                    $model->setCreatedTime(now());
                    $model->setUpdateTime(now());
                    $model->setCustomerId($customer->getId());
                    $model->setCustomerEmail($customer->getEmail());
                    $model->save();
                    if ($this->_checkDealer($customer, $company)) {
                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Dealer locator was successfully updated'));
                    } else {
                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Dealer locator was successfully created'));
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError('An error occurred while creating the corresponding dealer locator'.$e->getMessage());
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Dealer Locator was not created.'.$result['message']);
                $message = 'Dealer Locator was not created.'.$result['message'];
                $message .= $customer->getId();
                Mage::helper('binaryanvil_synccustomerdealer')->logError($message);
            }
        } else {
            $message = 'Dealer Locator was not created. No address';
            $message .= $customer->getId();
            Mage::helper('binaryanvil_synccustomerdealer')->logError($message);
        }
    }

    protected function _getLongitudeLatitude($address)
    {
        $address = urlencode($address);

        //$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=false";
        //$xml = simplexml_load_file($request_url);
        //$status = $xml->status;
        $message = '';
        $latitude = '';
        $longitude = '';
        //$isSecure = Mage::app()->getStore()->isCurrentlySecure();
        //if ($isSecure):
        $url = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false");
        $response = json_decode($url);
        $status = $response->status;
        if ($status == "OK") {
            //$latitude = $xml->result->geometry->location->lat;
            //$longitude = $xml->result->geometry->location->lng;
            $lat = $response->results[0]->geometry->location->lat;
            $long = $response->results[0]->geometry->location->lng;
            $result = array(
                'message'   => 'ok',
                'latitude'  => $lat,
                'longitude' => $long
            );
            return $result;
        } elseif ($status == "ZERO_RESULTS") {
            $message = 'No Results. Address passed maybe a non-existent.';
        } elseif ($status == "OVER_QUERY_LIMIT") {
            $message = 'Over the quota';
        } elseif ($status == "REQUEST_DENIED") {
            $message = 'Request was denied. Lack of sensor parameter';
        } elseif ($status == "INVALID_REQUEST") {
            $message = 'Invalid Request. Query is missing address';
        } elseif ($status == "UNKNOWN_ERROR") {
            $message = 'Request could not be processed due to a server error.';
        }

        return array('message' => $message, 'latitude' => $latitude, 'longitude' => $longitude);
    }

    protected function _checkDealer(Mage_Customer_Model_Customer $customer, $storeName)
    {

        $name = $customer->getName();
        Mage::log($name, null, 'synccustomerdealer.log', true);
        Mage::log($storeName, null, 'synccustomerdealer.log', true);

        $collection = Mage::getModel('storelocation/storelocation')
            ->getCollection()
            ->addFieldToFilter('customer_id',array('eq' => $customer->getId()))
            ->addFieldToFilter('customer_email',array('eq' => $customer->getEmail()))
            ->getFirstItem();
        Mage::log("customer_id".$customer->getId(), null, 'synccustomerdealer.log', true);
        if ($collection->getId()) {
            return $collection->getId();
        } else {
            $collection = Mage::getModel('storelocation/storelocation')
                ->getCollection()
                ->addFieldToFilter('store_name',array('eq' => $storeName))
                ->addFieldToFilter('owner_name',array('eq' => $name))
                ->getFirstItem();
            Mage::log("store_name".$storeName."-".$name, null, 'synccustomerdealer.log', true);
            return $collection->getId();
        }

        return false;
    }
}