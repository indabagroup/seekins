<?php
/**
 * Magento Enterprise Edition
 * 
 * Custompaygatemsg extension provides an additional information for Gateway error message. Specifically,
 * 'This transaction has been declined.'
 * 
 * @category    Mage
 * @package     BinaryAnvil_Paygate
 * @copyright   
 * @license     
 */


class BinaryAnvil_Custompaygatemsg_Model_Authorizenet extends Mage_Paygate_Model_Authorizenet
{

    /**
     * Gateway response wrapper
     *
     * @param string $text
     * @return string
     */
    protected function _wrapGatewayError($text)
    {
		$custom_msg = "\n\n\n Using a Prepaid Gift Card? Please check the back of \n the card for online registration instructions. Zip Code \n must match both the card and the billing and \n shipping address for your purchase.\n \n Thank you and please contact GI Brick Customer \n Service at 971-338-9454 for further assistance.";
    	
    	$errFilterMsg = 'This transaction has been declined.';
    	if(strcmp($text, $errFilterMsg )==0){
    		$text .= $custom_msg;
    	}
    	
        return Mage::helper('paygate')->__('Gateway error: %s', $text);
    }

}
