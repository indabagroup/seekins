<?php

class BinaryAnvil_Directory_Helper_Data extends Mage_Directory_Helper_Data
{
    protected function _getRegions($storeId)
    {
        $countryIds = array();

        $countryCollection = $this->getCountryCollection()->loadByStore($storeId);
        foreach ($countryCollection as $country) {
            $countryIds[] = $country->getCountryId();
        }

        /** @var $regionModel Mage_Directory_Model_Region */
        $regionModel = $this->_factory->getModel('directory/region');
        /** @var $collection Mage_Directory_Model_Resource_Region_Collection */
        $collection = $regionModel->getResourceCollection()
            ->addCountryFilter($countryIds)
            ->load();

        $regions = array(
            'config' => array(
                'show_all_regions' => $this->getShowNonRequiredState(),
                'regions_required' => $this->getCountriesWithStatesRequired()
            )
        );
        $excluderegions = array('AS', 'AF', 'AA', 'AC', 'AE', 'AM', 'AP', 'FM', 'GU', 'MP', 'PW', 'PR', 'VI', 'MH');
        foreach ($collection as $region) {
            if (!$region->getRegionId()) {
                continue;
            }

            $regionCode = $region->getCode();
            if (in_array($regionCode, $excluderegions)) {
                continue;
            }
            $regions[$region->getCountryId()][$region->getRegionId()] = array(
                'code' => $region->getCode(),
                'name' => $this->__($region->getName())
            );
        }
        Mage::log($regions,null,'debug.log');
        return $regions;
    }
}