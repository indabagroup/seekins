<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/16/14
 * Time: 7:25 PM
 */

class BinaryAnvil_Media_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getPath($filename)
    {
        return Mage::getBaseUrl('media')."seekins_media/".$filename;
    }
}