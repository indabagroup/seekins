<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/16/14
 * Time: 7:39 PM
 */

class BinaryAnvil_Media_Adminhtml_MediaController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction()
    {
        $mediaId = $this->getRequest()->getParam('id');

        $model = Mage::getModel('binaryanvil_media/media')->load($mediaId);
        if ($model->getData('entity_id') || $mediaId == 0)
        {
            Mage::register('current_media_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('customer/binaryanvil_media');
            $this->getLayout()->getBlock('head')
                ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                ->createBlock('binaryanvil_media/adminhtml_media_edit'))
                ->_addLeft($this->getLayout()
                        ->createBlock('binaryanvil_media/adminhtml_media_edit_tabs')
                );
            $breadcrumbMessage = $mediaId ? Mage::helper('binaryanvil_media')->__('Edit media')
                : Mage::helper('binaryanvil_media')->__('Add new media');
            $this->_addBreadcrumb($breadcrumbMessage, $breadcrumbMessage);
            $this->renderLayout();
        }
        else
        {
            Mage::getSingleton('adminhtml/session')
                ->addError('Customer media does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    protected $_filename;

    protected function _saveFile()
    {
        $errors = array();

        if (isset($_FILES['file_name']['name']) && $_FILES['file_name']['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader('file_name');
                $uploader->setAllowedExtensions(array('pdf', 'zip'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'seekins_media' . DS;
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $uploader->save($path, $_FILES['file_name']['name']);
                $this->_filename = $uploader->getUploadedFileName();

            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->__( 'An error occurred while saving media information.' );
                $errors[] = $message;
            }
        } else {
            $id  = $this->getRequest()->getParam('id');
            if (empty($id)) {
                $message = $this->__( 'The media image cannot be empty.' );
                $errors[] = $message;
            }
        }//if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

        return $errors;
    }

    public function validate()
    {
        $errors = array();
        $post = $this->getRequest()->getPost();

        if (!Zend_Validate::is( trim($post['file_human_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_media')->__('The File Human Name cannot be empty.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    public function saveAction()
    {
        $post = $this->getRequest()->getPost();
        $id = $this->getRequest()->getParam('id');

        $errors = array();

        $formErrors = $this->validate();
        if ($formErrors !== true) {
            $errors = array_merge($errors, $formErrors);
        }

        $fileErrors = $this->_saveFile();
        if (count($fileErrors) !== 0) {
            $errors = array_merge($errors, $fileErrors);
        }

        if (count($errors) === 0) {
            try {
                $date = Mage::getModel( 'core/date' )->gmtDate();
                $message = $this->__( 'Media was successfully saved.' );
                if (isset($id)) {
                    $post['entity_id'] = $id;
                    $message = $this->__( 'Media was successfully edited.' );
                } else {
                    $post['time_stamp'] = $date;
                    $post['created_time'] = $date;
                }
                if ($this->_filename) {
                    $post['file_name'] = $this->_filename;
                }
                $model = Mage::getModel('binaryanvil_media/media');
                $model->setData($post);
                $model->setUpdatedTime($date);
                $model->save();
                $this->_getSession()->addSuccess( $message );
                $this->_redirect( '*/*/edit',array('id'=>$model->getId()));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->__( 'An error occurred while saving media information.' );
                $this->_getSession()->addError( $message );
            }
        } else {
            foreach ($errors as $errorMessage) {
                $this->_getSession()->addError($errorMessage);
            }
        } // if (count($errors) === 0) {

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {

        if( $this->getRequest()->getParam('id') > 0 ){
            try{

                $mediaId = $this->getRequest()->getParam('id');
                $media = Mage::getModel('binaryanvil_media/media');
                $media->setId($mediaId)->delete();

                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('media was successfully deleted'));
                $this->_redirect('*/*/');

            } catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        //die("massDeleteAction");
        $mediaIds = $this->getRequest()->getParam('id');
        $session = Mage::getSingleton('adminhtml/session');
        if (!is_array($mediaIds)) {
            $session->addError($this->__('Please select media(s).'));
        } else {
            if (!empty($mediaIds)) {
                try {
                    foreach ($mediaIds as $mediaId) {
                        $media = Mage::getModel('binaryanvil_media/media')->load($mediaId);
                        $media->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($mediaIds))
                    );
                } catch (Exception $e) {
                    $session->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massPublishAction()
    {
        $mediaIds = $this->getRequest()->getParam('id');
        $status     = (int)$this->getRequest()->getParam('publish');
        $session = Mage::getSingleton('adminhtml/session');
        if (!is_array($mediaIds)) {
            $session->addError($this->__('Please select product(s).'));
        } else {
            if (!empty($mediaIds)) {
                try {
                    foreach ($mediaIds as $mediaId) {
                        $media = Mage::getModel('binaryanvil_media/media')->load($mediaId);
                        $media->setPublish($status);
                        $media->save();

                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been updated.', count($mediaIds))
                    );
                } catch (Mage_Core_Model_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Mage_Core_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Exception $e) {
                    $this->_getSession()
                        ->addException($e, $this->__('An error occurred while updating the media(s) publish.'));
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massAvailabilityAction()
    {
        $mediaIds = $this->getRequest()->getParam('id');
        $availability = (int)$this->getRequest()->getParam('availability');
        $session = Mage::getSingleton('adminhtml/session');
        if (!is_array($mediaIds)) {
            $session->addError($this->__('Please select media(s).'));
        } else {
            if (!empty($mediaIds)) {
                try {
                    foreach ($mediaIds as $mediaId) {
                        $media = Mage::getModel('binaryanvil_media/media')->load($mediaId);
                        $media->setAvailability($availability);
                        $media->save();

                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been updated.', count($mediaIds))
                    );
                } catch (Mage_Core_Model_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Mage_Core_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Exception $e) {
                    $this->_getSession()
                        ->addException($e, $this->__('An error occurred while updating the media(s) publish.'));
                }
            }
        }
        $this->_redirect('*/*/index');
    }
}