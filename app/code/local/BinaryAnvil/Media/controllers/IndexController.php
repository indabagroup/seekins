<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/18/14
 * Time: 9:16 AM
 */

class BinaryAnvil_Media_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
    }
}