<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/16/14
 * Time: 7:30 PM
 */

$installer = $this;

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('sp_media')};
CREATE TABLE {$this->getTable('sp_media')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `file_name` text DEFAULT NULL,
  `file_human_name` text DEFAULT NULL,
  `file_description` varchar(150) NOT NULL default '',
  `availability` varchar(255) NOT NULL default '',
  `publish` varchar(255) NOT NULL default '',
  `time_stamp` datetime NULL,
  `created_time` datetime NULL,
  `updated_time` datetime NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();