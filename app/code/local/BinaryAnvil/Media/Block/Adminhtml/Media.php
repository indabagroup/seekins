<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 6:58 PM
 */

class BinaryAnvil_Media_Block_Adminhtml_Media extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        /*
         * note :
         *  - _blockGroup - is your module name
         *  - _controller - is actually the path to your block class (NOT YOUR CONTROLLER).
         */
        $this->_blockGroup = 'binaryanvil_media';
        $this->_controller = 'adminhtml_media';
        $this->_headerText = Mage::helper('binaryanvil_media')->__('Manage Media');
        parent::__construct();

    }
}