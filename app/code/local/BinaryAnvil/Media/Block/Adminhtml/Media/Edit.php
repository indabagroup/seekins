<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:00 PM
 */

class BinaryAnvil_Media_Block_Adminhtml_Media_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {

        $this->_objectId = 'id';
        $this->_blockGroup = 'binaryanvil_media';
        $this->_controller = 'adminhtml_media';


        $this->_updateButton('save', 'label', Mage::helper('binaryanvil_media')->__('Save media'));
        $this->_updateButton('delete', 'label', Mage::helper('binaryanvil_media')->__('Delete media'));
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }";

        parent::__construct();

    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_media_data')->getId()) {
            return $this->escapeHtml('Edit media');
        } else {
            return Mage::helper('binaryanvil_media')->__('Add New media');
        }
    }

}