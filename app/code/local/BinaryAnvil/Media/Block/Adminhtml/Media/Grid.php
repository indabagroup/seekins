<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:02 PM
 */

class BinaryAnvil_Media_Block_Adminhtml_Media_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('adminhtmlMediaGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return BinaryAnvil_Media_Model_Media
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('binaryanvil_media/media_collection');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('binaryanvil_media')->__('ID'),
            'width'     => '300',
            'index'     => 'entity_id',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('file_name', array(
            'header'    => Mage::helper('binaryanvil_media')->__('File Name'),
            'width'     => '300',
            'index'     => 'file_name',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('file_human_name', array(
            'header'    => Mage::helper('binaryanvil_media')->__('File Human Name'),
            'width'     => '300',
            'index'     => 'file_human_name',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('file_description', array(
            'header'    => Mage::helper('binaryanvil_media')->__('File Description'),
            'width'     => '300',
            'index'     => 'file_description',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('availability', array(
            'header'    => Mage::helper('binaryanvil_media')->__('Availability'),
            'width'     => '300',
            'index'     => 'availability',
            'type'      => 'options',
            'options'   => Mage::getSingleton('binaryanvil_media/adminhtml_system_config_source_availability')->toArray()
        ));

        $this->addColumn('publish', array(
            'header'    => Mage::helper('binaryanvil_media')->__('Publish'),
            'width'     => '300',
            'index'     => 'publish',
            'type'      => 'options',
            'options'   => Mage::getSingleton('binaryanvil_media/adminhtml_system_config_source_status')->toArray()
        ));

        $this->addColumn('time_stamp', array(
            'header'    => Mage::helper('binaryanvil_media')->__('Time Stamp'),
            'width'     => '300',
            'index'     => 'time_stamp',
            'type'      => 'datetime'
        ));
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('binaryanvil_media')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getEntityId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('binaryanvil_media')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'entity_id',
                'is_system' => true,
            ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('binaryanvil_media')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('binaryanvil_media')->__('Are you sure?')
        ));

        $availability = Mage::getSingleton('binaryanvil_media/adminhtml_system_config_source_availability')->toOptionArray();

        //array_unshift($statuses, array('label'=>'', 'value'=>''));

        $this->getMassactionBlock()->addItem('availability', array(
            'label'=> Mage::helper('binaryanvil_media')->__('Change Availability'),
            'url'  => $this->getUrl('*/*/massAvailability', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'availability',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('binaryanvil_media')->__('Availability'),
                    'values' => $availability
                )
            )
        ));

        $statuses = Mage::getSingleton('binaryanvil_media/adminhtml_system_config_source_status')->toOptionArray();

        //array_unshift($statuses, array('label'=>'', 'value'=>''));

        $this->getMassactionBlock()->addItem('publish', array(
            'label'=> Mage::helper('binaryanvil_media')->__('Change Publish'),
            'url'  => $this->getUrl('*/*/massPublish', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'publish',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('binaryanvil_media')->__('Publish'),
                    'values' => $statuses
                )
            )
        ));

        return $this;

    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}