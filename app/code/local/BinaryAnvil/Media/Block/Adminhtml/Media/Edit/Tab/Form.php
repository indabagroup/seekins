<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:30 PM
 */

class BinaryAnvil_Media_Block_Adminhtml_Media_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('media_form',
            array('legend' => 'Media Information'));

        $mediaFileName = Mage::registry('current_media_data')->getData('file_name');
        if($mediaFileName) {
            $file = Mage::helper('binaryanvil_media')->getPath($mediaFileName);
            $DownloadURL = '&nbsp;&nbsp;<a href='.$file.' target="_blank">Download</a>';
        } else {
            $DownloadURL = '';
        }
        $fieldset->addField('file_name', 'file', array(
            'name'      => 'file_name',
            'class'     => (($mediaFileName) ? '' : 'required-entry'),
            'required'  => (($mediaFileName) ? false : true),
            'label'     => Mage::helper('binaryanvil_media')->__('File Name'),
            'title'     => Mage::helper('binaryanvil_media')->__('File Name'),
            'after_element_html' => $DownloadURL,
            'readonly' => (($mediaFileName) ? 'true' : 'false'),
        ));

        $fieldset->addField('file_human_name', 'text',
            array(
                'label' => 'File Human Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'file_human_name',
            ));
        $afterElementHtml = '<p class="note">Maximum Characters: 150</p>';
        $fieldset->addField('file_description', 'textarea',
            array(
                'label' => 'File Description',
                'max_length' => '150',
                'class' => 'maximum-length-150 validate-length required-entry',
                'required' => true,
                'name' => 'file_description',
                'after_element_html' => $afterElementHtml,
            ));

        $fieldset->addField('availability', 'select', array(
            'label'     => 'Availability',
            'name'      => 'availability',
            'class'      => '',
            'required'  => true,
            'values'    => Mage::getSingleton('binaryanvil_media/adminhtml_system_config_source_availability')->toOptionArray()
        ));

//        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
//        $timeStamp = Mage::registry('current_media_data')->getData('time_stamp');
//        $fieldset->addField('time_stamp', 'date', array(
//            'name'   => 'time_stamp',
//            'label'  => Mage::helper('binaryanvil_media')->__('Time Stamp'),
//            'title'  => Mage::helper('binaryanvil_media')->__('Time Stamp'),
//            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
//            'input_format' => Varien_Date::DATETIME_PHP_FORMAT,
//            'format'       => $dateFormatIso,
//            'time' => true,
//            'readonly' => (($timeStamp) ? 'true' : 'false'),
//        ));

        $fieldset->addField('publish', 'select', array(
            'label'     => 'Publish',
            'name'      => 'publish',
            'class'      => '',
            'required'  => true,
            'values'    => Mage::getSingleton('binaryanvil_media/adminhtml_system_config_source_status')->toOptionArray()
        ));

        if (Mage::registry('current_media_data')) {
            $form->setValues(Mage::registry('current_media_data')->getData());

        }
        return parent::_prepareForm();
    }
}