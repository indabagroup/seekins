<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:30 PM
 */

class BinaryAnvil_Media_Block_Adminhtml_Media_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('media_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('binaryanvil_media')->__('Media'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => 'Media Information',
            'title' => 'Media Information',
            'content'   => $this->getLayout()
                ->createBlock('binaryanvil_media/adminhtml_media_edit_tab_form')
                ->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}