<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/18/14
 * Time: 9:19 AM
 */

class BinaryAnvil_Media_Block_Media extends Mage_Core_Block_Template
{

    public function getCurrentCustomer()
    {

        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getMediaList()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $collection = Mage::getResourceModel('binaryanvil_media/media_collection')
            ->addFieldToFilter('publish',array('eq'=>1));
        if ($storeId != Mage::getStoreConfig('binaryanvil_media/settings/b2b_store_id')) {
            $collection->addFieldToFilter('availability',array('neq'=>2));
        }

        return $collection;
    }
}