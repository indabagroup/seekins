<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:05 PM
 */

class BinaryAnvil_Media_Model_Resource_Media_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_media/media');
    }
}