<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/18/14
 * Time: 10:03 AM
 */

class BinaryAnvil_Media_Model_Observer
{
    protected $_redirectSetFlag = false;

    public function controllerActionPredispatch(Varien_Event_Observer $observer)
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $session = Mage::getSingleton('customer/session');
        Mage::log("start:". $this->_redirectSetFlag);
        $whiteList = Mage::getStoreConfig('binaryanvil_media/settings/requirelogin_whitelist');
        if ($this->_redirectSetFlag || $storeId != 2)
        {
            return;
        }

        $controllerAction = $observer->getEvent()->getControllerAction();
        /* @var $controllerAction Mage_Core_Controller_Front_Action */
        $requestString = $controllerAction->getRequest()->getRequestString();
        if (!$session->isLoggedIn() && preg_match($whiteList, $requestString)) {
            $controllerAction->getResponse()->setRedirect(Mage::getUrl('media'));
            $controllerAction->getResponse()->sendResponse();
            $this->_redirectSetFlag = true;
            Mage::log("end:". $this->_redirectSetFlag);
            exit;
        }
    }
}