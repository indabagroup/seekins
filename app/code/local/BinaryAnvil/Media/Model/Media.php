<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:03 PM
 */

class BinaryAnvil_Media_Model_Media extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_media/media');
    }
}