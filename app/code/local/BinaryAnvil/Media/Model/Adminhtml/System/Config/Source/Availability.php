<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:11 PM
 */

class BinaryAnvil_Media_Model_Adminhtml_System_Config_Source_Availability
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label'=>Mage::helper('adminhtml')->__('-- Please Select --')),
            array('value' => '0', 'label'=>Mage::helper('adminhtml')->__('All User Groups')),
            array('value' => '1', 'label'=>Mage::helper('adminhtml')->__('Public')),
            array('value' => '2', 'label'=>Mage::helper('adminhtml')->__('B2B')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('All User Groups'),
            1 => Mage::helper('adminhtml')->__('Public'),
            2 => Mage::helper('adminhtml')->__('B2B'),
        );
    }
}