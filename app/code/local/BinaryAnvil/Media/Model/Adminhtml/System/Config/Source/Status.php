<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 7:14 PM
 */

class BinaryAnvil_Media_Model_Adminhtml_System_Config_Source_Status
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label'=>Mage::helper('adminhtml')->__('-- Please Select --')),
            array('value' => '0', 'label'=>Mage::helper('adminhtml')->__('No')),
            array('value' => '1', 'label'=>Mage::helper('adminhtml')->__('Yes')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('No'),
            1 => Mage::helper('adminhtml')->__('Yes'),
        );
    }
}