<?php
/**
 * Package: BinaryAnvil
 * User: edgarcia
 * Date: 5/20/15
 * Time: 6:00 PM
 */
class BinaryAnvil_Accountsecurity_QuestionController extends Mage_Core_Controller_Front_Action {

    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
    public function mailExists(Varien_Object $customer, $mail, $websiteId)
    {
        if($websiteId){
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($mail);
        if($customer->getId()){
            return $customer->getId();
        }
        return FALSE;
    }
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function formAction()
    {
        $customer = Mage::getModel('customer/customer');
        $websiteId = Mage::app()->getWebsite()->getId();
        $email = $_POST['email'] == '' ? $this->getRequest()->getParam('email'): $_POST['email'];

        if($this->mailExists($customer, $email, $websiteId))
        {
            $option_id = $customer->getSecurityq();
            $attributes = Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()->join('attribute','attribute.attribute_id=main_table.attribute_id', 'attribute_code');
            foreach ($attributes as $attribute) {
                if ($attribute->getOptionId()== $option_id) {
                    $securityQuestion = $attribute->getValue();
                }
            }
            $SecurityAnswer = $customer->getSecuritya();
            $customerSession =  Mage::getModel('customer/session');
            $customerSession->setEmail($email);
            $customerSession->setQuestion($securityQuestion);
            $customerSession->setAnswer($SecurityAnswer);
            if($securityQuestion == NULL){
                Mage::getSingleton('core/session')->addError('Account is not valid');
                $this->getResponse()->setRedirect(Mage::getUrl('*/question'));
                $this->setFlag('',self::FLAG_NO_DISPATCH,True);
                return $this;
            }
        }
        else{
            Mage::getSingleton('core/session')->addError('Account is not valid');
            $this->getResponse()->setRedirect(Mage::getUrl('*/question'));
            $this->setFlag('',self::FLAG_NO_DISPATCH,True);
            return $this;
        }
        $this->loadLayout();
        $this->renderLayout();
    }
    public function formPostAction() {
        $customer = Mage::getModel('customer/customer');
        $websiteId = Mage::app()->getWebsite()->getId();
        $email = $_POST['email'];
        if($this->mailExists($customer, $email, $websiteId))
        {
            $userAnswer = $_POST['security_question_answer'];
            $SecurityAnswer = $customer->getSecuritya();

            if($userAnswer == $SecurityAnswer){
                $email = (string) $this->getRequest()->getPost('email');
                $customer = Mage::getModel('customer/customer')
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email);
                //$id = $customer->getId();
                //$path = Mage::getUrl('customer/account/resetpassword');
                if ($customer->getId()) {
                    $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    //$url = "$path?id=$id&token=$newResetPasswordLinkToken";
                    //$this->_redirectUrl($url);

                    /*START new added code by Edward Garcia*/
                    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                    $pass = array(); //remember to declare $pass as an array
                    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                    for ($i = 0; $i < 8; $i++) {
                        $n = rand(0, $alphaLength);
                        $pass[] = $alphabet[$n];
                    }
                    $password =  implode($pass);
                    $fname = $customer->getFirstname();
                    $lname = $customer->getLastname();

                    /*End GENERATE Temporary password*/
                    $customer->setData('password', $password);
                    $customer->setPasswordHash($customer->hashPassword($password));
                    $customer->save();

                    /*Start Send Email*/
                    $customerData = array(
                        'email' => $email,
                        'fname' => $fname,
                        'lname' => $lname,
                        'password' => $password
                    );
                    $templateId = 57;
                    // Set sender information
                    $senderName = Mage::getStoreConfig('trans_email/ident_support/name');
                    $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');
                    $sender = array('name' => $senderName,
                        'email' => $senderEmail);

                    // Set recepient information
                    $recepientEmail = $customerData['email'];

                    $translate = Mage::getSingleton('core/translate');

                    // Send Transactional Email
                    try {
//                        Mage::getModel('core/email_template')->setDesignConfig(array('area' => 'frontend'))
//                            ->setReplyTo($recepientEmail)
//                            ->sendTransactional(
//                                $templateId,
//                                $sender,
//                                $recepientEmail,
//                                null,
//                                $customerData
//                            );
//                        $translate->setTranslateInline(true);

                        /*start code added by camille*/
                        Mage::log("START SEND EMAIL");
                        Mage::log($recepientEmail);
                        $mailer = Mage::getModel('core/email_template_mailer');

                        $emailInfo = Mage::getModel('core/email_info');
                        $bccEmail = 'alacaman_27@yahoo.com';
                        $emailInfo->addTo($recepientEmail, $fname);
                        $emailInfo->addTo($bccEmail);
                        $mailer->addEmailInfo($emailInfo);
                        $storeId = Mage::app()->getStore()->getStoreId();
                        // Set all required params and send emails
                        $mailer->setSender($sender);
                        $mailer->setStoreId($storeId);
                        $mailer->setTemplateId($templateId);
                        $mailer->setTemplateParams($customerData);
                        $mailer->send();
                        Mage::log("END SEND EMAIL");
                        /*end added by camille*/

                        /*End Send Email*/
                        /*Set Re-direct to login page*/
                        Mage::getSingleton('core/session')->addSuccess('If there is an account associated with '.$email.' you will receive an email with a new generated password.');
                        $this->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
                        $this->setFlag('',self::FLAG_NO_DISPATCH,True);
                        return $this;
                        /*END new added code by Edward Garcia*/
                    } catch (Exception $e) {
                        Mage::getSingleton('core/session')->addError($e->getMessage());
                        $this->getResponse()->setRedirect(Mage::getUrl('accountsecurity/question/form',array('email'=>$email)));
                        $this->setFlag('',self::FLAG_NO_DISPATCH,True);
                        return $this;
                    }

                }
            }
            else{
                Mage::getSingleton('core/session')->addError('You have provided the wrong answer!');
                MAge::log(Mage::getUrl('accountsecurity/question/form'));
                $this->getResponse()->setRedirect(Mage::getUrl('accountsecurity/question/form',array('email'=>$email)));
                $this->setFlag('',self::FLAG_NO_DISPATCH,True);
                return $this;

            }
            $this->loadLayout();
            $this->renderLayout();
        }
    }
    public function sendEmailToCutomer($customerData)
    {

    }
}