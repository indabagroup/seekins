<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';
/**
 * Magento Commercial Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Commercial Edition License
 * that is available at: http://www.magentocommerce.com/license/commercial-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/commercial-edition
 */

/**
 * Shopping cart controller
 */
class BinaryAnvil_Quickcart_AjaxcartController extends Mage_Checkout_CartController {

//Ajax Add Product	
    public function ajaxAddAction() {
        //Mage::log('pass here');
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        //Mage::log($params);
        $response = array();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
//            Mage::log($product, null, 'binaryanvil.log');
            $related = $this->getRequest()->getParam('related_product');

            
            
            /**
             * Check product availability
             */
            if (!$product) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Unable to find Product ID');
            }
            
            $log = "wala error 2!";
             Mage::log($log, null, 'binaryanvil.log');

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();   
            
            $log = "wala error 3!";
             Mage::log($log, null, 'binaryanvil.log');

            $this->_getSession()->setCartWasUpdated(true);
            
            $log = "wala error 4!";
             Mage::log($log, null, 'binaryanvil.log');

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete', array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $log = "start -- no error" . $cart->getQuote()->getHasError() . "\r\n";
                    $log .= "DATE = " . date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())) . "\r\n";
                    $log .= print_r($params, true) . "\r\n";
                    $log .= $product->getName() . "---" . $product->getSku() . "\r\n";
                    $log .= "end \r\n";
                    Mage::log($log, null, 'binaryanvil.log');



//	                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
//	                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));

                    $product = Mage::getModel('catalog/product')->load($product->getId());

                    $category_id = '';
                    $msg_substr = '';
                    $cats = $product->getCategoryIds();
                    foreach ($cats as $category_id) {
                        //$_cat = Mage::getModel('catalog/category')->load($category_id);
                        $category_id = $category_id;
                    }

                    if ($category_id == 3) {
                        $msg_substr = "has joined the ranks.";
                    } else {
                        $msg_substr = "has been added to your cart.";
                    }

                    $message = $this->__('<img src="' . Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(87, 87) . '" alt="" />' . '<span class="addToText"><strong>%s</strong> ' . $msg_substr . '</span>', Mage::helper('core')->htmlEscape($product->getName()));

                    $response['status'] = 'SUCCESS';
                    $response['message'] = $message;
                    //New Code Here
                    $this->loadLayout();
                    $sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
                    $sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
                    $sidebar = $sidebar_block->toHtml();
                    $sidebarcontent = $sidebarcontent_block->toHtml();
                    $response['sidebar'] = $sidebar;
                    $response['sidebarcontent'] = $sidebarcontent;
                } else {
                    $log = "start -- has error" . $cart->getQuote()->getHasError() . "\r\n";
                    $log .= "DATE = " . date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())) . "\r\n";
                    $log .= print_r($params, true) . "\r\n";
                    $log .= $product->getName() . "---" . $product->getSku() . "\r\n";
                    $log .= "end \r\n";
                    Mage::log($log, null, 'binaryanvil.log');
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
                    $response['status'] = 'SUCCESS';
                    $response['message'] = $message;
                    //New Code Here
                    $this->loadLayout();
                    $sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
                    $sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
                    $sidebar = $sidebar_block->toHtml();
                    $sidebarcontent = $sidebarcontent_block->toHtml();
                    $response['sidebar'] = $sidebar;
                    $response['sidebarcontent'] = $sidebarcontent;
                }
            }
        } catch (Mage_Core_Exception $e) {
            $msg = "";
            if ($this->_getSession()->getUseNotice(true)) {
                $msg = $e->getMessage();
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $msg .= $message . '<br/>';
                }
            }

            $response['status'] = 'ERROR';
            $response['message'] = $msg;
        } catch (Exception $e) {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('Cannot add the item to shopping cart.');
            Mage::logException($e);
//				Mage::log($e, null, 'binaryanvil.log');
        }
        Mage::log($this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response)), null, 'binaryanvil.log');
        
        $response['subtotal'] = number_format(Mage::helper('checkout/cart')->getQuote()->getSubtotal(), 2);
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

    public function ajaxupdatePostAction() {

        $updateAction = (string) $this->getRequest()->getParam('update_cart_action');

        switch ($updateAction) {
            case 'remove_cart':
                $this->_ajaxremoveShoppingCart();
                break;
            case 'update_qty':
                $this->_ajaxupdateShoppingCart();
                break;
            default:
                $this->_ajaxupdateShoppingCart();
        }
    }

    protected function _ajaxupdateShoppingCart() {
        $response = array();
        try {
            $cartData = $this->getRequest()->getParam('cart');
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                $cart = $this->_getCart();
                if (!$cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

                $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                        ->save();
            }
            $this->_getSession()->setCartWasUpdated(true);
            //New Code Here
            $this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
            $sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
            $sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
            Mage::register('referrer_url', $this->_getRefererUrl());
            $sidebar = $sidebar_block->toHtml();
            $sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
            $response['sidebar'] = $sidebar;
            $response['sidebarcontent'] = $sidebarcontent;
        } catch (Mage_Core_Exception $e) {
            $msg = Mage::helper('core')->escapeHtml($e->getMessage());
            $response['status'] = 'ERROR';
            $response['message'] = $msg;
        } catch (Exception $e) {
            $msg = $this->__('Cannot update shopping cart.');
            Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

    protected function _ajaxremoveShoppingCart() {
        $response = array();
        try {
            $cartData = $this->getRequest()->getParam('cartremove');
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                $cart = $this->_getCart();
                if (!$cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

                $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                        ->save();
            }
            $this->_getSession()->setCartWasUpdated(true);
            //New Code Here
            $this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
            $sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
            $sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
            Mage::register('referrer_url', $this->_getRefererUrl());
            $sidebar = $sidebar_block->toHtml();
            $sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
            $response['sidebar'] = $sidebar;
            $response['sidebarcontent'] = $sidebarcontent;
        } catch (Mage_Core_Exception $e) {
            $msg = Mage::helper('core')->escapeHtml($e->getMessage());
        } catch (Exception $e) {
            $msg = $this->__('Cannot update shopping cart.');
            Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

    /**
     * Delete shoping cart item action
     */
    public function ajaxDeleteAction() {
        $id = (int) $this->getRequest()->getParam('id');
        $response = array();
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                        ->save();
                //New Code Here
                $this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
                $sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
                $sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
                Mage::register('referrer_url', $this->_getRefererUrl());
                $sidebar = $sidebar_block->toHtml();
                $sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
                $response['sidebar'] = $sidebar;
                $response['sidebarcontent'] = $sidebarcontent;
            } catch (Exception $e) {
                $msg = $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
                $response['status'] = 'ERROR';
                $response['message'] = $msg;
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
//        $this->_redirectReferer(Mage::getUrl('*/*'));
    }

}
