<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'CartController.php';
/**
 * Magento Commercial Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Commercial Edition License
 * that is available at: http://www.magentocommerce.com/license/commercial-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/commercial-edition
 */

/**
 * Shopping cart controller
 */
class BinaryAnvil_Quickcart_AjaxcartController extends Mage_Checkout_CartController
{

//Ajax Add Product	
	public function ajaxAddAction()
    {
    	$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();
			$response = array();
			try {
				if (isset($params['qty'])) {
					$filter = new Zend_Filter_LocalizedToNormalized(
					array('locale' => Mage::app()->getLocale()->getLocaleCode())
					);
					$params['qty'] = $filter->filter($params['qty']);
				}

				$product = $this->_initProduct();
				$related = $this->getRequest()->getParam('related_product');

				/**
				 * Check product availability
				 */
				if (!$product) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Unable to find Product ID');
				}

				$cart->addProduct($product, $params);
				if (!empty($related)) {
					$cart->addProductsByIds(explode(',', $related));
				}

				$cart->save();

				$this->_getSession()->setCartWasUpdated(true);

				/**
				 * @todo remove wishlist observer processAddToCart
				 */
				Mage::dispatchEvent('checkout_cart_add_product_complete',
				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);

				if (!$cart->getQuote()->getHasError()){
					$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
					$response['status'] = 'SUCCESS';
					$response['message'] = $message;
					//New Code Here
					$this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
					$sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
					$sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
					Mage::register('referrer_url', $this->_getRefererUrl());
					$sidebar = $sidebar_block->toHtml();
					$sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
					$response['sidebar'] = $sidebar;
					$response['sidebarcontent'] = $sidebarcontent;
				}
			} catch (Mage_Core_Exception $e) {
				$msg = "";
				if ($this->_getSession()->getUseNotice(true)) {
					$msg = $e->getMessage();
				} else {
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						$msg .= $message.'<br/>';
					}
				}

				$response['status'] = 'ERROR';
				$response['message'] = $msg;
			} catch (Exception $e) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Cannot add the item to shopping cart.');
				Mage::logException($e);
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
    }
    
	public function ajaxupdatePostAction()
    {
    	
        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

        switch ($updateAction) {
            case 'remove_cart':
                $this->_ajaxremoveShoppingCart();
                break;
            case 'update_qty':
                $this->_ajaxupdateShoppingCart();
                break;
            default:
                $this->_ajaxupdateShoppingCart();
        }

    }
    
    protected function _ajaxupdateShoppingCart()
    {
    	$response = array();
        try {
            $cartData = $this->getRequest()->getParam('cart');
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                $cart = $this->_getCart();
                if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

                $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                    ->save();
            }
            $this->_getSession()->setCartWasUpdated(true);
            //New Code Here
					$this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
					$sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
					$sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
					Mage::register('referrer_url', $this->_getRefererUrl());
					$sidebar = $sidebar_block->toHtml();
					$sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
					$response['sidebar'] = $sidebar;
					$response['sidebarcontent'] = $sidebarcontent;
        } catch (Mage_Core_Exception $e) {
            $msg =  Mage::helper('core')->escapeHtml($e->getMessage());
            $response['status'] = 'ERROR';
			$response['message'] = $msg;
        } catch (Exception $e) {
             $msg = $this->__('Cannot update shopping cart.');
            Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
    }
    
	protected function _ajaxremoveShoppingCart()
    {
    	$response = array();
        try {
            $cartData = $this->getRequest()->getParam('cartremove');
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                $cart = $this->_getCart();
                if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

                $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                    ->save();
            }
            $this->_getSession()->setCartWasUpdated(true);
            //New Code Here
					$this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
					$sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
					$sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
					Mage::register('referrer_url', $this->_getRefererUrl());
					$sidebar = $sidebar_block->toHtml();
					$sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
					$response['sidebar'] = $sidebar;
					$response['sidebarcontent'] = $sidebarcontent;
        } catch (Mage_Core_Exception $e) {
            $msg = Mage::helper('core')->escapeHtml($e->getMessage());
        } catch (Exception $e) {
            $msg = $this->__('Cannot update shopping cart.');
            Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
    }

    /**
     * Delete shoping cart item action
     */
	public function ajaxDeleteAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $response = array();
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                  ->save();
            //New Code Here
					$this->loadLayout();
//					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
					$sidebar_block = $this->getLayout()->getBlock('cart_sidebarcount');
					$sidebarcontent_block = $this->getLayout()->getBlock('cart_sidebarcontent');
					Mage::register('referrer_url', $this->_getRefererUrl());
					$sidebar = $sidebar_block->toHtml();
					$sidebarcontent = $sidebarcontent_block->toHtml();
//					$response['toplink'] = $toplink;
					$response['sidebar'] = $sidebar;
					$response['sidebarcontent'] = $sidebarcontent;
            } catch (Exception $e) {
                $msg = $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
                $response['status'] = 'ERROR';
				$response['message'] = $msg;
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		return;
//        $this->_redirectReferer(Mage::getUrl('*/*'));
    }

}