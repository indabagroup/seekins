<?php

class BinaryAnvil_Requirelogin_Helper_Data extends Mage_Core_Helper_Data
{
    public function isLoginRequired($store = null)
    {
        return Mage::getStoreConfigFlag('customer/startup/requirelogin', $store);
    }

    public function getWhitelist($store = null)
    {
        return Mage::getStoreConfig('customer/startup/requirelogin_whitelist', $store);
    }
}