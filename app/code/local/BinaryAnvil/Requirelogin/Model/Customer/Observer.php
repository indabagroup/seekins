<?php

class BinaryAnvil_Requirelogin_Model_Customer_Observer {

    public function requireLogin($observer) {
        $helper = Mage::helper('requirelogin');
        $session = Mage::getSingleton('customer/session');
        $actionName = $observer->getControllerAction()->getFullActionName();                
        
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $moduleName = Mage::app()->getRequest()->getModuleName();
        
        $storeCode = Mage::app()->getStore()->getCode();                
        
        $allowedLink = array('customer_account_loginPost', 'customer_account_login');
        
        if (!$session->isLoggedIn() && $moduleName!='customer' && $controllerName!='account' && $storeCode == 'b2b_default_view') {
            $controllerAction = $observer->getEvent()->getControllerAction();
            /* @var $controllerAction Mage_Core_Controller_Front_Action */
            $requestString = $controllerAction->getRequest()->getRequestString();

            if (!preg_match($helper->getWhitelist(), $requestString)) {
                $session->setBeforeAuthUrl($requestString);
                $controllerAction->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
                $controllerAction->getResponse()->sendResponse();
                exit;
            }
        }
    }

}