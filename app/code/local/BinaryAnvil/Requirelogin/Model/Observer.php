<?php

class BinaryAnvil_Requirelogin_Model_Observer {

    public function requireLogin($observer) {
        $helper = Mage::helper('BinaryAnvil_Requirelogin');
        $session = Mage::getSingleton('customer/session');
        
        $actionName = $observer->getControllerAction()->getFullActionName();

        if (!$session->isLoggedIn() && $helper->isLoginRequired()) {
            $controllerAction = $observer->getEvent()->getControllerAction();
            /* @var $controllerAction Mage_Core_Controller_Front_Action */
            $requestString = $controllerAction->getRequest()->getRequestString();

            if (!preg_match($helper->getWhitelist(), $requestString)) {
                $session->setBeforeAuthUrl($requestString);
                $controllerAction->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
                $controllerAction->getResponse()->sendResponse();
                exit;
            }
        }
    }
}