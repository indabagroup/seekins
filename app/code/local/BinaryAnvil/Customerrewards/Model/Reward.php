<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_Reward
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Reward model
 *
 * @category    Enterprise
 * @package     Enterprise_Reward
 * @author      BinaryAnvil Team <binaryanvil.com>
 */
class BinaryAnvil_Customerrewards_Model_Reward extends Enterprise_Reward_Model_Reward
{
 	public function getPointsBalance()
    {

        $request = Mage::app()->getRequest();
        $rewardTotal = 0;
        if ($request->getModuleName() == 'checkout' || $request->getModuleName() == 'quickcart') {
//        	Mage::log($request);
//        	Mage::log("get");
//        	Mage::log($request->getParams());
//        	Mage::log("post");
//        	Mage::log($request->getPost());
//        	Mage::log("pass here checkout and quickcart");
        	if(Mage::helper('customer')->isLoggedIn()){
	            if (($quote = Mage::getModel('checkout/session')->getQuote())) {

					$pointsLeft = $this->_getActualPointsLeft($quote);
	            	
	            	$items = $quote->getAllItems();
					$category = Mage::getModel('catalog/category')->load(13); //Gi brick rewards category
//					$category = Mage::getModel('catalog/category')->load(4); //Selected category
	            	$_rewardItems = $category->getProductCollection();
	            	
	            	$rewardItems = array();
	            	foreach($_rewardItems as $_rewardItem){
	            		$rewardItems[] = $_rewardItem->getId();
	            	}
	            	
	            	$rewardTotal = 0;
	            	foreach($items as $item){
//	            		Mage::log($item->getName()."-".$item->getQty()."-".$item->getProductId());
	            		if(in_array($item->getProductId(), $rewardItems)){
	            			$rewardTotal += $item->getPrice()* $item->getQty();
	            		}
	            	}
//	            	Mage::log('total:'.$rewardTotal);
	            	if($rewardTotal > 0){
//	            		Mage::log("allowed");
						$allowedMaxRewardPoints = (int)$this->getPointsEquivalent($rewardTotal);
						if($this->getData('points_balance') > $allowedMaxRewardPoints){
							 return $allowedMaxRewardPoints;
						}
	            	}else{
//	            		Mage::log("na");
	            		return 0;	            		
	            	}
	            }
        	}
        }
//        Mage::log("not checkout and quickcart");
        return $this->getData('points_balance');

    } 
    
	protected function _getActualPointsLeft(Mage_Sales_Model_Quote $quote){
		$reward = $quote->getRewardInstance();
		if (!$reward || !$reward->getId()) {
			$reward = Mage::getModel('enterprise_reward/reward')
		    	->setCustomer($quote->getCustomer())
		        ->setCustomerId($quote->getCustomer()->getId())
		        ->setWebsiteId($quote->getStore()->getWebsiteId())
		        ->loadByCustomer();
		}

		$pointsLeft = $reward->getData('points_balance') - $quote->getRewardPointsBalance();
		return $pointsLeft;
	}    
	
	public function getCurrentRemainingPointsBalance(){
		return $this->getData('points_balance');
	}	
	
    protected function _preparePointsBalance()
    {
        $points = 0;
        if ($this->hasPointsDelta()) {
            $points = $this->getPointsDelta();
        }
        $pointsBalance = 0;
        $pointsBalance = (int)$this->getCurrentRemainingPointsBalance() + $points;
        $maxPointsBalance = (int)(Mage::helper('enterprise_reward')
            ->getGeneralConfig('max_points_balance', $this->getWebsiteId()));
        if ($maxPointsBalance != 0 && ($pointsBalance > $maxPointsBalance)) {
            $pointsBalance = $maxPointsBalance;
            $pointsDelta   = $maxPointsBalance - (int)$this->getPointsBalance();
            $croppedPoints = (int)$this->getPointsDelta() - $pointsDelta;
            $this->setPointsDelta($pointsDelta)
                ->setIsCappedReward(true)
                ->setCroppedPoints($croppedPoints);
        }
        $this->setPointsBalance($pointsBalance);
        return $this;
    }	


	
}
