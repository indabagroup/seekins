<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_Reward
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Reward model
 *
 * @method Enterprise_Reward_Model_Resource_Reward _getResource()
 * @method Enterprise_Reward_Model_Resource_Reward getResource()
 * @method int getCustomerId()
 * @method Enterprise_Reward_Model_Reward setCustomerId(int $value)
 * @method Enterprise_Reward_Model_Reward setWebsiteId(int $value)
 * @method int getPointsBalance()
 * @method Enterprise_Reward_Model_Reward setPointsBalance(int $value)
 * @method Enterprise_Reward_Model_Reward setWebsiteCurrencyCode(string $value)
 *
 * @category    Enterprise
 * @package     Enterprise_Reward
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class BinaryAnvil_Customerrewards_Model_Reward extends Enterprise_Reward_Model_Reward
{
 	public function getPointsBalance()
    {
        $request = Mage::app()->getRequest();
        if ($request->getModuleName() == 'checkout') {
        	if(Mage::helper('customer')->isLoggedIn()){
	            if (($quote = Mage::getModel('checkout/session')->getQuote())) {
	            	$items = $quote->getAllItems();
//	            	$category = Mage::getModel('catalog/category')->load(4);
					$category = Mage::getModel('catalog/category')->load(13);
	            	$_rewardItems = $category->getProductCollection();
	            	$rewardItems = array();
	            	foreach($_rewardItems as $_rewardItem){
	            		$rewardItems[] = $_rewardItem->getId();
	            	}
	            	$rewardTotal = 0;
	            	foreach($items as $item){
	            		if(in_array($item->getProductId(), $rewardItems)){
	//            			Mage::log("true");
	//            			Mage::log($item->getPrice()* $item->getQty());
	            			$rewardTotal += $item->getPrice()* $item->getQty();
	            		}
	            	}
	            	
	            	if($rewardTotal > 0){
//	            		$rewardpoints = $this->loadByCustomer()->getFormatedCurrencyAmount();
//		            	Mage::log($this->getData('points_balance'));
						$allowedMaxRewardPoints = $this->getPointsEquivalent($rewardTotal);
//		            	Mage::log($this->getPointsEquivalent($rewardTotal));
						if($this->getData('points_balance') > $allowedMaxRewardPoints){
							 return $allowedMaxRewardPoints;
						}
//		                $totals = $quote->getTotals();
//		                $cartTotal = floor($totals["subtotal"]->getValue());
//		                if ($cartTotal > 0) {
//		                    $rpPercent = 23; /* some integer value representing percent, could read this trough some config */
//		                    $rpAllowedMax = $cartTotal * ($rpPercent / 100);
//		                    $rpAllowedMax = floor($rpAllowedMax);
//		                    if ((int)$this->getData('points_balance') >= $rpAllowedMax) {
//		                        return $rpAllowedMax;
//		                    }
//		                }
	            	}
	            }
        	}
        }
        return $this->getData('points_balance');
    } 
}
