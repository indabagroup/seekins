<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/25/14
 * Time: 5:06 PM
 */

class BinaryAnvil_Shipment_Adminhtml_ShipmentController extends Mage_Adminhtml_Controller_Action
{
    const XML_PATH_EMAIL_COPY_TO                = 'binaryanvil_shipment/settings/bcc_email';
    const XML_PATH_EMAIL_TEMPLATE               = 'binaryanvil_shipment/settings/email_template';
    const XML_PATH_EMAIL_GUEST_TEMPLATE         = 'binaryanvil_shipment/settings/email_guest_email';
    const XML_PATH_EMAIL_IDENTITY               = 'sales_email/shipment/identity';
    /**
     * Initialize shipment items QTY
     */
    protected function _getItemQtys()
    {
        $data = $this->getRequest()->getParam('shipment');
        $totalQty = 0;
        if (isset($data['items'])) {
            foreach ($data['items'] as $orderItemId=> $qty) {
                $totalQty += $qty;
            }
        }


        return $totalQty;
    }
    /**
     * Initialize shipment model instance
     *
     * @return Mage_Sales_Model_Order_Shipment|bool
     */
    protected function _initShipment()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Shipments'));

        $shipment = false;
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        $orderId = $this->getRequest()->getParam('order_id');

        if ($shipmentId) {
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
        } elseif ($orderId) {
            $order      = Mage::getModel('sales/order')->load($orderId);

            /**
             * Check order existing
             */
            if (!$order->getId()) {
                $this->_getSession()->addError($this->__('The order no longer exists.'));
                return false;
            }
            /**
             * Check shipment is available to create separate from invoice
             */
            if ($order->getForcedDoShipmentWithInvoice()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order separately from invoice.'));
                return false;
            }
            /**
             * Check shipment create availability
             */
            /*if (!$order->canShip()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order.'));
                return false;
            }*/
            $savedQtys = $this->_getItemQtys();

            $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($savedQtys);
            $tracks = $this->getRequest()->getPost('tracking');
            if ($tracks) {
                foreach ($tracks as $data) {
                    if (empty($data['number'])) {
                        Mage::throwException($this->__('Tracking number cannot be empty.'));
                    }
                    $track = Mage::getModel('sales/order_shipment_track')
                        ->addData($data);
                    $shipment->addTrack($track);
                }
            }
        }

        Mage::register('current_shipment', $shipment);
        return $shipment;
    }

    /**
     * Save shipment and order in one transaction
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @return Mage_Adminhtml_Sales_Order_ShipmentController
     */
    protected function _saveShipment($shipment)
    {
        $shipment->getOrder()->setIsInProcess(true);
        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();

        return $this;
    }


    protected $_requiredFields = array('shipment_id', 'order_id', 'store_id', 'shipment_item_id', 'order_item_id');

    protected $_orderItemTable;

    protected $_orderShipmentTable;

    protected $_orderShipmentTrackTable;

    protected $_orderShipmentGridTable;

    protected $_orderShipmentItemTable;

    protected $_orderShipmentCommentTable;

    protected $_connection;

    protected function _initTables()
    {
        $coreResource = Mage::getSingleton( 'core/resource' );
        $this->_orderItemTable = $coreResource->getTableName( 'sales/order_item' );
        $this->_orderShipmentTable = $coreResource->getTableName( 'sales/shipment' );
        $this->_orderShipmentTrackTable = $coreResource->getTableName( 'sales/shipment_track' );
        $this->_orderShipmentGridTable = $coreResource->getTableName( 'sales/shipment_grid' );
        $this->_orderShipmentItemTable = $coreResource->getTableName( 'sales/shipment_item' );
        $this->_orderShipmentCommentTable = $coreResource->getTableName('sales/shipment_comment');
        $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    }


    public function editAction()
    {
        $data = $this->getRequest()->getParams();
        $this->_initTables();
        if (isset($data['shipment_id']) && isset($data['order_id']) && isset($data['store_id'])) {

/***********start code for updating shipment quantity on 4 tables*************************************/
            $orderId = $data['order_id'];
            $storeId = $data['store_id'];
            $shipmentId = $data['shipment_id'];

            $shipmentInfo = Mage::getModel("sales/order_shipment")->load($shipmentId);
            /**
             * Check shipment existing
             */
            if (!$shipmentInfo->getId()) {
                $this->_getSession()->addError($this->__('The shipment no longer exists.'));
                return false;
            }
            $order      = Mage::getModel('sales/order')->load($orderId);

            /**
             * Check order existing
             */
            if (!$order->getId()) {
                $this->_getSession()->addError($this->__('The order no longer exists.'));
                return false;
            }
            $shipped_items = $shipmentInfo->getItemsCollection();
            $_shipmentItemQtyShipped = 0;
            $_shipmentTotalQtyShipped = 0;
            $oldItemQty = array();
            $orderItemTotalQtyShipped = array();
            $shipmentItemQtyShipped = array();
            $shipmentTotalQtyShipped = array();
            $qtyToShipped = array();
            $changeDetails = '';
            $totalQtyShipped = 0;
            foreach ($shipped_items as $shipped_item) {
                //shipped item detail
                $itemId = $shipped_item->getData('order_item_id');
                $orderItemQtyShipped = Mage::getModel('sales/order_item')->load($itemId)->getData('qty_shipped');
                $oldQty = $shipped_item->getData('qty');
                $oldItemQty[$itemId] = $oldQty;
                $newQty = isset($data['shipment']['items'][$itemId])? $data['shipment']['items'][$itemId] : 0;
                $orderItemQtyShipped = $orderItemQtyShipped - $oldQty;
//                if ($newQty > 0 && $newQty < $oldQty) {
                if ($newQty < $oldQty) { //updated to allow 0 qty for shipping
                    $orderItemQtyShipped += $newQty;
                    $_shipmentItemQtyShipped = $newQty;
                    $_shipmentTotalQtyShipped += $newQty;
                    $changeDetails = $changeDetails . '<br>SKU: ' . $shipped_item->getSku() . ' modified Qty From: ' . intval($oldQty) . ' To: ' . intval($newQty) ;
                } else {
                    $orderItemQtyShipped += $oldQty;
                    $_shipmentItemQtyShipped = $oldQty;
                    $_shipmentTotalQtyShipped += $oldQty;
                }

                $totalQtyShipped += $orderItemQtyShipped;
                $orderItemTotalQtyShipped[$itemId] = $orderItemQtyShipped;
                $shipmentItemQtyShipped[$itemId] = $_shipmentItemQtyShipped;
                $shipmentTotalQtyShipped = $_shipmentTotalQtyShipped;
            } //foreach ($shipped_items as $shipped_item) {

            //Mage::log($shipmentTotalQtyShipped);
            $success = array();
            $failed = array();
            $date = Mage::getModel( 'core/date' )->gmtDate();
            $itemsCount = count($data['shipment']['items']);
            $noChangesMade = array();
            foreach ($data['shipment']['items'] as $orderItemId=> $qty) {
                $shipmentItemId = $data['shipment_item_id_'.$orderItemId];
//                if ($shipmentId && $orderId && $storeId && $orderItemId && $shipmentItemId && $qty > 0 && $qty < $oldItemQty[$orderItemId]) {
                if ($shipmentId && $orderId && $storeId && $orderItemId && $shipmentItemId && $qty < $oldItemQty[$orderItemId]) { //updated to allow 0 qty for shipping
                    try {
                        /********* update sales_flat_order_item************/
                        $orderItemData = array();
                        $_orderItemData = array();
                        $_orderItemData[ 'item_id' ] = $orderItemId;
                        $_orderItemData[ 'store_id' ] = $storeId;
                        $_orderItemData[ 'order_id' ] = $orderId;
                        $_orderItemData[ 'qty_shipped' ] = $orderItemTotalQtyShipped[$orderItemId];
                        $_orderItemData['updated_at'] = $date;
                        $orderItemData[] = $_orderItemData;
                        $this->_connection->insertOnDuplicate(
                            $this->_orderItemTable,
                            $orderItemData,
                            array (
                                'updated_at', 'qty_shipped',
                            )
                        );
                        /********* update sales_flat_order_item************/

                        /********* update sales_flat_shipment_item************/
                        $shipmentItemData = array();
                        $_shipmentItemData = array();
                        $_shipmentItemData[ 'entity_id' ] = $data['shipment_item_id_'.$orderItemId];
                        $_shipmentItemData[ 'parent_id' ] = $shipmentId;
                        $_shipmentItemData[ 'qty' ] = $shipmentItemQtyShipped[$orderItemId];
                        $shipmentItemData[] = $_shipmentItemData;
                        $this->_connection->insertOnDuplicate(
                            $this->_orderShipmentItemTable,
                            $shipmentItemData,
                            array (
                                'qty',
                            )
                        );
                        /********* update sales_flat_shipment_item************/

                        /********* update sales_flat_shipment************/
                        $shipmentData = array();
                        $_shipmentData = array();
                        $_shipmentData[ 'entity_id' ] = $shipmentId;
                        $_shipmentData[ 'store_id' ] = $storeId;
                        $_shipmentData[ 'order_id' ] = $orderId;
                        $_shipmentData[ 'total_qty' ] = $shipmentTotalQtyShipped;
                        $shipmentData[] = $_shipmentData;
                        $this->_connection->insertOnDuplicate(
                            $this->_orderShipmentTable,
                            $shipmentData,
                            array (
                                'total_qty',
                            )
                        );
                        /********* update sales_flat_shipment************/

                        /********* update sales_flat_shipment_grid************/
                        $shipmentGridData = array();
                        $_shipmentGridData = array();
                        $_shipmentGridData[ 'entity_id' ] = $shipmentId;
                        $_shipmentGridData[ 'store_id' ] = $storeId;
                        $_shipmentGridData[ 'total_qty' ] = $shipmentTotalQtyShipped;
                        $shipmentGridData[] = $_shipmentGridData;
                        $this->_connection->insertOnDuplicate(
                            $this->_orderShipmentGridTable,
                            $shipmentGridData,
                            array (
                                'total_qty',
                            )
                        );
                        /********* update sales_flat_shipment_grid************/

                        $success[$data['item_sku_'.$orderItemId]] = $data['item_name_'.$orderItemId];
                    } catch (Mage_Core_Exception $e) {
                        Mage::logException($e);
                        $failed[$data['item_sku_'.$orderItemId]] = $data['item_name_'.$orderItemId];

                    } catch (Exception $e) {
                        Mage::logException($e);
                        $failed[$data['item_sku_'.$orderItemId]] = $data['item_name_'.$orderItemId];
                    }
                } else {
                    if ($qty == $oldItemQty[$orderItemId]) {
                        $noChangesMade[] = $orderItemId;
                    } else {
                        $failed[$data['item_sku_'.$orderItemId]] = $data['item_name_'.$orderItemId];
                    }
                }//if ($shipmentId && $orderId && $storeId && $orderItemId && $shipmentItemId && $qty >= 0 && $qty <= $oldItemQty[$orderItemId]) {
            } //foreach ($data['shipment']['items'] as $orderItemId=> $qty) {

/**************end code for updating shipment quantity on 4 tables****************************/
            if (!empty($failed)) {
                $this->_getSession()->addError($this->__('Cannot save shipment for some items'));
            }
            if (!empty($success)) {
                if ($noChangesMade != $itemsCount) {
                    /********* update sales_flat_shipment_comment************/
                    $shipmentInfoReload = Mage::getModel("sales/order_shipment")->load($shipmentId);
                    $shipmentCommentData = array();
                    $_shipmentCommentData = array();
                    $_shipmentCommentData[ 'parent_id' ] = $shipmentId;
                    $_shipmentCommentData[ 'is_customer_notified' ] = 0;
                    $_shipmentCommentData[ 'is_visible_on_front' ] = 0;
                    $_shipmentCommentData['admin'] = Mage::getSingleton('admin/session')->getUser()->getUsername();
                    $_shipmentCommentData[ 'created_at' ] = $date;

                    if($shipmentTotalQtyShipped == 0){
                        $_shipmentCommentData['comment'] = "Shipment VOIDED by ".Mage::getSingleton('admin/session')->getUser()->getUsername().".";
                        $shipmentCommentData[] = $_shipmentCommentData;
                        $shipmentInfoReload->setVoidShipment(true);
                    }

                    $_shipmentCommentData['comment'] = Mage::getStoreConfig( 'binaryanvil_shipment/settings/comment' )." by ".Mage::getSingleton('admin/session')->getUser()->getUsername()."." . $changeDetails;
                    $shipmentCommentData[] = $_shipmentCommentData;

                    $this->_connection->insertMultiple( $this->_orderShipmentCommentTable, $shipmentCommentData );

                    /********* update sales_flat_shipment_comment************/
                    // send email
                    //$comment = Mage::getStoreConfig( 'binaryanvil_shipment/settings/comment' ).
                    //$shipmentInfo->sendUpdateEmail($notifyCustomer = true, Mage::getStoreConfig( 'binaryanvil_shipment/settings/comment' ));
                    $this->sendUpdateShipmentEmail($order,$shipmentInfoReload);
                    $this->_getSession()->addSuccess($this->__('Shipment successfully save.'));
                }
            }
            $this->_redirect('*/sales_order_shipment/view', array(
                'shipment_id' => $this->getRequest()->getParam('shipment_id')
            ));
        } else {
            $this->_forward('noRoute');
        } //if (isset($data['shipment_id']) && isset($data['order_id']) && isset($data['store_id'])) {
    }

    protected function _getEmails($configPath)
    {
        $data = Mage::getStoreConfig($configPath);
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

    /**
     * Send email with shipment data
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function sendUpdateShipmentEmail($order,$shipment)
    {
        $storeId = $order->getStore()->getId();

        /*if (!Mage::helper('sales')->canSendNewShipmentEmail($storeId)) {
            return $this;
        }*/
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = 'bcc';
        // Check if at least one recepient is found
        /*if (!$notifyCustomer && !$copyTo) {
            return $this;
        }*/

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE);
            $customerName = $order->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        //if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        //}

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy')) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'        => $order,
                'shipment'     => $shipment,
                'billing'      => $order->getBillingAddress(),
                'payment_html' => $paymentBlockHtml
            )
        );
        $mailer->send();

        return $this;
    }
}

