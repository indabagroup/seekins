<?php

class BinaryAnvil_ExtendUsaUsps_Model_Shipping_Carrier_Ups extends Mage_Usa_Model_Shipping_Carrier_Ups{

    /**
     * Collect and get rates
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result|bool|null
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag($this->_activeFlag)) {
            return false;
        }

        if (Mage::app()->getStore()->isAdmin()) {
            //check if cart order value falls between the minimum and maximum order amounts required
            $packagevalue = $request->getBaseCurrency()->convert($request->getPackageValue(), $request->getPackageCurrency());
            $minorderval = (int)$this->getConfigData('min_order_value');
            if(($packagevalue <= $minorderval) && ($minorderval > 0) && Mage::helper('binaryanvil_upsaccounts')->customerHasUPSAccountAndShouldBeRemovingUPSMethod()){
                return false;
            }
        } else {
            if (Mage::helper('binaryanvil_extendusausps')->isEnabled()) {
                //check if cart order value falls between the minimum and maximum order amounts required
                $packagevalue = $request->getBaseCurrency()->convert($request->getPackageValue(), $request->getPackageCurrency());
                $minorderval = (int)$this->getConfigData('min_order_value');
                if(($packagevalue <= $minorderval) && ($minorderval > 0) && Mage::helper('binaryanvil_upsaccounts')->customerHasUPSAccountAndShouldBeRemovingUPSMethod()){
                    return false;
                }
            }
        }
        //Mage::log($request);
        $this->setRequest($request);

        $this->_result = $this->_getQuotes();

        $this->_updateFreeMethodQuote($request);

        return $this->getResult();
    }
}