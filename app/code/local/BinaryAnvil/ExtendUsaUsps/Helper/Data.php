<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/9/14
 * Time: 2:01 PM
 */

class BinaryAnvil_ExtendUsaUsps_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'binaryanvil_extendusausps_settings/general_settings/enabled';

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag( self::XML_PATH_ENABLED );
    }
}