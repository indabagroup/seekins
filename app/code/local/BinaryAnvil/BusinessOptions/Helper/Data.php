<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/8/14
 * Time: 3:23 PM
 */

class BinaryAnvil_BusinessOptions_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'binaryanvil_businessoptions_settings/general_settings/enabled';
    const OPTIONAL_ZIP_COUNTRIES_CONFIG_PATH = 'binaryanvil_businessoptions_settings/general_settings/cfd_ffl_state_required';

    protected $_optionalCfdFflStates = null;

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag( self::XML_PATH_ENABLED );
    }

    /**
     * Return states code, which have optional sp_b2b_cfd_number (CFD Number) and sp_b2b_ffl_number (FFL Number)
     *
     */
    public function getStatesWithCfdFfl($asJson = false)
    {
        if ($this->isEnabled()) {

            if (null === $this->_optionalCfdFflStates) {
                $this->_optionalCfdFflStates = preg_split('/\,/',
                    Mage::getStoreConfig(self::OPTIONAL_ZIP_COUNTRIES_CONFIG_PATH), 0, PREG_SPLIT_NO_EMPTY);
            }
            if ($asJson) {
                return Mage::helper('core')->jsonEncode($this->_optionalCfdFflStates);
            }
            return $this->_optionalCfdFflStates;
        }
    }
}