<?php

class BinaryAnvil_Customreport_Adminhtml_Customreport_CustomreportController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function table2csvAction() {
        $name = 'OpenSalesOrdersReport.csv';
        $file = Mage::getBaseDir('var') . DS . 'csvreport' . DS . $name;
        $content = file_get_contents($file);
        $this->_prepareDownloadResponse($name, $content);
    }

}
