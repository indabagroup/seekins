<?php

class BinaryAnvil_Customreport_Block_Adminhtml_Index extends Mage_Core_Block_Template {
    
    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/*', array('_current' => true));
    }

    public function getReportNewOrders() {
        if (!$this->hasData('customreport')) {
            $this->setData('customreport', Mage::registry('customreport'));
        }
        return $this->getData('customreport');
    }

    public function getSalesOrderByItem($from = null, $to = null) {
        if(!$from){ $from = time();}
        if(!$to){ $to = time();}
        
        $first = date('Y-m-d', strtotime($from));
        $last = date('Y-m-d', strtotime($to . ' + 1 day'));

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $salesOrderItemTable = $resource->getTableName('sales/order_item');
        $salesOrderTable = $resource->getTableName('sales/order');
        $productEntityTable = 'catalog_product_entity';
        $productVarcharTable = 'catalog_product_entity_varchar';
        $customerVarcharTable = 'customer_entity_varchar';
        
        $query = ' SELECT salesitem.`product_id`,  salesitem.`created_at`, salesitem.`order_id`, salesitem.`qty_ordered`,  salesitem.`qty_invoiced`, salesitem.`qty_backordered`, ' .
                ' salesitem.`parent_item_id`, salesitem.`item_id`, ' .
                ' salesitem.`qty_canceled`, salesitem.`qty_refunded`, salesitem.`qty_shipped`, '.
                ' productentity.`sku`, ' .
                ' productvarchar.`value` as product_name, ' .
                ' salesorder.`customer_id`, salesorder.`increment_id`, ' .
                ' SUBSTRING_INDEX(salesorder.`increment_id`, "-", 1) as increment_id_stripped, (SELECT `created_at` FROM  `sales_flat_order` WHERE `increment_id` = increment_id_stripped) as original_date_created, ' .
                ' customerfirst.`value` as firstname, ' .
                ' customerlast.`value` as lastname ' .
                ' FROM ' . $salesOrderItemTable . ' as salesitem ' .
                ' LEFT JOIN  ' . $salesOrderTable . ' as salesorder ' .
                ' ON salesitem.`order_id`=salesorder.`entity_id` ' .
                ' LEFT JOIN  ' . $productEntityTable . ' as productentity ' .
                ' ON salesitem.`product_id`=productentity.`entity_id` ' .
                ' LEFT JOIN  ' . $productVarcharTable . ' as productvarchar ' .
                ' ON salesitem.`product_id`=productvarchar.`entity_id` ' .
                ' LEFT JOIN  ' . $customerVarcharTable . ' as customerfirst ' .
                ' ON salesorder.`customer_id`=customerfirst.`entity_id` ' .
                ' LEFT JOIN  ' . $customerVarcharTable . ' as customerlast ' .
                ' ON salesorder.`customer_id`=customerlast.`entity_id` ' .
                ' WHERE salesitem.`created_at` BETWEEN \'' . $first . '\' AND \'' . $last . '\'' .
                ' AND salesorder.`customer_id` IS NOT NULL ' .
                ' AND salesorder.`status` != \'canceled\' AND salesorder.`status` != \'closed\' AND salesorder.`status` != \'complete\' ' .
                ' AND productvarchar.`entity_type_id` = 4 AND productvarchar.`attribute_id` = 71 AND productvarchar.`store_id` = 0 ' .
                ' AND customerfirst.`entity_type_id` = 1 AND customerfirst.`attribute_id` = 5 ' .
                ' AND customerlast.`entity_type_id` = 1 AND customerlast.`attribute_id` = 7 ' .
                ' AND salesitem.`item_id` NOT IN ( ' .
                    ' SELECT  salesitem.`parent_item_id` ' .
                    ' FROM ' . $salesOrderItemTable . ' as salesitem ' .
                    ' LEFT JOIN ' . $salesOrderTable . ' as salesorder ' .
                    ' ON salesorder.`entity_id`=salesitem.`order_id` ' .
                    ' WHERE salesitem.`created_at` BETWEEN \'' . $first . '\' AND \'' . $last . '\'' .
                    ' AND salesitem.`parent_item_id` IS NOT NULL ' .
                    ' AND salesorder.`customer_id` IS NOT NULL ' .
                    ' AND salesorder.`status` != \'canceled\' AND salesorder.`status` != \'closed\' AND salesorder.`status` != \'complete\' ' .
                ' ) ' .
                ' ORDER BY productentity.`sku` ASC, `original_date_created` ASC ' ;


        //var_dump($query);
        //die();
        $results = $readConnection->fetchAll($query);
        return $results;
//        var_dump($query);
    }
    
    public function getSalesOrderByCustomer($from = null, $to = null) {
        if(!$from){ $from = time();}
        if(!$to){ $to = time();}
        
        $first = date('Y-m-d', strtotime($from));
        $last = date('Y-m-d', strtotime($to . ' + 1 day'));

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $salesOrderItemTable = $resource->getTableName('sales/order_item');
        $salesOrderTable = $resource->getTableName('sales/order');
        $productEntityTable = 'catalog_product_entity';
        $productVarcharTable = 'catalog_product_entity_varchar';
        $customerVarcharTable = 'customer_entity_varchar';
        
        $query = ' SELECT salesitem.`product_id`,  salesitem.`created_at`, salesitem.`order_id`, salesitem.`qty_ordered`,  salesitem.`qty_invoiced`, salesitem.`qty_backordered`, ' .
                ' salesitem.`parent_item_id`, salesitem.`item_id`, ' .
                ' salesitem.`qty_canceled`, salesitem.`qty_refunded`, salesitem.`qty_shipped`, '.
                ' productentity.`sku`, ' .
                ' productvarchar.`value` as product_name, ' .
                ' salesorder.`customer_id`,  salesorder.`increment_id`, ' .
                ' customerfirst.`value` as firstname, ' .
                ' customerlast.`value` as lastname ' .
                ' FROM ' . $salesOrderTable . ' as salesorder ' .
                ' LEFT JOIN ' . $salesOrderItemTable . ' as salesitem ' .
                ' ON salesorder.`entity_id`=salesitem.`order_id` ' .
                ' LEFT JOIN  ' . $productEntityTable . ' as productentity ' .
                ' ON salesitem.`product_id`=productentity.`entity_id` ' .
                ' LEFT JOIN  ' . $productVarcharTable . ' as productvarchar ' .
                ' ON salesitem.`product_id`=productvarchar.`entity_id` ' .
                ' LEFT JOIN  ' . $customerVarcharTable . ' as customerfirst ' .
                ' ON salesorder.`customer_id`=customerfirst.`entity_id` ' .
                ' LEFT JOIN  ' . $customerVarcharTable . ' as customerlast ' .
                ' ON salesorder.`customer_id`=customerlast.`entity_id` ' .
                ' WHERE salesitem.`created_at` BETWEEN \'' . $first . '\' AND \'' . $last . '\'' .
                ' AND salesorder.`customer_id` IS NOT NULL ' .
                ' AND salesorder.`status` != \'canceled\' AND salesorder.`status` != \'closed\' AND salesorder.`status` != \'complete\' ' .
                ' AND productvarchar.`entity_type_id` = 4 AND productvarchar.`attribute_id` = 71 AND productvarchar.`store_id` = 0 ' .
                ' AND customerfirst.`entity_type_id` = 1 AND customerfirst.`attribute_id` = 5 ' .
                ' AND customerlast.`entity_type_id` = 1 AND customerlast.`attribute_id` = 7 ' .
                ' AND salesitem.`item_id` NOT IN ( ' .
                    ' SELECT  salesitem.`parent_item_id` ' .
                    ' FROM ' . $salesOrderItemTable . ' as salesitem ' .
                    ' LEFT JOIN ' . $salesOrderTable . ' as salesorder ' .
                    ' ON salesorder.`entity_id`=salesitem.`order_id` ' .
                    ' WHERE salesitem.`created_at` BETWEEN \'' . $first . '\' AND \'' . $last . '\'' .
                    ' AND salesitem.`parent_item_id` IS NOT NULL ' .
                    ' AND salesorder.`customer_id` IS NOT NULL ' .
                    ' AND salesorder.`status` != \'canceled\' AND salesorder.`status` != \'closed\' AND salesorder.`status` != \'complete\' ' .
                ' ) ' .
                ' ORDER BY salesorder.`customer_id` ASC, salesitem.`created_at` ASC, productentity.`sku` ASC ' ;

        $results = $readConnection->fetchAll($query);
        return $results;
//        var_dump($query);
    }

}
