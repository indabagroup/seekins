<?php

class BinaryAnvil_Freebies_Model_Observer extends BinaryAnvil_Freebies_Block_Checkout_Cart_Crosssell
{   
    
    var $freebies = array();
    var $currentTotalFreebies = 0;
    
    public function updateFreeItems(Varien_Event_Observer $observer){
        $this->getCurrentTotalFreebies($observer);
    }
    
    public function getCurrentTotalFreebies($observer){
        $freebiesItems = $this->getFreebiesProductIds();
                
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote();
        $this->currentTotalFreebies = 0;
        
        foreach($quotes->getAllItems() as $quote){
            if(in_array($quote->getProduct()->getId(),$freebiesItems)){                
                $this->freebies[$quote->getId()] = $quote->getQty();
                $this->currentTotalFreebies += $quote->getQty();
            }
        }
        
        return $this->currentTotalFreebies;
    }
    
    public function updateFreeItemsAfter($observer){
        $subTotal = $this->getCartSubtotal();        
        $freebiesLimit = floor($subTotal/25) + 1;
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote();                       
        
        $freebiesItems = $this->getFreebiesProductIds(); //$this->getProductIdsFromIndexForCartProducts($this->getItemsCount());                                
                
        //if allowed quantity of freebies is less than the current total added freebies
        //remove all the added freebies
        if($freebiesLimit<$this->currentTotalFreebies){
            foreach($quotes->getAllItems() as $quote){
                if(in_array($quote->getProduct()->getId(),$freebiesItems)){
                    $cart->removeItem($quote->getId());
                }
            }
        }
        
//        Mage::log($freebiesLimit."<".$this->currentTotalFreebies);
        
        $cart->save();
    }
    
    public function updateFreeItemsQuantity($observer){
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote();
        
        $freebiesItemsAdded = 0;
        
        $deletedItemPrice = $observer->getQuoteItem()->getPrice();
        $deletedItemQty = $observer->getQuoteItem()->getQty();
        
        $subTotal = $this->getCartSubtotal() - ($deletedItemPrice*$deletedItemQty );
        $freebiesLimit = floor($subTotal/25) + 1;
        $allItems = $quotes->getAllItems();
        
        foreach($allItems as $quote) {
            if ($quote->getPrice()==0) {
                $freebiesItemsAdded += $quote->getQty();
            }
        }
        
        foreach($allItems as $quote){            
            if($quote->getPrice()==0 && $freebiesLimit<$freebiesItemsAdded){
                $cart->removeItem($quote->getId());
            }
        }                
    }
    
    public function saveFreeItemsAfter($observer){
        $totalFreeItemAdded = 0;
        $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
        $subTotal = $totals["subtotal"]->getValue();   
        $freebiesLimit = floor($subTotal/25) + 1;
        
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote(); 
        
        $allItems = $quotes->getAllItems();
        
        $freebiesItems = $this->getFreebiesProductIds(); //$this->getProductIdsFromIndexForCartProducts($this->getItemsCount());
        
         foreach($allItems as $quote) {
            if (in_array($quote->getProduct()->getId(), $freebiesItems)) {
                $totalFreeItemAdded = $totalFreeItemAdded + $quote->getQty();
            }
        }
        
        //if the allowed freebies is less than the total free item added
        // reset to the default quantity
        if($freebiesLimit < $totalFreeItemAdded) {
            foreach ($allItems as $quote) {
                if (in_array($quote->getProduct()->getId(), $freebiesItems)) {
                    if (isset($this->freebies[$quote->getId()]))
                    $cart->updateItem($quote->getId(), array('qty' => $this->freebies[$quote->getId()]));
                }
            }
        }        
        
        //if allowed quantity of freebies is less than the current total added freebies
        //remove all the added freebies
        if($freebiesLimit<$this->currentTotalFreebies || $subTotal==0){
            foreach($allItems as $quote){
                if(in_array($quote->getProduct()->getId(),$freebiesItems)){
                    $cart->removeItem($quote->getId());
                }
            }                        
        }
    }
            
    
    // find qty 
    // (for the whole cart it is $rule->getDiscountQty()
    // for items it is (qty * (number of matched non-free items) / step)
    protected function _getFreeItemsQty($rule, $quote)
    {  
        $amount = max(1, $rule->getDiscountAmount());
        $qty    = 0;
        if ('ampromo_cart' == $rule->getSimpleAction()){
            $qty = $amount;
        }
        else {
            $step = max(1, $rule->getDiscountStep());
            foreach ($quote->getItemsCollection() as $item) {
                if (!$item) 
                    continue;
                    
                if ($item->getOptionByCode('ampromo_rule')) 
                    continue;
                    
                if (!$rule->getActions()->validate($item)) {
                    continue;
                }
                
               $qty = $qty + $item->getQty();
            } 
            
            $qty = floor($qty / $step) * $amount; 
            $max = $rule->getDiscountQty();
            if ($max){
                $qty = min($max, $qty);
            }
        }
        return $qty;        
    }  
    
	public function getCartSubtotal(){
    	return Mage::getModel('checkout/cart')->getQuote()->getSubtotal();
    	
    }
}