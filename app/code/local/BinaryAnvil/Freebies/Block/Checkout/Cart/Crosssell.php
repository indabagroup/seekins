<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_TargetRule
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * TargetRule Checkout Cart Cross-Sell Products Block
 *
 * @category   Enterprise
 * @package    Enterprise_TargetRule
 */
class BinaryAnvil_Freebies_Block_Checkout_Cart_Crosssell extends Enterprise_TargetRule_Block_Checkout_Cart_Crosssell {

    protected $_productsCartQty;
    protected $_categoryID = 14;
    protected $_defaultQty = 1;

    protected function _getCartProductsQty() {
        if (is_null($this->_productsCartQty)) {
            $this->_productsCartQty = array();
            foreach ($this->getQuote()->getAllItems() as $quoteItem) {
                /* @var $quoteItem Mage_Sales_Model_Quote_Item */
                $this->_productsCartQty[$quoteItem->getProductId()] = $quoteItem->getQty();
            }
        }

        return $this->_productsCartQty;
    }

    public function hasItems() {
        $freebiesItems = $this->getFreebiesProductIds();
        if(count($freebiesItems)){
            return true;
        }else{
            return false;            
        }
    }

    /*
     * Freebies limit was reached
     * 
     * @return boolean
     */
    public function isLimitReached() {
        $subTotal = $this->getCartSubtotal();
        $_allowedFreeItems = floor($subTotal / 25) + $this->_defaultQty;

        $freebiesItems = $this->getFreebiesProductIds();
        
        $session = Mage::getSingleton('checkout/session');
        $totalFreebiesAdded = 0;

        foreach ($session->getQuote()->getAllItems() as $item) {
            if (in_array($item->getProduct()->getId(), $freebiesItems)) {
                $totalFreebiesAdded += $item->getQty();
            }
        }

        if ($_allowedFreeItems > $totalFreebiesAdded) {
            return false;
        } else {
            return true;
        }
    }
    
    public function totalFreebiesAddedToCart(){
        $session = Mage::getSingleton('checkout/session');
        $totalFreebiesAdded = 0;
        
        $freebiesItems = $this->getFreebiesProductIds();

        foreach ($session->getQuote()->getAllItems() as $item) {
            if (in_array($item->getProduct()->getId(), $freebiesItems)) {
                $totalFreebiesAdded += $item->getQty();
            }
        }
        
        return $totalFreebiesAdded;
    }
    
    public function allowedFreebies(){
        $subTotal = $this->getCartSubtotal();
        $_allowedFreeItems = floor($subTotal / 25) + $this->_defaultQty;
        
        return $_allowedFreeItems;
    }
    
    public function freebiesAccquired(){
        $subTotal = $this->getCartSubtotal();
        $_allowedFreeItems = floor($subTotal / 25) + $this->_defaultQty;
        
        $freebiessInCart = $this->totalFreebiesAddedToCart();
        
        return $_allowedFreeItems - $freebiessInCart;
    }

    /**
     * Retrieve count of product in collection
     *
     * @return int
     */
    public function getItemsCount() {        
        return count($this->getItemCollection());
    }

    public function getCartSubtotal() {
        return Mage::getModel('checkout/cart')->getQuote()->getSubtotal();
    }

    public function _getProductIdsFromIndexForCartProducts($limit, $excludeProductIds = array()) {
        $resultIds = array();

        foreach ($this->_getCartProducts() as $product) {
            if ($product->getEntityId() == $this->getLastAddedProductId()) {
                continue;
            }

            $productIds = $this
                    ->_getProductIdsFromIndexByProduct($product, $this->getPositionLimit(), $excludeProductIds);
            $resultIds = array_merge($resultIds, $productIds);
        }

        $resultIds = array_unique($resultIds);
        shuffle($resultIds);

        return array_slice($resultIds, 0, $limit);
    }

    /**
     * Retrieve Product Ids from Cross-sell rules based products index by products in shopping cart
     *
     * @param Mage_Catalog_Model_Product $product
     * @param int $limit
     * @param array $excludeProductIds
     * @return array
     */
    public function getProductIdsFromIndexForCartProducts($limit, $excludeProductIds = array()) {
        $resultIds = array();

        foreach ($this->_getCartProducts() as $product) {
            if ($product->getEntityId() == $this->getLastAddedProductId()) {
                continue;
            }

            $productIds = $this
                    ->_getProductIdsFromIndexByProduct($product, $this->getPositionLimit(), $excludeProductIds);
            $resultIds = array_merge($resultIds, $productIds);
        }

        $resultIds = array_unique($resultIds);
        shuffle($resultIds);

        return array_slice($resultIds, 0, $limit);
    }

    public function _getTargetRuleProducts() {        
        $totalFreebiesOnCategory = Mage::getModel('catalog/category')->load($this->_categoryID)->getProductCount();
        $productIds = $this->getFreebiesProductIds();//$this->_getProductIdsFromIndexForCartProducts($totalFreebiesOnCategory);        

        $items = array();
        if ($productIds) {
            $collection = $this->_getProductCollectionByIds($productIds);
            foreach ($collection as $product) {
                $items[$product->getEntityId()] = $product;
            }
        }

        return $items;
    }
    
    // this function was added and used rather that the default function to query the crosssell items
    // because the default function got some problem when you have a grouped products added to cart
    public function getFreebiesProductIds() {
        $category = new Mage_Catalog_Model_Category();
        $category->load(14);
        $prodCollection = $category->getProductCollection();
        foreach ($prodCollection as $product) {
            $prdIds[] = $product->getId(); ///Store all th eproduct id in $prdIds array
        }
        
//        Mage::log('--- product collection ---');
//        Mage::log($prdIds);
        
        return $prdIds;
    }
    
/**
     * Retrieve Maximum Number Of Product
     *
     * @return int
     */
    public function getPositionLimit()
    {
        return 100;
    }

}
