<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/6/14
 * Time: 5:17 PM
 */
class BinaryAnvil_FooterCategories_Model_Config_Source_Catalog_Parentcategories
{

    public function getStoreId()
    {
        if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level
        {
            $store_id = Mage::getModel('core/store')->load($code)->getId();
        }
        elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
        {
            $website_id = Mage::getModel('core/website')->load($code)->getId();
            $store_id = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
        }
        else // default level
        {
            $store_id = 0;
        }

        return $store_id;
    }
    public function toOptionArray($addEmpty = true)
    {
        $tree = Mage::getResourceModel('catalog/category_tree');

        $categoryRootId = Mage::app()->getStore($this->getStoreId())->getRootCategoryId();;

        $collection = Mage::getResourceModel('catalog/category_collection');

        $collection->addAttributeToSelect('name')
            ->addLevelFilter(2)
            ->addIsActiveFilter();
        if ($this->getStoreId() > 0) {
            $collection->addFieldToFilter('path', array('like' => "%/{$categoryRootId}/%"));
        }
            $collection->load();

        $options = array();
        $options[] = array(
                'label' => Mage::helper('adminhtml')->__('-- Please Select a Category --'),
                'value' => ''
            );
        foreach ($collection as $category) {
            $path = explode('/', $category->getPath());
            if ($category->getLevel() != 1 && !empty($path[0])) {
                $options[] = array(
                    'label' => $category->getName(),
                    'value' => $category->getId()
                );
            }
        }

        return $options;
    }
}