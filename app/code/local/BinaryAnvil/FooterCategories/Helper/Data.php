<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/6/14
 * Time: 5:24 PM
 */

class BinaryAnvil_FooterCategories_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'binaryanvil_footercategories_settings/general_settings/enabled';
    const XML_PATH_PARENT_ID = 'binaryanvil_footercategories_settings/general_settings/shop_by';

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag( self::XML_PATH_ENABLED );
    }

    public function getParentId()
    {
        return Mage::getStoreConfig( self::XML_PATH_PARENT_ID );
    }

    public function getChildCategories()
    {
        if($this->isEnabled()) {
            if ($this->getParentId()) {
                $_category = Mage::getModel('catalog/category')->load($this->getParentId());
                $_subcategories = $_category->getChildrenCategories();
                if (count($_subcategories) > 0) {
                    return $_subcategories;
                }
            }
        }
    }
}