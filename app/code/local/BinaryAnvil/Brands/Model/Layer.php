<?php

class BinaryAnvil_Brands_Model_Layer extends Mage_Catalog_Model_Layer {

    public function getBrandProductCollection() {        
        
        $brands = Mage::app()->getRequest()->getParam('brands');                

        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('category_id', array(
                    array('finset' => '3'),
                    array('finset' => '4'),
                    array('finset' => '5'),
                    array('finset' => '6'),
                    array('finset' => '7'),
                    array('finset' => '8')                    
                    ));
        if($brands){
            $collection->addAttributeToFilter('brands',$brands);
        }
        
        
        $collection->addAttributeToSort('category_id', 'asc');
        $this->prepareProductCollection($collection);
        $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;

        return $collection;
    }

}