<?php

class BinaryAnvil_Brands_Model_Category extends Mage_Catalog_Model_Category {

    public function getLayoutUpdateHandle() {
        Mage::log('BinaryAnvil_Brands_Model_Category');
        $layout = 'catalog_category_';        
        $layout .= 'layered';
        return $layout;
    }

}