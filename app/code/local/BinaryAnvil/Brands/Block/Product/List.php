<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class BinaryAnvil_Brands_Block_Product_List extends Mage_Catalog_Block_Product_List {

    public function checkOrder() {
//        $this->setTemplate('sales/order/history.phtml');
//TODO: add full name logic
//        $orders = Mage::getResourceModel('sales/order_collection')
//                ->addAttributeToSelect('*')
//                ->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
//                ->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
//                ->addAttributeToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
//                ->addAttributeToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
//                ->addAttributeToFilter('billing_email', array('eq' => 'add email here'))
//                ->addAttributeToFilter('status', array('eq' => 'complete'))
//                ->setPageSize(1)
//                ->addAttributeToSort('created_at', 'desc');
//        $this->setOrders($orders);
//        $order = Mage::getResourceModel('sales/order_collection')
//            ->addAttributeToSelect('*')
//            ->joinAttribute('billing_firstname', 'order_address/firstname', 'billing_address_id', null, 'left')
//            ->setPageSize(2)
//            ->load();
//        $order = Mage::getModel('sales/order')->getItemsCollection(array('billing_email'));

//        SELECT orders.increment_id, address.street, address.city, address.region,
//        address.postcode, address.telephone, track.track_number
//        FROM sales_flat_order AS orders
//        JOIN sales_flat_order_address AS address ON address.parent_id = orders.entity_id
//        JOIN sales_flat_shipment_track AS track ON track.order_id = orders.entity_id
//        WHERE address.address_type = 'shipping'

        Mage::app();
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $billing_pid_sql = "SELECT `parent_id` FROM sales_flat_order_address WHERE `address_type`='billing' AND `email`='pa2jac@aol.com'";
        $pids = $connection->fetchAll($billing_pid_sql);

        $_pid_arr = array();

        foreach($pids as $pid){
            array_push($_pid_arr, $pid["parent_id"]);
        }

        $sql = "SELECT 
                orders.increment_id, 
                address.street, 
                address.city, 
                address.region,
                address.postcode, 
                address.telephone, 
                track.track_number,
                track.title as carrier,
                orders.`state`,
                orders.`created_at`,
                orders.`base_subtotal` as subtotal,
                orders.`base_shipping_amount`
        FROM sales_flat_order AS orders
        JOIN sales_flat_order_address AS address ON address.parent_id = orders.entity_id
        JOIN sales_flat_shipment_track AS track ON track.order_id = orders.entity_id
        WHERE `address_type`='shipping' AND `postcode`='48390' AND address.`parent_id` IN (". implode(',', $_pid_arr) . ")";

        $rows = $connection->fetchAll($sql);
        Zend_Debug::dump($rows);
    }

    protected function _getProductCollection(){
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            $this->_productCollection = $layer->getBrandProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

}
