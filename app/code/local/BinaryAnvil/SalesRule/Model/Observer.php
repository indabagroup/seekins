<?php
/**
 * Created by PhpStorm.
 * User: Trainee
 * Date: 6/21/16
 * Time: 8:12 PM
 */

class BinaryAnvil_SalesRule_Model_Observer extends Mage_SalesRule_Model_Observer{

    public function sales_order_afterPlace($observer)
    {
        $order = $observer->getEvent()->getOrder();

        if (!$order) {
            return $this;
        }

        // lookup rule ids
        $ruleIds = explode(',', $order->getAppliedRuleIds());
        $ruleIds = array_unique($ruleIds);

        $ruleCustomer = null;
        $customerId = $order->getCustomerId();

        // use each rule (and apply to customer, if applicable)
        if ($order->getDiscountAmount() != 0) {
            foreach ($ruleIds as $ruleId) {
                if (!$ruleId) {
                    continue;
                }
                $rule = Mage::getModel('salesrule/rule');
                $rule->load($ruleId);
                if ($rule->getId()) {
                    if(!Mage::getSingleton('admin/session')->isLoggedIn()){
                        $rule->setTimesUsed($rule->getTimesUsed() + 1);
                    }
                    else{
                        if($rule->getTimesUsed() < $rule->getUsagePerCustomer()){
                            $rule->setTimesUsed($rule->getTimesUsed() + 1);
                        }
                    }
                    $rule->save();

                    if ($customerId) {
                        $ruleCustomer = Mage::getModel('salesrule/rule_customer');
                        $ruleCustomer->loadByCustomerRule($customerId, $ruleId);

                        if ($ruleCustomer->getId()) {
                            if(!Mage::getSingleton('admin/session')->isLoggedIn()){
                                $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() + 1);
                            }
                            else{
                                if($ruleCustomer->getTimesUsed() < $ruleCustomer->getUsagePerCustomer()){
                                    $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() + 1);
                                }
                            }
                        }
                        else {
                            $ruleCustomer
                                ->setCustomerId($customerId)
                                ->setRuleId($ruleId)
                                ->setTimesUsed(1);
                        }
                        $ruleCustomer->save();
                    }
                }
            }
            $coupon = Mage::getModel('salesrule/coupon');
            /** @var Mage_SalesRule_Model_Coupon */
            $coupon->load($order->getCouponCode(), 'code');
            if ($coupon->getId()) {
                if(!Mage::getSingleton('admin/session')->isLoggedIn()){
                    $coupon->setTimesUsed($coupon->getTimesUsed() + 1);
                }
                else{
                    if($coupon->getTimesUsed() < $coupon->getUsagePerCustomer()){
                        $coupon->setTimesUsed($coupon->getTimesUsed() + 1);
                    }
                }
                $coupon->save();
                if ($customerId) {
                    $couponUsage = Mage::getResourceModel('salesrule/coupon_usage');
                    $couponUsage->updateCustomerCouponTimesUsed($customerId, $coupon->getId());
                }
            }
        }
    }
}