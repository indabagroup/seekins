<?php

class BinaryAnvil_Welcomemessage_Block_Welcomemessage extends Mage_Core_Block_Template
{

    const XML_PATH_DEFAULT_GENERAL_PUBLIC_CUSTOMER_GROUP = 'binaryanvil_customerprofile_settings/precor_customer_group_settings/defaultgeneralpublic';

    public function _getCustomerSession() {

        return Mage::getSingleton('customer/session');
    }

    public function getCustomerWelcomeName()
    {
        if (Mage::isInstalled() && $this->_getCustomerSession()->isLoggedIn()) {
            $name = $this->__('%s', $this->escapeHtml($this->_getCustomerSession()->getCustomer()->getName()));
        } else {
            $name = Mage::getStoreConfig('design/header/welcome');
        }

        return $name;

    }

    public function getCustomerWelcomeMessage()
    {
        if (Mage::isInstalled() && $this->_getCustomerSession()->isLoggedIn()) {
             $welcome = $this->__('Logged in as:');
        } else {
             $welcome = $this->__('');
        }
        return $welcome;
     }
    
    public function getLoginLink()
    {
        $link = '';
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {

            if (Mage::isInstalled() && !$this->_getCustomerSession()->isLoggedIn()) {
                $text = $this->__('Login');
                $url = Mage::helper('customer')->getLoginUrl();

                $link = '<a class="login-link" href="'.$url.'" title="'.$text.'">'.$text.'</a>';
            } /*else {
                $text = $this->__('Login');
                $url = Mage::helper('customer')->getAccountUrl();
            }*/

        }
        return $link;
    }

    public function getLogoutLink()
    {
        $link = '';
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {

            if (Mage::isInstalled() && $this->_getCustomerSession()->isLoggedIn()) {
                $text = $this->__('Logout');
                $url = Mage::helper('customer')->getLogoutUrl();
            } /*else {
                $text = $this->__('Login');
                $url = Mage::helper('customer')->getAccountUrl();
            }*/

            $link = '<a class="customer-logout-link" href="'.$url.'" title="'.$text.'">'.$text.'</a>';
        }
        return $link;
    }

    public function getMyAccountLink()
    {
        $link = '';
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {
            if (Mage::isInstalled() && $this->_getCustomerSession()->isLoggedIn()) {
                $text = $this->__('My Account');
                $url = Mage::helper('customer')->getAccountUrl();
            } else {
                $text = $this->__('My Account');
                $url = Mage::helper('customer')->getAccountUrl();
            }
            $link = '<a href="'.$url.'" title="'.$text.'"><strong>'.$text.'</strong></a>';
        }

        return $link;
    }

    public function getCompanyName()
    {

        $data = '';
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {
            $customerSession = Mage::getSingleton('customer/session');//the customer session

            if (Mage::isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {//if the customer is logged in get the name
                $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
                if ($customerAddressId){
                    $address = Mage::getModel('customer/address')->load($customerAddressId);
                    $data = $this->__('%s', $this->escapeHtml($address->getCompany()));
                }else{
                    $data = '';
                }
            } else {//if the customer is not logged in print 'My account'
                $data = $this->__('');
            }
        }
        return $data;
    }

    protected function _getCustomerGroup()
    {
        return $this->_getCustomerSession()->getCustomerGroupId();
    }

    public function getCustomerLabel()
    {
        $text = '';
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {
            if (Mage::isInstalled() && $this->_getCustomerSession()->isLoggedIn() ) {
                $text  = $this->__(Mage::getModel('customer/group')->load($this->_getCustomerGroup())->getCustomerGroupCode());
            }
        }

        return $text;
    }

    public function getTrackOrderLink()
    {
        $link = '';
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {

            if (Mage::isInstalled() && $this->_getCustomerSession()->isLoggedIn()) {
                $text = $this->__('Track Orders');
                $url = Mage::getUrl('sales/order/history/');
            } else {
                $text = $this->__('Track Orders');
                $url = Mage::getUrl('sales/order/history/');
            }
            $link = '<a href="'.$url.'" title="'.$text.'"><strong>'.$text.'</strong></a>';
        }
        return $link;
    }

    public function getCustomerRewardPoints()
    {
        $customer_id = $this->_getCustomerSession()->getCustomer()->getId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $rewardPoints = Mage::getModel('enterprise_reward/reward')
            ->setCustomer($customer)
            ->setWebsiteId(Mage::app()->getWebsite()->getId())
            ->loadByCustomer()
            ->getFormatedCurrencyAmount();

        return $rewardPoints;
    }
}