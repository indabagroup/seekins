<?php
class BinaryAnvil_Welcomemessage_Model_PageCache_Container_Welcomemessage extends
    Enterprise_PageCache_Model_Container_Customer
{
    /**
     * Get identifier from cookies
     *
     * @return string
     */
    protected function _getIdentifier()
    {
        $cacheId = $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CUSTOMER, '')
            . '_'
            . $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CUSTOMER_LOGGED_IN, '');
        return $cacheId;
    }

    /**
     * Get cache identifier
     *
     * @return string
     */
    protected function _getCacheId()
    {
        return 'WELCOMEMESSAGE' . md5($this->_placeholder->getAttribute('cache_id') . $this->_getIdentifier());
    }

    protected function _renderBlock()
    {
        $blockClass = $this->_placeholder->getAttribute('block');
        //Mage::log($blockClass);
        $template = $this->_placeholder->getAttribute('template');
        //Mage::log($template);
        $block = new $blockClass;
        $block->setTemplate($template);
        return $block->toHtml();
    }

    /*protected function _saveCache($data, $id, $tags = array(), $lifetime = null) {
        return false;
    }*/

}