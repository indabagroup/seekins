<?php
/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license/
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category BinaryAnvil
 * @package BinaryAnvil_InventorySnapshot
 * @copyright Copyright (c) 2016-2017 Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license http://www.binaryanvil.com/software/license
 */

class BinaryAnvil_InventorySnapshot_Block_Adminhtml_Inventorysnapshot_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('gridSnapshot');
        $this->setUseAjax(false);
    }

    protected function _prepareCollection()
    {
        /** @var $collection Mage_Reports_Model_Resource_Product_Lowstock_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToFilter(
                'status',
                array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            )
            ->addAttributeToFilter('type_id', array('eq' => 'simple'));

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinTable(
                'cataloginventory/stock_item',
                'product_id=entity_id',
                array('qty', 'is_in_stock'),
                '{{table}}.stock_id=1',
                'left'
            );
        }
        $collection->getSelect()
            ->columns(
                array('attribute_set' => new Zend_Db_Expr('(SELECT attribute_set_name FROM eav_attribute_set WHERE attribute_set_id = e.attribute_set_id)'))
            )->order('attribute_set','asc')->order('sku','asc');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('attribute_set', array(
            'header'    =>Mage::helper('reports')->__('Attribute Set Name'),
            'sortable'  => false,
            'filter'  => false,
            'index'     =>'attribute_set'
        ));

        $this->addColumn('sku', array(
            'header' => Mage::helper('reports')->__('Product SKU'),
            'sortable' => false,
            'filter'  => false,
            'index' => 'sku'
        ));

        $this->addColumn('product_name', array(
            'header' => Mage::helper('reports')->__('Product Name'),
            'sortable' => false,
            'filter'  => false,
            'index' => 'name'
        ));

        $this->addColumn('qty', array(
            'header' => Mage::helper('reports')->__('Inventory'),
            'width' => '215px',
            'align' => 'right',
            'sortable' => false,
            'filter'  => false,
            'index' => 'qty',
            'type' => 'number'
        ));

        $this->addColumn('is_in_stock',
            array(
                'header'=> Mage::helper('catalog')->__('Stock Availability'),
                'width' => '80px',
                'index' => 'is_in_stock',
                'type'  => 'options',
                'filter'  => false,
                'sortable' => false,
                'options' => array('1'=>'In Stock','0'=>'Out Of Stock'),
            ));
        $this->addExportType('*/*/exportInventorySnapshotCsv', Mage::helper('reports')->__('CSV'));

        return parent::_prepareColumns();
    }

    /**
     * Sets sorting order by some column
     *
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _setCollectionOrder($column)
    {
        $collection = $this->getCollection();
        if ($collection) {
            $columnIndex = $column->getFilterIndex() ?
                $column->getFilterIndex() : $column->getIndex();
            if($columnIndex == 'attribute_set') {
                $collection->getSelect()->order($columnIndex.' '.strtoupper($column->getDir()));
            }
            else {
                $collection->setOrder($columnIndex, strtoupper($column->getDir()));
            }
        }
        return $this;
    }
}