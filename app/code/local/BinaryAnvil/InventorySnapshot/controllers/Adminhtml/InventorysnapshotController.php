<?php
/**
 * Binary Anvil, Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Binary Anvil, Inc. Software Agreement
 * that is bundled with this package in the file LICENSE_BAS.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.binaryanvil.com/software/license/
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@binaryanvil.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this software to
 * newer versions in the future. If you wish to customize this software for
 * your needs please refer to http://www.binaryanvil.com/software for more
 * information.
 *
 * @category BinaryAnvil
 * @package BinaryAnvil_InventorySnapshot
 * @copyright Copyright (c) 2016-2017 Binary Anvil,Inc. (http://www.binaryanvil.com)
 * @license http://www.binaryanvil.com/software/license
 */

class BinaryAnvil_InventorySnapshot_Adminhtml_InventorysnapshotController extends Mage_Adminhtml_Controller_Report_Abstract
{
    /**
     * Add report/products breadcrumbs
     *
     * @return Mage_Adminhtml_Report_ProductController
     */
    public function _initAction()
    {
        parent::_initAction();
        $this->loadLayout()
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Reports'), Mage::helper('adminhtml')->__('Reports'));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Reports'))
            ->_title($this->__('Inventory Snapshot'));

        $this->_initAction()
            ->_setActiveMenu('report/inventorysnapshot')
            ->_addBreadcrumb(Mage::helper('reports')->__('Inventory Snapshot'), Mage::helper('reports')->__('Inventory Snapshot'))
            ->_addContent($this->getLayout()->createBlock('inventorysnapshot/adminhtml_inventorysnapshot'))
            ->renderLayout();
    }

    /**
     * Export inventory snapshot products report to CSV format
     *
     */
    public function exportInventorySnapshotCsvAction()
    {
        $fileName   = 'inventory_snapshot_products.csv';
        $content    = $this->getLayout()->createBlock('inventorysnapshot/adminhtml_inventorysnapshot_grid')
            ->setSaveParametersInSession(true)
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _isAllowed()
    {
        return true;
    }
}