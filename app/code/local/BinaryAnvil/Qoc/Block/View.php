<?php

class BinaryAnvil_Qoc_Block_View extends Mage_Core_Block_Template{

    protected $_message = '';
    public $zip;
    public $orderid;
    
    protected function _construct() {
        $this->addData(array(
            'cache_lifetime' => 120,
            'cache_tags' => array(Mage_Catalog_Model_Product::CACHE_TAG),
        ));
    }


    public function checkOrder() {
        Mage::app();
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $zip = trim(Mage::app()->getRequest()->getPost('zip'));
        $order_num = trim(Mage::app()->getRequest()->getPost('orderid'));
        
        $sql = "SELECT 
                DISTINCT orders.increment_id, 
                CONCAT_WS(' ', address.`firstname`, address.`lastname`) as name,
                orders.`status` as state,
                date_format(orders.`created_at`, '%m/%d/%Y') as date,
                orders.`grand_total`,
                track.track_number,
                track.`carrier_code` as carrier
        FROM 
            sales_flat_order AS orders
        LEFT JOIN 
            sales_flat_order_address AS address 
        ON 
            address.parent_id = orders.entity_id        
        LEFT JOIN 
            sales_flat_shipment_track AS track 
        ON
            track.order_id = orders.entity_id
        WHERE
            LOWER(orders.`status`)!='cancel'
        AND 
            orders.`increment_id`='{$order_num}'
        AND 
            address.`postcode`='{$zip}'";        
        
        $rows = $connection->fetchAll($sql);        
        
        if(count($rows)<1 && $zip){
            $this->_message = 'Your search returns no results.';
        }                
        
        $this->zip = $zip;
        $this->orderid = $order_num;
        return $rows;
    }
    
    function seachMessage(){
        return $this->_message;
    }
    
    function getTrackingUrl($tracking_number, $carrier){
        
        $carriers = array(
            'usps'=>"https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1={$tracking_number}",
            'ups'=>"http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums={$tracking_number}",
            'fedex'=>"http://www.fedex.com/Tracking?action=track&tracknumbers={$tracking_number}"
            );
        
            return isset($carriers[$carrier])?$carriers[$carrier]:'#';
        
    }
}