<?php

class BinaryAnvil_Qoc_Model_Observer extends Varien_Event_Observer {

    public function disableCache(Varien_Event_Observer $observer) {
        $action = $observer->getEvent()->getControllerAction();

        $id = $action->getRequest()->getParam('page_id', $action->getRequest()->getParam('id', false));

        //if quick order check cms page
        if ($id == '19') {
            $cache = Mage::app()->getCacheInstance();
            $cache->banUse('full_page'); // Tell Magento to 'ban' the use of FPC for this request
        }
    }
}