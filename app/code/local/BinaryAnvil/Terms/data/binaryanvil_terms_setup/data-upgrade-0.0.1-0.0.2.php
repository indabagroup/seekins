<?php
/**
 * Created by PhpStorm.
 * User: jmagbanua
 * Date: 11/21/2014
 * Time: 2:12 PM
 */
$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->updateAttribute ( 'customer', 'sp_payment_terms', 'sort_order', 95);


$installer->endSetup();