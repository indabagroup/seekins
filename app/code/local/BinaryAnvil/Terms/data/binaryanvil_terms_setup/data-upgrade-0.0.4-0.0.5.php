<?php
/*
 * add custom field for terms in order table
 * by Mike Quisto
 * */
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->addAttribute( 'order', 'customer_term', array ( 'type' => 'varchar' ) );