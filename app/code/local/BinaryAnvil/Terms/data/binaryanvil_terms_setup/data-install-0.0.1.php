<?php
/**
 * Created by PhpStorm.
 * User: jmagbanua
 * Date: 11/20/2014
 * Time: 4:11 PM
 */
$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->addAttribute('customer', 'sp_payment_terms', array(
    'type' => 'int',
    'input' => 'select',
    'label' => 'Customer Payment Terms',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default' => '0',
    'visible_on_front' => 0,
    'is_system' => 0,
    'source' => 'binaryanvil_terms/attribute_source_type'
));

$defaultUsedInForms = array(
    'adminhtml_customer'
);

$customer = Mage::getModel('customer/customer');
$attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
$setup->addAttributeToSet('customer', $attrSetId, 'General', 'sp_payment_terms');

if (version_compare(Mage::getVersion(), '1.4.2', '>=')) {
    Mage::getSingleton('eav/config')
        ->getAttribute('customer', 'sp_payment_terms')
        ->setData('used_in_forms', $defaultUsedInForms)
        ->save();
}

$installer->endSetup();