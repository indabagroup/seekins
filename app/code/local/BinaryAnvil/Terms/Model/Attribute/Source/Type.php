<?php
/**
 * Created by PhpStorm.
 * User: jmagbanua
 * Date: 11/21/2014
 * Time: 12:16 PM
 */
class BinaryAnvil_Terms_Model_Attribute_Source_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    'label' => 'N/A',
                    'value' =>  'na'

                ),
                array(
                    'label' => 'Net 30',
                    'value' =>  1
                ),
                array(
                    'label' => 'Net 15',
                    'value' =>  2
                ),
                array(
                    'label' => '2% 15 Net 30',
                    'value' =>  3
                ),
                array(
                    'label' => 'Due on Reciept',
                    'value' =>  4
                )
            );
        }
        return $this->_options;
    }
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}