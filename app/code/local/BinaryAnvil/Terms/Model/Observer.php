<?php
/**
 * Package: BinaryAnvil
 * Author: calabanzas
 * Date: 7/19/13
 * Time: 11:31 AM
 */
class BinaryAnvil_Terms_Model_Observer
{
    protected function _getHelper()
    {
        return Mage::helper( 'binaryanvil_terms' );

    }

    /**
     * Retrieve current customer group
     *
     * @return int
     */
    protected function _getCustomerGroup()
    {
        return Mage::getSingleton( 'customer/session' )->getCustomerGroupId();
    }

    public function updateOrderStatus(Varien_Event_Observer $observer)
    {
        $payment_data = Mage::app()->getRequest()->getParam( 'payment' );
        //Mage::log($payment_data,null, "test_payment_ponumber.log");
        $order = $observer->getEvent()->getOrder();
        $orderID = $order->getId();
        if ( isset($payment_data[ 'po_number_cc' ]) && $payment_data[ 'po_number_cc' ]) {

            $order_payment = Mage::getSingleton( 'sales/order_payment' )->getCollection()
                ->addFieldToSelect( 'parent_id' )
                ->addFieldToSelect( 'entity_id' )
                ->addFieldToSelect( 'po_number' )
                ->addFieldToSelect( 'cc_po_number' )
                ->addFieldToFilter( 'parent_id', $orderID )->getFirstItem();

            $coll = Mage::getModel( 'sales/order_payment' )->load( $order_payment->getEntityId() );
            $coll->setData( 'cc_po_number', $payment_data[ "po_number_cc" ] )->getResource()->saveAttribute($coll, "cc_po_number");
            //Mage::log($coll->getData(),null, "test_payment_ponumber.log");
            if($payment_data[ "po_number_cc" ] && $payment_data[ "po_number_cc" ] !=""){
                //$coll1 = Mage::getModel( 'sales/order' )->load( $orderID );
                //$coll1->setData("", $payment_data[ "po_number_cc" ] )->getResource()->saveAttribute($oCustomer, $vAttrCode);
            }
            //Mage::log($coll1->getData(),null, "test_payment_ponumber.log");

        }elseif(isset($payment_data[ 'customer_term' ]) && $payment_data[ 'customer_term' ]){
            Mage::getSingleton( 'sales/order' )->load($orderID)->setData('customer_term', $payment_data[ 'customer_term' ])->save();
        }

    }


}