<?php


class BinaryAnvil_ProductStatus_CheckController extends Mage_Core_Controller_Front_Action
{
    public function getStockStatus($parentProduct,$optionIds)
    {
        //Mage::log($optionIds);
        $allProducts = $parentProduct->getTypeInstance(true)
            ->getUsedProducts(null, $parentProduct);
        foreach ($allProducts as $product) {
            $productId  = $product->getId();
            $allowedProductAttributes = $parentProduct->getTypeInstance(true)
                ->getConfigurableAttributes($parentProduct);
            foreach ($allowedProductAttributes as $attribute) {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$attributeValue][] = $productId;
            }
        }
    }
    
    public function getStockStatusAction()
    {
        $parentId  = $this->getRequest()->getParam('product_id');
        $chosenQty  = $this->getRequest()->getParam('chosen_qty');
        $optionIds  = substr_count($this->getRequest()->getParam('option_id'),',') >= 1 ? explode(',',$this->getRequest()->getParam('option_id')) : array($this->getRequest()->getParam('option_id'));
        
        $product = Mage::getModel('catalog/product')->load($parentId);
        $parentProduct = $product;
        $this->getStockStatus($parentProduct,$optionIds);
        $configurableAttributeCollection = $parentProduct->getTypeInstance(true)
            ->getConfigurableAttributes($parentProduct);
        $result = array();
        $i=0;
        $conditions = '';
        foreach($configurableAttributeCollection as $attribute){
            $result[$attribute->getProductAttribute()->getAttributeCode()] = $optionIds[$i];
            $conditions .= $attribute->getProductAttribute()->getAttributeCode().' = '.$optionIds[$i++].' ';
            if(count($optionIds)>1 && $i<count($optionIds))
                $conditions .= ' AND ';
        }
        
        $entityId=null;
        $configOptions = $product->getData('_cache_instance_products');
        foreach($configOptions as $_configOption){
            $countofmatch=0;
            foreach($result as $key=>$val){
                if($_configOption->getData($key) == $val){
                    $countofmatch++;
                    if($countofmatch == count($result)){
                        $match = true;
                        $entityId = $_configOption->getEntityId();
                        break;    
                    }
                }
            }
        }

        if(!$entityId)return;

        $productConfigurable = Mage::getModel('catalog/product')->load($entityId);
        $status = $this->_getProductStockStatus($productConfigurable, $chosenQty);
        echo $status;
    }


    public function _getProductStockStatus($productConfigurable, $chosenQty)
    {
        $attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($productConfigurable->getAttributeSetId())->getAttributeSetName();
        if ($productConfigurable->isAvailable($productConfigurable)):
            $productConfigurable = $productConfigurable->getStockItem();
            $configBackOrdered = $productConfigurable->getUseConfigBackorders();
            $isBackOrdered = $productConfigurable->getBackOrders();
            $qty = $productConfigurable->getQty();
            if ($qty <= 0 && $isBackOrdered):
                $stockStatus = 'Backordered';
            else:
                if($qty < $chosenQty){
                    $extraQty = $chosenQty - $qty;
                    if($isBackOrdered){
                        $stockStatus = $extraQty.' Qty is Backordered';
                    }
                    else{
                        $stockStatus = $extraQty.' Qty is Out of Stock';
                    }
                }else{
                    $stockStatus = 'In stock';
                }
            endif;
        else:
            if ($attributeSetName == "Apparel"):
                $stockStatus = 'Out of Stock';
            else:
                $stockStatus = 'Backordered';
            endif;
        endif;

        return $stockStatus;
    }
}
