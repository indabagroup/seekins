<?php

class BinaryAnvil_LowerSerial_Model_Observer {

    public function saveSerial(Varien_Event_Observer $observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $postData = Mage::app()->getRequest()->getPost('lowerSerial');
        if ($postData) {
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('sales/order_item');

            foreach ($postData as $itemId => $rawSerialKeys) {
                $serialKeys = explode(',',$rawSerialKeys);
                // load sales flat order item
                $salesOrderItem = Mage::getModel('sales/order_item')->load($itemId);
                // edit lower serial number
                if ($salesOrderItem->getData('order_id')) {
                    $itemId = $salesOrderItem->getData('item_id');

                    if ($salesOrderItem->getData('lower_serial_number')) {
                        $oldLowerSerialNumber = unserialize($salesOrderItem->getData('lower_serial_number'));
                    }else{
                        $oldLowerSerialNumber = array();
                    }
                    $oldLowerSerialNumber[$invoice->getData('entity_id')] = (array) $serialKeys;
                    $lowerSerialNumber = serialize($oldLowerSerialNumber);

                    $query = "UPDATE {$table} SET lower_serial_number = '{$lowerSerialNumber}' WHERE item_id = "
                            . (int) $itemId;

                    $writeConnection->query($query);
                }
            }
        }
    }

}
