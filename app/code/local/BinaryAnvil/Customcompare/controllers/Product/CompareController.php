<?php

require_once 'Mage/Catalog/controllers/Product/CompareController.php';
class BinaryAnvil_Customcompare_Product_CompareController extends Mage_Catalog_Product_CompareController
{
    const COMPARE_PRODUCTS_COUNT = 'binaryanvil_customcompare_settings/general_settings/compare_limit';

	
    public function indexAction()
    {
        $items = $this->getRequest()->getParam('items');

        if ($beforeUrl = $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
            Mage::getSingleton('catalog/session')
                ->setBeforeCompareUrl(Mage::helper('core')->urlDecode($beforeUrl));
        }

        if ($items) {
            $items = explode(',', $items);
            $list = Mage::getSingleton('catalog/product_compare_list');
            $this->_redirect('*/*/*');
            return;
        }

        $this->loadLayout();
        $this->renderLayout();
    }
	
    /**
     * Add item to compare list
     */
    public function addAction()
    {

    	$_helper = Mage::helper('catalog/product_compare');
    	if($_helper->getItemCount() < Mage::getStoreConfig(self::COMPARE_PRODUCTS_COUNT, Mage::app()->getStore()->getStoreId())){
        	$productId = (int) $this->getRequest()->getParam('product');
        	$product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())->load($productId);
        	
        	if($_helper->checkCompareProduct($productId)){
        		if (!$this->getRequest()->getParam('isAjax', false)) {	
	        		Mage::getSingleton('catalog/session')->addError(
	                    $this->__('The product %s is already added to comparison list.', Mage::helper('core')->escapeHtml($product->getName()))
	                );
		            $this->_redirectReferer();
        		}
	            return ;
        	}
	        if ($productId && (Mage::getSingleton('log/visitor')->getId() || Mage::getSingleton('customer/session')->isLoggedIn())) {
	            
	
	            if ($product->getId()/* && !$product->isSuper()*/) {
	                Mage::getSingleton('catalog/product_compare_list')->addProduct($product);
	                if (!$this->getRequest()->getParam('isAjax', false)) {	
		                Mage::getSingleton('catalog/session')->addSuccess(
		                    $this->__('The product %s has been added to comparison list.', Mage::helper('core')->escapeHtml($product->getName()))
		                );
	                }
	                Mage::dispatchEvent('catalog_product_compare_add_product', array('product'=>$product));
	            }
	
	            Mage::helper('catalog/product_compare')->calculate();
	        }
    	}else{
    		if (!$this->getRequest()->getParam('isAjax', false)) {	
	    		Mage::getSingleton('catalog/session')->addError(
	                    $this->__('Only %s items can be compared at a time.', Mage::getStoreConfig(self::COMPARE_PRODUCTS_COUNT))
	                );
				$this->_redirect('catalog/product_compare');
    		}
    	}

    	if (!$this->getRequest()->getParam('isAjax', false)) {
            $this->_redirectReferer();
        }
    }

    /**
     * Remove item from compare list
     */
    public function removeAction()
    {
        if ($productId = (int) $this->getRequest()->getParam('product')) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
                
			
            if($product->getId()) {
                /** @var $item Mage_Catalog_Model_Product_Compare_Item */
                $item = Mage::getModel('catalog/product_compare_item');
                if(Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $item->addCustomerData(Mage::getSingleton('customer/session')->getCustomer());
                } elseif ($this->_customerId) {
                    $item->addCustomerData(
                        Mage::getModel('customer/customer')->load($this->_customerId)
                    );
                } else {
                    $item->addVisitorId(Mage::getSingleton('log/visitor')->getId());
                }

                $item->loadByProduct($product);

                if($item->getId()) {
                    $item->delete();
                    if (!$this->getRequest()->getParam('isAjax', false)) {
	                    Mage::getSingleton('catalog/session')->addSuccess(
	                        $this->__('The product %s has been removed from comparison list.', $product->getName())
	                    );
                    }
                    Mage::dispatchEvent('catalog_product_compare_remove_product', array('product'=>$item));
                    Mage::helper('catalog/product_compare')->calculate();
                }
            }
        }

        if (!$this->getRequest()->getParam('isAjax', false)) {
            $this->_redirectReferer();
        }
    }
     /**
     * Remove all items from comparison list
     */
    public function clearAction()
    {
        $items = Mage::getResourceModel('catalog/product_compare_item_collection');

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $items->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId());
        } elseif ($this->_customerId) {
            $items->setCustomerId($this->_customerId);
        } else {
            $items->setVisitorId(Mage::getSingleton('log/visitor')->getId());
        }

        /** @var $session Mage_Catalog_Model_Session */
        $session = Mage::getSingleton('catalog/session');

        try {
            $items->clear();
            if (!$this->getRequest()->getParam('isAjax', false)) {
          		$session->addSuccess($this->__('The comparison list was cleared.'));
            }
            Mage::helper('catalog/product_compare')->calculate();
        } catch (Mage_Core_Exception $e) {
            $session->addError($e->getMessage());
        } catch (Exception $e) {
            $session->addException($e, $this->__('An error occurred while clearing comparison list.'));
        }
        if (!$this->getRequest()->getParam('isAjax', false)) {
        	$this->_redirectReferer();
        }
    }
    


}
