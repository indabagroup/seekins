<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/6/14
 * Time: 1:33 PM
 */

class BinaryAnvil_Customcompare_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'binaryanvil_customcompare_settings/general_settings/enabled';
    const XML_PATH_COMPARE_LIMIT = 'binaryanvil_customcompare_settings/general_settings/compare_limit';

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag( self::XML_PATH_ENABLED );
    }

    public function getCustomCompareLimit()
    {
        return Mage::getStoreConfig( self::XML_PATH_COMPARE_LIMIT );
    }
}