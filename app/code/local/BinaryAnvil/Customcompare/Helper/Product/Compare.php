<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Catalog Product Compare Helper
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class BinaryAnvil_Customcompare_Helper_Product_Compare extends Mage_Catalog_Helper_Product_Compare
{

    public function numberOfCompareItemsAllowed()
    {
        $_customCompareHelper = Mage::helper('binaryanvil_customcompare');

        return $_customCompareHelper->getCustomCompareLimit();
    }
    
    public function checkCompareProduct($productId){
        $existingComapreItems = array();
        $isItemExistinCompare = false;
        foreach($this->getItemCollection() as $_index => $_item){
            if($_index == $productId) $isItemExistinCompare = true;
        }
        return $isItemExistinCompare;
    }

    // remove compare items that exceeds the limit
    public function checkCollection($collection){
        
        $compareItemsToBeShownIds = array();
        
        foreach($collection as $productId => $_item){
            $compareItemsToBeShownIds[] = $productId;
        }
        
        $compareItems = array_slice($compareItemsToBeShownIds, 0, $this->numberOfCompareItemsAllowed(), true);

        foreach($collection as $_index => $_item){
            if(!in_array($_index, $compareItems)) {
                $product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())->load($_index);
                $item = Mage::getModel('catalog/product_compare_item');
                if(Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $item->addCustomerData(Mage::getSingleton('customer/session')->getCustomer());
                } elseif ($this->_customerId) {
                    $item->addCustomerData(
                        Mage::getModel('customer/customer')->load($this->_customerId)
                    );
                } else {
                    $item->addVisitorId(Mage::getSingleton('log/visitor')->getId());
                }

                $item->loadByProduct($product);

                if($item->getId()) $item->delete();
//                Mage::getSingleton('catalog/product_compare_list')->removeProduct($product);
            }
        }
        return $this;
        
    }

    public function getItemCollection()
    {
        if (!$this->_itemCollection) {

            $this->_itemCollection = Mage::getResourceModel('catalog/product_compare_item_collection')
                ->useProductItem(true)
                ->setStoreId(Mage::app()->getStore()->getId());
            
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_itemCollection->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId());
            } elseif ($this->_customerId) {
                $this->_itemCollection->setCustomerId($this->_customerId);
            } else {
                $this->_itemCollection->setVisitorId(Mage::getSingleton('log/visitor')->getId());
            }

            Mage::getSingleton('catalog/product_visibility')
                ->addVisibleInSiteFilterToCollection($this->_itemCollection);

            /* Price data is added to consider item stock status using price index */
            $this->_itemCollection->addPriceData();

            $this->_itemCollection->addAttributeToSelect('name')
                ->addUrlRewrite()
                ->setOrder('catalog_compare_item_id', 'desc')
                ->load();
                
            // remove compare items that exceeds the limit    
            if(count($this->_itemCollection) >= $this->numberOfCompareItemsAllowed()){
                 $this->checkCollection($this->_itemCollection);
            }
            /* update compare items count */
            $this->_getSession()->setCatalogCompareItemsCount(count($this->_itemCollection));
        }

        return $this->_itemCollection;
    }

    /**
     * Calculate cache product compare collection
     *
     * @param  bool $logout
     * @return Mage_Catalog_Helper_Product_Compare
     */
    public function calculate($logout = false)
    {
        // first visit
        if (!$this->_getSession()->hasCatalogCompareItemsCount() && !$this->_customerId) {
            $count = 0;
        } else {
            /** @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Compare_Item_Collection */
            $collection = Mage::getResourceModel('catalog/product_compare_item_collection')
                ->useProductItem(true);
            if (!$logout && Mage::getSingleton('customer/session')->isLoggedIn()) {
                $collection->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId());
            } elseif ($this->_customerId) {
                $collection->setCustomerId($this->_customerId);
            } else {
                $collection->setVisitorId(Mage::getSingleton('log/visitor')->getId());
            }

            /* Price data is added to consider item stock status using price index */
            $collection->addPriceData();
            // latest added compare first
            $collection->setOrder('catalog_compare_item_id', 'desc');

            
            Mage::getSingleton('catalog/product_visibility')
                ->addVisibleInSiteFilterToCollection($collection);

            $count = $collection->getSize();
            // remove compare items that exceeds the limit
            if($count >= $this->numberOfCompareItemsAllowed()){
                 $this->checkCollection($collection);
            }
        }

        $this->_getSession()->setCatalogCompareItemsCount($count);

        return $this;
    }
     public function convertToMetric($value){
         $_value = str_split($value);
         $unitvalue = end($_value);
//         return $unitvalue;
//         print_r($value);
//         die();
//    if($unitvalue == '"'){
//        die("here");
//    }
         switch ($unitvalue) {
             
             case "\"": 
//                 die("here");
//                 return $unitvalue;
                 $returnvalue = preg_replace("/[^0-9.]/", "", $value);
//                 echo 
                $_returnvalue = $returnvalue * 2.54. " cm";
                 return $_returnvalue;
            break;
            
            case 'oz': 
                $returnvalue = preg_replace("/[^0-9.]/", "", $value);
//                 echo 
                $_returnvalue = $returnvalue / 28.35." g";
                 return $_returnvalue;
                break;
                
            default: 
//                echo "here";
             break;
         }
//         $_returnvalue = $value * 2.54;
         
//         return $this;
     }
}
