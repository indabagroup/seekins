<?php

class BinaryAnvil_Pricereport_Model_Entity_Attribute_Source_Groups extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Returns period types as array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $customerGroups = Mage::getSingleton('adminhtml/system_config_source_customer_group')->toOptionArray();
        array_shift($customerGroups);

        array_unshift($customerGroups, array('value' => -1, 'label' => Mage::helper('binaryanvil_pricereport')->__('NOT LOGGED IN')));
        array_unshift(
            $customerGroups, array('value' => 0, 'label' => Mage::helper('binaryanvil_pricereport')->__('--Please Select--'))
        );
        return $customerGroups;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
