<?php

class BinaryAnvil_Pricereport_Block_Adminhtml_Product_Price_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
//    protected $_saveParametersInSession = true;
    public function __construct()
    {
        parent::__construct();
        $this->setId('gridLowstock');
        $this->setUseAjax(false);
    }

    protected function _prepareCollection()
    {
        $helper = Mage::helper('binaryanvil_pricereport');

        $productCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*');
        $productCollection->getSelect()
            ->columns(
                array('attribute_set' => new Zend_Db_Expr('(SELECT attribute_set_name FROM eav_attribute_set WHERE attribute_set_id = e.attribute_set_id)'))
            );

        $customerGroupIds = $helper->getCustomerGroupIdsForDisplay();
        foreach($customerGroupIds as $customerGroupId) {
            $productCollection->getSelect()
                ->columns(
                    array('group_price_' . $customerGroupId => new Zend_Db_Expr(sprintf('(SELECT group_price FROM catalog_product_index_price WHERE entity_id = e.entity_id AND customer_group_id = %s AND website_id = 1)', $customerGroupId)))
                );
        }
        $collection = $productCollection;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('binaryanvil_pricereport');

        $this->addColumn('attribute_set', array(
            'header'    => Mage::helper('reports')->__('Product Family'),
            'sortable'  => true,
            'index'     => 'attribute_set'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('reports')->__('Product Name'),
            'sortable'  => true,
            'index'     => 'name'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('reports')->__('Product SKU'),
            'sortable'  => true,
            'index'     => 'sku'
        ));

        $this->addColumn('price', array(
            'header'    => Mage::helper('reports')->__('Price'),
            'sortable'  => false,
            'index'     => 'price'
        ));

        $this->addColumn('msrp', array(
            'header'    => Mage::helper('reports')->__('MSRP'),
            'sortable'  => false,
            'index'     => 'msrp'
        ));

        $this->addColumn('sp_price_map', array(
            'header'    => Mage::helper('reports')->__('MAP'),
            'sortable'  => false,
            'index'     => 'sp_price_map'
        ));

        $this->addColumn('status',
            array(
                'header'=> Mage::helper('reports')->__('STATUS'),
                'width' => '70px',
                'index' => 'status',
                'sortable'  => true,
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
            ));

        $customerGroupIds = $helper->getCustomerGroupIdsForDisplay();
        foreach($customerGroupIds as $customerGroupId) {
            $customerGroup = Mage::getModel('customer/group')->load($customerGroupId);
            if ($customerGroup->getId()) {
                $this->addColumn('group_price_' . $customerGroupId, array(
                    'header'    => Mage::helper('reports')->__($customerGroup->getData('customer_group_code')),
                    'sortable'  => false,
                    'filter'    => false,
                    'index'     => 'group_price_' . $customerGroupId
                ));
            }
        }

        $this->addExportType('*/*/exportLowstockCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportLowstockExcel', Mage::helper('reports')->__('Excel XML'));

        return parent::_prepareColumns();
    }
}
