<?php

class BinaryAnvil_Pricereport_Block_Adminhtml_Product_Price extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'binaryanvil_pricereport';
        $this->_controller = 'adminhtml_product_price';
        $this->_headerText = Mage::helper('reports')->__('Price Report');
        parent::__construct();
        $this->_removeButton('add');
    }

    public function getStoreSwitcherHtml()
    {
        if (Mage::app()->isSingleStoreMode()) {
            return '';
        }
        return $this->getChildHtml('store_switcher');
    }

    public function getGridHtml()
    {
        return $this->getStoreSwitcherHtml() . parent::getGridHtml();
    }
}
