<?php

class BinaryAnvil_Pricereport_Adminhtml_BinaryAnvil_ReportController extends Mage_Adminhtml_Controller_Action {

    /**
     * Add report/products breadcrumbs
     *
     * @return Mage_Adminhtml_Report_ProductController
     */
    public function _initAction()
    {
        $this->loadLayout();
        $this->_addBreadcrumb(Mage::helper('reports')->__('Products'), Mage::helper('reports')->__('Products'));
        return $this;
    }
    /**
     * price report action
     *
     */
    public function priceAction()
    {

        $this->_title($this->__('Reports'))
             ->_title($this->__('Products'))
             ->_title($this->__('Price'));

            $this->_initAction()
            ->_setActiveMenu('report/product/price_report')
            ->_addBreadcrumb(Mage::helper('reports')->__('Low Stock'), Mage::helper('reports')->__('Low Stock'))
            ->renderLayout();

    }

    /**
     * Export price report to CSV format
     *
     */
    public function exportLowstockCsvAction()
    {
        $fileName   = 'products_price' . '_' . date('Ymd_His') . '.csv';
        $content    = $this->getLayout()->createBlock('binaryanvil_pricereport/adminhtml_product_price_grid')
            ->setSaveParametersInSession(true)
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export price report to XML format
     *
     */
    public function exportLowstockExcelAction()
    {
        $fileName   = 'products_price' . '_' . date('Ymd_His') . '.xml';
        $content    = $this->getLayout()->createBlock('binaryanvil_pricereport/adminhtml_product_price_grid')
            ->setSaveParametersInSession(true)
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }

}
