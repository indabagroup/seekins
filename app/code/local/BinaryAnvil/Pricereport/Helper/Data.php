<?php

class BinaryAnvil_Pricereport_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_PATH_PRICEREPORT_CUSTOMER_GROUP = 'binaryanvil_pricereport/general/customer_group';

    public function getCustomerGroupIdsForDisplay(){
        return explode(",", Mage::getStoreConfig(self::XML_PATH_PRICEREPORT_CUSTOMER_GROUP));
    }

}
