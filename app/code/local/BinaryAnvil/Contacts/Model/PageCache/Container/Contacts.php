<?php
class BinaryAnvil_Contacts_Model_PageCache_Container_Contacts extends
    Enterprise_PageCache_Model_Container_Customer
{
    /**
     * Get identifier from cookies
     *
     * @return string
     */
    protected function _getIdentifier()
    {
        return $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CUSTOMER, '');
    }

    /**
     * Get cache identifier
     *
     * @return string
     */
    protected function _getCacheId()
    {
        return 'BINARYANVIL_CONTACTS' . md5($this->_placeholder->getAttribute('cache_id') . $this->_getIdentifier());
    }

    protected function _renderBlock()
    {
        $blockClass = $this->_placeholder->getAttribute('block');
        //Mage::log($blockClass);
        $template = $this->_placeholder->getAttribute('template');
        //Mage::log($template);
        $block = new $blockClass;
        $block->setTemplate($template);
        return $block->toHtml();
    }

    /*protected function _saveCache($data, $id, $tags = array(), $lifetime = null) {
        return false;
    }*/

}