<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/4/14
 * Time: 11:29 AM
 */

$installer = $this;

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('sp_ffL_archive')};
CREATE TABLE {$this->getTable('sp_ffL_archive')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `ffl_id` int(11) unsigned NULL,
  `ffl_replace_id` int(11) unsigned NULL,
  `created_time` datetime NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();