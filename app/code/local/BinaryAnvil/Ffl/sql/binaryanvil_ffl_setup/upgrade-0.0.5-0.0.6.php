<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/11/14
 * Time: 1:42 PM
 */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
    $this->getTable('sp_ffL_information'), //table name
    'is_default',      //column name
    'int(11) NOT NULL DEFAULT 0'  //datatype definition
);

$installer->getConnection()->changeColumn(
    $this->getTable('sp_ffL_information'), //table name
    'entity_id',      //old column name
    'id',      //new column name
    'int(11) unsigned NOT NULL auto_increment'  //datatype definition
);


$installer->endSetup();