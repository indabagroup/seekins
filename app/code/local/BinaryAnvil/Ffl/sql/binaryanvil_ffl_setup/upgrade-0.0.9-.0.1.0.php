<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 8/4/14
 * Time: 4:16 PM
 */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->modifyColumn($this->getTable('sp_ffL_information'), 'ffl_expiration_date', 'date');

$installer->endSetup();