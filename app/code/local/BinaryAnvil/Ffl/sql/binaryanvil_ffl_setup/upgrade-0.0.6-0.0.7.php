<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/11/14
 * Time: 5:13 PM
 */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sp_ffL_information'),
    'ffl_holder_state_id',
    'varchar(255) AFTER ffl_holder_state'
);

$installer->endSetup();