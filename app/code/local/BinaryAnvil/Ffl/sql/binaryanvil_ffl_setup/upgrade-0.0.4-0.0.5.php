<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/9/14
 * Time: 10:16 AM
 */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropColumn($this->getTable('sp_ffL_information'), 'magento_customer_id');

$installer->getConnection()->dropColumn($this->getTable('sp_ffL_information'), 'magento_customer_email');

$installer->getConnection()->dropColumn($this->getTable('sp_ffL_information'), 'magento_customer_website');


$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('sp_ffL_customer')};
CREATE TABLE {$this->getTable('sp_ffL_customer')} (
  `relation_id` int(11) unsigned NOT NULL auto_increment,
  `ffl_id` int(11) unsigned NULL,
  `magento_customer_id` varchar(30) NOT NULL default '',
  `magento_customer_email` varchar(255) NOT NULL default '',
  `magento_customer_website` varchar(30) NOT NULL default '',
  `remarks` varchar(30) NOT NULL default '',
  `created_time` datetime NULL,
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");


$installer->endSetup();