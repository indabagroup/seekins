<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/21/14
 * Time: 1:44 PM
 */

$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropColumn($this->getTable('sp_ffL_information'), 'is_default');

$installer->getConnection()->addColumn(
    $this->getTable('sp_ffL_customer'), //table name
    'is_default',      //column name
    'int(11) NOT NULL DEFAULT 0'  //datatype definition
);

$installer->getConnection()->addColumn(
    $this->getTable('sp_ffL_information'), //table name
    'owner_customer_id',      //column name
    'varchar(30) NOT NULL default ""'  //datatype definition
);

$installer->endSetup();