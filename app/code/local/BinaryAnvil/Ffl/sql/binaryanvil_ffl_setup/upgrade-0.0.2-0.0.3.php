<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/2/14
 * Time: 6:46 PM
 */
$installer = $this;

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('sp_ffL_information')};
CREATE TABLE {$this->getTable('sp_ffL_information')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `ffl_holder_name` varchar(255) NOT NULL default '',
  `ffl_holder_business_name` text DEFAULT NULL,
  `ffl_holder_address_line` varchar(255) NOT NULL default '',
  `ffl_holder_address_line2` varchar(255) NOT NULL default '',
  `ffl_holder_phone_number` varchar(17) NOT NULL default '',
  `ffl_number` varchar(255) NOT NULL default '',
  `ffl_expiration_date` datetime NULL,
  `ffl_class` varchar(255) NOT NULL default '',
  `ffl_image` varchar(255) NOT NULL default '',
  `ffl_holder_state` varchar(255) NOT NULL default '',
  `ffl_holder_city` text NOT NULL default '',
  `ffl_holder_zip_code` int(11) unsigned NULL,
  `ffl_status` varchar(255) NOT NULL default '',
  `created_time` datetime NULL,
  `updated_time` datetime NULL,
  `remarks` varchar(30) NOT NULL default '',
  `magento_customer_id` varchar(30) NOT NULL default '',
  `magento_customer_email` varchar(255) NOT NULL default '',
  `magento_customer_website` varchar(30) NOT NULL default '',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();