<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/16/14
 * Time: 2:25 PM
 */

class BinaryAnvil_Ffl_Model_Adminhtml_System_Config_Source_Status
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label'=>Mage::helper('adminhtml')->__('-- Please Select --')),
            array('value' => '0', 'label'=>Mage::helper('adminhtml')->__('Pending')),
            array('value' => '1', 'label'=>Mage::helper('adminhtml')->__('Validated')),
            array('value' => '2', 'label'=>Mage::helper('adminhtml')->__('None')),
            array('value' => '3', 'label'=>Mage::helper('adminhtml')->__('Expired')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('Pending'),
            1 => Mage::helper('adminhtml')->__('Validated'),
            2 => Mage::helper('adminhtml')->__('None'),
            3 => Mage::helper('adminhtml')->__('Expired'),
        );
    }
}