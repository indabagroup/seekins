<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/8/14
 * Time: 9:51 AM
 */

class BinaryAnvil_Ffl_Model_Adminhtml_System_Config_Source_Class
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label'=>Mage::helper('adminhtml')->__('-- Please Select --')),
            array('value' => 'class_1', 'label'=>Mage::helper('adminhtml')->__('Class 1')),
            array('value' => 'class_2', 'label'=>Mage::helper('adminhtml')->__('Class 2')),
            array('value' => 'class_3', 'label'=>Mage::helper('adminhtml')->__('Class 3')),
            array('value' => 'class_4', 'label'=>Mage::helper('adminhtml')->__('Class 4')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'class_1' => Mage::helper('adminhtml')->__('Class 1'),
            'class_2' => Mage::helper('adminhtml')->__('Class 2'),
            'class_3' => Mage::helper('adminhtml')->__('Class 3'),
            'class_4' => Mage::helper('adminhtml')->__('Class 4')
        );
    }
}