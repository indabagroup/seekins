<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 3:22 PM
 */

class BinaryAnvil_Ffl_Model_Adminhtml_Observer
{
    protected function associateCustomerToFfl($fflId, $customer, $date)
    {
        $fflExist = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
            ->addFieldToFilter('ffl_id', array('eq'=> $fflId ))
            ->addFieldToFilter('magento_customer_id' , array('eq',$customer->getId()));
        $fflExist = $fflExist->getFirstItem();

        $defaultFfl = 0;
        $relationId = null;
        if ($fflExist->getId()) {
            $relationId = $fflExist->getData('relation_id');
            $defaultFfl = $fflExist->getData('is_default');
        }

        if ($customer != false) {
            $customerDetails = array(
                'magento_customer_id'       => $customer->getId(),
                'magento_customer_email'    => $customer->getEmail(),
                'magento_customer_website'  => $customer->getWebsiteId(),
                'created_time'              => $date,
                'is_default'                => isset($data['is_default'])?$data['is_default']:$defaultFfl,
                'ffl_id'                    => $fflId,
            );
            if ($relationId !== null) {
                $customerDetails['relation_id'] = $relationId;
            }
            $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
            $customerFFlModel->setData($customerDetails)->save();
        }
    }

    public function adminhtmlCustomerSaveAfter(Varien_Event_Observer $observer)
    {

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        $fflPostData = Mage::app()->getRequest()->getPost('ffl');

        if (isset($fflPostData['_template_'])) {
            unset($fflPostData['_template_']);
        }

        if (isset($fflPostData['is_default']) && $fflPostData['is_default'] > 0) {

            $fflId = $fflPostData['is_default'];
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                ->addFieldToFilter('magento_customer_id' , array('eq',$customer->getId()));

            /* remove all rows */
            foreach ($collection as $defaultFfl) {
                if ($fflId == $defaultFfl->getFflId()) {
                    $defaultFfl->setIsDefault(1)->save();
                } else {
                    $defaultFfl->setIsDefault(0)->save();
                }
            }

            unset($fflPostData['is_default']);
        } //if (isset($fflPostData['is_default']) && $fflPostData['is_default'] > 0) {

        if (is_array($fflPostData)) {
            foreach ($fflPostData as $fflId => $data) {
                if (isset($data['_deleted']) && $data['_deleted'] == 1) {

                    $customerFfl = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                        ->addFieldToFilter('magento_customer_id' , array('eq',$customer->getId()))
                        ->addFieldToFilter('ffl_id' , array('eq',$fflId));
                    if (count($customerFfl) > 0) {
                        foreach ($customerFfl as $fflCustomer) {
                            $fflCustomer->delete();
                        }
                    }

                } else {

                    $filename = '';
                    if (isset($_FILES['ffl']['name'][$fflId]['ffl_image']) && $_FILES['ffl']['name'][$fflId]['ffl_image'] != '') {
                        try {
                            $uploader = new Varien_File_Uploader(
                                array(
                                    'name' => $_FILES['ffl']['name'][$fflId]['ffl_image'],
                                    'type' => $_FILES['ffl']['type'][$fflId]['ffl_image'],
                                    'tmp_name' => $_FILES['ffl']['tmp_name'][$fflId]['ffl_image'],
                                    'error' => $_FILES['ffl']['error'][$fflId]['ffl_image'],
                                    'size' => $_FILES['ffl']['size'][$fflId]['ffl_image']
                                )
                            );
                            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
                            $uploader->setAllowRenameFiles(false);
                            $uploader->setFilesDispersion(false);
                            $path = Mage::getBaseDir('media') . DS . 'customer_ffl' . DS;
                            if (!is_dir($path)) {
                                mkdir($path, 0777, true);
                            }
                            $path_parts = pathinfo($_FILES['ffl']['name'][$fflId]['ffl_image']);
                            $filename = $path_parts['filename']. '_' . date('Ymd_His'). '.' .$path_parts['extension'];
                            $uploader->save($path, $filename);
                            $filename = $uploader->getUploadedFileName();
                        } catch (Exception $e) {
                            Mage::logException($e);
                        }
                    } //if (isset($_FILES['ffl']['name'][$fflId]['ffl_image']) && $_FILES['ffl']['name'][$fflId]['ffl_image'] != '') {
                    $date = Mage::getModel( 'core/date' )->gmtDate();
                    $state = isset($post['region'])?$post['region']:'';
                    if (isset($post['region_id'])) {
                        $state = Mage::getModel('directory/region')->load($post['region_id'])->getName();
                    }
                    $addData = array(
                        'ffl_holder_state_id'       => isset($data['region_id'])?$data['region_id']:'',
                        'ffl_holder_state'          => $state,
                        'updated_time'              => $date
                    );
                    if (is_array($data)) {
                        $_data = array_merge($data,$addData);
                    } else {
                        $_data = $addData;
                    }

                    try {
                        if ($filename != '') {
                                $_data['ffl_image'] = $filename;
                        }
                        if ($fflId > 0) {
                            $_data['id'] = $fflId;
                        } else {
                            $_data['ffl_status'] = BinaryAnvil_Ffl_Helper_Data::PENDING;
                        }
                        $ffl = Mage::getModel('binaryanvil_ffl/ffl');
                        $ffl->setData($_data)->save();
                        //Mage::helper('binaryanvil_ffl')->saveFflStatusOnOrder($ffl->getId(), $ffl->getFflStatus());
                    } catch (Exception $e) {
                        Mage::logException($e);
                    } //try {
                    $this->associateCustomerToFfl($ffl->getId(), $customer, $date);

                } //if (isset($data['_deleted']) && $data['_deleted'] == 1) {
            }
        }
    }


}