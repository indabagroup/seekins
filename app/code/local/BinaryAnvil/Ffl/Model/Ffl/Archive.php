<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/4/14
 * Time: 2:02 PM
 */

class BinaryAnvil_Ffl_Model_Ffl_Archive extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl_archive');
    }
}