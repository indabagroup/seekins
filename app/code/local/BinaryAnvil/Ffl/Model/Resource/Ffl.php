<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/3/14
 * Time: 2:37 PM
 */

class BinaryAnvil_Ffl_Model_Resource_Ffl extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl', 'id');
    }
}