<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/4/14
 * Time: 2:03 PM
 */

class BinaryAnvil_Ffl_Model_Resource_Ffl_Archive extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl_archive', 'id');
    }
}