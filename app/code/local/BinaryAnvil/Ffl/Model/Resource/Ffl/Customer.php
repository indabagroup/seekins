<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/9/14
 * Time: 10:54 AM
 */

class BinaryAnvil_Ffl_Model_Resource_Ffl_Customer extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl_customer', 'relation_id');
    }
}