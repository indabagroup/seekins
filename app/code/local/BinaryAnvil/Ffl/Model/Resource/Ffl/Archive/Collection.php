<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/4/14
 * Time: 2:08 PM
 */

class BinaryAnvil_Ffl_Model_Resource_Ffl_Archive_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl_archive');
    }
}