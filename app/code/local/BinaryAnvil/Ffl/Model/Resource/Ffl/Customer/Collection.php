<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/9/14
 * Time: 10:55 AM
 */

class BinaryAnvil_Ffl_Model_Resource_Ffl_Customer_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl_customer');
    }

    public function addFflInformation()
    {
        $this->getSelect()->join(
            array
            ('ffl_table' => $this->getTable( 'ffl' )),
            'main_table.ffl_id = ffl_table.id',
            array ('*')
        );

        return $this;
    }
}