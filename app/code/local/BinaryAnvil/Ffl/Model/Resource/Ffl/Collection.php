<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/3/14
 * Time: 2:38 PM
 */

class BinaryAnvil_Ffl_Model_Resource_Ffl_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('binaryanvil_ffl/ffl');
    }

    public function addCustomerIdFilter($id)
    {
        $ids = array ($id );
        $this->getSelect()->join(
            array
            ('customer_table' => $this->getTable( 'ffl_customer' )),
            'main_table.id = customer_table.ffl_id',
            array ('*')
        )
            ->where (
                'customer_table.magento_customer_id in (?)',
                $ids
            );

        return $this;
    }

    protected function _toOptionArray($valueField='id', $labelField='name', $additional=array())
    {
        $res = array();
        $additional['value'] = $valueField;
        $additional['label'] = $labelField;

        foreach ($this as $item) {
            foreach ($additional as $code => $field) {
                $data[$code] = $item->getData($field);
            }
            $res[] = $data;
        }
        return $res;
    }
}