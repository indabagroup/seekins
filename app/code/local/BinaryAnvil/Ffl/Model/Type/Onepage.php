<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/14/14
 * Time: 6:07 PM
 */

class BinaryAnvil_Ffl_Model_Type_Onepage extends Mage_Checkout_Model_Type_Onepage
{
    /**
     * Initialize quote state to be valid for one page checkout
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function initCheckout()
    {
        $checkout = $this->getCheckout();
        if (is_array($checkout->getStepData())) {
            foreach ($checkout->getStepData() as $step=>$data) {
                if (!($step==='login'
                    || Mage::getSingleton('customer/session')->isLoggedIn() && $step==='billing')) {
                    $checkout->setStepData($step, 'allow', false);
                }
            }
        }

        $checkout->setStepData('ffl', 'allow', true);

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($customer) {
            $this->getQuote()->assignCustomer($customer);
        }
        if ($this->getQuote()->getIsMultiShipping()) {
            $this->getQuote()->setIsMultiShipping(false);
            $this->getQuote()->save();
        }
        return $this;
    }
    /**
     */
    protected function _isFFlExist($fflNumber)
    {
        $customerSession = $this->getCustomerSession();
        $id = $customerSession->getCustomerId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
        $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber))
            ->addCustomerIdFilter($id);
        $collection->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
        $ffl = $collection->getFirstItem();
        if ($ffl && $ffl->getId()) {
            return true;

        } else {
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
            $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber));
            $collection->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            $ffl = $collection->getFirstItem();
            if ($ffl && $ffl->getId()) {
                return $ffl;
            }
        }
        return false;
    }

    public function validateFFl($post)
    {
        $errors = array();

        if (!Zend_Validate::is( trim($post['ffl_holder_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder name cannot be empty.');
        }

        if (!Zend_Validate::is( trim($post['ffl_holder_business_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Business Name cannot be empty.');
        }

        if (!Zend_Validate::is($post['ffl_holder_address_line'], 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Address Line cannot empty');
        }

        if (!Zend_Validate::is( trim($post['ffl_holder_phone_number']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Phone Number cannot be empty.');
        }

        if (!Zend_Validate::is($post['ffl_number'], 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Number cannot be empty.');
        }

        /*if (!Zend_Validate::is($post['ffl_class'] , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Class cannot be empty.');
        }*/

        if (!Zend_Validate::is( trim($post['ffl_expiration_date']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Expiration Date cannot be empty.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    protected function associateCustomer($fflId,$isDefault)
    {
        $customerSession = $this->getCustomerSession();
        $customerId = $customerSession->getCustomerId();
        $date = Mage::getModel( 'core/date' )->gmtDate();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection');
        $collection->addFieldToFilter('ffl_id', array('eq'=> $fflId));
        $collection->addFieldToFilter('magento_customer_id', array('eq'=> $customerId ));
        $customerFfl = $collection->getFirstItem();
        $relationId = $customerFfl->getData('relation_id');

        if ($relationId == '') {
            if (isset($data['is_default']) && $data['is_default'] == 1) {
                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('magento_customer_id' , array('eq',$customerId));

                /* remove all rows */
                foreach ($collection as $defaultFfl) {
                    $defaultFfl->setIsDefault(0)->save();
                }
            }
            $customerDetails = array(
                'magento_customer_id'       => $customerId,
                'magento_customer_email'    => $customerSession->getCustomer()->getData('website_id'),
                'magento_customer_website'  => $customerSession->getCustomer()->getData('email'),
                'created_time'              => $date,
                'is_default'                => $isDefault,
            );

            $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
            $customerFFlModel->setData($customerDetails);
            $customerFFlModel->setFflId($fflId);
            $customerFFlModel->save();
        }
    }


    public function saveFfl($data)
    {

        if (empty($data)) {
            return array('error' => -1, 'message' => $this->_helper->__('Invalid data.'));
        }
        $customerSession = $this->getCustomerSession();
        $customerId = $customerSession->getCustomerId();
        //Mage::log($data);
        $fflId = '';
        if (isset($data['associate']) && $data['associate'] == 1 && isset($data['new_ffl_id']) && $data['new_ffl_id'] > 0) {
            //Mage::log("pass here");
            try {
                $isDefault = 0;
                if (isset($data['is_default']) && $data['is_default'] == 1) {
                    $isDefault = 1;
                }
                $this->associateCustomer($data['new_ffl_id'], $isDefault);
                $this->getQuote()->setFflId($data['new_ffl_id']);

                $this->getQuote()->collectTotals();
                $this->getQuote()->save();

                $this->getCheckout()
                    ->setStepData('ffl', 'allow', true)
                    ->setStepData('ffl', 'complete', true)
                    ->setStepData('billing', 'allow', true);

                return array('new_ffl_id' => $data['new_ffl_id'], 'associate' => 1);

            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->_helper->__( 'An error occurred while saving ffl information.' );
                return array('error' => -1, 'message' => $message);
            }
        } //if (isset($data['associate']) && $data['associate'] == 1 && isset($data['new_ffl_id']) && $data['new_ffl_id'] > 0) {
        //Mage::log("12345");
        if (empty($data['ffl'])) {
            if (isset($data['no_ffl']) && $data['no_ffl'] == 1) {
                $this->getQuote()->setFflId(0);

                $this->getQuote()->collectTotals();
                $this->getQuote()->save();

                $this->getCheckout()
                    ->setStepData('ffl', 'allow', true)
                    ->setStepData('ffl', 'complete', true)
                    ->setStepData('billing', 'allow', true);

                return array();
            } //if (isset($data['no_ffl']) && $data['no_ffl'] == 1) {
            //Mage::log("123456");
            $ffl = $this->_isFFlExist($data['ffl_number']);
            if (empty($data['new_ffl_id'])) {

                if ($ffl === true) {
                    $errorMessage = $this->_helper->__('FFL is already associated to your account.' );
                    return array('error' => -1, 'message' => $errorMessage);
                }

                if ($ffl && $ffl->getId()) {
                    $fflId = $ffl->getId();

                    $html = Mage::helper('binaryanvil_ffl')->getFflHtml($ffl);
                    return array('error' => -1, 'exist' => 1, 'number' => $data['ffl_number'], 'new_ffl_id' => $fflId, 'html' => $html, 'url' => Mage::getUrl('checkout/onepage/saveFfl'));
                }
            } //if (empty($data['new_ffl_id'])) {

            $formErrors = $this->validateFFl($data);
            if ($formErrors !== true) {
                return array('error' => -1, 'message' => $formErrors);
            }

            try {
                $date = Mage::getModel( 'core/date' )->gmtDate();
                $state = isset($data['ffl_holder_state'])?$data['ffl_holder_state']:'';
                if (isset($data['ffl_holder_state_id'])) {
                    $state = Mage::getModel('directory/region')->load($data['ffl_holder_state_id'])->getName();
                }
                //Mage::log("12345678910");
                if (isset($data['new_ffl_id']) && $data['new_ffl_id'] > 0) {
                    $fflCustomerCollection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                        ->addFieldToFilter('magento_customer_id' , array('neq',$customerId))
                        ->addFieldToFilter('ffl_id', array('eq', $data['new_ffl_id']));
                    if (count($fflCustomerCollection) > 1) {
                        $data['ffl_status'] = BinaryAnvil_Ffl_Helper_Data::PENDING;
                    } else {
                        $data['id'] = $data['new_ffl_id'];
                    }
                }
                //Mage::log("12345678910 ----");
                if (empty($data['id'])) {
                    $data['ffl_status'] = BinaryAnvil_Ffl_Helper_Data::PENDING;
                }

               // Mage::log("1234567891011");
                $data['ffl_holder_state'] = $state;
                $model = Mage::getModel('binaryanvil_ffl/ffl');
                $model->setData($data);
                $model->setCreatedTime($date);
                $model->setUpdatedTime($date);
                $model->save();
                $fflId = $model->getId();

                //Mage::log("123456789101112");
                $isDefault = 0;
                if (isset($data['is_default']) && $data['is_default'] == 1) {
                    $isDefault = 1;
                }
                $this->associateCustomer($fflId, $isDefault);
                if($fflId) {
                    $this->getQuote()->setFflId($fflId);

                    $this->getQuote()->collectTotals();
                    $this->getQuote()->save();

                    $this->getCheckout()
                        ->setStepData('ffl', 'allow', true)
                        ->setStepData('ffl', 'complete', true)
                        ->setStepData('billing', 'allow', true);
                    /*if ($ffl && $ffl->getId()) {
                        return array('exist' => 1, 'number' => $data['ffl_number'], 'new_ffl_id' => $fflId);
                    } else {*/
                        return array('new_ffl_id' => $fflId);
                    /*}*/

                } else {
                    return array('error' => -1, 'message' => $this->_helper->__('An error occurred while saving the ffl'));
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->_helper->__( 'An error occurred while saving ffl information.' );
                return array('error' => -1, 'message' => $message);
            }

        } elseif (isset($data['ffl'])) {
            $model = Mage::getModel('binaryanvil_ffl/ffl')->load($data['ffl']);
            if($model->getId()) {
                $fflId = $model->getId();
            } else {
                return array('error' => -1, 'message' => $this->_helper->__('Invalid data.'));
            }
        } else {
            return array('error' => -1, 'message' => $this->_helper->__('Invalid data.'));
        }

        $this->getQuote()->setFflId($fflId);

        $this->getQuote()->collectTotals();
        $this->getQuote()->save();

        $this->getCheckout()
            ->setStepData('ffl', 'allow', true)
            ->setStepData('ffl', 'complete', true)
            ->setStepData('billing', 'allow', true);

        return array();
    }
}