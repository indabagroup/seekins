<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/10/14
 * Time: 1:22 PM
 */

class BinaryAnvil_Ffl_Model_Observer
{
    protected function getCustomerNameDetails($ffl)
    {
        if ($ffl->getOwnerCustomerId()) {
            $customer = Mage::getSingleton('customer/customer')->load($ffl->getOwnerCustomerId());
            if ($customer->getId()) {
                $ffl->setData('firstname',$customer->getFirstname());
                $ffl->setData('lastname',$customer->getLastname());
                $ffl->setData('name',$customer->getName());
                return $ffl;
            }
        }


        return false;
    }
    public function sendExpiryEmail()
    {
        $now = date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));

        /*$collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection');
        $coreResource = Mage::getSingleton( 'core/resource' );
        $fflInfoTable = $coreResource->getTableName( 'binaryanvil_ffl/ffl' );
        $collection->getSelect()->joinLeft(
            array ( 'sfi' => $fflInfoTable ),
            "main_table.ffl_id = sfi.id AND sfi.ffl_status=".BinaryAnvil_Ffl_Helper_Data::VALIDATE,
            array (
                '*'
            )
        );*/
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
        $collection->addFieldToFilter('ffl_status',array('eq'=>BinaryAnvil_Ffl_Helper_Data::VALIDATE));
        /*$collection->getSelect()->joinLeft(
            array ( 'cf' => $fflCustomerTable ),
            "main_table.owner_customer_id = cf.magento_customer_id",
            array (
                '*'
            )
        );*/
        /*$collection = Mage::getResourceModel('customer/customer_collection');
        $collection->addNameToSelect();
        $coreResource = Mage::getSingleton( 'core/resource' );
        $fflInfoTable = $coreResource->getTableName( 'binaryanvil_ffl/ffl' );
        $collection->getSelect()->join(
            array ( 'sfi' => $fflInfoTable ),
            "e.entity_id = sfi.owner_customer_id AND sfi.ffl_status=".BinaryAnvil_Ffl_Helper_Data::VALIDATE,
            array (
                '*'
            )
        );*/


        $helper = Mage::helper('binaryanvil_ffl');
        foreach ($collection as $ffl) {
            $fflExpiryDate = $ffl->getData('ffl_expiration_date');
            if (!empty($fflExpiryDate)) {
                Mage::log($fflExpiryDate);
                $dStart = new DateTime($now);
                $dEnd  = new DateTime($fflExpiryDate);
                $dDiff = $dStart->diff($dEnd);
                $result = $dDiff->days;
                $pointOutRelation = $dDiff->format('%R');
                Mage::log("---------------------------start--------------------------");
                Mage::log($result);
                Mage::log($pointOutRelation);
                Mage::log(Mage::helper('core')->formatDate($fflExpiryDate, 'short', false));
                Mage::log("---------------------------end--------------------------");
                if ($pointOutRelation = '+') {

                    if ($result == 14) {
                        // send email
                        if ($ffl->getOwnerCustomerId()) {
                            $ffl = $this->getCustomerNameDetails($ffl);
                            if ($this->getCustomerNameDetails($ffl) !== false) {
                                $ffl->setData('days_to_expire', $result);
                                //$date = Mage::helper('core')->formatDate($fflExpiryDate, 'medium', false);
                                $date = new DateTime($ffl->getFflExpirationDate());
                                $date = $date->format('F d, Y');
                                $ffl->setData('expiration_date',$date);
                                $helper->sendEmailExpiryNotification($ffl);
                                $helper->sendBccEmailNotification($ffl);
                            }
                        }
                    }

                    if ($result == 3) {
                        // send email
                        if ($ffl->getOwnerCustomerId()) {
                            $ffl = $this->getCustomerNameDetails($ffl);
                            if ($this->getCustomerNameDetails($ffl) !== false) {
                                $ffl->setData('days_to_expire', $result);
                                //$date = Mage::helper('core')->formatDate($fflExpiryDate, 'medium', false);
                                $date = new DateTime($ffl->getFflExpirationDate());
                                $date = $date->format('F d, Y');
                                $ffl->setData('expiration_date',$date);
                                $helper->sendEmailExpiryNotification($ffl);
                                $helper->sendBccEmailNotification($ffl);
                            }
                        }
                    }

                    if ($result == 2) {
                        // send email
                        if ($ffl->getOwnerCustomerId()) {
                            $ffl = $this->getCustomerNameDetails($ffl);
                            if ($this->getCustomerNameDetails($ffl) !== false) {
                                $ffl->setData('days_to_expire', $result);
                                //$date = Mage::helper('core')->formatDate($fflExpiryDate, 'medium', false);
                                $date = new DateTime($ffl->getFflExpirationDate());
                                $date = $date->format('F d, Y');
                                $ffl->setData('expiration_date',$date);
                                $helper->sendEmailExpiryNotification($ffl);
                                $helper->sendBccEmailNotification($ffl);
                            }
                        }
                    }

                    if ($result == 1) {
                        // send email
                        if ($ffl->getOwnerCustomerId()) {
                            $ffl = $this->getCustomerNameDetails($ffl);
                            if ($this->getCustomerNameDetails($ffl) !== false) {
                                $ffl->setData('days_to_expire', $result);
                                //$date = Mage::helper('core')->formatDate($fflExpiryDate, 'medium', false);
                                $date = new DateTime($ffl->getFflExpirationDate());
                                $date = $date->format('F d, Y');
                                $ffl->setData('expiration_date',$date);
                                $helper->sendEmailExpiryNotification($ffl);
                                $helper->sendBccEmailNotification($ffl);
                            }
                        }

                    } //if ($result == 1) {

                    if ($result <= 1) {
                        $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($ffl->getId());
                        $ffl->setFflStatus(3)->save();
                        // save ffl_status on sales_flat_order
                        Mage::helper('binaryanvil_ffl')->saveFflStatusOnOrder($ffl->getId(), 3);
                    }
                } //if ($pointOutRelation = '+') {
            } //if (!empty($fflExpiryDate)) {
        } //foreach ($collection as $ffl) {

        //start - send email to confirm that cron si running
        /*
        if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'localhost') {
            $to_email = "change me?";
        } else {
            $to_email = 'change me!';
        }

        $subject = 'Cron to sent expiry notification email is running';
        $url = Mage::getBaseUrl();
        if (preg_match('/dev/',$url)) {
            $subject = 'Cron to sent expiry notification email is running on dev site';
        }

        $to_name = 'Camille Alabanzas'; // @TODO: add name of recipient
        $Body = $subject;
        $sender_email = $subject;
        $sender_name = $subject;

        $mail = new Zend_Mail (); //class for mail
        $mail->setBodyHtml ( $Body ); //for sending message containing html code
        $mail->setFrom ( $sender_email, $sender_name );
        $mail->addTo ( $to_email, $to_name );
        $mail->setSubject ( $subject );
        $mail->send ();
        */
        //start - send email to confirm that cron si running
    }

    /**
     * After save observer for quote
     *
     * @param Varien_Event_Observer $observer
     * @return Enterprise_Customer_Model_Observer
     */
    /*public function salesQuoteSaveBefore(Varien_Event_Observer $observer)
    {

        $quote = $observer->getEvent()->getQuote();
        if (!$quote) {
            return;
        }
        $fflId = Mage::app()->getRequest()->getParam('ffl_id');
        $ffl = Mage::app()->getRequest()->getParam('ffl');
        $noFfl = Mage::app()->getRequest()->getParam('no_ffl');
        $quoteFflId = $quote->getFflId();
        if (empty($quoteFflId)) {
            if ($ffl > 0) {
                $quote->setFflId($fflId);
            } elseif ($fflId > 0) {
                $quote->setFflId($fflId);
            } elseif ($noFfl) {
                $quote->setFflId(-1);
            }
        }
    }*/

    protected function hasFflRestrictedItems($quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            $attributeSetName = '';
            if ($product->getData('attribute_set_id')) {
                $attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($product->getAttributeSetId())->getAttributeSetName();
            }
            $fflNeeded = $product->getSpFflNeeded();
            if($fflNeeded == 1 || $attributeSetName == 'Complete Rifles' || $attributeSetName == 'AR Lowers') {
                return true;
            }
        }

        return false;
    }

    public function salesConvertQuoteToOrder(Varien_Event_Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $order = $observer->getEvent()->getOrder();
//        Mage::log("start - convert");
//        Mage::log($quote->getFflId());
//        Mage::log("end - convert");
        $hasFflRestrictedItems = $this->hasFflRestrictedItems($quote);
        $fflQuoteId = $quote->getFflId();
        if (!is_null($fflQuoteId) || $hasFflRestrictedItems) {
            if ($fflQuoteId > 0) {
                $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflQuoteId);
                if ($ffl->getId()) {
                    $status = $ffl->getData('ffl_status');
                    $order->setFflStatus($status);
                }
                $order->setFflId($quote->getFflId());
            } elseif ($fflQuoteId == 0) {
                $order->setFflId(0);
                $order->setFflStatus(2);
            }
        }

        return $this;
    }

}