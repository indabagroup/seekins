<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 2:31 PM
 */

class BinaryAnvil_Ffl_Adminhtml_FflController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed(){
        return true;
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->addJs('binaryanvil/ffl.js');
        if ($this->getRequest()->getParam('popup')) {
            $this->_title($this->__('Associate FFL'));
        } else {
            $this->_title($this->__('Manage Customer FFLs'));
        }

        $this->renderLayout();
    }

    protected function _getRedirectUrl()
    {
        if ($this->getRequest()->getParam('popup')) {
            $orderId = $this->getRequest()->getParam('order_id');
            $customerId = $this->getRequest()->getParam('customer_id');
            $new = $this->getRequest()->getParam('new');
            $this->_redirect('*/*/',array('order_id' => $orderId, 'customer_id'=>$customerId, 'popup' => '1', 'new'=>$new));
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function editAction()
    {
        $fflId = $this->getRequest()->getParam('id');
        $fflModel = Mage::getModel('binaryanvil_ffl/ffl');
        $model = $fflModel->load($fflId);
        // set entered data if was error when we do save
        $data = $this->_getSession()->getFflData(true);
        if ($model->getData('id') || $fflId == 0 || $data)
        {
            if ($data) {
                $fflModel->addData($data);
                Mage::register('current_ffl_data', $fflModel);
            } else {
                Mage::register('current_ffl_data', $model);
            }

            $this->loadLayout();
            $this->getLayout()->getBlock('head')->addJs('binaryanvil/ffl.js');
            $this->_setActiveMenu('customer/binaryanvil_ffl');
            $this->getLayout()->getBlock('head')
                ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                ->createBlock('binaryanvil_ffl/adminhtml_ffl_edit'))
                ->_addLeft($this->getLayout()
                        ->createBlock('binaryanvil_ffl/adminhtml_ffl_edit_tabs')
                );
            $breadcrumbMessage = $fflId ? Mage::helper('binaryanvil_ffl')->__('Edit FFL')
                : Mage::helper('binaryanvil_ffl')->__('Add new FFL');
            $this->_addBreadcrumb($breadcrumbMessage, $breadcrumbMessage);
            $this->_title($fflId ? $model->getData('ffl_holder_name') : $this->__('New FFL'));
            $this->renderLayout();
        }
        else
        {
            Mage::getSingleton('adminhtml/session')
                ->addError('Customer FFL does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    protected $_filename;

    /**
     */
    protected function _isFFlExist()
    {
        $id = $this->getRequest()->getPost('magento_customer_id');
        $fflNumber = $this->getRequest()->getPost('ffl_number');
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
        $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber))
            ->addCustomerIdFilter($id);
        $collection->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
        $ffl = $collection->getFirstItem();
        if ($ffl && $ffl->getId()) {
            return true;

        } else {
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
            $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber));
            $collection->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            $ffl = $collection->getFirstItem();
            if ($ffl && $ffl->getId()) {
                return $ffl;
            }
        }

        return false;
    }

    protected function _saveImagePdf()
    {
        $errors = array();

        if (isset($_FILES['ffl_image']['name']) && $_FILES['ffl_image']['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader('ffl_image');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'customer_ffl' . DS;
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $path_parts = pathinfo($_FILES['ffl_image']['name']);
                if (isset($path_parts['filename']) && isset($path_parts['extension'])) {
                    $filename = $path_parts['filename']. '_' . date('Ymd_His'). '.' .$path_parts['extension'];
                    $uploader->save($path, $filename);
                    $this->_filename = $uploader->getUploadedFileName();
                }

            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->__( 'An error occurred while saving ffl information.' );
                $errors[] = $message;
            }
        } else {
            if ($this->_isFFlExist() == false) {
                $message = $this->__( 'The FFL image cannot be empty.' );
                $errors[] = $message;
                return;
            }

        }//if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

        return $errors;
    }

    public function validate()
    {
        $errors = array();
        $post = $this->getRequest()->getPost();

        if (!Zend_Validate::is( trim($post['ffl_holder_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder name cannot be empty.');
        }

        if (!Zend_Validate::is( trim($post['ffl_holder_business_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Business Name cannot be empty.');
        }

        if (!Zend_Validate::is($post['ffl_holder_address_line'], 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Address Line cannot empty');
        }

        if (!Zend_Validate::is( trim($post['ffl_holder_phone_number']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Phone Number cannot be empty.');
        }

        if (!Zend_Validate::is($post['ffl_number'], 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Number cannot be empty.');
        }

        /*if (!Zend_Validate::is($post['ffl_class'] , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Class cannot be empty.');
        }*/

        if (!Zend_Validate::is( trim($post['ffl_expiration_date']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Expiration Date cannot be empty.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    protected function _getCustomer($customerId)
    {
        if ($customerId) {
            $customer = Mage::getModel('customer/customer')->load($customerId);
            if ($customer->getId()) {
                return $customer;
            }
        }

        return false;
    }

    public function saveAction()
    {
        $post = $this->getRequest()->getPost();
        $fflId = $this->getRequest()->getParam('id');

        $session = $this->_getSession();
        $errors = array();
            $ffl = $this->_isFFlExist();

            if ($ffl === true) {
                $errorMessage = $this->__( 'FFL is already associated to your account.' );
                $errors[] = $errorMessage;
            }

            if (empty($fflId) && $ffl && $ffl->getId()) {
                $message = $this->__( 'FFL already exist.' );
                $errors[] = $message;
            }
            $customers = '';
            if(isset($post['links'])){
                $customers = Mage::helper('adminhtml/js')->decodeGridSerializedInput($post['links']['customers']); //Save the array to your database
            }
            /*print_r($customers);
            die();*/
            $formErrors = $this->validate();
            if ($formErrors !== true) {
                $errors = array_merge($errors, $formErrors);
            }
            if(isset($data['ffl_image']['delete']) && $data['ffl_image']['delete'] == 1){
                // delete existing photo
                $delete_path = Mage::getBaseDir('media') . DS . 'customer_ffl' . DS . $data['ffl_image']['value'];
                unset($delete_path);
                $fileName = '';
                $this->_filename = $fileName;
            } else {
                $imagePdfErrors = $this->_saveImagePdf();
                if (count($imagePdfErrors) !== 0) {
                    $errors = array_merge($errors, $formErrors);
                }
            }

            /*start - set data*/
            $state = isset($post['ffl_holder_state'])?$post['ffl_holder_state']:'';
            if (isset($post['ffl_holder_state_id'])) {
                $state = Mage::getModel('directory/region')->load($post['ffl_holder_state_id'])->getName();
            }
            $date = Mage::getModel( 'core/date' )->gmtDate();
            $data = array(
                'ffl_holder_name'           => $post['ffl_holder_name'],
                'ffl_holder_business_name'  => $post['ffl_holder_business_name'],
                'ffl_holder_address_line'   => $post['ffl_holder_address_line'],
                'ffl_holder_address_line2'  => $post['ffl_holder_address_line2'],
                'ffl_holder_phone_number'   => $post['ffl_holder_phone_number'],
                'ffl_number'                => $post['ffl_number'],
                'ffl_expiration_date'       => $post['ffl_expiration_date'],
                'ffl_holder_state_id'       => isset($post['ffl_holder_state_id'])?$post['ffl_holder_state_id']:'',
                'ffl_holder_state'          => $state,
                'ffl_holder_zip_code'       => $post['ffl_holder_zip_code'],
                'owner_customer_id'         => isset($post['owner_customer_id']) ? $post['owner_customer_id'] : '',
                'ffl_image'                 => $this->_filename,
                'ffl_status'                => $post['ffl_status'],
                'created_time'              => $date,
            );
            /*end - set data*/

            if (count($errors) === 0) {

                try {
                    $message = $this->__( 'FFL was successfully save.' );
                    if ($fflId) {
                        if ($this->_filename == '') { unset($data['ffl_image']); }
                        unset($data['created_time']);
                        unset($data['magento_customer_id']);
                        unset($data['magento_customer_email']);
                        unset($data['magento_customer_website']);
                        $message = $this->__( 'FFL was successfully edited.' );
                        $model = Mage::getModel('binaryanvil_ffl/ffl');
                        $data['id'] = $fflId;
                        $model->setData($data);
                    } else {
                        $model = Mage::getModel('binaryanvil_ffl/ffl');
                        $model->setData($data);
                    }

                    $model->setUpdatedTime($date);
                    $model->save();

                    $id = $model->getId();

                    if (is_array($customers)) {
                        $saveCustomerIds = array();
                        foreach ($customers as $customerId=>$data) {

                            if (isset($data['is_default']) && $data['is_default'] == 1) {
                                $defaultFflCollection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                                    ->addFieldToFilter('is_default',array('eq'=>1))
                                    ->addFieldToFilter('ffl_id',array('neq'=>$fflId))
                                    ->addFieldToFilter('magento_customer_id' , array('eq',$customerId));
                                /* update all rows */

                                if (count($defaultFflCollection)) {
                                    foreach ($defaultFflCollection as $defaultFfl) {
                                        $defaultFfl->setIsDefault(0)->save();
                                    }
                                }
                            } //if (isset($data['is_default']) && $data['is_default'] == 1) {

                            $fflExist = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                                ->addFieldToFilter('ffl_id', array('eq'=> $id ))
                                ->addFieldToFilter('magento_customer_id' , array('eq',$customerId));
                            $fflExist = $fflExist->getFirstItem();

                            $defaultFfl = 0;
                            $relationId = null;
                            if ($fflExist->getId()) {
                                $relationId = $fflExist->getData('relation_id');
                                $defaultFfl = $fflExist->getData('is_default');
                            }

                            $customer = $this->_getCustomer($customerId);
                            if ($customer != false) {
                                $customerDetails = array(
                                    'magento_customer_id'       => $customerId,
                                    'magento_customer_email'    => $customer->getEmail(),
                                    'magento_customer_website'  => $customer->getWebsiteId(),
                                    'created_time'              => $date,
                                    'is_default'                => isset($data['is_default'])?$data['is_default']:$defaultFfl,
                                    'ffl_id'                    => $id,
                                );
                                if ($relationId !== null) {
                                    $customerDetails['relation_id'] = $relationId;
                                }
                                $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
                                $customerFFlModel->setData($customerDetails)->save();
                                $saveCustomerIds[] = $customerId;
                            }

                        } //foreach ($customers as $customerId=>$data) {
                        // unassociated customer

                        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                            ->addFieldToFilter('ffl_id', array('eq'=> $id ));
                        if (count($collection) > 0) {
                            foreach ($collection as $fflCustomer) {
                                $fflCustomerId = $fflCustomer->getData('magento_customer_id');
                                if (!in_array($fflCustomerId,$saveCustomerIds)) {
                                    $fflCustomer->delete();
                                }
                                //$fflCustomer->delete();
                            }
                        }

                    } //if (is_array($customers)) {
                    // save ffl status on sales_flat_order
                    $status = $model->getData('ffl_status');
                    Mage::helper('binaryanvil_ffl')->saveFflStatusOnOrder($id, $status);
                    $session->addSuccess( $message );
                    $fflId = $id;
                } catch (Exception $e) {
                    Mage::logException($e);
                    $this->_getSession()->setFflData($data);
                    $message = $this->__( 'An error occurred while saving ffl information.' );
                    $session->addError( $message );
                }
            } else {
                $this->_getSession()->setFflData($data);
                foreach ($errors as $errorMessage) {
                    $this->_getSession()->addError($errorMessage);
                }
            } // if (count($errors) === 0) {

        if ($this->getRequest()->getParam('popup') && count($errors) === 0) {

            $this->_redirect('*/*/created', array(
                '_current'   => true,
                'id'         => $fflId,
                'edit'       => $fflId
            ));
        } elseif ($this->getRequest()->getParam('popup') && count($errors) !== 0) {
            $this->_getRedirectUrl();

        } elseif ($this->getRequest()->getParam('back')) {
            if ($fflId) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
            } else {
                $this->_redirect('*/*/');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function associateAction()
    {

        $orderId = $this->getRequest()->getParam('order_id');
        $customerId = $this->getRequest()->getParam('customer_id');
        $fflId = $this->getRequest()->getParam('ffl_id');
        if ($this->getRequest()->getParam('new')) {
            $errors = array();
            $post = $this->getRequest()->getPost();
                $ffl = $this->_isFFlExist();

                if ($ffl === true) {
                    $errorMessage = $this->__( 'FFL is already associated to your account.' );
                    $errors[] = $errorMessage;
                }

                if (empty($id) && $ffl && $ffl->getId()) {
                    $message = $this->__( 'FFL already exist.' );
                    $errors[] = $message;
                }

                $formErrors = $this->validate();
                if ($formErrors !== true) {
                    $errors = array_merge($errors, $formErrors);
                }

                $imagePdfErrors = $this->_saveImagePdf();
                if (count($imagePdfErrors) !== 0) {
                    $errors = array_merge($errors, $formErrors);
                }

                if (count($errors) === 0) {
                    $state = isset($post['ffl_holder_state'])?$post['ffl_holder_state']:'';
                    if (isset($post['ffl_holder_state_id'])) {
                        $state = Mage::getModel('directory/region')->load($post['ffl_holder_state_id'])->getName();
                    }
                    $date = Mage::getModel( 'core/date' )->gmtDate();
                    $data = array(
                        'ffl_holder_name'           => $post['ffl_holder_name'],
                        'ffl_holder_business_name'  => $post['ffl_holder_business_name'],
                        'ffl_holder_address_line'   => $post['ffl_holder_address_line'],
                        'ffl_holder_address_line2'  => $post['ffl_holder_address_line2'],
                        'ffl_holder_phone_number'   => $post['ffl_holder_phone_number'],
                        'ffl_number'                => $post['ffl_number'],
                        'ffl_expiration_date'       => $post['ffl_expiration_date'],
                        'ffl_holder_state_id'       => isset($post['ffl_holder_state_id'])?$post['ffl_holder_state_id']:'',
                        'ffl_holder_state'          => $state,
                        'ffl_holder_zip_code'       => $post['ffl_holder_zip_code'],
                        'ffl_image'                 => $this->_filename,
                        'ffl_status'                => $post['ffl_status'],
                        'created_time'              => $date,
                    );

                    try {


                        $model = Mage::getModel('binaryanvil_ffl/ffl');
                        $model->setData($data);
                        $model->setUpdatedTime($date);
                        $model->save();

                        $id = $model->getId();
                        $customer = $this->_getCustomer($customerId);
                        $customerDetails = array(
                            'magento_customer_id'       => $customerId,
                            'magento_customer_email'    => $customer->getEmail(),
                            'magento_customer_website'  => $customer->getWebsiteId(),
                            'created_time'              => $date,
                            'is_default'                => 0,
                            'ffl_id'                    => $id,
                        );
                        $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
                        $customerFFlModel->setData($customerDetails)->save();


                        $order = Mage::getModel('sales/order')->load($orderId);
                        $order->setFflId($id);
                        $order->setFflStatus($model->getData('ffl_status'));
                        $order->save();
                        $this->_redirect('*/*/created', array(
                            '_current'   => true,
                            'id'         => $fflId,
                            'edit'       => $fflId
                        ));
                        return;

                    } catch (Exception $e) {
                        Mage::logException($e);
                        $message = $this->__( 'An error occurred while saving ffl information.' );
                        $this->_getSession()->addError( $message );
                    }
                } else {
                    foreach ($errors as $errorMessage) {
                        $this->_getSession()->addError($errorMessage);
                    }
                } // if (count($errors) === 0) {
        } elseif ($fflId) {
            $fflExist = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                ->addFieldToFilter('ffl_id', array('eq'=> $fflId ))
                ->addFieldToFilter('magento_customer_id' , array('eq',$customerId));
            $fflExist = $fflExist->getFirstItem();

            $order = Mage::getModel('sales/order')->load($orderId);
            $order->setFflId($fflId);
            $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
            $order->setFflStatus($ffl->getData('ffl_status'));
            $order->save();
            if ($fflExist->getId()) {
                $this->_redirect('*/*/created', array(
                    '_current'   => true,
                    'id'         => $fflId,
                    'edit'       => $fflId
                ));
                return;
            } else {

                $date = Mage::getModel( 'core/date' )->gmtDate();
                $customer = $this->_getCustomer($customerId);
                $customerDetails = array(
                    'magento_customer_id'       => $customerId,
                    'magento_customer_email'    => $customer->getEmail(),
                    'magento_customer_website'  => $customer->getWebsiteId(),
                    'created_time'              => $date,
                    'is_default'                => 0,
                    'ffl_id'                    => $fflId,
                );
                try {
                    $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
                    $customerFFlModel->setData($customerDetails)->save();

                    $this->_redirect('*/*/created', array(
                        '_current'   => true,
                        'id'         => $fflId,
                        'edit'       => $fflId
                    ));
                    return;
                } catch (Exception $e) {
                    Mage::logException($e);
                    $message = $this->__( 'An error occurred while saving ffl information.' );
                    $this->_getSession()->addError( $message );
                }
            }
        } else {
            $message = $this->__( 'Please select an FFL to associate' );
            $this->_getSession()->addError( $message );
        }


        $this->_getRedirectUrl();
    }

    public function createdAction()
    {
        $this->_getSession()->addNotice(
            Mage::helper('catalog')->__('Please click on the Close Window button if it is not closed automatically.')
        );
        $this->loadLayout('popup');
        $this->_addContent(
            $this->getLayout()->createBlock('binaryanvil_ffl/adminhtml_ffl_created')
        );
        $this->renderLayout();
    }

    public function deleteAction()
    {
        if( $this->getRequest()->getParam('id') > 0 ){
            try{
                $fflId = $this->getRequest()->getParam('id');
                $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
                $ffl->setFflStatus(BinaryAnvil_Ffl_Helper_Data::ARCHIVE)->save();

                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('ffl_id', array('eq', $fflId));
                if (count($collection) > 0) {
                    foreach ($collection as $customerFfl) {
                        $customerFfl->delete();
                    }
                }
                $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('FFL was successfully deleted'));
                $this->_redirect('*/*/');

            } catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $this->_getRedirectUrl();
    }

    public function massDeleteAction()
    {
        $fflIds = $this->getRequest()->getParam('id');
        $session = Mage::getSingleton('adminhtml/session');
        if (!is_array($fflIds)) {
            $session->addError($this->__('Please select product(s).'));
        } else {
            if (!empty($fflIds)) {
                try {
                    foreach ($fflIds as $fflId) {
                        $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
                        $ffl->setFflStatus(4)->save();

                        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                            ->addFieldToFilter('ffl_id', array('eq', $fflId));
                        Mage::log(count($collection));
                        if (count($collection) > 0) {
                            foreach ($collection as $customerFfl) {
                                $customerFfl->delete();
                            }
                        }
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($fflIds))
                    );
                } catch (Exception $e) {
                    $session->addError($e->getMessage());
                }
            }
        }
        $this->_getRedirectUrl();
    }

    public function massStatusAction()
    {
        $fflIds = $this->getRequest()->getParam('id');
        $status     = (int)$this->getRequest()->getParam('status');
        $session = Mage::getSingleton('adminhtml/session');
        if (!is_array($fflIds)) {
            $session->addError($this->__('Please select product(s).'));
        } else {
            if (!empty($fflIds)) {
                try {
                    foreach ($fflIds as $fflId) {
                        if ($fflId) {
                            $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
                            $ffl->setFflStatus($status);
                            $ffl->save();
                            Mage::helper('binaryanvil_ffl')->saveFflStatusOnOrder($ffl->getId(), $status);
                        }
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been updated.', count($fflIds))
                    );
                } catch (Mage_Core_Model_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Mage_Core_Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                } catch (Exception $e) {
                    $this->_getSession()
                        ->addException($e, $this->__('An error occurred while updating the location(s) status.'));
                }
            }
        }
        $this->_getRedirectUrl();
    }

    /**
     * Grid Action
     * Display list of products related to current store location
     *
     * @return void
     */
    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('binaryanvil_ffl/adminhtml_ffl_grid', 'adminhtml_ffl_grid')
                ->toHtml()
        );
    }

    public function customerGridAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('customer.grid')
            ->setCustomers($this->getRequest()->getPost('customers', null));
        $this->renderLayout();
    }


    public function customerAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('customer.grid')
            ->setCustomers($this->getRequest()->getPost('customers', null));
        $this->renderLayout();
    }



    public function validateFflAction()
    {
        $message = '';

        $customerId = $this->getRequest()->getParam('customerId');
        $fflNumber = $this->getRequest()->getParam('fflNumber');
        $fflId = $this->getRequest()->getParam('fflId');
        if ($fflId < 0) {
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
            $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber))
                ->addCustomerIdFilter($customerId);
            $ffl = $collection->getFirstItem();
            if ($ffl && $ffl->getId()) {
                $errorMessage = $this->__( 'FFL is already associated to your account.' );
                $message = $errorMessage;

            } else {
                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
                $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber));
                if ($ffl && $ffl->getId()) {
                    $message = $this->__('FFL number already exists.');
                }
            }

        } else {
            $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
            $origFflNumber = $ffl->getFflNumber();
            if ($fflNumber != $origFflNumber) {
                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
                $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber))
                    ->addCustomerIdFilter($customerId);
                $ffl = $collection->getFirstItem();
                if ($ffl && $ffl->getId()) {
                    $errorMessage = $this->__( 'FFL is already associated to your account.' );
                    $message = $errorMessage;

                } /*else {
                    $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
                    $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber));
                    if ($ffl && $ffl->getId()) {
                        $message = $this->__('FFL number already exists.');
                    }
                }*/
            }

        }//if ($fflId < 0) {

        $this->getResponse()->setBody( $message );
        return;

    }
}