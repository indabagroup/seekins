<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/29/14
 * Time: 9:56 AM
 */

class BinaryAnvil_Ffl_SearchController extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    protected function _getSession()
    {
        return Mage::getSingleton( 'customer/session' );
    }

    public function indexAction()
    {
        $this->loadLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle("FFL Search");
        }
        $this->_initLayoutMessages('customer/session');
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('ffl/manage/index');
        }
        $this->renderLayout();
    }

    public function resultAction()
    {
        $this->loadLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle("FFL Search");
        }
        $this->_initLayoutMessages('customer/session');
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('ffl/manage/index');
        }
        $this->renderLayout();
    }

    public function assignedAction()
    {
        $session = $this->_getSession();
        if( $this->getRequest()->getParam('id') > 0 ){
            try{
                $fflId = $this->getRequest()->getParam('id');
                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('magento_customer_id' , array('eq',Mage::getSingleton('customer/session')->getCustomer()->getId()))
                    ->addFieldToFilter('ffl_id', array('eq', $fflId));
                $ffl = $collection->getFirstItem();
                if ($ffl->getData('relation_id')) {
                    $session->addError(Mage::helper('binaryanvil_ffl')->__('FFL was already assigned to your account'));
                    $this->_redirect('*/*/index');
                    return;
                }
                $date = Mage::getModel( 'core/date' )->gmtDate();
                $customerDetails = array(
                    'magento_customer_id'       => Mage::getSingleton('customer/session')->getCustomer()->getId(),
                    'magento_customer_email'    => Mage::getSingleton('customer/session')->getCustomer()->getEmail(),
                    'magento_customer_website'  => Mage::app()->getWebsite()->getId(),
                    'created_time'              => $date,
                    'is_default'                => isset($post['is_default'])?$post['is_default']:0,
                );

                $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
                $customerFFlModel->setData($customerDetails);
                $customerFFlModel->setFflId($fflId);
                $customerFFlModel->save();
                $session->addSuccess(Mage::helper('binaryanvil_ffl')->__('FFL was successfully assigned to your account'));


            } catch(Exception $e) {
                $session->addError($e->getMessage());
                //$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $queryText = $this->getRequest()->getParam('q');
        $searchBy = $this->getRequest()->getParam('by');
        if ($searchBy && $queryText) {
            $parameters = array(
                'by' => $searchBy,
                'q' => $queryText
            );
            $this->_redirect('ffl/search/result', $parameters);
        } else {
            $this->_redirect('*/*/');
        }

    }

    public function unassignedAction()
    {

        $session = $this->_getSession();
        if( $this->getRequest()->getParam('id') > 0 ){
            try{
                $fflId = $this->getRequest()->getParam('id');

                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('magento_customer_id' , array('eq',Mage::getSingleton('customer/session')->getCustomer()->getId()))
                    ->addFieldToFilter('ffl_id', array('eq', $fflId));
                foreach ($collection as $customerFfl) {
                    $customerFfl->delete();
                }
                $session->addSuccess(Mage::helper('binaryanvil_ffl')->__('FFL was successfully unassigned to your account'));
                //$this->_redirect('*/*/');

            } catch(Exception $e) {
                $session->addError($e->getMessage());
                //$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $queryText = $this->getRequest()->getParam('q');
        $searchBy = $this->getRequest()->getParam('by');
        if ($searchBy && $queryText) {
            $parameters = array(
                'by' => $searchBy,
                'q' => $queryText
            );
            $this->_redirect('ffl/search/result', $parameters);
        } else {
            $this->_redirect('*/*/');
        }
    }
}