<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/3/14
 * Time: 1:40 PM
 */

class BinaryAnvil_Ffl_ManageController extends Mage_Core_Controller_Front_Action
{
    protected $_filename;

    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    public function indexAction()
    {
        $id = $this->_getSession()->getCustomer()->getId();
        $ffl = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')->addCustomerIdFilter($id)->getFirstItem();

        //if ($ffl && $ffl->getId()) {
            $this->loadLayout();
            if ($headBlock = $this->getLayout()->getBlock('head')) {
                $headBlock->setTitle("FFL Book");
            }
            $this->_initLayoutMessages('customer/session');
            $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
            if ($navigationBlock) {
                $navigationBlock->setActive('ffl/manage/index');
            }
            $this->renderLayout();
        //} else {
        //    $this->_forward('form');
            //$this->getResponse()->setRedirect(Mage::getUrl('*/*/new'));
        //}

    }

    /**
     */
    protected function _isFFlExist()
    {
        $id = $this->_getSession()->getCustomer()->getId();
        $fflNumber = $this->getRequest()->getPost('ffl_number');
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
        $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber))
        ->addCustomerIdFilter($id);
        $collection->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
        $ffl = $collection->getFirstItem();
        if ($ffl && $ffl->getId()) {
            return true;

        } else {
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
            $collection->addFieldToFilter('ffl_number',array('eq'=>$fflNumber));
            $collection->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            $ffl = $collection->getFirstItem();
            if ($ffl && $ffl->getId()) {
                return $ffl;
            }
        }

        return false;
    }

    public function newAction()
    {
        Mage::getSingleton('customer/session')->setFflStatus();
        $this->_forward('form');
    }

    public function editAction()
    {
        Mage::getSingleton('customer/session')->setFflStatus();
        $this->_forward('form');
    }

    /**
     * FFL form
     */
    public function formAction()
    {
        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('ffl/manage/index');
        }
        $this->renderLayout();
    }

    protected function _getSession()
    {
        return Mage::getSingleton( 'customer/session' );
    }

    public function validate()
    {
        $errors = array();
        $post = $this->getRequest()->getPost();

        if (!Zend_Validate::is( trim($post['ffl_holder_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder name cannot be empty.');
        }

        if (!Zend_Validate::is( trim($post['ffl_holder_business_name']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Business Name cannot be empty.');
        }

        if (!Zend_Validate::is($post['ffl_holder_address_line'], 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Address Line cannot empty');
        }

        if (!Zend_Validate::is( trim($post['ffl_holder_phone_number']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Holder Phone Number cannot be empty.');
        }

        if (!Zend_Validate::is($post['ffl_number'], 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Number cannot be empty.');
        }

        /*if (!Zend_Validate::is($post['ffl_class'] , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Class cannot be empty.');
        }*/

        if (!Zend_Validate::is( trim($post['ffl_expiration_date']) , 'NotEmpty')) {
            $errors[] = Mage::helper('binaryanvil_ffl')->__('The FFL Expiration Date cannot be empty.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    protected function _saveImagePdf()
    {
        $errors = array();
        if (isset($_FILES['ffl_image']['name']) && $_FILES['ffl_image']['name'] != '') {
            try {
                /*$fileName = $_FILES['ffl_image']['name'];
                $fileExt = strtolower(substr(strrchr($fileName, "."), 1));
                $fileNameWithoutExt = rtrim($fileName, $fileExt);
                $fileName = preg_replace('/\s+/', '', $fileNameWithoutExt). '.' . $fileExt;*/

                $uploader = new Varien_File_Uploader('ffl_image');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'customer_ffl' . DS;
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $path_parts = pathinfo($_FILES['filename']['name']);
                $filename = $path_parts['filename']. '_' . date('Ymd_His'). '.' .$path_parts['extension'];
                $uploader->save($path, $filename);
                $this->_filename = $uploader->getUploadedFileName();
            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->__( 'An error occurred while saving ffl information.' );
                $errors[] = $message;
            }
        } else {

            if ($this->_isFFlExist() == false) {
                $message = $this->__( 'The FFL image cannot be empty.' );
                $errors[] = $message;
                return;
            }

        }//if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

        return $errors;
    }
    /**
     */

    public function saveAction()
    {
        $post = $this->getRequest()->getPost();

        $session = $this->_getSession();
        $fileName = '';
        $id = $this->getRequest()->getParam('id');
        $paramId = $this->getRequest()->getParam('param_id');
        $status = Mage::getSingleton('customer/session')->getFflStatus();
        $errors = array();
        if ($post) {
            $ffl = $this->_isFFlExist();

            if ($ffl === true) {
                $post['is_associated'] = 1;
                Mage::getSingleton('core/session')->setFflData($post);
                $errorMessage = $this->__( 'FFL is already associated to your account.' );
                $this->_getSession()->addError($errorMessage);
                return $this->_redirect('*/*/edit');
            }

            if ($ffl && $ffl->getId()) {
                $message = $this->__( 'FFL already exist.' );
                $errors[] = $message;
            }

            $formErrors = $this->validate();
            if ($formErrors !== true) {
                $errors = array_merge($errors, $formErrors);
            }

            /*$imagePdfErrors = $this->_saveImagePdf();
            if (count($imagePdfErrors) !== 0) {
                $errors = array_merge($errors, $formErrors);
            }*/

            if (count($errors) === 0) {
                $state = isset($post['ffl_holder_state'])?$post['ffl_holder_state']:'';
                if (isset($post['ffl_holder_state_id'])) {
                    $state = Mage::getModel('directory/region')->load($post['ffl_holder_state_id'])->getName();
                }
                try {
                    $date = Mage::getModel( 'core/date' )->gmtDate();
                    $data = array(
                        'ffl_holder_name'           => $post['ffl_holder_name'],
                        'ffl_holder_business_name'  => $post['ffl_holder_business_name'],
                        'ffl_holder_address_line'   => $post['ffl_holder_address_line'],
                        'ffl_holder_address_line2'  => $post['ffl_holder_address_line2'],
                        'ffl_holder_phone_number'   => $post['ffl_holder_phone_number'],
                        'ffl_number'                => $post['ffl_number'],
                        'ffl_holder_state_id'       => isset($post['ffl_holder_state_id'])?$post['ffl_holder_state_id']:'',
                        'ffl_holder_state'          => $state,
                        'ffl_holder_zip_code'       => $post['ffl_holder_zip_code'],
                    //    'is_default'                => isset($post['is_default'])?$post['is_default']:0,
                        'ffl_expiration_date'       => $post['ffl_expiration_date'],
                        //'ffl_class'                 => implode(",",$post['ffl_class']),
                        'ffl_image'                 => $post['ffl_image'],
                        'ffl_status'                => BinaryAnvil_Ffl_Helper_Data::PENDING,
                        'created_time'              => $date
                    );

                    $customerDetails = array(
                        'magento_customer_id'       => Mage::getSingleton('customer/session')->getCustomer()->getId(),
                        'magento_customer_email'    => Mage::getSingleton('customer/session')->getCustomer()->getEmail(),
                        'magento_customer_website'  => Mage::app()->getWebsite()->getId(),
                        'created_time'              => $date,
                        'is_default'                => isset($post['is_default'])?$post['is_default']:0,
                    );

                    if (isset($post['is_default']) && $post['is_default'] == 1) {
                        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                            ->addFieldToFilter('magento_customer_id' , array('eq',Mage::getSingleton('customer/session')->getCustomer()->getId()));

                        /* remove all rows */
                        foreach ($collection as $defaultFfl) {
                            $defaultFfl->setIsDefault(0)->save();
                        }
                    }

                    $model = Mage::getModel('binaryanvil_ffl/ffl');
                    $model->setData($data);
                    $model->setUpdatedTime($date);
                    $model->save();
                        //
                    $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
                    $customerFFlModel->setData($customerDetails);
                    $customerFFlModel->setFflId($model->getId());
                    $customerFFlModel->save();
                    $message = $this->__( 'FFL was successfully save. FFL is pending for approval.' );
                    // end  saving data save()
                    $session->addSuccess( $message );
                    if($id) {
                        $this->_redirect( '*/*/edit',array('id'=>$id));
                    } else {
                        $this->_redirect( 'ffl/manage/index');
                    }
                    return;
                } catch (Exception $e) {
                    Mage::logException($e);
                    $message = $this->__( 'An error occurred while saving ffl information.' );
                    $session->addError( $message );
                    Mage::getSingleton('core/session')->setFflData($post);
                }
            } else {
                Mage::getSingleton('core/session')->setFflData($post);
                foreach ($errors as $errorMessage) {
                    $this->_getSession()->addError($errorMessage);
                }
            } // if (count($errors) === 0) {

        }
        if ($status == BinaryAnvil_Ffl_Helper_Data::ARCHIVE) {
            return $this->_redirectError(Mage::getUrl('*/*/replace', array('id' => $paramId)));
        } else {
            return $this->_redirectError(Mage::getUrl('*/*/edit', array('id' => $id)));
        }
    }

    public function disableAction()
    {
        $id = $this->getRequest()->getParam('id');
        $session = $this->_getSession();
        if ($id && $this->_isFFlExist()) {
            $status = BinaryAnvil_Ffl_Helper_Data::DISABLED;
            $date = Mage::getModel( 'core/date' )->gmtDate();
            $model = Mage::getModel('binaryanvil_ffl/ffl')->load($id);
            $model->setData('ffl_status',$status);
            try {
                $model->setUpdatedTime($date);
                $model->save();
                $message = $this->__( 'FFL was successfully disabled.' );
                $session->addSuccess( $message );
                $this->_redirect( 'ffl/manage/index');
            } catch (Exception $e) {
                Mage::logException($e);
                $message = $this->__( 'An error occurred while saving ffl information.' );
                $session->addError( $message );
                $this->_redirect( 'ffl/manage/index');
            }

        }
        return $this->_redirectError(Mage::getUrl('ffl/manage/index'));
    }

    public function replaceAction()
    {
        Mage::getSingleton('customer/session')->setFflStatus(BinaryAnvil_Ffl_Helper_Data::ARCHIVE);
        $this->_forward('form');
    }

//    public function deleteAction()
//    {
//
//        $session = $this->_getSession();
//        if( $this->getRequest()->getParam('id') > 0 ){
//            try{
//                $fflId = $this->getRequest()->getParam('id');
//                $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
//                $ffl->setFflStatus(4)->save();
//
//                $customerFfl = Mage::getModel('binaryanvil_ffl/ffl_customer');
//                $customerFfl->setFflId($fflId)->delete();
//
//                $session->addSuccess(Mage::helper('binaryanvil_ffl')->__('FFL was successfully deleted'));
//                $this->_redirect('*/*/');
//
//            } catch(Exception $e) {
//                $session->addError($e->getMessage());
//                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
//            }
//
//        }
//        $this->_redirect('*/*/');
//    }
    public function unassignedAction()
    {

        $session = $this->_getSession();
        if( $this->getRequest()->getParam('id') > 0 ){
            try{
                $fflId = $this->getRequest()->getParam('id');

                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('magento_customer_id' , array('eq',Mage::getSingleton('customer/session')->getCustomer()->getId()))
                    ->addFieldToFilter('ffl_id', array('eq', $fflId));
                if (count($collection > 0)) {
                    foreach ($collection as $customerFfl) {
                        $customerFfl->delete();
                    }
                }
                $session->addSuccess(Mage::helper('binaryanvil_ffl')->__('FFL was successfully unassigned to your account'));
                $this->_redirect('*/*/');

            } catch(Exception $e) {
                $session->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $this->_redirect('*/*/');
    }

    public function assignedAction()
    {
        $session = $this->_getSession();
        if( $this->getRequest()->getParam('id') > 0 ){
            try{
                $fflId = $this->getRequest()->getParam('id');
                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('magento_customer_id' , array('eq',Mage::getSingleton('customer/session')->getCustomer()->getId()))
                    ->addFieldToFilter('ffl_id', array('eq', $fflId));
                if (count($collection > 0)) {
                    $ffl = $collection->getFirstItem();
                    if ($ffl->getData('relation_id')) {
                        $session->addError(Mage::helper('binaryanvil_ffl')->__('FFL was already assigned to your account'));
                        $this->_redirect('*/*/index');
                        return;
                    }
                }

                $date = Mage::getModel( 'core/date' )->gmtDate();
                $customerDetails = array(
                    'magento_customer_id'       => Mage::getSingleton('customer/session')->getCustomer()->getId(),
                    'magento_customer_email'    => Mage::getSingleton('customer/session')->getCustomer()->getEmail(),
                    'magento_customer_website'  => Mage::app()->getWebsite()->getId(),
                    'created_time'              => $date,
                    'is_default'                => isset($post['is_default'])?$post['is_default']:0,
                );

                $customerFFlModel = Mage::getModel('binaryanvil_ffl/ffl_customer');
                $customerFFlModel->setData($customerDetails);
                $customerFFlModel->setFflId($fflId);
                $customerFFlModel->save();
                $session->addSuccess(Mage::helper('binaryanvil_ffl')->__('FFL was successfully assigned to your account'));
                $this->_redirect('*/*/');

            } catch(Exception $e) {
                $session->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $this->_redirect('*/*/');
    }

    public function defaultAction()
    {

        $session = $this->_getSession();
        if( $this->getRequest()->getParam('id') > 0 ){
            try{
                $fflId = $this->getRequest()->getParam('id');
                $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
                    ->addFieldToFilter('magento_customer_id' , array('eq',Mage::getSingleton('customer/session')->getCustomer()->getId()));

                /* remove all rows */
                foreach ($collection as $defaultFfl) {
                    if ($fflId == $defaultFfl->getFflId()) {
                        $defaultFfl->setIsDefault(1)->save();
                    } else {
                        $defaultFfl->setIsDefault(0)->save();
                    }
                }

                $session->addSuccess(Mage::helper('binaryanvil_ffl')->__('FFL was successfully set as default'));
                $this->_redirect('*/*/');

            } catch(Exception $e) {
                $session->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }

        }
        $this->_redirect('*/*/');
    }
}