<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/14/14
 * Time: 4:44 PM
 */
require_once 'Mage/Checkout/controllers/OnepageController.php';

class BinaryAnvil_Ffl_OnepageController extends Mage_Checkout_OnepageController
{
    /**
     * Ffl  save action
     */
    public function saveFflAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $result = array();
            $data = $this->getRequest()->getPost();
            $result = $this->getOnepage()->saveFfl($data);

            if (!isset($result['error'])) {
                $result['goto_section'] = 'billing';

                $this->getOnepage()->getCheckout()
                    ->setStepData('ffl', 'allow', true)
                    ->setStepData('ffl', 'complete', true)
                    ->setStepData('billing', 'allow', true);

            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function uploadAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            $type = 'Filedata';

            Mage::log($_FILES[$type]['name']);

            if(isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
                try {
                    $uploader = new Varien_File_Uploader($type);
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $path = Mage::getBaseDir('media') . DS . 'customer_ffl' . DS;
                    /*if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }*/
                    $path_parts = pathinfo($_FILES[$type]['name']);
                    $_filename = $path_parts['filename']. '_' . date('Ymd_His'). '.' .$path_parts['extension'];

                    $uploader->save($path, $_filename );
                    $filename = $uploader->getUploadedFileName();

                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
            echo $filename;
        }
    }

}