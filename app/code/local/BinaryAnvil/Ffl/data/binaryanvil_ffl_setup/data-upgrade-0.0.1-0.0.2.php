<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/2/14
 * Time: 6:32 PM
 */
$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->updateAttribute('catalog_product', 'sp_ffl_needed', 'is_configurable', 0);

$setup->updateAttribute('catalog_product', 'sp_ffl_needed', 'required', true);

$setup->updateAttribute('catalog_product', 'sp_ffl_needed', 'apply_to', join(',', array('simple','grouped','configurable','virtual','bundle','downloadable')));

$installer->endSetup();