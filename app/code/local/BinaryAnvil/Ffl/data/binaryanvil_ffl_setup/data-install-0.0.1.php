<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/2/14
 * Time: 5:45 PM
 */

$installer = $this;

$installer->startSetup();

$types = Mage::getModel('catalog/product_type')->getOptionArray();
$applyTo = join(',', array_keys($types));

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'sp_ffl_needed',
    array(
        'group'             => 'General',
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'FFL Needed',
        'input'             => 'select',
        'source'            => 'eav/entity_attribute_source_boolean',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '0',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'is_configurable'   => false,
        'apply_to'          => $applyTo
    )
);

$setup->updateAttribute('catalog_product', 'sp_ffl_needed', 'backend_model', '');

$installer->endSetup();