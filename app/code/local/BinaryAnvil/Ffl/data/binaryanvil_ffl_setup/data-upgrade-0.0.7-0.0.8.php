<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/13/14
 * Time: 2:39 PM
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/*$installer->getConnection()->addColumn($this->getTable('sales_flat_quote_item'),
    'backorder_note',
    'varchar(50) AFTER order_type');*/
$installer->getConnection()->addColumn($this->getTable('sales_flat_order'),
    'ffl_id',
    'varchar(50)');

$installer->getConnection()->addColumn($this->getTable('sales_flat_quote'),
    'ffl_id',
    'varchar(50)');

/*$installer->getConnection()->addColumn($this->getTable('sales_flat_order_item'),
    'ffl_id',
    'varchar(50)');

$installer->getConnection()->addColumn($this->getTable('sales_flat_quote_item'),
    'ffl_id',
    'varchar(50)');*/


$installer->endSetup();