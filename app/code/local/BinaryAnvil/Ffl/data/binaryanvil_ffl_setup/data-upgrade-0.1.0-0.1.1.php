<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 8/8/14
 * Time: 12:16 PM
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/*$installer->getConnection()->addColumn($this->getTable('sales_flat_quote_item'),
    'backorder_note',
    'varchar(50) AFTER order_type');*/
$installer->getConnection()->addColumn($this->getTable('sales_flat_order'),
    'ffl_status',
    'varchar(50)');


$installer->endSetup();