<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/2/14
 * Time: 4:05 PM
 */

class BinaryAnvil_Ffl_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PENDING   = 0;
    const VALIDATE  = 1;
    const NONE      = 2;
    const EXPIRED   = 3;
    const ARCHIVE   = 4;

    const XML_PATH_EMAIL_SENDER                         = 'binaryanvil_ffl/settings/sender_email_identity';
    const XML_PATH_EXPIRY_NOTIFICATION_EMAIL_TEMPLATE   = 'binaryanvil_ffl/settings/email_template';
    const XML_PATH_BCC_EMAILS                           = 'binaryanvil_ffl/settings/bcc_email';


    public function getStatus($status)
    {
        $statuses = Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_status')->toArray();
        //$statuses = $this->getFflStatuses();

        if (isset($statuses[$status])) {
            return $statuses[$status];
        } else {
            return 'pending';
        }
    }

    public function sendBccEmailNotification($info)
    {
        //$postObject = new Varien_Object ();
        //$postObject->setData( $info );
        $bccMail = Mage::getStoreConfig(self::XML_PATH_BCC_EMAILS);
        if (!empty($bccMail)) {
            $sender = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
            $template = Mage::getStoreConfig(self::XML_PATH_EXPIRY_NOTIFICATION_EMAIL_TEMPLATE);
            $bccMail = Mage::getStoreConfig(self::XML_PATH_BCC_EMAILS);
            $bccMail = explode(",",$bccMail);;
            if (is_array($bccMail)) {
                foreach ($bccMail as $email) {
                    $to = $email;
                    $this->_sendEmail($to,$sender,$template,$info);
                }
            } else {
                $to = $bccMail;
                $this->_sendEmail($to,$sender,$template,$info);
            }
        }
        return $this;
    }

    public function sendEmailExpiryNotification($info)
    {
        /*$postObject = new Varien_Object ();
        $postObject->setData( $info );*/
        $sender = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
        $template = Mage::getStoreConfig(self::XML_PATH_EXPIRY_NOTIFICATION_EMAIL_TEMPLATE);
        $to = $info->getMagentoCustomerEmail();
        $this->_sendEmail($to,$sender,$template,$info);
    }

    public function _sendEmail($to,$sender,$template,$params)
    {
        if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'localhost') {
            $to = "test@localhost.localdomain.edu";
        }
        $translate = Mage::getSingleton( 'core/translate' );
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline( false );
        $mailTemplate = Mage::getModel( 'core/email_template' );
        $mailTemplate->setDesignConfig( array ( 'area' => 'frontend' ) )
            ->sendTransactional(
                $template, //template
                $sender,// sender
                $to,//recipient
                null,
                array ( 'customer' => $params )
            );
        $translate->setTranslateInline(true);
    }

    public function getFflHtml($ffl)
    {
        //$date = Mage::helper('core')->formatDate($ffl->getFflExpirationDate(), 'long', false);
        $date = new DateTime($ffl->getFflExpirationDate());

        $html =  $ffl->getFflNumber().", ". $date->format('F d, Y'). "<br />";
        $html .= $ffl->getFflHolderBusinessName(). ", " .$ffl->getFflHolderName(). "<br />";
        $html .= $ffl->getFflHolderAddressLine(). "<br />";

        if ($ffl->getFflHolderAddressline2()):
            $html .= $ffl->getFflHolderAddressLine2(). "<br />";
        endif;

        $state = $ffl->getFflHolderState();
        $stateId = $ffl->getFflHolderStateId();
        if ($stateId):
            $state = Mage::getModel('directory/region')->load($stateId)->getName();
        endif;

        $html .= $state. ", " .$ffl->getFflZipCode(). "<br />";
        $html .= "T: " .$ffl->getFflHolderPhoneNumber(). "<br />";

        return $html;
    }

    public function getFflOneLineFormat($ffl)
    {
        //$date = Mage::helper('core')->formatDate($ffl->getFflExpirationDate(), 'short', false);
        $date = new DateTime($ffl->getFflExpirationDate());

        $html =  $ffl->getFflNumber().", ". $date->format('F d, Y'). ",";
        $html .= $ffl->getFflHolderBusinessName(). ", " .$ffl->getFflHolderName(). ",";
        $html .= $ffl->getFflHolderAddressLine(). ",";

        if ($ffl->getFflHolderAddressline2()):
            $html .= $ffl->getFflHolderAddressLine2(). ",";
        endif;

        $state = $ffl->getFflHolderState();
        $stateId = $ffl->getFflHolderStateId();
        if ($stateId):
            $state = Mage::getModel('directory/region')->load($stateId)->getName();
        endif;

        //$html =  $ffl->getFflNumber();
        $html =  $ffl->getFflHolderBusinessName();
        if ($ffl->getFflHolderAddressLine())    $html.= " , ". $ffl->getFflHolderAddressLine();
        $addressLine2 = $ffl->getData('ffl_holder_address_line2');
        if (!empty($addressLine2)):
            $html.= " , ".$addressLine2;
        endif;
        if ($state)                             $html.= " , ". $state;
        if ($ffl->getFflHolderZipCode())        $html.= " , ". $ffl->getFflHolderZipCode();
        $html .= " , ".  $ffl->getFflNumber();
        if ($ffl->getFflExpirationDate())       $html.= " , ". $date->format('d F Y');
        //if ($ffl->getFflHolderBusinessName())   $html.= ", ". $ffl->getFflHolderBusinessName();
        if ($ffl->getFflHolderName())           $html.= " , ". $ffl->getFflHolderName();
        //if ($ffl->getFflHolderAddressLine())    $html.= ", ". $ffl->getFflHolderAddressLine();

        //if ($ffl->getFflHolderPhoneNumber())    $html.= ", ". $ffl->getFflHolderPhoneNumber();

        return $html;
    }

    public function getPath($filename)
    {
        return Mage::getBaseUrl('media')."customer_ffl/".$filename;
    }

    public function getJsFormat()
    {
        return '#{ffl_number}, #{ffl_expiration_date}<br/> #{ffl_holder_business_name}, #{ffl_holder_name}<br/>#{ffl_holder_address_line}<br/>#{ffl_holder_address_line2}<br/>#{region} ,#{ffl_holder_zip_code}<br/>T: #{ffl_holder_phone_number}';
    }

    public function saveFflStatusOnOrder($fflId, $status)
    {
        if ($fflId) {
            $collection = Mage::getResourceModel('sales/order_collection')->addFieldToFilter('ffl_id', array('eq' => $fflId));
            if (count($collection) > 0) {
                foreach($collection as $order) {
                    $order->setData('ffl_status', $status)->save();
                }
            }
        }
    }
}