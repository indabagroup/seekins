<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/16/14
 * Time: 7:51 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Customer_Edit_Tab_Ffl_Ffl extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('binaryanvil/ffl/customer/tab/ffl/form.phtml');
    }

    public function getRegionsUrl()
    {
        return $this->getUrl('*/json/countryRegion');
    }

    protected function _prepareLayout()
    {
        $this->setChild('delete_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'  => Mage::helper('customer')->__('Delete FFL'),
                    'name'   => 'delete_ffl',
                    'element_name' => 'delete_ffl',
                    'disabled' => $this->isReadonly(),
                    'class'  => 'delete' . ($this->isReadonly() ? ' disabled' : '')
                ))
        );
        $this->setChild('add_ffl_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'  => Mage::helper('customer')->__('Add New FFL'),
                    'id'     => 'add_ffl_button',
                    'name'   => 'add_ffl_button',
                    'element_name' => 'add_ffl_button',
                    'disabled' => $this->isReadonly(),
                    'class'  => 'add'  . ($this->isReadonly() ? ' disabled' : ''),
                    'onclick'=> 'customerffl.addNewFfl()'
                ))
        );
        $this->setChild('cancel_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'  => Mage::helper('binaryanvil_ffl')->__('Cancel'),
                    'id'     => 'cancel_add_ffl'.$this->getTemplatePrefix(),
                    'name'   => 'cancel_ffl',
                    'element_name' => 'cancel_ffl',
                    'class'  => 'cancel delete-ffl'  . ($this->isReadonly() ? ' disabled' : ''),
                    'disabled' => $this->isReadonly(),
                    'onclick'=> 'customerffl.cancelAdd(this)',
                ))
        );

        $this->initForm();
        return parent::_prepareLayout();
    }

    public function getCancelButtonHtml()
    {
        return $this->getChildHtml('cancel_button');
    }

    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_ffl_button');
    }

    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    /**
     * Check block is readonly.
     *
     * @return boolean
     */
    public function isReadonly()
    {
        $customer = Mage::registry('current_customer');
        return $customer->isReadonly();
    }

    public function getTemplatePrefix()
    {
        return '_template_';
    }

    /**
     * Return JSON object with countries associated to possible websites
     *
     * @return string
     */
    public function getDefaultCountriesJson() {
        $websites = Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(false, true);
        $result = array();
        foreach ($websites as $website) {
            $result[$website['value']] = Mage::app()->getWebsite($website['value'])->getConfig(
                Mage_Core_Helper_Data::XML_PATH_DEFAULT_COUNTRY
            );
        }

        return Mage::helper('core')->jsonEncode($result);
    }

    public function initForm()
    {
        /* @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::registry('current_customer');

        $form = new Varien_Data_Form();

        /*start - form*/
        $fieldset = $form->addFieldset('ffl_fieldset', array(
                'legend'    => Mage::helper('binaryanvil_ffl')->__("Edit Customer's FFL"))
        );

        $fieldset->addField('ffl_holder_name', 'text',
            array(
                'label' => 'FFL Holder Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_name',
            ));

        $fieldset->addField('ffl_holder_business_name', 'text',
            array(
                'label' => 'FFL Holder Business Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_business_name',
            ));

        $fieldset->addField('ffl_holder_address_line', 'text',
            array(
                'label' => 'FFL Holder Address Line',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_address_line',
            ));

        $fieldset->addField('ffl_holder_address_line2', 'text',
            array(
                'label' => 'FFL Holder Address Line 2',
                //'class' => 'required-entry',
                'required' => false,
                'name' => 'ffl_holder_address_line2',
            ));

        $countries = Mage::getModel('adminhtml/system_config_source_country')
            ->toOptionArray();

        $fieldset->addField('country_id', 'select', array(
            'label' => Mage::helper('binaryanvil_ffl')->__('Country'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'country_id',
            'values' => $countries,
        ));

        /*$fieldset->addField('ffl_holder_state', 'note', array(
            'label' => Mage::helper('binaryanvil_ffl')->__('State/Province'),
            'name' => 'stateEl',
            'text' => $this->getLayout()->createBlock('binaryanvil_ffl/adminhtml_region')->setTemplate('binaryanvil/ffl/region.phtml')->toHtml(),
        ));*/

        $fieldset->addField('region', 'text',
            array(
                'label' => 'State/Province',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'region',
            ));

        $fieldset->addField('region_id', 'text',
            array(
                'label' => 'FFL Holder State Id',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'region_id',
            ));

        $fieldset->addField('ffl_holder_zip_code', 'text',
            array(
                'label' => 'FFL Holder Zip Code',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_zip_code',
            ));

        $fieldset->addField('ffl_number', 'text',
            array(
                'label' => 'FFL Number',
                'class' => 'required-entry validate-ffl-associate-number validate-ffl-number validate-ffl-number-format',
                'required' => true,
                'name' => 'ffl_number',
            ));

        $fieldset->addField('ffl_holder_phone_number', 'text',
            array(
                'label' => 'FFL Holder Phone Number',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_phone_number',
            ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('ffl_expiration_date', 'date', array(
            'name'   => 'ffl_expiration_date',
            'label'  => Mage::helper('binaryanvil_ffl')->__('FFL Expiration Date'),
            'title'  => Mage::helper('binaryanvil_ffl')->__('FFL Expiration Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
            'format'       => $dateFormatIso,
            'time' => true
        ));

        /*$fieldset->addField('ffl_class', 'multiselect', array(
            'name'      => 'ffl_class[]',
            'label'     => Mage::helper('binaryanvil_ffl')->__('FFL Class'),
            'title'     => Mage::helper('binaryanvil_ffl')->__('FFL Class'),
            'required'  => true,
            'values'    => Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_class')->toOptionArray(),
        ));*/

        $fflImage = "";
        $DownloadURL = '';
        /*Mage::registry('current_ffl_data')->getData('ffl_image');
        if($fflImage) {
            $file = Mage::helper('binaryanvil_ffl')->getPath($fflImage);
            $DownloadURL = '&nbsp;&nbsp;<a href='.$file.' target="_blank">View FFL</a>';
        } else {
            $DownloadURL = '';
        }*/
        $fieldset->addType('ffl_file','BinaryAnvil_Ffl_Block_Adminhtml_File');

        $fieldset->addField('ffl_image', 'ffl_file', array(
            'name'      => 'ffl_image',
            'label'     => Mage::helper('binaryanvil_ffl')->__('FFL Image'),
            'title'     => Mage::helper('binaryanvil_ffl')->__('FFL Image'),
        ));

        /*$fieldset->addField('ffl_status', 'select', array(
            'label'     => 'Status',
            'name'      => 'ffl_status',
            'class'      => '',
            'required'  => true,
            'values'    => Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_statuses')->toOptionArray()
        ));*/

        $regionElement = $form->getElement('region');
        $regionElement->setRequired(true);
        if ($regionElement) {
            $regionElement->setRenderer(Mage::getModel('adminhtml/customer_renderer_region'));
        }

        $regionElement = $form->getElement('region_id');
        if ($regionElement) {
            $regionElement->setNoDisplay(true);
        }

        $country = $form->getElement('country_id');
        if ($country) {
            $country->addClass('countries');
        }
        /*end - form*/
        $fflModel = Mage::getModel('binaryanvil_ffl/ffl');
        $fflModel->setCountryId(Mage::helper('core')->getDefaultCountry($customer->getStore()));

        $fflCollection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($customer->getId())
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)))
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE))
            ->setOrder('is_default','desc');
        $this->assign('customer', $customer);
        $this->assign('fflCollection', $fflCollection);
        $form->setValues($fflModel->getData());
        $this->setForm($form);


        return $this;
    }

    /**
     * Return predefined additional element types
     *
     * @return array
     */
    protected function _getAdditionalElementTypes()
    {
        return array(
            'file'  => Mage::getConfig()->getBlockClassName('adminhtml/customer_form_element_file'),
        );
    }
}