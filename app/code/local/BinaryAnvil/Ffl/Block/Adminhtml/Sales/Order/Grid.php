<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/13/14
 * Time: 5:08 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{

    public function setCollection($collection)
    {
        $collection = Mage::getResourceModel( $this->_getCollectionClass() );
        $collection->getSelect()->joinLeft( array ( 'sfo' => 'sales_flat_order' ),
            'sfo.entity_id=main_table.entity_id', array ( 'sfo.ffl_id', 'sfo.ffl_status') );
        /*$collection->getSelect()->joinLeft( array ( 'sfi' => 'sp_ffL_information' ),
            'sfi.id=sfo.ffl_id', array ( 'sfi.ffl_status') );*/


        parent::setCollection( $collection );
    }

    protected function _filterOrdersByFfl($collection, $column)
    {
        $value = $column->getFilter()->getValue();

        if ($value == '-1') {
            $collection->getSelect()->where('sfo.ffl_id =?',$value);
        } else {
            $collection->getSelect()->where('sfo.ffl_status =?',$value);
        }

        return $this;

    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
            'filter_index'  => 'main_table.increment_id'
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
                'filter_index'  => 'main_table.store_id'
            ));
        }

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
            'filter_index'  => 'main_table.created_at'
        ));

        /*$this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));*/

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
            'filter_index'  => 'main_table.base_grand_total'
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
            'filter_index'  => 'main_table.grand_total'
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            'filter_index' => 'sfo.status',
        ));

        $status = array_filter(Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_status')->toArray());

        $this->addColumn('ffl_id', array(
            'header' => Mage::helper('sales')->__('FFL'),
            'index' => 'ffl_id',
    //        'filter_index' => 'sfi.ffl_status',
            'type'  => 'options',
            'width' => '100px',
            'renderer' => 'BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Ffl',
            'filter_condition_callback' => array($this, '_filterOrdersByFfl'),
            'options'   => $status
        ));


//        $this->addColumn('ffl_id',
//            array(
//                'header'    => Mage::helper('binaryanvil_ffl')->__('FFL ID'),
//                'width'     => '50px',
//                'type'      => 'action',
//                'getter'     => 'getId',
//                'actions'   => array(
//                    array(
//                        'caption' => Mage::helper('binaryanvil_ffl')->__('Status'),
//                        'url'     => array('base'=>'*/sales_order/view'),
//                        'field'   => 'order_id'
//                    )
//                ),
//                'filter'    => false,
//                'sortable'  => false,
//                'index'     => 'ffl_id',
//                'is_system' => true,
//            ));

        $this->addColumnsOrder( 'ffl_id', 'status' );
        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }
}