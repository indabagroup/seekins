<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 1:56 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        /*
         * note :
         *  - _blockGroup - is your module name
         *  - _controller - is actually the path to your block class (NOT YOUR CONTROLLER).
         */
        $this->_blockGroup = 'binaryanvil_ffl';
        $this->_controller = 'adminhtml_ffl';


        if ($this->getRequest()->getParam('popup')) {

            $this->_headerText = Mage::helper('binaryanvil_ffl')->__('Associate FFL');
            $orderId = $this->getRequest()->getParam('order_id');
            $customerId = $this->getRequest()->getParam('customer_id');
            $this->_addButton('add_new_ffl_for_order', array(
                'label'     => Mage::helper('adminhtml')->__('Add New FFL to Associate to Order'),
                'onclick'   => 'setLocation(\'' . $this->getUrl('adminhtml/ffl/new', array('order_id' => $orderId, 'customer_id'=>$customerId, 'popup' => '1', 'new'=>1)) . '\')',
                'class'     => 'save',
            ), -1);

            $this->_addButton('save_associate_ffl', array(
                'label'     => Mage::helper('adminhtml')->__('Associate FFL to Order'),
                'onclick'   => 'saveAssociateFfl(\'' . $this->getUrl('adminhtml/ffl/associate', array('order_id' => $orderId, 'customer_id'=>$customerId, 'popup' => '1')) . '\')',
                'class'     => 'save',
            ), -1);

        } else {
            $this->_headerText = Mage::helper('binaryanvil_ffl')->__('Manage Customer FFLs');
        }
        parent::__construct();
        if ($this->getRequest()->getParam('popup')) {
            $this->_removeButton('add');
        }

    }
}