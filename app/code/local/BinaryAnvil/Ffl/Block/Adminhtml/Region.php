<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/11/14
 * Time: 7:23 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Region extends Mage_Core_Block_Template
{
    public function getFfl()
    {
        $collection = null;
        $id = $this->getRequest()->getParam('id');
        if($id)
        {
            $collection = Mage::getModel('binaryanvil_ffl/ffl')->load($id);
        }
        return $collection;
    }
}