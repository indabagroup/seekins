<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/17/14
 * Time: 3:46 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_File extends Varien_Data_Form_Element_Abstract
{
    /**
     * Initialize Form Element
     *
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->setType('file');
    }

    public function getElementHtml() {
        $html = '';
        if ($this->getValue()) {
            $url = $this->_getUrl($this->getValue());
            $html .= '&nbsp;&nbsp;<a class="view-ffl" href='.$url.' target="_blank">View FFL</a>';
            $this->setClass('input-file');
        } else {
            $this->setClass('required-entry');
        }
        $html.= parent::getElementHtml();
        return $html;
    }

    protected function _getUrl($fflImage)
    {
        $file = Mage::helper('binaryanvil_ffl')->getPath($fflImage);
        return $file;
    }
}