<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 6:28 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Class extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = implode(', ',unserialize($row->getFflClass()));

        return $value;
    }
}