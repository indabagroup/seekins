<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 3:35 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        if ($this->getRequest()->getParam('new')) {
            $orderId = $this->getRequest()->getParam('order_id');
            $customerId = $this->getRequest()->getParam('customer_id');
            $popup = $this->getRequest()->getParam('popup');
            $new = 1;
            $url = $this->getUrl('*/*/associate', array('id' => $this->getRequest()->getParam('id'), 'order_id' => $orderId, 'customer_id'=>$customerId, 'popup' => '1', 'new'=>1));
        } else {
            $url = $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'), 'popup' => $this->getRequest()->getParam('popup')));
        }
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $url,
                'method' => 'post',
                'enctype' => 'multipart/form-data',
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}