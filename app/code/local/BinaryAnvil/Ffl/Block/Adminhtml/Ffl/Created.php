<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/16/14
 * Time: 1:48 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Created extends Mage_Adminhtml_Block_Widget
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('binaryanvil/ffl/widget/created.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'close_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'   => Mage::helper('binaryanvil_ffl')->__('Close Window'),
                    'onclick' => 'addProduct(true)'
                ))
        );
    }

    public function isEdit()
    {
        return (bool) $this->getRequest()->getParam('edit');
    }

    public function getCloseButtonHtml()
    {
        return $this->getChildHtml('close_button');
    }
}