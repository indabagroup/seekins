<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 4:27 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('customer_ffl_form',
            array('legend' => 'FFL Information'));

        $fieldset->addField('ffl_holder_name', 'text',
            array(
                'label' => 'FFL Holder Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_name',
            ));

        $fieldset->addField('ffl_holder_business_name', 'text',
            array(
                'label' => 'FFL Holder Business Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_business_name',
            ));

        $fieldset->addField('ffl_holder_address_line', 'text',
            array(
                'label' => 'FFL Holder Address Line',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_address_line',
            ));

        $fieldset->addField('ffl_holder_address_line2', 'text',
            array(
                'label' => 'FFL Holder Address Line 2',
                'class' => '',
                'required' => false,
                'name' => 'ffl_holder_address_line2',
            ));

        $countries = Mage::getModel('adminhtml/system_config_source_country')
            ->toOptionArray();
        //Mage::log($countries);

        $fieldset->addField('country', 'select', array(
            'label' => Mage::helper('binaryanvil_ffl')->__('Country'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'country',
            'values' => $countries,
        ));
        /*$fieldset->addField('country', 'note', array(
            'label' => Mage::helper('binaryanvil_ffl')->__('Country'),
            'name' => 'country',
            'text' => $this->getLayout()->createBlock('binaryanvil_ffl/adminhtml_region')->setTemplate('binaryanvil/ffl/country.phtml')->toHtml(),
        ));*/

        $fieldset->addField('ffl_holder_state', 'note', array(
            'label' => Mage::helper('binaryanvil_ffl')->__('State/Province'),
            'name' => 'stateEl',
            'text' => $this->getLayout()->createBlock('binaryanvil_ffl/adminhtml_region')->setTemplate('binaryanvil/ffl/region.phtml')->toHtml(),
        ));

        $fieldset->addField('ffl_holder_zip_code', 'text',
            array(
                'label' => 'FFL Holder Zip Code',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_zip_code',
            ));


        $fieldset->addField('ffl_number', 'text',
            array(
                'label' => 'FFL Number',
                'class' => 'required-entry validate-ffl-number-format',
                'required' => true,
                'name' => 'ffl_number',
            ));

        $fieldset->addField('ffl_holder_phone_number', 'text',
            array(
                'label' => 'FFL Holder Phone Number',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'ffl_holder_phone_number',
            ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        //Mage::helper('core')->formatDate($customerFfl->getFflExpirationDate(), 'short', false);
        $fieldset->addField('ffl_expiration_date', 'date', array(
            'name'   => 'ffl_expiration_date',
            'label'  => Mage::helper('binaryanvil_ffl')->__('FFL Expiration Date'),
            'title'  => Mage::helper('binaryanvil_ffl')->__('FFL Expiration Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'format'       => $dateFormatIso,
            //'time' => true
        ));

        /*$fieldset->addField('ffl_class','text' ,
            array(
                'label' => 'FFL Class',
                'name' => 'ffl_class'
            ));*/

        /*$fieldset->addField('ffl_class', 'multiselect', array(
            'name'      => 'ffl_class[]',
            'label'     => Mage::helper('binaryanvil_ffl')->__('FFL Class'),
            'title'     => Mage::helper('binaryanvil_ffl')->__('FFL Class'),
            'required'  => true,
            'values'    => Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_class')->toOptionArray(),
        ));*/

        $fflImage = Mage::registry('current_ffl_data')->getData('ffl_image');
        if($fflImage) {
            $file = Mage::helper('binaryanvil_ffl')->getPath($fflImage);
            $DownloadURL = '&nbsp;&nbsp;<a href='.$file.' target="_blank">View FFL</a>';
        } else {
            $DownloadURL = '';
        }
        $fieldset->addField('ffl_image', 'file', array(
            'name'      => 'ffl_image',
            'class'     => (($fflImage) ? '' : 'required-entry'),
            'required'  => (($fflImage) ? false : true),
            'label'     => Mage::helper('binaryanvil_ffl')->__('FFL Image'),
            'title'     => Mage::helper('binaryanvil_ffl')->__('FFL Image'),
            'after_element_html' => $DownloadURL,
        ));

        $fieldset->addField('ffl_status', 'select', array(
            'label'     => 'Status',
            'name'      => 'ffl_status',
            'class'      => '',
            'required'  => true,
            'values'    => Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_status')->toOptionArray()
        ));

        /*$fieldset->addField('is_default', 'select', array(
            'label'     => 'Is Default',
            'name'      => 'is_default',
            'class'      => '',
            'required'  => true,
            'values'    => array(
                0 => Mage::helper('binaryanvil_ffl')->__('No'),
                1 => Mage::helper('binaryanvil_ffl')->__('Yes'),
            )
        ));*/

        if (Mage::registry('current_ffl_data')) {
            Mage::registry('current_ffl_data')->setData('country', 'US');
            $form->setValues(Mage::registry('current_ffl_data')->getData());

        }
        return parent::_prepareForm();
    }
}