<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 3:30 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {

        $this->_objectId = 'id';
        $this->_blockGroup = 'binaryanvil_ffl';
        $this->_controller = 'adminhtml_ffl';


        if ($this->getRequest()->getParam('popup')) {
            $orderId = $this->getRequest()->getParam('order_id');
            $customerId = $this->getRequest()->getParam('customer_id');
            $this->_addButton('close', array(
                'label'     => Mage::helper('adminhtml')->__('Close Window'),
                'onclick'   => 'window.close()',
                'class'     => 'cancel',
            ), 100);

            /*$this->_addButton('save_ffl', array(

                'label'     => Mage::helper('adminhtml')->__('Save'),
                'onclick'   => 'setLocation(\'' . $this->getUrl('adminhtml/ffl/associate', array('order_id' => $orderId, 'customer_id'=>$customerId, 'popup' => '1', 'new'=> 1)) . '\')',
                'class'     => 'save',
            ), 100);*/

        }

        $this->_updateButton('save', 'label', Mage::helper('binaryanvil_ffl')->__('Save FFL'));
        $this->_updateButton('delete', 'label', Mage::helper('binaryanvil_ffl')->__('Delete FFL'));
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }";

        parent::__construct();

        if ($this->getRequest()->getParam('popup')) {
            $this->_removeButton('back');
            $this->_removeButton('saveandcontinue');
            //$this->_removeButton('save');
            $this->_removeButton('delete');
        }

    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_ffl_data')->getId()) {
            return $this->escapeHtml('Edit FFL');
        } else {
            return Mage::helper('binaryanvil_ffl')->__('Add New FFL');
        }
    }

}