<?php
/**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/8/14
 * Time: 4:52 PM
 */
class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Edit_Tab_Customer extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('customerGrid');
        $this->setUseAjax(true); // Using ajax grid is important
        $this->setDefaultSort('entity_id');
        $this->setDefaultFilter(array('in_products'=>1)); // By default we have added a filter for the rows, that in_products value to be 1
        $this->setSaveParametersInSession(false);  //Dont save paramters in session or else it creates problems
    }

    protected function _prepareCollection()
    {
        $fflId = $this->getRequest()->getParam('id');
        $collection = Mage::getResourceModel('customer/customer_collection')
            ->addNameToSelect()
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('created_at')
            ->addAttributeToSelect('group_id')
            ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
            ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
            ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
            ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
            ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _addColumnFilterToCollection($column)
    {

        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $customerId = $this->_getSelectedCustomers();
            if (empty($customerId)) {
                $customerId = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$customerId));
            }
            elseif(!empty($customerId)) {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$customerId));
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_products', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'customer',
            'values'            => $this->_getSelectedCustomers(),
            'align'             => 'center',
            'index'             => 'entity_id'
        ));

        $this->addColumn('ffl_id', array(
            'header' => Mage::helper('binaryanvil_ffl')->__('Select Owner'),
            'index' => 'ffl_id',
            'type'  => 'radio',
            'width' => '100px',
            'renderer' => 'BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Owner',

        ));

        $this->addColumn('in_products', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'customer',
            'values'            => $this->_getSelectedCustomers(),
            'align'             => 'center',
            'index'             => 'entity_id'
        ));

        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('ID'),
            'width'     => '50px',
            'index'     => 'entity_id',
            'type'  => 'number',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('Name'),
            'index'     => 'name'
        ));
        $this->addColumn('email', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('Email'),
            'width'     => '150',
            'index'     => 'email'
        ));

        $groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('group', array(
            'header'    =>  Mage::helper('binaryanvil_ffl')->__('Group'),
            'width'     =>  '100',
            'index'     =>  'group_id',
            'type'      =>  'options',
            'options'   =>  $groups,
        ));

        $this->addColumn('customer_since', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('Customer Since'),
            'type'      => 'datetime',
            'align'     => 'center',
            'index'     => 'created_at',
            'gmtoffset' => true
        ));


        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('website_id', array(
                'header'    => Mage::helper('binaryanvil_ffl')->__('Website'),
                'align'     => 'center',
                'width'     => '80px',
                'type'      => 'options',
                'options'   => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(true),
                'index'     => 'website_id',
            ));
        }


        /*$this->addColumn('position', array(
            'header'            => Mage::helper('binaryanvil_ffl')->__('Position'),
            'name'              => 'position',
            'width'             => 60,
            'type'              => 'hidden',
            'validate_class'    => 'validate-number',
            'index'             => 'position',
            'editable'          => true, // do not comment because it will not get the associated customer
            'edit_only'         => true // do not comment because it will not get the associated customer
        ));*/

        $this->addColumn('is_default', array(
            'header'            => Mage::helper('binaryanvil_ffl')->__('Is Default'),
            'name'              => 'is_default',
            'width'             => 60,
            'type'              => 'hidden',
            'validate_class'    => 'validate-digits-range digits-range-0-1',
            'index'             => 'is_default',
            'editable'          => true, // do not comment because it will not get the associated customer
            'edit_only'         => true // do not comment because it will not get the associated customer
        ));


        return parent::_prepareColumns();
    }
    public function getOwner()
    {
        $fflId = $this->getRequest()->getParam('id');
        $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
        $fflCustomerId = $ffl->getOwnerCustomerId();
        $ids = array();
        $ids[] = $fflCustomerId;
        return $ids;
    }
    protected function _getSelectedCustomers()   // Used in grid to return selected customers values.
    {
        $customers = $this->getSelectedCustomers();
        if (is_array($customers)) {
            $customers = array_keys($this->getSelectedCustomers());
        }

        return $customers;
    }

    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/customerGrid', array('_current'=>true));
    }
    public function getSelectedCustomers()
    {
        $fflId = $this->getRequest()->getParam('id');
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection');
        $collection->addFieldToFilter('ffl_id', array('eq'=> $fflId ));

        if (count($collection) > 0) {
            $ids = array();
            foreach ($collection as $customer) {
                $customerId = $customer->getMagentoCustomerId();
                $ids[$customerId] = 0;
            }
            return $ids;
            //return array_keys($ids);
        } elseif ($this->getRequest()->getParam('customer_id')) {
            $ids = array();
            $ids[] = $this->getRequest()->getParam('customer_id');
            return $ids;
        } else {
            return null;
        }

    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/customer/edit', array('id' => $row->getId(), 'ffl_id'=>$this->getRequest()->getParam('id')));
    }

}
