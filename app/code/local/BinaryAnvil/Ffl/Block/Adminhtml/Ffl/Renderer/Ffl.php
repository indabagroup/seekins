<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/14/14
 * Time: 8:44 AM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Ffl extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $statuses = Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_status')->toArray();
        $_status = $row->getData('ffl_status');
        $status = isset($statuses[$_status]) ? $statuses[$_status] : $_status;
        $fflId = $row->getFflId();

        $html = '';
        if (is_null($fflId)) {
            $html = '';
        } elseif ($fflId > 0) {
            $url = $this->_getUrl($row->getFflId());
            $popWin = "openNewTab('$url','ffl','top:0,left:0,width=820,height=600,resizable=yes,scrollbars=yes');";
            $html = '<a target="_blank" href="'.$this->_getUrl($fflId).'" onclick="'.$popWin.' return false;">' . $status .'</a>';
        } elseif ($fflId == 0) {
            $url = $this->getUrl('adminhtml/ffl/index', array('order_id' => $row->getData('entity_id'),'customer_id' => $row->getData('customer_id'), 'popup' => '1'));
            $popWin = "openNewTab('$url','ffl','top:0,left:0,width=820,height=600,resizable=yes,scrollbars=yes');";
            $html = '<a target="_blank" href="'.$url.'" onclick="'.$popWin.' return false;">None</a>';

        }

        return $html;
    }

    protected function _getUrl($id)
    {
        return $this->getUrl('adminhtml/ffl/edit', array('id' => $id, 'popup' => '1'));
    }


}