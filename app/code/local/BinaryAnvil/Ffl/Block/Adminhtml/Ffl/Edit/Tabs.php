<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 3:34 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('ffl_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('binaryanvil_ffl')->__('Manage Customer FFLs'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => 'FFL Information',
            'title' => 'FFL Information',
            'content'   => $this->getLayout()
                ->createBlock('binaryanvil_ffl/adminhtml_ffl_edit_tab_form')
                ->toHtml()
        ));
        $popup = $this->getRequest()->getParam('popup');
        if (!isset($popup)) {
            $this->addTab('associated_customer', array(
                'label' => 'Associated Customer',
                'title' => 'Associated Customer',
                'url'       => $this->getUrl('*/*/customer', array('_current' => true)),
                'class'     => 'ajax',
                /*'content'   => $this->getLayout()
                    ->createBlock('binaryanvil_ffl/adminhtml_ffl_edit_tab_customer', 'associated.customer.grid')
                    ->toHtml()*/
            ));
        }
        return parent::_beforeToHtml();
    }
}