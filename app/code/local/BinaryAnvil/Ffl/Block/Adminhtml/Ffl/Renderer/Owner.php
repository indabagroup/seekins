<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/30/14
 * Time: 7:49 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Owner extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $fflId = $this->getRequest()->getParam('id');
        $ffl = Mage::getModel('binaryanvil_ffl/ffl')->load($fflId);
        $fflCustomerId = $ffl->getOwnerCustomerId();
        $customerId = $row->getId();
        $input = '<input class="radio validate-one-required-by-name" checked="checked" type="radio" value="'.$row->getId().'" name="owner_customer_id">';
        if ($fflCustomerId != $customerId) {
            $input = '<input class="radio" type="radio" value="'.$row->getId().'" name="owner_customer_id">';
        }
        return $input;
    }
}