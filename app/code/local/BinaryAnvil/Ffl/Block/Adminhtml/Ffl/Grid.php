<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/7/14
 * Time: 2:21 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('adminhtmlFflGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return BinaryAnvil_Ffl_Model_Ffl
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));
            //->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        if ($this->getRequest()->getParam('popup') > 0) {
            $this->addColumn('ffl_id', array(
                'header' => Mage::helper('binaryanvil_ffl')->__('Select FFL'),
                'index' => 'ffl_id',
                //        'filter_index' => 'sfi.ffl_status',
                'type'  => 'options',
                'width' => '100px',
                'renderer' => 'BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Radio',
            ));
        }

        $this->addColumn('id', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('ID'),
            'width'     => '300',
            'index'     => 'id',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('ffl_holder_name', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Holder Name'),
            'width'     => '300',
            'index'     => 'ffl_holder_name',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('ffl_holder_business_name', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Holder Business Name'),
            'width'     => '300',
            'index'     => 'ffl_holder_business_name',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('ffl_holder_address_line', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Holder Address Line'),
            'width'     => '300',
            'index'     => 'ffl_holder_address_line',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('ffl_holder_phone_number', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Holder Phone Number'),
            'width'     => '300',
            'index'     => 'ffl_holder_phone_number',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('ffl_number', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Number'),
            'width'     => '300',
            'index'     => 'ffl_number',
            //'filter'   => false,
            //'sortable' => false
        ));

        $this->addColumn('ffl_holder_state', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Holder State'),
            'width'     => '300',
            'index'     => 'ffl_holder_state',
        ));

        $this->addColumn('ffl_holder_zip_code', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Holder Zip Code'),
            'width'     => '300',
            'index'     => 'ffl_holder_zip_code',
        ));

        /*$this->addColumn('ffl_class', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Class'),
            'width'     => '300',
            'index'     => 'ffl_class',
//            'renderer' => 'BinaryAnvil_Ffl_Block_Adminhtml_FFl_Renderer_Class',
        ));*/

        $this->addColumn('ffl_expiration_date', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Expiration Date'),
            'width'     => '300',
            'index'     => 'ffl_expiration_date',
            'type'      => 'date'
        ));

        $this->addColumn('ffl_status', array(
            'header'    => Mage::helper('binaryanvil_ffl')->__('FFL Status'),
            'width'     => '300',
            'index'     => 'ffl_status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_status')->toArray()
        ));
        $popup = $this->getRequest()->getParam('popup');
        if (!isset($popup)) {

            $this->addColumn('action',
                array(
                    'header'    =>  Mage::helper('binaryanvil_ffl')->__('Action'),
                    'width'     => '100',
                    'type'      => 'action',
                    'getter'    => 'getId',
                    'actions'   => array(
                        array(
                            'caption'   => Mage::helper('binaryanvil_ffl')->__('Edit'),
                            'url'       => array('base'=> '*/*/edit'),
                            'field'     => 'id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
                ));
        }


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $popup = $this->getRequest()->getParam('popup');
        if (!isset($popup)) {
            $this->setMassactionIdField('id');
            $this->getMassactionBlock()->setFormFieldName('id');

            $this->getMassactionBlock()->addItem('delete', array(
                'label'=> Mage::helper('binaryanvil_ffl')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('binaryanvil_ffl')->__('Are you sure?')
            ));

            $statuses = Mage::getSingleton('binaryanvil_ffl/adminhtml_system_config_source_status')->toOptionArray();

            //array_unshift($statuses, array('label'=>'', 'value'=>''));

            $this->getMassactionBlock()->addItem('status', array(
                'label'=> Mage::helper('binaryanvil_ffl')->__('Change status'),
                'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('binaryanvil_ffl')->__('Status'),
                        'values' => $statuses
                    )
                )
            ));

            return $this;
        }

    }

    public function getRowUrl($row)
    {
        $popup = $this->getRequest()->getParam('popup');
        if (!isset($popup)) {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        } else {
            return '';
        }
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}