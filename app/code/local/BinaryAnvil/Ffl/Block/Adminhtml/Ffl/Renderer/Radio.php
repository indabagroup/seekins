<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/30/14
 * Time: 6:09 PM
 */

class BinaryAnvil_Ffl_Block_Adminhtml_Ffl_Renderer_Radio extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        return '<input class="radio validate-one-required-by-name" type="radio" value="'.$row->getId().'" name="associate">';
    }
}