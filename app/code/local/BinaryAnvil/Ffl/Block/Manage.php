<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/3/14
 * Time: 1:47 PM
 */

class BinaryAnvil_Ffl_Block_Manage extends Mage_Core_Block_Template
{
    public function getAddNewUrl()
    {
        return $this->getUrl('ffl/manage/new');
    }

    public function getSearchUrl()
    {
        return $this->getUrl('ffl/search/index');
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('ffl/manage/delete');
    }

    public function getUnAssignedUrl()
    {
        return $this->getUrl('ffl/manage/unassigned');
    }

    public function getAssignedUrl()
    {
        return $this->getUrl('ffl/manage/assigned');
    }

    public function getDefaultUrl()
    {
        return $this->getUrl('ffl/manage/default');
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account');
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getDefaultFfl()
    {
        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($id)
            ->addFieldToFilter('is_default',array('eq'=>1))
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));
        $defaultFfl = $collection->getFirstItem();

        if ($defaultFfl->getId()) {
            return $defaultFfl;
        }

        return false;
    }


    public function getFflList()
    {
        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($id)
            ->addFieldToFilter('is_default',array('neq'=>1))
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));

        return $collection;
    }

    public function getArchiveFflList()
    {

        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            //->addCustomerIdFilter($id)
            //->addFieldToFilter('magento_customer_id',array('eq'=>$id))
            ->addFieldToFilter('ffl_status',array('eq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));

        return $collection;
    }
}