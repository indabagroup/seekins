<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/12/14
 * Time: 3:27 PM
 */

class BinaryAnvil_Ffl_Block_List extends Mage_Core_Block_Template
{
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getDefaultFflId()
    {
        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($id)
            ->addFieldToFilter('is_default',array('eq'=>1))
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));
        $defaultFfl = $collection->getFirstItem();

        if ($defaultFfl->getId()) {
            return $defaultFfl->getId();
        }

        return false;
    }

    public function getFflList()
    {
        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($id)
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));

        return $collection;
    }

    /**
     * Retrieve options for addresses dropdown
     *
     * @return array
     */
    public function getFflOptions()
    {
        $options = array();
        $collection = $this->getFflList();
        if (count($collection) > 0) {
            foreach ($collection as $ffl) {
                //Mage::log($ffl);
                $options[] = array(
                    'value' => $ffl->getId(),
                    'label' => Mage::helper('binaryanvil_ffl')->getFflOneLineFormat($ffl)
                );
            }

        }

        return $options;
    }

    public function getFflSelectHtml()
    {
        $fflId = $this->getDefaultFflId();

        if (Mage::getSingleton('checkout/session')->getQuote()->getFflId()) {
            $fflId  = Mage::getSingleton('checkout/session')->getQuote()->getFflId();
        }

        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName('ffl')
            ->setId('ffl-select')
            ->setClass('ffl-select')
            ->setExtraParams('onchange="newFfl(!this.value)"')
            ->setValue($fflId)
            ->setOptions($this->getFflOptions());

        $select->addOption('', Mage::helper('binaryanvil_ffl')->__('New FFL'));
        if ($this->getFflOptions()) {
            return $select->getHtml();
        } else {
            return false;
        }

    }
}