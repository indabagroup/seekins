<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 6/3/14
 * Time: 1:47 PM
 */

class BinaryAnvil_Ffl_Block_Edit extends Mage_Core_Block_Template
{

    protected $_formData;
    protected function _getFflCustomerIds()
    {
        $fflId = $this->getRequest()->getParam('id');
        $fflCustomerIds = array();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection');
        $collection->addFieldToFilter('ffl_id', array('eq'=> $fflId ));
        foreach ($collection as $customer) {
            $fflCustomerIds[] = $customer->getMagentoCustomerId();
        }

        return $fflCustomerIds;
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->_formData = Mage::getModel('binaryanvil_ffl/ffl');

        // Init address object
        if ($id = $this->getRequest()->getParam('id')) {
            $this->_formData->load($id);
            if (!in_array(Mage::getSingleton('customer/session')->getCustomerId(), $this->_getFflCustomerIds())) {
                $this->_formData->setData(array());
            } elseif (Mage::getSingleton('customer/session')->getFflStatus() == BinaryAnvil_Ffl_Helper_Data::ARCHIVE) {
                $this->_formData->setData(array('id'=>$id));
            }
        }

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->getTitle());
        }

        if ($postedData = Mage::getSingleton('core/session')->getFflData(true)) {
            $this->_formData->addData($postedData);
        }
    }

    public function getTitle()
    {
        if ($title = $this->getData('title')) {
            return $title;
        }
        if (Mage::getSingleton('customer/session')->getFflStatus() == BinaryAnvil_Ffl_Helper_Data::ARCHIVE) {
            $title = $this->__('Replace FFL');
        } elseif ($this->getRequest()->getParam('id')) {
            $title = $this->__('Edit FFL');
        } else {
            $title = $this->__('Add New FFL');
        }

        return $title;
    }

    /**
     * Retrieve form data
     *
     * @return Varien_Object
     */
    public function getFormData()
    {
        return $this->_formData;
    }

    public function getSaveUrl()
    {
        return $this->getUrl('ffl/manage/save');
    }

    public function getBackUrl()
    {
        return $this->getUrl('ffl/manage/index');
    }

    public function getImageUrl($image)
    {
        return Mage::getBaseDir('media') . DS . 'customer_ffl' . DS. $image;
    }

    public function getAssignedUrl()
    {
        return $this->getUrl('ffl/manage/assigned');
    }
}