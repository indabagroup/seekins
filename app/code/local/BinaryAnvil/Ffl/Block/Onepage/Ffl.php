<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/14/14
 * Time: 3:40 PM
 */

class BinaryAnvil_Ffl_Block_Onepage_Ffl extends Mage_Checkout_Block_Onepage_Abstract
{

    protected function _construct()
    {
        if ($this->isCustomerLoggedIn() && $this->hasFflRestrictedItems()) {
            $this->getCheckout()->setStepData('ffl', array(
                'label'     => Mage::helper('binaryanvil_ffl')->__('FFL Information'),
                'is_show'   => $this->isShow()
            ));
        } else {
            $this->getCheckout()->setStepData('ffl', array(
                'label'     => Mage::helper('binaryanvil_ffl')->__('FFL Information'),
                'is_show'   => false
            ));
        }

        if ($this->isCustomerLoggedIn() && $this->hasFflRestrictedItems()) {
            $this->getCheckout()->setStepData('ffl', 'allow', true);
            $this->getCheckout()->setStepData('billing', 'allow', false);
        } else {
            $this->getCheckout()->setStepData('ffl', 'allow', false);
            $this->getCheckout()->setStepData('billing', 'allow', true);
        }

        parent::_construct();
    }
    /**
     * Get all cart items
     *
     * @return array
     */
    protected function getCartItems()
    {
        return $this->getQuote()->getAllVisibleItems();
    }

    protected function hasFflRestrictedItems()
    {
        foreach ($this->getCartItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            $attributeSetName = '';
            if ($product->getData('attribute_set_id')) {
                $attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($product->getAttributeSetId())->getAttributeSetName();
            }
            $fflNeeded = $product->getSpFflNeeded();
            if($fflNeeded == 1 || $attributeSetName == 'Complete Rifles' || $attributeSetName == 'AR Lowers') {
                return true;
            }
        }

        return false;
    }


    protected $_ffl = null;

    protected function getFfl()
    {
        $id = $this->getCustomer()->getId();
        if (Mage::getSingleton('checkout/session')->getQuote()->getFflId()) {
            $fflId  = Mage::getSingleton('checkout/session')->getQuote()->getFflId();
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
                ->addCustomerIdFilter($id)
                ->addFieldToFilter('id',array('eq'=>$fflId))
                ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            $saveFfl = $collection->getFirstItem();
            $ffl = $saveFfl;
        } elseif ($this->getDefaultFfl()) {
            $ffl = $this->getDefaultFfl();
        } else {
            $ffl = $this->getFflList()->getFirstItem();
        }

        return $ffl;
    }

    public function getDefaultFfl()
    {
        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($id)
            ->addFieldToFilter('is_default',array('eq'=>1))
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
        $defaultFfl = $collection->getFirstItem();

        if ($defaultFfl->getId()) {
            return $defaultFfl;
        }

        return false;
    }

    public function getFflList()
    {
        $id = $this->getCustomer()->getId();
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
            ->addCustomerIdFilter($id)
            ->addFieldToFilter('ffl_status',array('nin'=>array(3,4)));
            //->addFieldToFilter('ffl_status',array('neq'=>BinaryAnvil_Ffl_Helper_Data::ARCHIVE));
            //->addFieldToFilter('ffl_status',array('nin'=>array(BinaryAnvil_Ffl_Helper_Data::ARCHIVE, BinaryAnvil_Ffl_Helper_Data::EXPIRED)));

        return $collection;
    }

    /**
     * Retrieve options for addresses dropdown
     *
     * @return array
     */
    public function getFflOptions()
    {
        $options = array();
        $collection = $this->getFflList();
        if (count($collection) > 0) {
            foreach ($collection as $ffl) {
                //Mage::log($ffl);
                $options[] = array(
                    'value' => $ffl->getId(),
                    'label' => Mage::helper('binaryanvil_ffl')->getFflOneLineFormat($ffl)
                );
            }

        }
        return $options;
    }

    public function getFflSelectHtml()
    {
        $ffl = $this->getFfl();
        $fflId = $ffl->getId();


        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName('ffl')
            ->setId('ffl-select')
            ->setClass('ffl-select')
            ->setExtraParams('onchange="binaryanvil.newAddress(!this.value)"')
            ->setValue($fflId)
            ->setOptions($this->getFflOptions());

        $select->addOption('', Mage::helper('binaryanvil_ffl')->__('New FFL'));

        return $select->getHtml();
    }
}