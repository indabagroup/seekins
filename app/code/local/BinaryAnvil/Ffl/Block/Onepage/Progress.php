<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/15/14
 * Time: 5:53 PM
 */

class BinaryAnvil_Ffl_Block_Onepage_Progress extends Mage_Checkout_Block_Onepage_Progress
{
    /**
     * Get checkout steps codes
     *
     * @return array
     */
    protected function _getStepCodes()
    {
        return array('login', 'ffl', 'billing', 'shipping', 'shipping_method', 'payment', 'review');
    }
    /**
     * Get is step completed. if is set 'toStep' then all steps after him is not completed.
     *
     * @param string $currentStep
     *  @see: Mage_Checkout_Block_Onepage_Abstract::_getStepCodes() for allowed values
     * @return bool
     */
    public function isStepComplete($currentStep)
    {
        $stepsRevertIndex = array_flip($this->_getStepCodes());

        $toStep = $this->getRequest()->getParam('toStep');

        if (empty($toStep) || !isset($stepsRevertIndex[$currentStep])) {
            return $this->getCheckout()->getStepData($currentStep, 'complete');
        }

        if ($stepsRevertIndex[$currentStep] > $stepsRevertIndex[$toStep]) {
            return false;
        }

        return $this->getCheckout()->getStepData($currentStep, 'complete');
    }
}