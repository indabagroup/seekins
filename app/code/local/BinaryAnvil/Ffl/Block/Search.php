<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/29/14
 * Time: 10:09 AM
 */

class BinaryAnvil_Ffl_Block_Search extends Mage_Core_Block_Template
{
    const QUERY_VAR_NAME = 'q';
    const QUERY_VAR_SEARCH_BY_NAME = 'by';

    /**
     * Query string
     *
     * @var string
     */
    protected $_queryText;

    protected $_queryBy;
    /**
     * Is a maximum length cut
     *
     * @var bool
     */
    protected $_isMaxLength = false;

    /**
     * Prepare layout
     *
     * @return Mage_CatalogSearch_Block_Result
     */
    protected function _prepareLayout()
    {
        // add Home breadcrumb
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadcrumbs) {
            $title = $this->__("Search results for: '%s'", $this->helper('catalogsearch')->getQueryText());

            $breadcrumbs->addCrumb('home', array(
                'label' => $this->__('Home'),
                'title' => $this->__('Go to Home Page'),
                'link'  => Mage::getBaseUrl()
            ))->addCrumb('search', array(
                    'label' => $title,
                    'title' => $title
                ));
        }

        // modify page title
        $title = $this->__("Search results for: '%s'", $this->helper('catalogsearch')->getEscapedQueryText());
        $this->getLayout()->getBlock('head')->setTitle($title);

        return parent::_prepareLayout();
    }

    /**
     * Retrieve search query parameter name
     *
     * @return string
     */
    public function getQueryParamName()
    {
        return self::QUERY_VAR_NAME;
    }

    /**
     * Retrieve search query parameter name
     *
     * @return string
     */
    public function getSearchByParamName()
    {
        return self::QUERY_VAR_SEARCH_BY_NAME;
    }

    /**
     * Retrieve search query text
     *
     * @return string
     */
    public function getQueryText()
    {
        if (!isset($this->_queryText)) {
            $this->_queryText = $this->getRequest()->getParam($this->getQueryParamName());
            if ($this->_queryText === null) {
                $this->_queryText = '';
            } else {
                /* @var $stringHelper Mage_Core_Helper_String */
                $stringHelper = Mage::helper('core/string');
                $this->_queryText = is_array($this->_queryText) ? ''
                    : $stringHelper->cleanString(trim($this->_queryText));

                $maxQueryLength = '';
                if ($maxQueryLength !== '' && $stringHelper->strlen($this->_queryText) > $maxQueryLength) {
                    $this->_queryText = $stringHelper->substr($this->_queryText, 0, $maxQueryLength);
                    $this->_isMaxLength = true;
                }
            }
        }
        return $this->_queryText;
    }

    /**
     * Retrieve search query text
     *
     * @return string
     */
    public function getQueryBy()
    {
        if (!isset($this->_queryBy)) {
            $this->_queryBy = $this->getRequest()->getParam($this->getSearchByParamName());
            if ($this->_queryBy === null) {
                $this->_queryBy = '';
            } else {
                /* @var $stringHelper Mage_Core_Helper_String */
                $stringHelper = Mage::helper('core/string');
                $this->_queryBy = is_array($this->_queryBy) ? ''
                    : $stringHelper->cleanString(trim($this->_queryBy));
            }
        }
        return $this->_queryBy;
    }

    /**
     * Retrieve HTML escaped search query
     *
     * @return string
     */
    public function getEscapedQueryText()
    {
        return $this->escapeHtml($this->getQueryText());
    }

    public function getResultUrl()
    {
        return $this->getUrl('ffl/search/result');
    }

    public function getResult()
    {
        $queryText = $this->getQueryText();
        $searchBy = $this->getQueryBy();
        if ($searchBy && $queryText) {
            //$collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection');
                //->addFflInformation()
                //->addFieldToFilter('ffl_status',array('eq'=>BinaryAnvil_Ffl_Helper_Data::VALIDATE))
                //->addFieldToFilter('magento_customer_id' , array('neq',Mage::getSingleton('customer/session')->getCustomer()->getId()));
            //$collection->getSelect()->group('ffl_id');
            $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection');
            $collection->addFieldToFilter('ffl_status',array('eq'=>BinaryAnvil_Ffl_Helper_Data::VALIDATE));
            /*$collection = Mage::getResourceModel('binaryanvil_ffl/ffl_collection')
                ->addFieldToFilter('ffl_status',array('eq'=>BinaryAnvil_Ffl_Helper_Data::VALIDATE))
                ->addNotEqCustomerFilter(Mage::getSingleton('customer/session')->getCustomer()->getId());*/
            if ($searchBy == 'holder_name') {
                $collection->addFieldToFilter('ffl_holder_name',array('like'=>"%".$queryText."%"));
            }

            if ($searchBy == 'ffl_number') {
                $collection->addFieldToFilter('ffl_number',array('like'=>"%".$queryText."%"));
            }

            return $collection;
        }
    }

    public function getCustomerFfl()
    {
        $collection = Mage::getResourceModel('binaryanvil_ffl/ffl_customer_collection')
        ->addFieldToFilter('magento_customer_id' , array('neq',Mage::getSingleton('customer/session')->getCustomer()->getId()));
        $fflIds = array();
        if (count($collection) > 0) {
            foreach ($collection as $ffl) {
                $fflIds[] = $ffl->getFflId();
            }
        }
        return $fflIds;
    }

    public function getAssignedUrl()
    {
        return $this->getUrl('ffl/search/assigned');
    }

    public function getUnAssignedUrl()
    {
        return $this->getUrl('ffl/search/unassigned');
    }

    public function getBackUrl()
    {
        return $this->getUrl('ffl/manage/index');
    }
}