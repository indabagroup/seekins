<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/14/14
 * Time: 2:38 PM
 */

class BinaryAnvil_Ffl_Block_Onepage extends Mage_Checkout_Block_Onepage
{
    public function getActiveStep()
    {
        //New Code, make step ffl active when user is already logged in
        if ($this->isCustomerLoggedIn() && $this->hasFflRestrictedItems()) {
            return 'ffl';
        } elseif ($this->isCustomerLoggedIn()) {
            return 'billing';
        } else {
            return 'login';
        }
    }

    /**
     * Get checkout steps codes
     *
     * @return array
     */
    protected function _getStepCodesWithFfl()
    {
        return array('ffl', 'billing', 'shipping', 'shipping_method', 'payment', 'review');
    }
    /**
     * Get 'one step checkout' step data
     *
     * @return array
     */
    public function getSteps()
    {
        $steps = array();
        $stepCodes = $this->_getStepCodes();

        if ($this->isCustomerLoggedIn()) {
            $stepCodes = array_diff($stepCodes, array('login'));
        }

        if ($this->isCustomerLoggedIn() && $this->hasFflRestrictedItems()) {
            $stepCodes = $this->_getStepCodesWithFfl();
        }

        foreach ($stepCodes as $step) {
            $steps[$step] = $this->getCheckout()->getStepData($step);
        }

        return $steps;
    }
    /**
     * Get all cart items
     *
     * @return array
     */
    protected function getCartItems()
    {
        return $this->getQuote()->getAllVisibleItems();
    }

    protected function hasFflRestrictedItems()
    {
        foreach ($this->getCartItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            $attributeSetName = '';
            if ($product->getData('attribute_set_id')) {
                $attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($product->getAttributeSetId())->getAttributeSetName();
            }
            $fflNeeded = $product->getSpFflNeeded();
            if($fflNeeded == 1 || $attributeSetName == 'Complete Rifles' || $attributeSetName == 'AR Lowers') {
                return true;
            }
        }

        return false;
    }

}