<?php
class BinaryAnvil_Adminhtmlgridlimit_Model_Observer extends Varien_Event_Observer
{
	public function setNewLimitToGrid(Varien_Event_Observer $observer)
	{
        $block = $observer->getEvent()->getBlock();
        if(($block instanceof Mage_Adminhtml_Block_Widget_Grid) && !($block  instanceof Mage_Adminhtml_Block_Dashboard_Grid))
        	$block->setDefaultLimit(200); // Default row numbers displayed in grid is 20 and Maximum is 200		
	}
}