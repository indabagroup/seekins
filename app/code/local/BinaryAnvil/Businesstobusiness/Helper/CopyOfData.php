<?php

class BinaryAnvil_Businesstobusiness_Helper_Data extends Mage_Core_Helper_Data
{
//	const XML_PATH_MODULE_DISABLED = 'requirelogin_options/requirelogin_adminconfig/disable_ext';
//	const WHITELIST  = 'requirelogin_options/requirelogin_adminconfig/requirelogin_whitelist';
	public function isLoginRequired($store = null)
    {
        return Mage::getStoreConfigFlag('requirelogin_options/requirelogin_adminconfig/require_login', $store);
    }

    public function getWhitelist($store = null)
    {
        return Mage::getStoreConfig('requirelogin_options/requirelogin_adminconfig/requirelogin_whitelist', $store);
    }

}