<?php

class BinaryAnvil_Businesstobusiness_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_ENABLE_CONFIGURABLE = 'businesstobusiness_settings/general_settings/configurable_enabled';

    public function isConfigurableAddingEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLE_CONFIGURABLE);
    }
}