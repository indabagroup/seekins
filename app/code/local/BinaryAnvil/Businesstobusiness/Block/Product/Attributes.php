<?php
class BinaryAnvil_Businesstobusiness_Block_Product_Attributes extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('binaryanvil/businesstobusiness/product/attributes.phtml');
    }

    protected function _toHtml()
    {
        return parent::_toHtml();
    }

    public function getProduct()
    {
        return $this->getData('currentProduct');
    }

    public function getAttributes(){
        $product = $this->getProduct();
        $attrib = array();
        $data = array();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront()) {
                $value = $attribute->getFrontend()->getValue($product);


                if (!$product->hasData($attribute->getAttributeCode())) {
                    $value = Mage::helper('catalog')->__('N/A');
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }
                $attributeLabel = $attribute->getStoreLabel() ? $attribute->getStoreLabel() : $this->__($attribute->getFrontendLabel());
                $attrib[] = $attributeLabel;
                if (is_string($value) && strlen($value) && !strstr($attribute->getStoreLabel(), "us")) {
                    $data[$attribute->getAttributeCode()] = array(
                        'label' => $attributeLabel,
                        'value' => $value,
                        'code'  => $attribute->getAttributeCode(),
                        'position' => $attribute->getPosition()
                    );
                }
            }
        }

        return $this->aasort($data,"position");;
    }

    public function aasort (&$array, $key) {

        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
        return $array;
    }




    public function getAttributeValue($label)
    {
        $data = array();
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if($attribute->getFrontendLabel() ==  $label){
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    $value = Mage::helper('catalog')->__('N/A');
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }
                return $value;
            }
        }
    }
}