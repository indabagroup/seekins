<?php
class BinaryAnvil_Businesstobusiness_Block_Product_Addtocart extends Mage_Core_Block_Template
{
	protected function _construct()
    {
    	
        parent::_construct();
        $this->setTemplate('binaryanvil/businesstobusiness/product/list/addtocart.phtml');
    }

	protected function _toHtml()
    {

    return parent::_toHtml();

    }
    protected $_product = null;
	public function setProduct($product)
    {
    	$productId = $product->getId();
    	if($productId){
    		$this->_product = Mage::getModel('catalog/product')->load($productId);
    	}else{
    		$this->_product = $product;
    	}
        

        return $this;
    }
    
	public function getProduct()
    {
        return $this->_product;
    }
    
    public function getConfigurableOptions(){
    	return Mage::getSingleton('core/layout')->createBlock('businesstobusiness/product_type_configurable')->setProduct($this->getProduct())->toHtml();
    }
    
	public function getMinimalQty()
    {
    	$product = $this->getProduct();
        if ($stockItem = $product->getStockItem()) {
            return ($stockItem->getMinSaleQty() && $stockItem->getMinSaleQty() > 0 ? $stockItem->getMinSaleQty() * 1 : null);
        }
        return null;
    }
   
}