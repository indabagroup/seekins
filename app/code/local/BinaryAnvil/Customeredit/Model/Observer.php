<?php
class BinaryAnvil_Customeredit_Model_Observer
{
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
    public function mailExists(Varien_Object $customer, $mail, $websiteId)
    {
        if($websiteId){
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($mail);
        if($customer->getId()){
            return $customer->getId();
        }
        return FALSE;
    }
    public function customerLogin(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {

           $customer = Mage::getSingleton('customer/session')->getCustomer();
            $Securityq = $customer->getSecurityq();

            if(empty($Securityq)){
                $param = '1';
                Mage::getSingleton('customer/session')->setParamID($param);

                $url = Mage::getUrl("customer/account/edit");
                Mage::app()->getResponse()->setRedirect($url)->sendResponse();
                exit();
            }
            else{
                $url = Mage::getUrl("customer/account/");
                Mage::app()->getResponse()->setRedirect($url)->sendResponse();
                exit();
            }
        }
    }

}