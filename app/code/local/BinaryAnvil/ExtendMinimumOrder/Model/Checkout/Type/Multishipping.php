<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 2/12/14
 * Time: 5:07 PM
 */

class BinaryAnvil_ExtendMinimumOrder_Model_Checkout_Type_Multishipping extends Mage_Checkout_Model_Type_Multishipping
{
    /**
     * Check if quote amount is allowed for multishipping checkout
     *
     * @return bool
     */
    public function validateMinimumAmount()
    {
        if (!(Mage::getStoreConfigFlag('sales/minimum_order/active')
            && Mage::getStoreConfigFlag('sales/minimum_order/multi_address')
            && !$this->getQuote()->validateMinimumAmount())) {


            $minimumOrderHelper = Mage::helper('binaryanvil_extendminimumorder');
            $noMinimumOrderCustomerGroup = $minimumOrderHelper->getNoMinimumOrderCustomerGroup();
            $noMinimumOrderCustomerGroup = explode("," , $noMinimumOrderCustomerGroup);

            if (!$minimumOrderHelper->isEnabled() || empty($noMinimumOrderCustomerGroup)) {
                return !(Mage::getStoreConfigFlag('sales/minimum_order/active')
                    && Mage::getStoreConfigFlag('sales/minimum_order/multi_address')
                    && !$this->getQuote()->validateMinimumAmount());
            }

            $currentCustomerGroup = Mage::getSingleton('customer/session')->getCustomer()->getGroupId();

            if (in_array($currentCustomerGroup,$noMinimumOrderCustomerGroup)) {
                return true;
            }
        } else {
            return !(Mage::getStoreConfigFlag('sales/minimum_order/active')
                && Mage::getStoreConfigFlag('sales/minimum_order/multi_address')
                && !$this->getQuote()->validateMinimumAmount());
        }
        /*return !(Mage::getStoreConfigFlag('sales/minimum_order/active')
            && Mage::getStoreConfigFlag('sales/minimum_order/multi_address')
            && !$this->getQuote()->validateMinimumAmount());*/
    }
}