<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 2/12/14
 * Time: 4:26 PM
 */

class BinaryAnvil_ExtendMinimumOrder_Model_Config_Customer_Group extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Returns period types as array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $customerGroups = Mage::getSingleton('adminhtml/system_config_source_customer_group')->toOptionArray();
        array_shift($customerGroups);

        array_unshift($customerGroups, array('value' => -1, 'label' => Mage::helper('salesrule')->__('NOT LOGGED IN')));
        array_unshift(
            $customerGroups, array('value' => 0, 'label' => Mage::helper('salesrule')->__('Disable functionality'))
        );
        return $customerGroups;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}