<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 2/12/14
 * Time: 5:04 PM
 */

class BinaryAnvil_ExtendMinimumOrder_Model_Sales_Quote_Address extends Mage_Sales_Model_Quote_Address
{
    /**
     * Validate minimum amount
     *
     * @return bool
     */
    public function validateMinimumAmount()
    {
        $storeId = $this->getQuote()->getStoreId();
        if (!Mage::getStoreConfigFlag('sales/minimum_order/active', $storeId)) {
            return true;
        }

        $minimumOrderHelper = Mage::helper('binaryanvil_extendminimumorder');
        $noMinimumOrderCustomerGroup = $minimumOrderHelper->getNoMinimumOrderCustomerGroup();
        $noMinimumOrderCustomerGroup = explode("," , $noMinimumOrderCustomerGroup);

        if (!$minimumOrderHelper->isEnabled() || empty($noMinimumOrderCustomerGroup)) {
            return parent::validateMinimumAmount();
        }

        $currentCustomerGroup = Mage::getSingleton('customer/session')->getCustomer()->getGroupId();

        if (in_array($currentCustomerGroup,$noMinimumOrderCustomerGroup)) {
            return true;
        }

        if ($this->getQuote()->getIsVirtual() && $this->getAddressType() == self::TYPE_SHIPPING) {
            return true;
        } elseif (!$this->getQuote()->getIsVirtual() && $this->getAddressType() != self::TYPE_SHIPPING) {
            return true;
        }

        $amount = Mage::getStoreConfig('sales/minimum_order/amount', $storeId);
        if ($this->getBaseSubtotalWithDiscount() < $amount) {
            return false;
        }
        return true;
    }
}