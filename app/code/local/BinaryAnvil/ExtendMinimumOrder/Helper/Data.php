<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 2/12/14
 * Time: 4:17 PM
 */

class BinaryAnvil_ExtendMinimumOrder_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'binaryanvil_extendminimumorder/general_settings/enabled';
    const XML_PATH_RESTRICT_CUSTOMER_GROUP_MINIMUM_ORDER = 'binaryanvil_extendminimumorder/no_minimum_order/customer_group';


    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED);
    }

    public function getNoMinimumOrderCustomerGroup()
    {
        return Mage::getStoreConfig(self::XML_PATH_RESTRICT_CUSTOMER_GROUP_MINIMUM_ORDER);
    }
}