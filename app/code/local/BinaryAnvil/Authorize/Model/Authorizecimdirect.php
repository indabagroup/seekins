<?php
/**
Copyright 2009-2012 Eric Levine
IDP
 */
class BinaryAnvil_Authorize_Model_Authorizecimdirect extends IDP_AuthorizeCIM_Model_Authorizecimdirect
{

	public function authorize(Varien_Object $payment, $amount) {

        if ( Mage::getSingleton( 'binaryanvil_authorize/config' )->canCustomAuthorize() ) {
            if(!(Mage::getSingleton('admin/session')->isLoggedIn())){
                $amount =  Mage::getSingleton( 'binaryanvil_authorize/config' )->getMinAuthorizeAmount();
            }
        }else{
            parent::authorize($payment,$amount);
        }

//        Mage::log(get_class($this));
//        Mage::log("amount authorized: " . $amount);
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering authorize()");
		}
		$this->setAmount($amount)
		->setPayment($payment);

		//if (!$payment->getCcNumber()) { $payment->setCcNumber($payment->getToken()); }
		
		$orderTable=Mage::getSingleton('core/resource')->getTableName('sales_flat_order'); $orderTransTable=Mage::getSingleton('core/resource')->getTableName('sales_payment_transaction'); 
		$orderno = $payment->getOrder()->getIncrementId();
		$sql = "DELETE p.* FROM $orderTransTable p, $orderTable q WHERE q.increment_id ='".$orderno."' AND q.entity_id=p.order_id AND p.txn_type='authorization';";
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query($sql);

		$result = $this->_call($payment, 'authorize', $amount);
		if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
		if($result === false)
		{
			$e = $this->getError();
			if (isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		}
		else
		{
			// Check if there is a gateway error
			if ($result['Status']['statusCode'] == "Ok")
			{
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001")
				{
					$payment->setStatus(self::STATUS_APPROVED);
					$payment->setTransactionId($result['Status']['transno']);
					$payment->setIsTransactionClosed(0);
					$payment->setTxnId($result['Status']['transno']);
					$payment->setParentTxnId($result['Status']['transno']);
					$payment->setCcTransId($result['Status']['transno']);					
					}
			}
			else
			{
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
	return $this;
	}
}