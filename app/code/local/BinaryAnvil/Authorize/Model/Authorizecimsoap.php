<?php

/**
 * Copyright 2009-2013 Eric Levine
 * IDP
 */
class BinaryAnvil_Authorize_Model_Authorizecimsoap extends IDP_AuthorizeCIM_Model_authorizecimsoap
{
    const STATUS_1DOLLAR_VOIDED = 'Voided previous $1 Authorized amount.';
    protected $c_order_id = null;

    public function getPaymentAction()
    {
        if (Mage::getSingleton('binaryanvil_authorize/config')->canCustomAuthorize() && !is_null($this->c_order_id)) {

            return IDP_AuthorizeCIM_Model_authorizecimsoap::PAYMENT_ACTION_AUTH_CAPTURE;
        }

        return Mage::getStoreConfig('payment/authorizecimsoap/payment_action');
    }

    public function authorize(Varien_Object $payment, $amount, $bypass1Auth = false)
    {

        $logger = Mage::helper('binaryanvil_authorize');
        $logger->info(__CLASS__ . '::' . __METHOD__, true);

        if (Mage::getSingleton('binaryanvil_authorize/config')->canCustomAuthorize() && !$bypass1Auth) {

            if (strpos($payment->getOrder()->getIncrementId(), '-') === false || !Mage::getSingleton('binaryanvil_authorize/config')->canFullAuthorizeOnEdit()) {
                // Do $1.00 Authorize
                $amount = Mage::getSingleton('binaryanvil_authorize/config')->getMinAuthorizeAmount();
                $payment->setBaseAmountAuthorized($amount);
            }
        } else {
            parent::authorize($payment, $amount);
            return;
        }

        $logger->info("entering authorize()");

        $this->setAmount($amount)
            ->setPayment($payment);

        $orderTable = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
        $orderTransTable = Mage::getSingleton('core/resource')->getTableName('sales_payment_transaction');
        $orderno = $payment->getOrder()->getIncrementId();
        $sql = "DELETE p.* FROM $orderTransTable p, $orderTable q WHERE q.increment_id ='" . $orderno . "' AND q.entity_id=p.order_id AND p.txn_type='authorization';";
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $write->query($sql);

        $result = $this->_call($payment, 'authorize', $amount);
        if ($result === false) {
            $e = $this->getError();
            if (isset($e['message'])) {
                $message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
            } else {
                $message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
            }
            Mage::throwException($message);
        } else {
            // Check if there is a gateway error
            if ($result['Status']['statusCode'] == "Ok") {
                // Check if there is an error processing the credit card
                if ($result['Status']['code'] == "I00001") {
                    $payment->setStatus(self::STATUS_APPROVED);
                    $payment->setTransactionId($result['Status']['transno']);
                    $payment->setIsTransactionClosed(0);
                    $payment->setTxnId($result['Status']['transno']);
                    $payment->setParentTxnId($result['Status']['transno']);
                    $payment->setCcTransId($result['Status']['transno']);
                }
            } else {
                Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
            }
        }
        return $this;
    }


    public function capture(Varien_Object $payment, $amount)
    {
        $logger = Mage::helper('binaryanvil_authorize');
        $logger->info(__CLASS__ . '::' . __METHOD__, true);

        if (!Mage::getSingleton('binaryanvil_authorize/config')->canCustomAuthorize()) {
            parent::capture($payment, $amount);
            return;
        }

        $logger->info("entering capture()");

        $this->setAmount($amount)
            ->setPayment($payment);
        $ttt = $this->getReauth();
        $logger->info("getReauth: $ttt");
        if ($this->getReauth()) {
            //if(false) {
            $orderTable = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
            $logger->info("Reauthorization Steps");
            $orderInvoiceTable = Mage::getSingleton('core/resource')->getTableName('sales_flat_invoice');
            $orderno = $payment->getOrder()->getIncrementId();
            $this->c_order_id = $payment->getOrder()->getId();

            //$this->getInvoice()->getId();  invoice number
            $sql = "SELECT * FROM $orderInvoiceTable p, $orderTable q WHERE q.increment_id ='$orderno' AND q.entity_id=p.order_id AND p.increment_id>'' ORDER BY p.entity_id desc LIMIT 1;";
            $data2 = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchRow($sql);
            $lastinvoice = $data2['increment_id'];
            $logger->info("lastinvoice: $lastinvoice");
            $logger->info("step4c");
            if (!$lastinvoice) {
                $logger->info("step4c:A:capture");

                if (Mage::getSingleton('binaryanvil_authorize/config')->canCustomAuthorize()) {
                    $result = $this->_call($payment, 'captureonly', $amount);
                }else{
                    $result = $this->_call($payment, 'capture', $amount);
                }

            } else {
                $logger->info("step4c:B:captureonly");
                    $result = $this->_call($payment, 'captureonly', $amount);
            }
            $logger->info(var_export($result, TRUE));
            if ($result['Status']['transno'] == '0') {
                $logger->info("step4c:C:captureonly");
                $result = $this->_call($payment, 'captureonly', $amount);
            }
        } else {
            $logger->info("step4b");
            $logger->info("step4b:capture");
            $result = $this->_call($payment, 'capture', $amount);
        }
        $logger->info("step5");
        $logger->info(var_export($result, TRUE));
        if ($result === false) {
            $e = $this->getError();
            if (isset($e['message'])) {
                $message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
            } else {
                $message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
            }
            Mage::throwException($message);
        } else {
            // Check if there is a gateway error
            if ($result['Status']['statusCode'] == "Ok") {
                // Check if there is an error processing the credit card
                if ($result['Status']['code'] == "I00001") {
                    //$payment->setIsTransactionClosed(1);
                    //$payment->registerCaptureNotification();
                    $payment->setIsTransactionClosed(0);
                    $payment->setTransactionId($result['Status']['transno']);
                    $payment->setLastTransId($result['Status']['transno'])
                        ->setCcApproval($result['Status']['code'])
                        ->setCcTransId($result['Status']['transno'])
                        ->setCcAvsStatus($result['Status']['statusCode'])
                        ->setCcCidStatus($result['Status']['statusCode']);
                }
            } else {
                Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
            }
        }
        return $this;
    }

    public function parseXML($start, $end, $xml)
    {
        $truePos = strpos($xml, '<messages>');
        return preg_replace('|^.*?' . $start . '(.*?)' . $end . '.*?$|i', '$1', substr($xml, $truePos));
    }

    public function createTransXML($amount, $tax, $CustomerProfileID, $PaymentProfileID, $ShippingProfileID, $callby, $invoiceno, $authtransID, $Approval, $cvv = 111, $storeId = 0)
    {
        $logger = Mage::helper('binaryanvil_authorize');
        $logger->info(__CLASS__ . '::' . __METHOD__, true);

        // Build Transaction Request
        $TxRq = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerProfileTransactionRequest/>');
        $TxRq->addAttribute('xmlns', 'AnetApi/xml/v1/schema/AnetApiSchema.xsd');
        $merchantAuth = $TxRq->addChild('merchantAuthentication');
        $merchantAuth->addChild('name', htmlentities($this->getUsername($storeId)));
        $merchantAuth->addChild('transactionKey', htmlentities($this->getPassword($storeId)));
        $transaction = $TxRq->addChild('transaction');
        if ($this->getPaymentAction() == 'authorize') {
            $logger->info('01');
            switch ($callby) {
                case 'captureonly':
                    $logger->info('01a');
                    $credit = $transaction->addChild('profileTransCaptureOnly');
                    $credit->addChild('amount', $amount);
                    break;
                case 'capture':
                    $logger->info('01b');
                    $credit = $transaction->addChild('profileTransPriorAuthCapture');
                    $credit->addChild('amount', $amount);
                    break;
                case 'refund':
                    $logger->info('01c');
                    $credit = $transaction->addChild('profileTransRefund');
                    $credit->addChild('amount', $amount);
                    break;
                case 'void':
                    $logger->info('01d');
                    $credit = $transaction->addChild('profileTransVoid');
                    break;
                default:
                    $logger->info('01e');
                    $credit = $transaction->addChild('profileTransAuthOnly');
                    $credit->addChild('amount', $amount);
                    if ($tax > 0) {
                        $credittax = $credit->addChild('tax');
                        $credittax->addChild('amount', $tax);
                    }
                    break;
            }
        } else {
            $logger->info('02');
            switch ($callby) {
                case 'refund':
                    $logger->info('02a');
                    $credit = $transaction->addChild('profileTransRefund');
                    $credit->addChild('amount', $amount);
                    break;
                case 'void':
                    $logger->info('02b');
                    $credit = $transaction->addChild('profileTransVoid');
                    break;
                default:
                    $logger->info('02c');
                    $credit = $transaction->addChild('profileTransAuthCapture');
                    $credit->addChild('amount', $amount);
                    if ($tax > 0) {
                        $credittax = $credit->addChild('tax');
                        $credittax->addChild('amount', $tax);
                    }
                    break;
            }
        }
        $credit->addChild('customerProfileId', $CustomerProfileID);
        $credit->addChild('customerPaymentProfileId', $PaymentProfileID);

        if ($this->getPaymentAction() == 'authorize') {
            $logger->info('03');
            switch ($callby) {
                case 'captureonly':
                    $logger->info('03a');
                    $credit->addChild('approvalCode', $Approval);
                    break;
                case 'capture':
                    $logger->info('03b');
                    $credit->addChild('transId', $authtransID);
                    break;
                case 'refund':
                    $logger->info('03c');
                    $credit->addChild('transId', $authtransID);
                    break;
                case 'void':
                    $logger->info('03d');
                    $credit->addChild('transId', $authtransID);
                    break;
                default:
                    $logger->info('03e');
                    if (isset($ShippingProfileID) AND ($ShippingProfileID > 0)) {
                        $credit->addChild('customerShippingAddressId', $ShippingProfileID);
                    }
                    $order = $credit->addChild('order');
                    $order->addChild('invoiceNumber', $invoiceno);
                    if ($cvv != 111) {
                        $credit->addChild('cardCode', $cvv);
                    }
                    break;
            }
        } else {
            $logger->info('04');
            switch ($callby) {
                case 'refund':
                    $logger->info('04a');
                    $credit->addChild('transId', $authtransID);
                    break;
                case 'void':
                    $logger->info('04b');
                    $credit->addChild('transId', $authtransID);
                    break;
                default:
                    $logger->info('04c');
                    if (isset($ShippingProfileID) AND ($ShippingProfileID > 0)) {
                        $credit->addChild('customerShippingAddressId', $ShippingProfileID);
                    }
                    $order = $credit->addChild('order');
                    $order->addChild('invoiceNumber', $invoiceno);
                    break;
            }
        }
        $TxRqXML = $TxRq->asXML();
        return $TxRqXML;
    }

    public function void(Varien_Object $payment)
    {
        $logger = Mage::helper('binaryanvil_authorize');
        $logger->info(__CLASS__ . '::' . __METHOD__, true);

        if (!Mage::getSingleton('binaryanvil_authorize/config')->canCustomAuthorize()) {
            $logger->info("using native CIM void()");
            parent::void($payment);
            return;
        }

        $logger->info("entering void()");

        //$this->setAmount($amount)
        //	->setPayment($payment);
        $result = $this->_call($payment, 'void', 99999);
        $logger->info(var_export($result, TRUE));
        if ($result === false) {
            $e = $this->getError();
            if (isset($e['message'])) {
                $message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
            } else {
                $message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
            }
            Mage::throwException($message);
        } else {
            // Check if there is a gateway error
            if ($result['Status']['statusCode'] == "Ok") {
                // Check if there is an error processing the credit card
                if ($result['Status']['code'] == "I00001") {
                    $cancelorder = true;
                    if ($cancelorder) {
                        $payment->getOrder()->setState('canceled', true, self::STATUS_1DOLLAR_VOIDED);
                    } else {
                        $payment->registerVoidNotification();
                    }
                    $payment->setCcApproval($result['Status']['code'])
                        ->setTransactionId($result['Status']['transno'])
                        ->setCcTransId($result['Status']['transno'])
                        ->setCcAvsStatus($result['Status']['statusCode'])
                        ->setCcCidStatus($result['Status']['statusCode']);
                    $payment->setStatus(self::STATUS_VOIDED);
                }
            } else {
                Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
            }
        }
        return $this;
    }


}