<?php
class BinaryAnvil_Authorize_Model_Observer
{
    public function orderStatusHistorySaveBefore(Varien_Event_Observer $observer)
    {
        $history = $observer->getEvent()->getStatusHistory();
        if (Mage::getSingleton('binaryanvil_authorize/config')->canCustomAuthorize() && (strlen($history->getComment()) > 0)) {


            $paymentAuthorized = Mage::getModel('sales/order_payment')->load($history->getOrder()->getId(), 'parent_id');
            if (is_null($paymentAuthorized->getBaseAmountAuthorized())) {
                return;
            }
//            Mage::log('Comments History for Order : ' . $history->getOrder()->getIncrementId());
//            Mage::log('base amount authorized :' . (int)$paymentAuthorized->getBaseAmountAuthorized());

            if ($history->getComment() && (int)$paymentAuthorized->getBaseAmountAuthorized() == 1 && is_null($paymentAuthorized->getBaseAmountPaid())) {
//            if ($history->getComment() && (strpos($history->getOrder()->getIncrementId(), '-') === false) && (int) $paymentAuthorized->getBaseAmountAuthorized() == 1 ) {
//                Mage::log('change history from :' . $history->getComment());

                $pattern = '/\$[0-9|,|.]+/';
                $replace = '\$1.';
                $updatedComment = preg_replace($pattern, $replace, $history->getComment());
                $history->setComment($updatedComment);

//                Mage::log('change history to :' . $history->getComment());
            }

        }
    }



    public function orderDisplayRuleSaveBefore(Varien_Event_Observer $observer)
    {

        $request = $observer->getEvent()->getRequestModel()->getPost();
        $orderDisplayRule = array('display_original_order' => isset($request['order']['display_original_order'])?$request['order']['display_original_order']:"");

        $orderCreateModel = $observer->getEvent()->getOrderCreateModel();
        $orderCreateModel->getQuote()->addData($orderDisplayRule);


    }


}
