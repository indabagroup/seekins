<?php

class BinaryAnvil_Authorize_Model_Config
{


    /**
     * Get possibility to check Inventory in real-time on the Front-end
     */
    public function canCustomAuthorize()
    {
        return Mage::getStoreConfig( 'binaryanvil_authorize_settings/general_settings/enabled' );
    }

    public function canAuthorizeCIM()
    {
        return Mage::getStoreConfig( 'payment/authorizecimsoap/active' );
    }

    public function getMinAuthorizeAmount()
    {
        return Mage::getStoreConfig( 'binaryanvil_authorize_settings/general_settings/min_auth_amount' );
    }

    public function canFullAuthorizeOnEdit()
    {
        return Mage::getStoreConfig( 'binaryanvil_authorize_settings/general_settings/auth_on_edit' );
    }
}
