<?php
/**
 * Package: BinaryAnvil
 * Author: rbonilla
 * Date: 2/6/14
 * Time: 5:00 PM
 * 
 */

class BinaryAnvil_Authorize_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_logger = false;

    protected function _initDebug(){

            $writer = new Zend_Log_Writer_Stream($this->getLogPath());
            $logger = new Zend_Log($writer);

            $this->_logger = $logger;
    }

    public function info($mesg, $stacktrace = false) {


        $canDebug = false;
        if((Mage::getStoreConfig('payment/authorizecimsoap/test')) & (file_exists(Mage::getBaseDir() . '/var/log'))) {
            $canDebug = Mage::getStoreConfig('payment/authorizecimsoap/debug');
        }

        if($canDebug){
            if($this->_logger === false){

                $this->_initDebug();
            }
            $this->_logger->info($mesg);
            if($stacktrace){
                foreach(debug_backtrace() as $key=>$info)
                {
                    if(isset($info['file']) && isset($info['line'])){
                        $this->_logger->info("#" . $key .
                            " Called " .
                            $info['function'] .
                            " in " .
                            $info['file'] .
                            " on line " .
                            $info['line']);
                    }
                }
            }
        }
    }

    public function getLogPath() {
        return Mage::getBaseDir() . '/var/log/authorizecimsoap.log';
    }


}