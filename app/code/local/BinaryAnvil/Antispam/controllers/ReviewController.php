<?php
include_once "Mage/Review/controllers/ProductController.php";
class BinaryAnvil_Antispam_ReviewController extends Mage_Review_ProductController
{

    public function postAction()
    {
    	$post = $this->getRequest()->getPost();
    	if($post){
  			try{
	    		$error = false;
	            if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
	            	$error = true;
	            }
  				if (Zend_Validate::is(trim($post['username']), 'NotEmpty')) {
	            	$error = true;
	            }
	    
				if ($error) {
	                  throw new Exception();
	            }
// start product review post action        
				if ($data = Mage::getSingleton('review/session')->getFormData(true)) {
	            	$rating = array();
		            if (isset($data['ratings']) && is_array($data['ratings'])) {
		                $rating = $data['ratings'];
		            }
		        } else {
		            $data   = $this->getRequest()->getPost();
		            $rating = $this->getRequest()->getParam('ratings', array());
		        }
	
		        if (($product = $this->_initProduct()) && !empty($data)) {
		            $session    = Mage::getSingleton('core/session');
		            /* @var $session Mage_Core_Model_Session */
		            $review     = Mage::getModel('review/review')->setData($data);
		            /* @var $review Mage_Review_Model_Review */
		
		            $validate = $review->validate();
		            if ($validate === true) {
		                try {
		                    $review->setEntityId($review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE))
		                        ->setEntityPkValue($product->getId())
		                        ->setStatusId(Mage_Review_Model_Review::STATUS_PENDING)
		                        ->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId())
		                        ->setStoreId(Mage::app()->getStore()->getId())
		                        ->setStores(array(Mage::app()->getStore()->getId()))
		                        ->save();
		
		                    foreach ($rating as $ratingId => $optionId) {
		                        Mage::getModel('rating/rating')
		                        ->setRatingId($ratingId)
		                        ->setReviewId($review->getId())
		                        ->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId())
		                        ->addOptionVote($optionId, $product->getId());
		                    }
		
		                    $review->aggregate();
		                    $session->addSuccess($this->__('Your review has been accepted for moderation.'));
		                }
		                catch (Exception $e) {
		                    $session->setFormData($data);
		                    $session->addError($this->__('Unable to post the review.'));
		                }
		            }
		            else {
		                $session->setFormData($data);
		                if (is_array($validate)) {
		                    foreach ($validate as $errorMessage) {
		                        $session->addError($errorMessage);
		                    }
		                }
		                else {
		                    $session->addError($this->__('Unable to post the review.'));
		                }
		            }
		        }
	
		        if ($redirectUrl = Mage::getSingleton('review/session')->getRedirectUrl(true)) {
		            $this->_redirectUrl($redirectUrl);
		            return;
		        }
		        $this->_redirectReferer();
// end product review post action
  			} catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('review')->__('Unable to post the review. Please, try again later'));
                Mage::getSingleton('core/session')->setFormData($post);
                if ($redirectUrl = Mage::getSingleton('review/session')->getRedirectUrl(true)) {
		            $this->_redirectUrl($redirectUrl);
		            return;
		        }
	        	$this->_redirectReferer();
            }
    	}
    }
}
