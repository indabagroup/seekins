<?php
/**
 * @category    Fishpig
 * @package     Fishpig_Wordpress
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */
include_once "Fishpig/Wordpress/controllers/Post/CommentController.php";
class BinaryAnvil_Antispam_CommentController extends Fishpig_Wordpress_Post_CommentController
{
	/**
	 * Post a comment action
	 *
	 */
	public function postAction()
	{
		$post = $this->getRequest()->getPost();
		if($post){
  			try{
	    		$error = false;
	            if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
	            	$error = true;
	            }
  				if (Zend_Validate::is(trim($post['username']), 'NotEmpty')) {
	            	$error = true;
	            }
	    
				if ($error) {
	                  throw new Exception();
	            }
				if ($this->isEnabledForStore()) {
					$challenge = $this->getRequest()->getPost('recaptcha_challenge_field');
					$field = $this->getRequest()->getPost('recaptcha_response_field');
		
					if ($post = $this->getPost()) {
						$data = new Varien_Object($this->getRequest()->getPost());
			
						try {
							Mage::getSingleton('wordpress/session')->setPostCommentData($post, $data->getAuthor(), $data->getEmail(), $data->getUrl(), $data->getComment());
							
							if (Mage::helper('wordpress/plugin_recaptcha')->isEnabled()) {
								if (!Mage::helper('wordpress/plugin_recaptcha')->isValidValue($challenge, $field, true)) {
									throw new Exception($this->getCaptchaErrorMessage());
								}
							}
			
							$comment = $post->postComment($data->getAuthor(), $data->getEmail(), $data->getUrl(), $data->getComment(), $data->getExtra());
							
							if (!$comment) {
								throw new Exception($this->getCommentErrorMessage());		
							}
				
							Mage::getSingleton('wordpress/session')->removePostCommentData($post);
							Mage::getSingleton('core/session')->addSuccess($this->__($this->getCommentSuccessMessage()));
						}
						catch (Exception $e) {
							Mage::getSingleton('core/session')->addError($this->__($e->getMessage()));
						}
				
						$this->_redirectUrl($post->getPermalink());
					}
					else {
						$this->_forward('noRoute');
					}
				}
				else {
					$this->_forward('noRoute');
				}
  			} catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('review')->__('Unable to post the review. Please, try again later'));
                Mage::getSingleton('core/session')->setFormData($post);
                if ($redirectUrl = Mage::getSingleton('wordpress/session')->getRedirectUrl(true)) {
		            $this->_redirectUrl($redirectUrl);
		            return;
		        }
	        	$this->_redirectReferer();
            }
		}else{
			$this->_forward('noRoute');
		}
	}
}
