<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/28/14
 * Time: 4:09 PM
 */
require_once Mage::getModuleDir('controllers', 'Ebizmarts_MageMonkey').DS.'SignupController.php';
 class BinaryAnvil_Newsletter_SignupController extends Ebizmarts_MageMonkey_SignupController
 {
     /**
      * Perform saving operation, update grouping and subscribe/unsubscribe operations
      */
     public function saveadditionalAction()
     {
         if($this->getRequest()->isPost()){

             $loggedIn = Mage::helper('customer')->isLoggedIn();
             $guestEmail = $this->getRequest()->getPost('monkey_email');

             if(!$loggedIn && !Zend_Validate::is($guestEmail, 'EmailAddress')){
                 Mage::getSingleton('core/session')
                     ->addError($this->__('Please specify a valid email address.'));
                 $this->_redirect($this->_getRedirectPath());
                 return;
             }

             Mage::helper('monkey')->handlePost($this->getRequest(), $guestEmail);

             /*if(!$loggedIn){
                 Mage::getSingleton('core/session')
                     ->addSuccess($this->__('Thanks for your subscription!'));
             }*/
         }

         $this->_redirect($this->_getRedirectPath());
     }
 }