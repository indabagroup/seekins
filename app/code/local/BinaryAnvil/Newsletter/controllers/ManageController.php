<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/28/14
 * Time: 3:37 PM
 */

require_once Mage::getModuleDir('controllers', 'Mage_Newsletter').DS.'ManageController.php';

class BinaryAnvil_Newsletter_ManageController extends Mage_Newsletter_ManageController
{
    public function saveAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('customer/account/');
        }
        try {
            $helper = Mage::helper('binaryanvil_newsletter');
            $subscribeMessage = $helper->subscribeMessage();
            $unSubscribeMessage = $helper->unSubscribeMessage();
            $confirmationSubscribeMessage = $helper->confirmationSubscribeMessage();
            $email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
            Mage::getSingleton('customer/session')->getCustomer()
                ->setStoreId(Mage::app()->getStore()->getId())
                ->setIsSubscribed((boolean)$this->getRequest()->getParam('is_subscribed', false))
                ->save();
            $confirmation = false;
            if( !Mage::helper('monkey')->isAdmin() && (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_CONFIRMATION_FLAG, Mage::app()->getStore()->getId()) == 1) ) {
                $confirmation = TRUE;
            }
            if ((boolean)$this->getRequest()->getParam('is_subscribed', false)) {

                if ($confirmation) {
                    if (!empty($confirmationSubscribeMessage)) {
                        Mage::getSingleton('customer/session')->addSuccess($this->__($confirmationSubscribeMessage));
                    }
                } elseif (!empty($subscribeMessage)) {
                    Mage::getSingleton('customer/session')->addSuccess($this->__($subscribeMessage));
                } else {
                    Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been saved.'));
                }

            } else {
                if (empty($unSubscribeMessage)) {
                    Mage::getSingleton('customer/session')->addSuccess($this->__('The subscription has been removed.'));
                } else {
                    Mage::getSingleton('customer/session')->addSuccess($this->__($unSubscribeMessage));
                }

            }
        }
        catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError($this->__('An error occurred while saving your subscription.'));
        }
        $this->_redirect('customer/account/');
    }
}