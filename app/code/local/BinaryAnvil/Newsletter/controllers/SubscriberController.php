<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Newsletter subscribe controller
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once Mage::getModuleDir('controllers', 'Mage_Newsletter').DS.'SubscriberController.php';
class BinaryAnvil_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController
{

    /**
     * New subscription action
     */
    public function newAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $email              = (string) $this->getRequest()->getPost('email');

            $helper = Mage::helper('binaryanvil_newsletter');
            $subscribeMessage = $helper->subscribeMessage();
            $confirmationSubscribeMessage = $helper->confirmationSubscribeMessage();
            try {
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    Mage::throwException($this->__('Please enter a valid email address.'));
                }

                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                    !$customerSession->isLoggedIn()) {
                    Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                }

                $ownerId = Mage::getModel('customer/customer')
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email)
                    ->getId();
                if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                    Mage::throwException($this->__('This email address is already assigned to another user.'));
                }

                $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                    if (empty($confirmationSubscribeMessage)) {
                        $session->addSuccess($this->__('Confirmation request has been sent.'));
                    } else {
                        $session->addSuccess($this->__($confirmationSubscribeMessage));
                    }
                }
                else {
                    if (empty($subscribeMessage)) {
                        $session->addSuccess($this->__('Thank you for your subscription.'));
                    } else {
                        $session->addSuccess($this->__($subscribeMessage));
                    }
                }
            }
            catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
            }
            catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription.'));
            }
        }
        $this->_redirectReferer();
    }
    /*public function newAction()
    {
        $response = array();
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email') && $this->getRequest()->getPost('fname') && $this->getRequest()->getPost('lname')) {
            $session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $email              = (string) $this->getRequest()->getPost('email');

            try {
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    if (!$this->getRequest()->getParam('isAjax', false))Mage::throwException($this->__('Please enter a valid email address.'));
                    if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('Please enter a valid email address.');
                }

                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && 
                    !$customerSession->isLoggedIn()) {
                    if (!$this->getRequest()->getParam('isAjax', false))Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                    if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl());
                }

                $ownerId = Mage::getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($email)
                        ->getId();
                if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                    if (!$this->getRequest()->getParam('isAjax', false))Mage::throwException($this->__('This email address is already assigned to another user.'));
                    if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('This email address is already assigned to another user.');
                }

                $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                    if (!$this->getRequest()->getParam('isAjax', false))$session->addSuccess($this->__('Confirmation request has been sent.'));
                    if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('Check your e-mail now and confirm your subsciption.');
                }
                else {
                    if (!$this->getRequest()->getParam('isAjax', false))$session->addSuccess($this->__('Thank you for your subscription.'));
                    if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('Check your e-mail now and confirm your subsciption.');
                }
            }
            catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
                if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('There was a problem with the subscription: %s', $e->getMessage());
            }
            catch (Exception $e) {
                if (!$this->getRequest()->getParam('isAjax', false))$session->addException($e, $this->__('There was a problem with the subscription.'));
                 if ($this->getRequest()->getParam('isAjax', false))$response['message'] = $this->__('There was a problem with the subscription.');
            }
        }else {
            if (!$this->getRequest()->getParam('isAjax', false))Mage::throwException($this->__('Invalid subscription.  Please be sure to fill-in all required fields'));
                    $response['message'] = $this->__('Invalid subscription.  Please be sure to fill-in all required fields');
        }
        if (!$this->getRequest()->getParam('isAjax', false)) {
            $this->_redirectReferer();
        }else{
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        }
    }*/

}
