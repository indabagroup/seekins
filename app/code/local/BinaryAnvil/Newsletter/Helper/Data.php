<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/28/14
 * Time: 3:39 PM
 */

class BinaryAnvil_Newsletter_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_SUBSCRIBE_MESSAGE = 'binaryanvil_newsletter/settings/subscribe_message';
    const XML_PATH_UNSUBSCRIBE_MESSAGE = 'binaryanvil_newsletter/settings/unsubscribe_message';
    const XML_PATH_SUBSCRIBE_MESSAGE_CONFIRMATION = 'binaryanvil_newsletter/settings/subscribe_message_confirmation';

    public function subscribeMessage()
    {
        return Mage::getStoreConfig(self::XML_PATH_SUBSCRIBE_MESSAGE);
    }

    public function unSubscribeMessage()
    {
        return Mage::getStoreConfig(self::XML_PATH_UNSUBSCRIBE_MESSAGE);
    }

    public function confirmationSubscribeMessage()
    {
        return Mage::getStoreConfig(self::XML_PATH_SUBSCRIBE_MESSAGE_CONFIRMATION);
    }
}