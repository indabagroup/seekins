<?php

class BinaryAnvil_Tierpricing_Model_Observer {

    public function updateItemsAfter(Varien_Event_Observer $observer) {
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote();
        $allItems = $quotes->getAllItems();

        foreach ($allItems as $quote) {
            $product = Mage::getModel('catalog/product')->load($quote->getProduct()->getId());

            $casePack = $product->getAttributeText('order_qty');
            $orderedQty = $quote->getQty();
            $percentDiscount = $this->getDiscountPercentage($casePack, $orderedQty);

            $new_price = (float) $quote->getProduct()->getPrice() * ($percentDiscount / 100);
            $discountedPrice = (float) $quote->getProduct()->getPrice() - $new_price;
            $quote->setOriginalCustomPrice($discountedPrice);
        }

        $quotes->save();
    }

    public function addToCart(Varien_Event_Observer $observer) {
        $event = $observer->getEvent();
        $casePack = $event->getQuoteItem()->getProduct()->getAttributeText('order_qty');
        $orderedQty = $event->getQuoteItem()->getProduct()->getQty();

        $quote_item = $event->getQuoteItem();

        $percentDiscount = $this->getDiscountPercentage($casePack, $orderedQty, $quote_item->getProduct());

        $new_price = (float) $quote_item->getProduct()->getPrice() * ($percentDiscount / 100);
        $discountedPrice = (float) $quote_item->getProduct()->getPrice() - $new_price;
        $quote_item->setOriginalCustomPrice($discountedPrice);
        $quote_item->save();
    }

    public function getDiscountPercentage($casePack, $orderedQty, $_product) {
        if ($casePack == 100) {
            $tierPrices =
                    array(
                        10 => 3,
                        20 => 6,
                        30 => 9,
                        40 => 12,
                        50 => 15,
                        75 => 18,
                        100 => 20,
                        110 => 25,
                        120 => 28,
                        130 => 31,
                        140 => 34,
                        150 => 37,
                        160 => 40,
                        170 => 43,
                        180 => 46,
                        90 => 49,
                        200 => 50
            );
        } else if ($casePack == 25) {
            $tierPrices =
                    array(
                        5 => 3,
                        10 => 6,
                        15 => 9,
                        20 => 12,
                        25 => 20,
                        30 => 25,
                        35 => 28,
                        40 => 31,
                        45 => 34,
                        50 => 37,
                        55 => 40,
                        60 => 43,
                        65 => 46,
                        70 => 49,
                        75 => 50
            );
        } else if ($casePack == 1) {
            $tierPrices =
                    array(
                        1 => 20,
                        5 => 25,
                        10 => 28,
                        15 => 31,
                        20 => 34,
                        25 => 37,
                        30 => 40,
                        35 => 43,
                        40 => 46,
                        45 => 49,
                        50 => 50
            );
        }

        $percentDiscount = 0;
        asort($tierPrices);

        foreach ($tierPrices as $key => $tierPrice) {
            if ($key <= $orderedQty) {
                $percentDiscount = $tierPrice;
            }
        }

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $tierGroup = $customer->getResource()
                    ->getAttribute('tier_group')
                    ->getFrontend()
                    ->getValue($customer);
        }
        
        
        echo $_product->getTierPlatinumDiscount();
        die;
        
        $discountPercentCP = array('platinum' => 30, 'gold' => 25, 'silver' => 15, 'bronze' => 0);
        $discountPercentPartialCP = array('platinum' => 25, 'gold' => 20, 'silver' => 10, 'bronze' => 0);

        if($orderedQty<$casePack){
            $percentDiscount = $discountPercentPartialCP[strtolower($tierGroup)];
        }else{
            $percentDiscount = $discountPercentCP[strtolower($tierGroup)];
        }
        
        return $percentDiscount;
    }

}