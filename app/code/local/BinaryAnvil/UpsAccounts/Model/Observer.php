<?php

class BinaryAnvil_UpsAccounts_Model_Observer {

    public function refreshCustomerSegment(Varien_Event_Observer $observer){
        $backendSegmentID = Mage::getStoreConfig('binaryanvil_upsaccounts/settings/segment_id');
        $segment = Mage::getSingleton('enterprise_customersegment/segment')->load($backendSegmentID);
        $segment->matchCustomers();
    }

    public function refreshCustomerSegmentCron(){
        $backendSegmentID = Mage::getStoreConfig('binaryanvil_upsaccounts/settings/segment_id');
        $segment = Mage::getSingleton('enterprise_customersegment/segment')->load($backendSegmentID);
        $segment->matchCustomers();
    }

}