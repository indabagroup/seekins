<?php

class BinaryAnvil_UpsAccounts_Helper_Data extends Mage_Core_Helper_Abstract {


    const XML_PATH_UPSACCOUNT_SUBOTAL_MINIMUM = 'binaryanvil_upsaccounts/settings/subtotal_minimum';
    const XML_PATH_UPSACCOUNT_RULE_ID         = 'binaryanvil_upsaccounts/settings/rule_id';
    const XML_PATH_USA_UPS_MINORDERVALUE      = 'carriers/ups/min_order_value';

    public function freeUpsShipping(){
        if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return false;
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $upsAccountNumber = $customerData->getData('sp_ups_account_number');
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $backendRuleID = Mage::getStoreConfig(self::XML_PATH_UPSACCOUNT_RULE_ID);
        $subtotalMinimum = Mage::getStoreConfig(self::XML_PATH_UPSACCOUNT_SUBOTAL_MINIMUM);
        $freeUpsShipping = false;
        $appliedRuleIds = Mage::getSingleton('checkout/session')->getQuote()->getAppliedRuleIds();
        $appliedRuleIds = explode(',', $appliedRuleIds);
        $rules =  Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id' , array('in' => $appliedRuleIds));
        foreach ($rules as $rule) {
            $ruleId = $rule->getRuleId();
            if($ruleId == $backendRuleID && $quote->getSubtotal() >= $subtotalMinimum && !empty($upsAccountNumber)){
                $freeUpsShipping = true;
            }
        }
        return $freeUpsShipping;
    }

    function customerHasUPSAccountAndSubtotalIsLessThanOnehundred(){
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $upsAccountNumber = $customerData->getData('sp_ups_account_number');
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $subtotalMinimum = Mage::getStoreConfig(self::XML_PATH_UPSACCOUNT_SUBOTAL_MINIMUM);
        if($quote->getSubtotal() < $subtotalMinimum && !empty($upsAccountNumber)){
            return true;
        }
        return false;
    }

    public function customerHasUPSAccountAndShouldBeRemovingUPSMethod(){ //lol method name
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $subtotalMinimum = Mage::getStoreConfig(self::XML_PATH_USA_UPS_MINORDERVALUE);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $upsAccountNumber = $customerData->getData('sp_ups_account_number');
        return !empty($upsAccountNumber) && $quote->getSubtotal() < $subtotalMinimum;
    }

}