<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 12/30/13
 * Time: 12:50 PM
 */
class BinaryAnvil_CartRemoveSelected_Model_Observer
{
    public function removeSelectedItems(Varien_Event_Observer $observer)
    {
        //$controllerAction = $observer->getControllerAction();
        $updateAction = (string)Mage::app()->getRequest()->getParam( 'update_cart_action' );
        $items = Mage::app()->getRequest()->getPost( 'delete' );
        if ( $updateAction != 'remove_selected' || empty( $items ) ) {
            return $observer;
        }

        if ( $updateAction == 'remove_selected' && $items ) {
            $cartHelper = Mage::helper( 'checkout/cart' );
            foreach ( $items as $itemId => $data ) {
                $cartHelper->getCart()->removeItem( $itemId )->save();

            }
        }
    }
}