<?php

class BinaryAnvil_Pointsystem_Model_Customer extends Mage_Core_Model_Abstract {

    public function getCustomerGoldPoints() {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $goldpoints = Mage::getModel('enterprise_reward_model_reward')
                ->setCustomer($customer)
                ->setWebsiteId(Mage::app()->getWebsite()->getId())
                ->loadByCustomer();
        
        return $goldpoints->getPointsBalance();
    }
    
    public function getCustomerGoldPointsAmount() {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $goldpoints = Mage::getModel('enterprise_reward_model_reward')
                ->setCustomer($customer)
                ->setWebsiteId(Mage::app()->getWebsite()->getId())
                ->loadByCustomer();        
        
        return $goldpoints->getRateToCurrency()->getCurrencyAmount() * $goldpoints->getPointsBalance();
    }
    
    public function getCustomerBalance(){
        $reward = Mage::getModel('enterprise_reward/reward')
                ->setCustomer(Mage::getSingleton('customer/session')->getCustomer())
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByCustomer();

       return $reward->getData('points_balance');
    }
}