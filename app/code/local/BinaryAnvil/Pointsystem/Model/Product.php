<?php

class BinaryAnvil_Pointsystem_Model_Product extends Mage_Core_Model_Abstract {

    public function getProductGoldPoints($product) {        
        $price = $product->getPrice();
        
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $goldpoints = Mage::getModel('enterprise_reward/reward')
                ->setCustomer($customer)
                ->setWebsiteId(Mage::app()->getWebsite()->getId())
                ->loadByCustomer()
                ->getPointsEquivalent($price);        

        return $goldpoints;
    }
    
    public function hasRewardItem(){
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote();
        $items = $quotes->getAllItems();        
        
        foreach ($items as $item) {
            if(in_array(13, $item->getProduct()->getCategoryIds())){
                return true;
            }            
        }
        
        return false;
    }
    
    public function getTotalRewardPrice(){
        $cart = Mage::getSingleton('checkout/cart');
        $quotes = $cart->getQuote();
        $items = $quotes->getAllItems();    
        
        $total_price = 0;
        
        foreach ($items as $item) {
            if(in_array(13, $item->getProduct()->getCategoryIds())){
                $total_price += $item->getProduct()->getPrice() * $item->getQty();
            }            
        }
        
        return $total_price*-1;
    }

}