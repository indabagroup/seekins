<?php

class BinaryAnvil_Pointsystem_Model_Observer {
	
    public function removeRewardItem(Varien_Event_Observer $observer){
    	$item = $observer->getEvent()->getQuoteItem();
    	$checkoutSession = Mage::getSingleton('checkout/session');
    	
    	$itemId = $item->getProduct()->getId();
    	
    	$customerSession = Mage::getSingleton('customer/session');
    	$currentGoldPointItems = $customerSession->getGoldPointItems();
    	
    	unset($currentGoldPointItems[$itemId]);
    	
    	if(count($currentGoldPointItems)>0){
    		$customerSession->setGoldPointItems($currentGoldPointItems);
    	}else{
    		
    		$dataArr = $customerSession->getData();
    		unset($dataArr['gold_point_items']);
    		$customerSession->setData($dataArr);
    		$checkoutSession->getQuote()->setUseRewardPoints(false)->save();
    	}
    }
    
    public function useRewards(Varien_Event_Observer $observer){
    	
    	if(Mage::helper('pointsystem')->hasRewardItems()){
    		
    		$checkoutSession = Mage::getSingleton('checkout/session');
    		
    		if(!$checkoutSession->getQuote()->getUseRewardPoints()){
    			Mage::log('BinaryAnvil_Pointsystem_Model_Observer::setUseRewardPoints()');
    			$checkoutSession->getQuote()->setUseRewardPoints(true)->collectTotals()->save();
    			
    		}
    	}
    	
//		$totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
//    	Mage::log(get_class($totals));
    	
    }

}