<?php
class BinaryAnvil_Pointsystem_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getTotalRequiredPoints(){
		$itemRedeemingPoints = Mage::getModel('customer/session')->getGoldPointItems();
		
		$totalRedeemingPoints = 0;
		foreach($itemRedeemingPoints as $itemProductId => $redeemingPoints){
			$totalRedeemingPoints += $redeemingPoints;
		}
		
		return $totalRedeemingPoints; 
	}
	
	public function hasRewardItems(){
		$result = false;
		
    	$rewardCategoryId = 13; 
    			
    	$checkoutSession = Mage::getSingleton('checkout/session');
    	foreach($checkoutSession->getQuote()->getAllItems() as $item){
    		if (in_array($rewardCategoryId, $item->getProduct()->getCategoryIds())){
    			return true;
    		}
    	}

//		if(count(Mage::getSingleton('customer/session')->getCustomer()->getGoldPointItems()) >0){
//			$result=true;
//		}
		
    	return $result;
	}	
	
	public function recalculateTotals($totals){
		
//		$customerSession = Mage::getSingleton('customer/session');
//		if(!$customerSession->isLoggedIn())return;
		
		if(!$this->hasRewardItems())return;
		
		$requiredData = array('subtotal','grand_total');
		
		$result = true;
		foreach($requiredData as $key){
			if(!array_key_exists($key,$totals))
				$result = $result && false;
		}
		
		if($result){
			$subtotal = $totals['subtotal'];  
			$grandtotal = $totals['grand_total'];
			if(array_key_exists('reward',$totals))
				$reward = $totals['reward'];
			
			$rewardTotal = 0;
			if(!array_key_exists('reward_points',$totals)){	
				$items =  $subtotal->getAddress()->getQuote()->getItemsCollection();
				$category = Mage::getModel('catalog/category')->load(13); //Gi brick rewards category
				$_rewardItems = $category->getProductCollection();
				
		        $rewardItems = array();
		        foreach($_rewardItems as $_rewardItem){
		        	$rewardItems[] = $_rewardItem->getId();
				}
		            	
		        
		        foreach($items as $item){
		            if(in_array($item->getProductId(), $rewardItems)){
		            	$rewardTotal += $item->getPrice()* $item->getQty();
		            }
	           	}
			}else{
				$rewardDiscount = $totals['reward_points'];
				$rewardTotal = $rewardDiscount->getValue() * -1;
			}
			
			
			$subtotal->setValue($subtotal->getValue()- $rewardTotal);
			if(array_key_exists('reward',$totals))
				$grandtotal->setValue($grandtotal->getValue()- $rewardTotal - $reward->getValue());
			else{
				if(!array_key_exists('reward_points',$totals))
					$grandtotal->setValue($grandtotal->getValue()- $rewardTotal);
			} 
				
			
			$totals['subtotal'] = $subtotal;
			$totals['grand_total'] = $grandtotal;
			
			return $totals; 
		}
		
		return false;		
	}
}