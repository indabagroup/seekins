<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Html page block
 *
 * @category   Mage
 * @package    Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class BinaryAnvil_Page_Block_Html_Header extends Mage_Page_Block_Html_Header
{
	
    public function getWelcome()
    {
//    	die("here");
        if (empty($this->_data['welcome'])) {
            if (Mage::isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {
            	$customer_account = $this->__("%s's Account", Mage::helper('customer')->getCustomer()->getFirstname());
            	$customer_account = "<a class='customer-name mods' href='".Mage::helper('customer')->getAccountUrl()."'>".$customer_account."</a>";
                $this->_data['welcome'] = $this->__('Welcome Back Soldier, %s', $customer_account);
            } 
            else {
                $this->_data['welcome'] = "";
            }
        }

        return $this->_data['welcome'];
    }
}
