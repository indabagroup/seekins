<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/22/14
 * Time: 5:49 PM
 */

class BinaryAnvil_Customer_Model_Adminhtml_Observer
{
    protected function _addCompanyColumn(Mage_Adminhtml_Block_Customer_Grid $block)
    {
        $helper = Mage::helper('binaryanvil_customer');

        // Add the attribute as a column to the grid
        $block->addColumnAfter(
            'billing_company',
            array(
                'header' => $helper->__('Company'),
                'width'     =>  '150',
                'index'     =>  'billing_company',
            ),
            'group'
        );

        // Set the new columns order.. otherwise our column would be the last one
        $block->sortColumnsByOrder();
    }

    public function coreBlockAbstractPrepareLayoutAfter(Varien_Event_Observer $observer)
    {
        if (Mage::app()->getRequest()->getControllerName() !== 'customer') {
            return;
        }

        $block = $observer->getBlock();
        if ($block->getType() === 'adminhtml/customer_grid') {
            // I don't think we need to limit applying the column by action
            //$action = Mage::app()->getRequest()->getActionName();
            //if (in_array($action, array('grid', 'index', 'exportCsv', 'exportXml'))) {

            $this->_addCompanyColumn($block);

            //}
        }
    }

    public function eavCollectionAbstractLoadBefore(Varien_Event_Observer $observer)
    {
        if (Mage::app()->getRequest()->getControllerName() !== 'customer') {
            return;
        }

        $collection = $observer->getEvent()->getCollection();

        // Only add attribute to customer collections
        $customerTypeId = Mage::getSingleton('eav/config')->getEntityType('customer')->getId();
        $collectionTypeId = $collection->getEntity()->getTypeId();
        Mage::log($customerTypeId."-".$collectionTypeId);
        if ($customerTypeId == $collectionTypeId) {

            $collection->joinAttribute('billing_company', 'customer_address/company', 'default_billing', null, 'left');
            Mage::log($collection->getSelect()->__toString());

        }
    }
}