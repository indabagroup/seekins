<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/8/14
 * Time: 6:11 PM
 */

class BinaryAnvil_Customer_Model_Observer
{
    public function controllerActionPredispatchCustomerAccountEditPost()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        //Mage::log(Mage::app()->getRequest()->getParams());
        //die();
        /*$_custom_address = array (
            'firstname' => 'Branko',
            'lastname' => 'Ajzele',
            'street' => array (
                '0' => 'Sample address part1',
                '1' => 'Sample address part2',
            ),
            'city' => 'Osijek',
            'region_id' => '',
            'region' => '',
            'postcode' => '31000',
            'country_id' => 'HR',
            'telephone' => '0038531555444',
        );*/
        $address = Mage::getModel('customer/address');
        $addressId = Mage::app()->getRequest()->getParam('id');
        if ($addressId) {
            $existsAddress = $customer->getAddressById($addressId);
            if ($existsAddress->getId() && $existsAddress->getCustomerId() == $customer->getId()) {
                $address->setId($existsAddress->getId());
            }
        }

        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
            ->setEntity($address);
        $addressData    = $addressForm->extractData(Mage::app()->getRequest());
        $addressForm->compactData($addressData);
        $address->setCustomerId($customer->getId())
            ->setIsDefaultBilling(Mage::app()->getRequest()->getParam('default_billing', false))
            ->setIsDefaultShipping(Mage::app()->getRequest()->getParam('default_shipping', false));
        try {
            $address->save();
        }
        catch (Exception $ex) {
            //Zend_Debug::dump($ex->getMessage());
        }
    }
}