<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 1/22/14
 * Time: 5:32 PM
 */

class BinaryAnvil_Customer_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid
{
    protected function _preparePage()
    {
        $this->getCollection()
            ->joinAttribute('billing_company', 'customer_address/company', 'default_billing', null, 'left');
        return parent::_preparePage();
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('billing_company', array(
            'header'=> Mage::helper('binaryanvil_customer')->__('Company'),
            'index' => 'billing_company',
            'type'  => 'text',
            'width' => '100px',
        ));
        $this->addColumnsOrder( 'billing_company', 'group' );

        return parent::_prepareColumns();
    }

}