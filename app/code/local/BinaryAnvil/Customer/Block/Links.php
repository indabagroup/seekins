<?php
class BinaryAnvil_Customer_Block_Links extends Mage_Checkout_Block_Links
{
/**
ads my account link to some block
*/
    public function addAccountLink()
    {
        $parentBlock = $this->getParentBlock();//this block will have a parent
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('Mage_Customer')) {//check if the customer module is not disabled
//            $customerSession = Mage::getSingleton('customer/session');//the customer session
//				Mage::helper('customer')->getCustomerName();
 
//            if( Mage::helper('customer')->isLoggedIn() ) {//if the customer is logged in get the name
//                $text = $this->__("%s's Account", Mage::helper('customer')->getCustomerName());
//                $text = $this->__("%s's Account", Mage::helper('customer')->getCustomerName());
//            } else {//if the customer is not logged in print 'My account'
//                $text = Mage::helper('customer')->__('My Account');
//            }
        if( Mage::helper('customer')->isLoggedIn() ) {//if the customer is logged in get the name
                $text = $this->__("%s's Account", Mage::helper('customer')->getCustomer()->getFirstname());
//                $text = $this->__("%s's Account", Mage::helper('customer')->getCustomerName());
            } else {//if the customer is not logged in print 'My account'
                $text = Mage::helper('customer')->__('My Account');
            }
//add the block to the set of links.
//            $parentBlock->addLink($text, Mage::helper('customer')->getAccountUrl(), $text, false, array(), 100, null, '');
//addLink($label, $url='', $title='', $prepare=false, $urlParams=array(),$position=null, $liParams=null, $aParams=null, $beforeText='', $afterText='')
            $parentBlock->addLink($text, Mage::helper('customer')->getAccountUrl(), $text, false, array(),100, 'id="welcome-customer-message"', 'class="customer-name"', 'Welcome Back Soldier ','');
        }
        return $this;
    }
    
	public function addCartLink()
    {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('Mage_Checkout')) {
            $count = $this->getSummaryQty() ? $this->getSummaryQty()
                : $this->helper('checkout/cart')->getSummaryCount();
//            if ($count == 1) {
//                $text = $this->__('My Cart (%s item)', $count);
//            } elseif ($count > 0) {
//                $text = $this->__('My Cart (%s items)', $count);
//            } else {
                $text = $this->__('My Cart');
//            }

            $parentBlock->removeLinkByUrl($this->getUrl('checkout/cart'));
            $parentBlock->addLink($text, 'checkout/cart', $text, true, array(), 50, null, 'class="top-link-cart"');
        }
        return $this;
    }
}