<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/18/14
 * Time: 8:01 PM
 */
require_once 'Mage/Customer/controllers/AddressController.php';
class BinaryAnvil_Storelocator_AddressController extends Mage_Customer_AddressController
{
    public function formPostAction()
    {
        //die("here");
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }
        // Save data
        if ($this->getRequest()->isPost()) {
            $customer = $this->_getSession()->getCustomer();
            /* @var $address Mage_Customer_Model_Address */
            $address  = Mage::getModel('customer/address');
            $addressId = $this->getRequest()->getParam('id');
            if ($addressId) {
                $existsAddress = $customer->getAddressById($addressId);
                if ($existsAddress->getId() && $existsAddress->getCustomerId() == $customer->getId()) {
                    $address->setId($existsAddress->getId());
                }
            }

            $errors = array();

            /* @var $addressForm Mage_Customer_Model_Form */
            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('customer_address_edit')
                ->setEntity($address);
            $addressData    = $addressForm->extractData($this->getRequest());
            $addressErrors  = $addressForm->validateData($addressData);
            if ($addressErrors !== true) {
                $errors = $addressErrors;
            }

            try {
                //var_dump($addressData);
                $addressForm->compactData($addressData);
                $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
                    ->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
                //die;
                $addressErrors = $address->validate();
                if ($addressErrors !== true) {
                    $errors = array_merge($errors, $addressErrors);
                }
                /*start - check if company field is empty*/
                if($this->getRequest()->getParam('default_store_locator')) {

                    if (!Zend_Validate::is($this->getRequest()->getParam('company'), 'NotEmpty')) {
                        $errors[] = Mage::helper('customer')->__('Please enter the company name.');
                    }
                }
                /*end - check if company field is empty*/
                if (count($errors) === 0) {
                    $address->save();
                    $currentAddressId = $address->getId();
                    if($this->getRequest()->getParam('default_store_locator')) {
                        /* start code when new default store locator address will be added*/
                        $customerModel = Mage::getModel('customer/customer')->load($customer->getId());
                        $customerModel->setDefaultStorelocation($currentAddressId);
                        $dealerStatus = $this->getRequest()->getParam('publish');
                        try {
                            $this->syncCustomerDealerAddress($address, $dealerStatus);
                            $customerModel->save();
                        } catch(Exception $e) {
                            Mage::logException($e);
                        }
                        /* end code when new default store locator address will be added*/
                    }

//                    if ($this->getRequest()->getParam('id')) {
//                        $default_store_location = $customer->getData('default_storelocation');
//                        if ($default_store_location == $this->getRequest()->getParam('id')) {
//                            if ($this->getRequest()->getParam('change_store_locator')) {
//                                if($this->getRequest()->getParam('default_store_locator')) {
//                                    if($this->getRequest()->getParam('publish') || $this->getRequest()->getParam('publish') == 1) {
//                                        $dealerStatus = $this->getRequest()->getParam('publish');
//                                        $customerModel = Mage::getModel('customer/customer')->load($customer->getId());
//                                        $customerModel->setDefaultStorelocation($currentAddressId)
//                                            ->setSpDisableAsDealerLocator(1);
//
//                                        try {
//                                            $this->syncCustomerDealerAddress($address, $dealerStatus);
//                                            $customerModel->save();
//                                        } catch(Exception $e) {
//                                            echo "{$e}";
//                                        }
//                                    } else {
//                                        $dealerStatus = $this->getRequest()->getParam('publish');
//                                        $customerModel = Mage::getModel('customer/customer')->load($customer->getId());
//                                        $customerModel->setDefaultStorelocation($currentAddressId)
//                                            ->setSpDisableAsDealerLocator(2);
//                                        try {
//                                            $this->syncCustomerDealerAddress($address, $dealerStatus);
//                                            $customerModel->save();
//                                        } catch(Exception $e) {
//                                            echo "{$e}";
//                                        }
//                                    }
//                                }
//                            } else {
//                                // start code to update dealer locator address when address is edited only
//                                try {
//                                    $dealerStatus = $this->getRequest()->getParam('publish');
//                                    $this->syncCustomerDealerAddress($address, $dealerStatus);
//                                } catch(Exception $e) {
//                                    echo "{$e}";
//                                }
//                                // start code to update dealer locator address when address is edited only
//                            }
//                        } else {
//
//                        } //if ($default_store_location == $this->getRequest()->getParam('id')) {
//
//                    } else {
//                        /* start code when new default store locator address will be added*/
//                        if($this->getRequest()->getParam('default_store_locator')) {
//                            if($this->getRequest()->getParam('publish') || $this->getRequest()->getParam('publish') == 1) {
//                                $dealerStatus = $this->getRequest()->getParam('publish');
//                                $customerModel = Mage::getModel('customer/customer')->load($customer->getId());
//                                $customerModel->setDefaultStorelocation($currentAddressId)
//                                    ->setSpDisableAsDealerLocator(1);
//
//                                try {
//                                    $this->syncCustomerDealerAddress($address, $dealerStatus);
//                                    $customerModel->save();
//                                } catch(Exception $e) {
//                                    echo "{$e}";
//                                }
//                            } else {
//                                $dealerStatus = $this->getRequest()->getParam('publish');
//                                $customerModel = Mage::getModel('customer/customer')->load($customer->getId());
//                                $customerModel->setDefaultStorelocation($currentAddressId)
//                                    ->setSpDisableAsDealerLocator(2);
//                                try {
//                                    $this->syncCustomerDealerAddress($address, $dealerStatus);
//                                    $customerModel->save();
//                                } catch(Exception $e) {
//                                    echo "{$e}";
//                                }
//                            }
//                        }
//                        /*end code when new default store locator address will be added*/
//                    }


                    $this->_getSession()->addSuccess($this->__('The address has been saved.'));
                    $this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure'=>true)));
                    return;
                } else {
                    $this->_getSession()->setAddressFormData($this->getRequest()->getPost());
                    foreach ($errors as $errorMessage) {
                        $this->_getSession()->addError($errorMessage);
                    }
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->setAddressFormData($this->getRequest()->getPost())
                    ->addException($e, $e->getMessage());
            } catch (Exception $e) {
                //Mage::log($e->getTraceAsString(),null,"mylog.log");
                $this->_getSession()->setAddressFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save address.'));
            }
        }

        return $this->_redirectError(Mage::getUrl('*/*/edit', array('id' => $address->getId())));
    }

    public function deleteAction()
    {
        //die("here");
        $addressId = $this->getRequest()->getParam('id', false);

        if ($addressId) {
            $address = Mage::getModel('customer/address')->load($addressId);

            // Validate address_id <=> customer_id
            if ($address->getCustomerId() != $this->_getSession()->getCustomerId()) {
                $this->_getSession()->addError($this->__('The address does not belong to this customer.'));
                $this->getResponse()->setRedirect(Mage::getUrl('*/*/index'));
                return;
            }

            try {
                $address->delete();
                $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
                if($addressId == $customer["default_storelocation"])
                {
                    $customerModel = Mage::getModel('customer/customer')->load($this->_getSession()->getCustomerId());
                    $customerModel->setDefaultStorelocation();
                    try {
                        $customerModel->save();
                    } catch(Exception $e) {
                        echo "{$e}";
                    }
                }
                //var_dump($customer["default_storelocation"]);
                //die;
                $this->_getSession()->addSuccess($this->__('The address has been deleted.'));
            } catch (Exception $e){
                $this->_getSession()->addException($e, $this->__('An error occurred while deleting the address.'));
            }
        }
        $this->getResponse()->setRedirect(Mage::getUrl('*/*/index'));
    }

    public function syncCustomerDealerAddress($address, $dealerStatus)
    {
        $status = $dealerStatus;
        /** @var $customerAddress Mage_Customer_Model_Address */
        //$customerAddress = $this->_getSession()->getCustomer()->getAddresses();
        $customer = $this->_getSession()->getCustomer();

        if (!Mage::helper('binaryanvil_synccustomerdealer')->isEnabled($customer->getData('store_id'))) {
            return false;
        }
        if (Mage::app()->getStore()->isAdmin()) {
            $orig = $customerAddress->getOrigData();
            unset($orig['is_customer_save_transaction']);
            unset($orig['updated_at']);
            $data = $customer->getData();
            unset($data['updated_at']);
            if (array_diff($orig,$data)) {
                $this->_createDealerLocator($address, $status);
            }
        } else {
            $orig = $customer->getOrigData();
            unset($orig['is_customer_save_transaction']);
            unset($orig['updated_at']);
            $data = $customer->getData();
            unset($data['is_customer_save_transaction']);
            unset($data['updated_at']);
            unset($data['store_id']);

            $this->_createDealerLocator($address, $status);
        }
    }
    protected function _createDealerLocator($address, $status)
    {
        $stat = $status;
        $customerData = $this->_getSession()->getCustomer()->getData();
        $customerAddress = $address;
        if ($customerAddress) {
            $region_id = $customerAddress->getData('region_id');
            $regionId = isset($region_id) ? $customerAddress->getData('region_id') : $customerAddress->getData('region');
            $street1 = $customerAddress->getStreet(1);
            $street2 = $customerAddress->getStreet(2);
            $company = $customerAddress->getData('company');
            $street = isset($street2) ? $street1. " ". $street2: $street1;
            $country = Mage::getModel('directory/country')->load($customerAddress->getData('country_id'))->getName();
            $address = $street. "," . $customerAddress->getData('city'). "," .$customerAddress->getData('region'). " ". $customerAddress->getData('postcode'). "," .$country;
            $result = $this->_getLongitudeLatitude($address);

            if ($result['message'] == 'ok') {
                $name = $customerData["firstname"] . " " . $customerData["lastname"];
                $model = Mage::getModel('storelocation/storelocation')
                    ->setId($this->_checkDealer($customerData, $company))
                    ->setStoreName($customerAddress->getData('company'))
                    ->setOwnerName($name)
                    ->setStreet($street)
                    ->setCity($customerAddress->getData('city'))
                    ->setLocationRegionId($regionId)
                    ->setPostalCode($customerAddress->getData('postcode'))
                    ->setLocationCountryId($customerAddress->getData('country_id'))
                    ->setPhone($customerAddress->getData('phone'))
                    ->setUrl()
                    ->setStoreType(BinaryAnvil_SyncCustomerDealer_Helper_Data::STORE_TYPE_DEALER)
                    ->setDescription()
                    ->setGoogleLatitude($result['latitude'])
                    ->setGoogleLongitude($result['longitude'])
                    ->setGoogleZoomLevel(BinaryAnvil_SyncCustomerDealer_Helper_Data::ZOOM_LEVEL_ONE);
                try {
                    if ($stat == 1) {
                        $model->setStatus(BinaryAnvil_SyncCustomerDealer_Helper_Data::STATUS_ENABLED);
                    } else {
                        $model->setStatus(BinaryAnvil_SyncCustomerDealer_Helper_Data::STATUS_DISABLED);
                    }
                    $model->setCreatedTime(now());
                    $model->setUpdateTime(now());
                    $model->setCustomerId($customerData["entity_id"]);
                    $model->setCustomerEmail($customerData["email"]);
                    $model->save();
                    if ($this->_checkDealer($customerData, $company)) {
                        $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Dealer locator was successfully updated'));
                    } else {
                        $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Dealer locator was successfully created'));
                    }
                } catch (Exception $e) {
                    $this->_getSession()->addError('An error occurred while creating the corresponding dealer locator'.$e->getMessage());
                }
            } else {
                $this->_getSession()->addError('Dealer Locator was not created.'.$result['message']);
                $message = 'Dealer Locator was not created.'.$result['message'];
                $message .= $customerData["entity_id"];
                Mage::helper('binaryanvil_synccustomerdealer')->logError($message);
            }
        } else {
            $message = 'Dealer Locator was not created. No address';
            $this->_getSession()->addError('Dealer Locator was not created.'.$message);
            $message .= $customerData["entity_id"];
            Mage::helper('binaryanvil_synccustomerdealer')->logError($message);

        }
    }

    protected function _getLongitudeLatitude($address)
    {
        $address = urlencode($address);

        //$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=false";
        //$xml = simplexml_load_file($request_url);
        //$status = $xml->status;
        $message = '';
        $latitude = '';
        $longitude = '';
        //$isSecure = Mage::app()->getStore()->isCurrentlySecure();
        //if ($isSecure):
        $url = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false");
        $response = json_decode($url);
        $status = $response->status;
        if ($status == "OK") {
            //$latitude = $xml->result->geometry->location->lat;
            //$longitude = $xml->result->geometry->location->lng;
            $lat = $response->results[0]->geometry->location->lat;
            $long = $response->results[0]->geometry->location->lng;
            $result = array(
                'message'   => 'ok',
                'latitude'  => $lat,
                'longitude' => $long
            );
            return $result;
        } elseif ($status == "ZERO_RESULTS") {
            $message = 'No Results. Address passed maybe a non-existent.';
        } elseif ($status == "OVER_QUERY_LIMIT") {
            $message = 'Over the quota';
        } elseif ($status == "REQUEST_DENIED") {
            $message = 'Request was denied. Lack of sensor parameter';
        } elseif ($status == "INVALID_REQUEST") {
            $message = 'Invalid Request. Query is missing address';
        } elseif ($status == "UNKNOWN_ERROR") {
            $message = 'Request could not be processed due to a server error.';
        }

        return array('message' => $message, 'latitude' => $latitude, 'longitude' => $longitude);
    }

    protected function _checkDealer($customer, $storeName)
    {
        //var_dump($customer);
        //var_dump($customer["firstname"] . " " . $customer["lastname"]);
        $name = $customer["firstname"] . " " . $customer["lastname"];
        //die;
        Mage::log($name, null, 'synccustomerdealer.log', true);
        Mage::log($storeName, null, 'synccustomerdealer.log', true);

        $collection = Mage::getModel('storelocation/storelocation')
            ->getCollection()
            ->addFieldToFilter('customer_id',array('eq' => $customer["entity_id"]))
            ->addFieldToFilter('customer_email',array('eq' => $customer["email"]))
            ->getFirstItem();
        Mage::log("customer_id".$customer["entity_id"], null, 'synccustomerdealer.log', true);
        if ($collection->getId()) {
            return $collection->getId();
        } else {
            $collection = Mage::getModel('storelocation/storelocation')
                ->getCollection()
                ->addFieldToFilter('store_name',array('eq' => $storeName))
                ->addFieldToFilter('owner_name',array('eq' => $name))
                ->getFirstItem();
            Mage::log("store_name".$storeName."-".$name, null, 'synccustomerdealer.log', true);
            return $collection->getId();
        }

        return false;
    }
}

