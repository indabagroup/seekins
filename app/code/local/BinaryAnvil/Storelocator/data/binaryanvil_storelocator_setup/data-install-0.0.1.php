<?php
 /**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 7/20/14
 * Time: 12:08 PM
 */

$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();


$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->removeAttribute('customer', 'sp_disable_as_dealer_locator');

//$setup->addAttribute('customer', 'sp_storelocator_company_name', array(
//    'type' => 'varchar',
//    'input' => 'text',
//    'label' => 'Default Store Locator Company Name',
//    'global' => 1,
//    'visible' => 1,
//    'required' => 0,
//    'user_defined' => 1,
//    'default' => '0',
//    'visible_on_front' => 0,
//    'is_system' => 0,
//));
//
//$defaultUsedInForms = array(
//    /*'customer_account_create',
//    'customer_account_edit',*/
//    'adminhtml_customer'
//);
//
//$customer = Mage::getModel('customer/customer');
//$attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
//$setup->addAttributeToSet('customer', $attrSetId, 'General', 'sp_storelocator_company_name');
//
//if (version_compare(Mage::getVersion(), '1.4.2', '>=')) {
//    Mage::getSingleton('eav/config')
//        ->getAttribute('customer', 'sp_storelocator_company_name')
//        ->setData('used_in_forms', $defaultUsedInForms)
//        ->save();
//}

$installer->endSetup();
