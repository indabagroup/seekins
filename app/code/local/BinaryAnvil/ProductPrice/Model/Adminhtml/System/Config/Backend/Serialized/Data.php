<?php
/**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 4/30/2015
 * Time: 2:32 PM
 */
class BinaryAnvil_ProductPrice_Model_Adminhtml_System_Config_Backend_Serialized_Data extends Mage_Adminhtml_Model_System_Config_Backend_Serialized
{
    /*protected function _afterLoad()
    {
        if (!is_array($this->getValue())) {
            $value = $this->getValue();
            $this->setValue(empty($value) ? false : unserialize($value));
        }
    }*/
    /**
     * Unset array element with '__empty' key
     *
     */
    protected function _beforeSave()
    {
        $value = $this->getValue();
        if (is_array($value)) {
            unset($value['__empty']);
        }
        foreach ($value as $id => $fields) {
            foreach ($fields as $key => $val) {
                if (is_array($val)) {
                    $value[$id][$key] = implode(',', $val);
                }
            }
        }

        $this->setValue($value);
        parent::_beforeSave();
    }
}