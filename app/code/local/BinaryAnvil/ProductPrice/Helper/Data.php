<?php
/**
 * Created by BinaryAnvil.
 * User: calabanzas
 * Date: 2/18/2016
 * Time: 4:54 PM
 */

class BinaryAnvil_ProductPrice_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag('binaryanvil_productprice/settings/enabled');
    }
    public function getGroupedProducts($product)
    {
        $groupedProducts = Mage::getStoreConfig('binaryanvil_productprice/settings/product');
        $groupedProducts = unserialize($groupedProducts);
        //Mage::Log($product->getData());
        if (!empty($groupedProducts)) {
            foreach ($groupedProducts as $key=>$_groupedProducts) {
                if ($_groupedProducts['grouped_product'] == $product->getId()) {
                    $_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
                    $prices = array();
                    foreach($_associatedProducts as $_associatedProduct) {
                        if ($_associatedProduct->getSku() == $_groupedProducts['exclude']) continue;
                        $prices[] = $_associatedProduct->getPrice();
                    }
                    return min($prices);
                }
            }
        }
        return false;
    }
}
