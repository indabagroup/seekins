<?php
/**
 * Package: BinaryAnvil
 * User: calabanzas
 * Date: 02/18/2016
 * Time: 2:50 PM
 */

class BinaryAnvil_ProductPrice_Block_Adminhtml_System_Config_Form_Field_Product extends
    Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn( 'grouped_product', array (
            'label' => Mage::helper( 'adminhtml' )->__( 'Grouped Product' ),
            'style' => 'width:120px',
            'class' => 'required-entry validate-unique-label',
        ) );

        $this->addColumn( 'exclude', array (
            'label' => Mage::helper( 'adminhtml' )->__( 'Exclude Product (Sku)' ),
            'style' => 'width:120px',
            'class' => 'validate-unique-value',
        ) );
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper( 'adminhtml' )->__( 'Add New Product' );
        parent::__construct();
        $this->setTemplate( 'binaryanvil/productprice/system/config/form/field/array.phtml' );
    }

    /**
     * Get select block for type
     *
     */
    protected function _getTypeRenderer()
    {
        if ( !$this->_typeRenderer ) {
            $this->_typeRenderer = $this->getLayout()
                ->createBlock( 'binaryanvil_productprice/select' )
                ->setIsRenderToJsTemplate( true );
        }
        return $this->_typeRenderer;
    }

    /**
     *
     * My renderer does need parameters that are not supported by original implementation
     *
     * @param string $columnName
     * @throws Exception
     */
    protected function _renderCellTemplate($columnName)
    {

        $inputName = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';

        if ( $columnName == "grouped_product") {
            $groupedProducts = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('name')
                ->addAttributeToFilter('type_id', array('eq' => 'grouped'));
            $options = array();
            foreach ($groupedProducts as $product) {
                $options[] = array('value' => $product->getId(),
                    'label' => $product->getName());
            }
            return $this->_getTypeRenderer()
                ->setName( $inputName )
                ->setTitle( $columnName )
                ->setExtraParams( 'style="width:200px"' )
                ->setClass( 'required-entry' )
                ->setOptions(
                // Options for the second column!
                    $options )
                ->toHtml();
        }

        return parent::_renderCellTemplate( $columnName );
    }

    /**
     * Assign extra parameters to row
     *
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getTypeRenderer()->calcOptionHash(
                $row->getData( 'grouped_product' ) ),
            'selected="selected"'
        );
    }

    protected function _toHtml()
    {
        $html = parent::_toHtml();
        return $html;
    }
}