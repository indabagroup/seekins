<?php
/**
Copyright 2009-2013 Eric Levine
IDP
 */
class IDP_AuthorizeCIM_Model_authorizecimsoap extends Mage_Payment_Model_Method_Cc
{   protected $_code  = 'authorizecimsoap';
    protected $_isGateway               = true;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = true;
    protected $_canRefund               = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid                 = true;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;
    protected $_canSaveCc               = false;
    const CC_URL_LIVE = 'https://api.authorize.net/xml/v1/request.api';
	const CC_URL_TEST = 'https://apitest.authorize.net/xml/v1/request.api';
    const STATUS_APPROVED = 'Approved';
	const STATUS_SUCCESS = 'Complete';
	const PAYMENT_ACTION_AUTH_CAPTURE = 'authorize_capture';
	const PAYMENT_ACTION_AUTH = 'authorize';
    const STATUS_COMPLETED    = 'Completed';
    const STATUS_DENIED       = 'Denied';
    const STATUS_FAILED       = 'Failed';
    const STATUS_REFUNDED     = 'Refunded';
    const STATUS_VOIDED       = 'Voided';
	public function getGatewayUrl() {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url_test');
		}
		else
		{
			return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url');
		}
	}
	public function getUsername($storeId=0) {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			//return 'xxxxxxxxx';   //hard code test ID username here
			return Mage::getStoreConfig('payment/authorizecimsoap/testusername',$storeId);
		}
		else
		{
			//if((isset(Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId))) AND Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId)>'') {
				return Mage::getStoreConfig('payment/authorizecimsoap/username',$storeId);
			//} else {
				//return Mage::getStoreConfig('payment/authorizenet/login',$storeId);
			//}
		}
	}
	public function getPassword($storeId=0) {
		if(Mage::getStoreConfig('payment/authorizecimsoap/test'))
		{
			//return 'xxxxxxxxxxxxxxxxxxx';  //hard code test ID key/password herekey/password here
			return Mage::getStoreConfig('payment/authorizecimsoap/testpassword',$storeId);
		}
		else
		{
			//if((isset(Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId))) AND Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId)>'') {
				return Mage::getStoreConfig('payment/authorizecimsoap/password',$storeId);
			//} else {
				//return Mage::getStoreConfig('payment/authorizenet/trans_key',$storeId);
			//}
		}
	}
	 public function getDebug1() {
				 return true;
	 }
	public function getDebug() {
			if((Mage::getStoreConfig('payment/authorizecimsoap/test')) & (file_exists(Mage::getBaseDir() . '/var/log'))) {
				return Mage::getStoreConfig('payment/authorizecimsoap/debug'); 
			} else {
				return false;
			}
	}
	public function getTest() {
		return Mage::getStoreConfig('payment/authorizecimsoap/test');
	}
	public function getLogPath() {
		return Mage::getBaseDir() . '/var/log/authorizecimsoap.log';
	}
	public function getLiveUrl() {
		return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url');
	}
	public function getTestUrl() {
		return Mage::getStoreConfig('payment/authorizecimsoap/cgi_url_test');
	}
	public function getPaymentAction()
	{
		return Mage::getStoreConfig('payment/authorizecimsoap/payment_action');
	}
	public function getStrictCVV() {
		return true;
	}
	public function getReauth() {
		if(Mage::getStoreConfig('payment/authorizecimsoap/reauth')>0) {return true;} else {return false;}
	}
	
	public function createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $CustomerId, $billToWho, $cvv,
	$storeId=0) {
		$doc = new SimpleXMLElement('<?xml version ="1.0" encoding = "utf-8"?><createCustomerProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('profile');
		$profile->addChild('merchantCustomerId', $CustomerId);
		$profile->addChild('email', $CustomerEmail);
		$PaymentProfile = $profile->addChild('paymentProfiles');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        } else { $billTo->addChild('company',"xxxx"); }
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment1 = $PaymentProfile->addChild('payment');
		$credit = $payment1->addChild('creditCard');
		
		if(substr($ccnum,0,4!="tkn-")) {
			$credit->addChild('cardNumber', $ccnum);
		} else {
			$credit->addChild('cardNumber', "XXXX".substr($ccnum, -4, 4));
		}

		$credit->addChild('expirationDate', $ExpirationDate);
		if($cvv) {$credit->addChild('cardCode', $cvv); 
					//$profile->addChild('validationMode','liveMode');
				}
		$CustProfileXML = $doc->asXML();
		return $CustProfileXML;
	}
	public function addPaymentProfileXML($CustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerPaymentProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$PaymentProfile = $doc->addChild('paymentProfile');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        } else { $billTo->addChild('company',"xxxx"); }
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment1 = $PaymentProfile->addChild('payment');
		$credit = $payment1->addChild('creditCard');

		if(substr($ccnum,0,4!="tkn-")) {
			$credit->addChild('cardNumber', $ccnum);
		} else {
			$credit->addChild('cardNumber', "XXXX".substr($ccnum, -4, 4));
		}

		
		$credit->addChild('expirationDate', $ExpirationDate);
		if($cvv) {$credit->addChild('cardCode', $cvv); 
					//$profile->addChild('validationMode','liveMode');
				}
		$CustPaymentXML = $doc->asXML();
		return $CustPaymentXML;
	}
	public function updatePaymentProfileXML($CustProfile, $PayProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><updateCustomerPaymentProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$PaymentProfile = $doc->addChild('paymentProfile');
		$billTo = $PaymentProfile->addChild('billTo');
		$billTo->addChild('firstName',$billToWho['firstname']);
		$billTo->addChild('lastName',$billToWho['lastname']);
		if(!empty($billToWho['company'])){
			$billTo->addChild('company',htmlentities($billToWho['company']));
        } else { $billTo->addChild('company',"xxxx"); }
		$billTo->addChild('address',$billToWho['address']);
		$billTo->addChild('city',$billToWho['city']);
		$billTo->addChild('state',$billToWho['region']);
		$billTo->addChild('zip',$billToWho['postcode']);
		$billTo->addChild('country',$billToWho['country_id']);
		$billTo->addChild('phoneNumber',$billToWho['telephone']);
		$billTo->addChild('faxNumber',$billToWho['fax']);
		$payment1 = $PaymentProfile->addChild('payment');
		$credit = $payment1->addChild('creditCard');

		if(substr($ccnum,0,4!="tkn-")) {
			$credit->addChild('cardNumber', $ccnum);
		} else {
			$credit->addChild('cardNumber', "XXXX".substr($ccnum, -4, 4));
		}

		
		$credit->addChild('expirationDate', $ExpirationDate);
		if($cvv) {$credit->addChild('cardCode', $cvv); 
					//$profile->addChild('validationMode','liveMode');
				}
		$PaymentProfile->addChild('customerPaymentProfileId', $PayProfile);
		$CustPaymentXML = $doc->asXML();
		return $CustPaymentXML;
	}
	public function createCustomerShippingAddressXML($CustProfile, $shipToWho, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerShippingAddressRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$shipTo = $doc->addChild('address');
		$shipTo->addChild('firstName',$shipToWho['firstname']);
		$shipTo->addChild('lastName',$shipToWho['lastname']);
		if(!empty($shipToWho['company'])){
			$shipTo->addChild('company',htmlentities($shipToWho['company']));
        } else { $shipTo->addChild('company',"xxxx"); }
		$shipTo->addChild('address',$shipToWho['address']);
		$shipTo->addChild('city',$shipToWho['city']);
		$shipTo->addChild('state',$shipToWho['region']);
		$shipTo->addChild('zip',$shipToWho['postcode']);
		$shipTo->addChild('country',$shipToWho['country_id']);
		$shipTo->addChild('phoneNumber',$shipToWho['telephone']);
		$shipTo->addChild('faxNumber',$shipToWho['fax']);
		$CustShipXML = $doc->asXML();
		return $CustShipXML;
	}
	public function getProfileXML($CustProfile, $storeId=0) {
		// Build the XML request 
		$doc = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><getCustomerProfileRequest/>');
		$doc->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		
		$merchantAuth = $doc->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		
		$profile = $doc->addChild('customerProfileId', $CustProfile);
		$getCustProfileXML = $doc->asXML();
		
		return $getCustProfileXML;
	}
	public function createTransXML($amount, $tax, $CustomerProfileID, $PaymentProfileID, $ShippingProfileID, $callby, $invoiceno, $authtransID, $Approval, $cvv=111, $storeId=0) {
		// Build Transaction Request
		$TxRq = new SimpleXMLElement('<?xml version = "1.0" encoding = "utf-8"?><createCustomerProfileTransactionRequest/>');
		$TxRq->addAttribute('xmlns','AnetApi/xml/v1/schema/AnetApiSchema.xsd');
		$merchantAuth = $TxRq->addChild('merchantAuthentication');
		$merchantAuth->addChild('name', htmlentities( $this->getUsername($storeId) ));
		$merchantAuth->addChild('transactionKey', htmlentities( $this->getPassword($storeId) ));
		$transaction = $TxRq->addChild('transaction');
		if($this->getPaymentAction()=='authorize')
		{
			switch($callby) {
				case 'captureonly':
					$credit = $transaction->addChild('profileTransCaptureOnly');
					$credit->addChild('amount', $amount);
					break;
				case 'capture':
					$credit = $transaction->addChild('profileTransPriorAuthCapture');
					$credit->addChild('amount', $amount);
					break;
				case 'refund':
					$credit = $transaction->addChild('profileTransRefund');
					$credit->addChild('amount', $amount);
					break;
				case 'void':
					$credit = $transaction->addChild('profileTransVoid');
					break;
				default:
					$credit = $transaction->addChild('profileTransAuthOnly');
					$credit->addChild('amount', $amount);
					if($tax>0) {
						$credittax = $credit->addChild('tax');
						$credittax->addChild('amount', $tax);
					}
					break;
			}
		} else {
			switch($callby) {
				case 'refund':
					$credit = $transaction->addChild('profileTransRefund');
					$credit->addChild('amount', $amount);
					break;
				case 'void':
					$credit = $transaction->addChild('profileTransVoid');
					break;
				default:
					$credit = $transaction->addChild('profileTransAuthCapture');
					$credit->addChild('amount', $amount);
					if($tax>0) {
						$credittax = $credit->addChild('tax');
						$credittax->addChild('amount', $tax);
					}
					break;
			}
		}
		$credit->addChild('customerProfileId', $CustomerProfileID);
		$credit->addChild('customerPaymentProfileId', $PaymentProfileID);
		
		if($this->getPaymentAction()=='authorize')
		{
			switch($callby) {
				case 'captureonly':
					$credit->addChild('approvalCode', $Approval);
					break;
				case 'capture':
					$credit->addChild('transId', $authtransID);
					break;
				case 'refund':
					$credit->addChild('transId', $authtransID);
					break;
				case 'void':
					$credit->addChild('transId', $authtransID);
					break;
				default:
					if(isset($ShippingProfileID) AND ($ShippingProfileID>0)) { 
					$credit->addChild('customerShippingAddressId', $ShippingProfileID); }
					$order = $credit->addChild('order');
					$order->addChild('invoiceNumber', $invoiceno);
					if($cvv!=111) {
						$credit->addChild('cardCode', $cvv); 
					}
					break;
			}
		} else {
			switch($callby) {
				case 'refund':
					$credit->addChild('transId', $authtransID);
					break;
				case 'void':
					$credit->addChild('transId', $authtransID);
					break;
				default:
					if(isset($ShippingProfileID) AND ($ShippingProfileID>0)) { 
					$credit->addChild('customerShippingAddressId', $ShippingProfileID); }
					$order = $credit->addChild('order');
					$order->addChild('invoiceNumber', $invoiceno);
					break;
			}
		}
		$TxRqXML = $TxRq->asXML();
		return $TxRqXML;
	}
	public function processRequest($url, $xml)	{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close ($ch);
	      		
	 if($this->getDebug()) {
	      	$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
	      	$logger->info("XML Sent: $xml");
	      	$logger->info("XML Received: $response");
	   }
	      		
	    return $response;
	}
		
	public function parseXML($start, $end, $xml)	 {
		return preg_replace('|^.*?'.$start.'(.*?)'.$end.'.*?$|i', '$1', substr($xml, 335));
	}
	public function parseMultiXML($xml, $ccnum, $forcevalue)	 {
		$ccnum='XXXX'.substr($ccnum, -4, 4);
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("MultiParseXML for $ccnum");
		}
		$ccnumorig=$ccnum;		
		$pos = strpos($xml,"<?xml version=");
		$returnvalue=0;
		$fixedXML=substr($xml,$pos);
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$dom=new DOMDocument;
		$dom->loadXML($fixedXML);
		//print_r($dom);
		$profileID=array();
		$CardNo=array();
		$ProfileIDs=$dom->getElementsByTagname('customerPaymentProfileId');
		$index=0;
		foreach ($ProfileIDs as $Profile) {
			$value=$Profile->nodeValue;
			$profileID[]=$value;
			$index++;
		}
		{$ccnumflag=0;}
		//print_r($profileID);
		$Cards=$dom->getElementsByTagname('cardNumber');
		foreach ($Cards as $Card) {
			$value=$Card->nodeValue;
			$CardNo[]=$value;
			if($value==$ccnumorig) {$ccnumflag=$value;}
			if($this->getDebug()) { $logger->info("ccnumflag: $ccnumflag"); }		
		}
		//print_r($CardNo);
		for ($iindex=0; $iindex<$index; $iindex++) {
			if($this->getDebug())
			{
				$logger->info(" Payment: " . $profileID[$iindex]);
				$logger->info(" Card Number: " . $CardNo[$iindex]);
			}	
			if($CardNo[$iindex]==$ccnum) {
				$returnvalue=$profileID[$iindex];
				//break;
			}
		}
		if($this->getDebug()) { $logger->info("returnvalue5: $returnvalue"); }	
		if($returnvalue==0) 
		{if($ccnumflag>0) { $returnvalue=$ccnumorig; }
			else { if((isset($profile[0])) and ($profile[0]>0) and ($forcevalue)) { $returnvalue=$profileID[0]; }}
		}
			return $returnvalue;
	}
	public function parseMultiShippingXML($xml, $shipToWho, $returnfirst) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("MultiParseShippingXML...");
		}
		$vvalue=0;	$customerShippingAddressId=0;

		$pos = strpos($xml,"<?xml version=");
		$fixedXML=substr($xml,$pos);
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$fixedXML=substr($xml,$pos);		
		$fixedXML = preg_replace('/xmlns="(.+?)"/', '', $fixedXML);
		$dom=new DOMDocument;
		if($this->getDebug()) { $logger->info("fixedXML: $fixedXML"); }			

		$dom=new DOMDocument;
		$dom->loadXML($fixedXML);
		$profileID=array();
		//$ProfileIDs=$dom->getElementsByTagname('shipToList')->getElementsByTagname('customerAddressId');
		$ProfileIDs=$dom->getElementsByTagname('customerAddressId');
		$Shippingindex=0;
		foreach ($ProfileIDs as $Profile) {
			$value=$Profile->nodeValue;
			$profileID[]=trim($value);
			$Shippingindex++;
		}
		$firstName=array();
		$firstnames=$dom->getElementsByTagname('firstName');
		$index=0;
		foreach ($firstnames as $Profile) {
			$value=$Profile->nodeValue;
			$firstName[]=trim($value);
			$index++;
		}
		$firstName=array_slice($firstName,$index-$Shippingindex);
		$lastName=array();
		$lastnames=$dom->getElementsByTagname('lastName');
		foreach ($lastnames as $Profile) {
			$value=$Profile->nodeValue;
			$lastName[]=trim($value);
		}
	$lastName=array_slice($lastName,$index-$Shippingindex);
		$company=array();
		$companys=$dom->getElementsByTagname('company');
		foreach ($companys as $Profile) {
			$value=$Profile->nodeValue;
			$company[]=trim($value);
		}
	$company=array_slice($company,$index-$Shippingindex);
		$address=array();
		$addresss=$dom->getElementsByTagname('address');
		foreach ($addresss as $Profile) {
			$value=$Profile->nodeValue;
			$address[]=trim($value);
		}
	$address=array_slice($address,$index-$Shippingindex);	
		$city=array();
		$citys=$dom->getElementsByTagname('city');
		foreach ($citys as $Profile) {
			$value=$Profile->nodeValue;
			$city[]=trim($value);
		}
	$city=array_slice($city,$index-$Shippingindex);	
		$state=array();
		$states=$dom->getElementsByTagname('state');
		foreach ($states as $Profile) {
			$value=$Profile->nodeValue;
			$state[]=trim($value);
		}
	$state=array_slice($state,$index-$Shippingindex);
		$zip=array();
		$zips=$dom->getElementsByTagname('zip');
		foreach ($zips as $Profile) {
			$value=$Profile->nodeValue;
			$zip[]=trim($value);
		}
	$zip=array_slice($zip,$index-$Shippingindex);
		$country=array();
		$countrys=$dom->getElementsByTagname('country');
		foreach ($countrys as $Profile) {
			$value=$Profile->nodeValue;
			$country[]=trim($value);
		}
	$country=array_slice($country,$index-$Shippingindex);
		$phoneNumber=array();
		$phoneNumbers=$dom->getElementsByTagname('phoneNumber');
		foreach ($phoneNumbers as $Profile) {
			$value=$Profile->nodeValue;
			$phoneNumber[]=trim($value);
		}
	$phoneNumber=array_slice($phoneNumber,$index-$Shippingindex);
		$faxNumber=array();
		$faxNumbers=$dom->getElementsByTagname('faxNumber');
		foreach ($faxNumbers as $Profile) {
			$value=$Profile->nodeValue;
			$faxNumber[]=trim($value);
		}
	$faxNumber=array_slice($faxNumber,$index-$Shippingindex);
		$i=0;
		$valuee=0;
		$cShippingAddressId=0;
		foreach ($ProfileIDs as $Profile) {
			$match=true;
			if($valuee==0) {$valuee=$Profile->nodeValue; }
			if(($match) and (strtoupper($shipToWho['firstname'])!=strtoupper($firstName[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['lastname'])!=strtoupper($lastName[$i])))
			{ $match=false;}
			if(strtoupper($shipToWho['company'])==""){$shipToWho['company']="XXXX"; }
			if(($match) and (strtoupper($shipToWho['company'])!=strtoupper($company[$i])))
			{ $match=false;}
			if(($match) and (strtoupper(preg_replace('/ /', $shipToWho['address'],'')!=strtoupper(preg_replace('/ /', $address[$i],'')))))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['city'])!=strtoupper($city[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['region'])!=strtoupper($state[$i]))) 
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['postcode'])!=strtoupper($zip[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['country_id'])!=strtoupper($country[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['telephone'])!=strtoupper($phoneNumber[$i])))
			{ $match=false;}
			if(($match) and (strtoupper($shipToWho['fax'])!=strtoupper($faxNumber[$i])))
			{ $match=false;}
			if(($match) AND ($vvalue==0)) {$customerShippingAddressId=$valuee; $vvalue=$valuee;}
			if($vvalue==0) {$cShippingAddressId=$valuee; }			
			//PRINT "MATCH!"
			$i++;
		}
			if($vvalue==0) {$customerShippingAddressId=$cShippingAddressId; }
		$i=0;
		foreach ($ProfileIDs as $Profile) {
			if($this->getDebug()) {
				$logger->info("i: $i");
				$logger->info((strtoupper($shipToWho['firstname']))."/".(strtoupper($firstName[$i]))); 
				$logger->info((strtoupper($shipToWho['lastname']))."/".strtoupper($lastName[$i])); 
				$logger->info((strtoupper($shipToWho['company']))."/".strtoupper($company[$i])); 
				$logger->info((strtoupper(preg_replace('/[^A-Za-z0-9_]/', ' ',$shipToWho['address']))."/".strtoupper(preg_replace('/[^A-Za-z0-9_]/', ' ',$address[$i])))); 
				$logger->info((strtoupper($shipToWho['city']))."/".strtoupper($city[$i])); 
				$logger->info((strtoupper($shipToWho['region']))."/".strtoupper($state[$i])); 
				$logger->info((strtoupper($shipToWho['postcode']))."/".strtoupper($zip[$i])); 
				$logger->info((strtoupper($shipToWho['country_id']))."/".strtoupper($country[$i])); 
				$logger->info((strtoupper($shipToWho['telephone']))."/".strtoupper($phoneNumber[$i])); 
				$logger->info((strtoupper($shipToWho['fax']))."/".strtoupper($faxNumber[$i])); 
				if($match) {$logger->info("MATCHED!");} 
				if (($match) and ($vvalue<1)) {$vvalue=$valuee;}
				$logger->info("VALUE: $valuee"); 
				$logger->info("VVALUE: $vvalue"); 
				if($this->getDebug()) {$logger->info($i);	};
			}
			$i++;
		}
		
		//if((!$match) and ($returnfirst) AND ($customerShippingAddressId==0)) {$customerShippingAddressId=$value; }
		if($valuee>1) {
			if(($match) and ($customerShippingAddressId<1)) { $customerShippingAddressId=$valuee; }
			if($returnfirst) {$customerShippingAddressId=$valuee; }
		} elseif(($returnfirst) and ($vvalue>0)){$customerShippingAddressId=$vvalue; }
		if($this->getDebug()) {$logger->info("customerShippingAddressIdsearch:".$customerShippingAddressId);	};
		
		return $customerShippingAddressId;
	}
	public function validate() {
		if($this->getDebug())
		{
	    	$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering validate()");
		}
		return $this;
    }
	public function authorize(Varien_Object $payment, $amount) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering authorize()");
		}
		$this->setAmount($amount)
		->setPayment($payment);
		
		$orderTable=Mage::getSingleton('core/resource')->getTableName('sales_flat_order'); $orderTransTable=Mage::getSingleton('core/resource')->getTableName('sales_payment_transaction'); 
		$orderno = $payment->getOrder()->getIncrementId();
		$sql = "DELETE p.* FROM $orderTransTable p, $orderTable q WHERE q.increment_id ='".$orderno."' AND q.entity_id=p.order_id AND p.txn_type='authorization';";
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query($sql);

		$result = $this->_call($payment, 'authorize', $amount);
		if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
		if($result === false)
		{
			$e = $this->getError();
			if (isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		}
		else
		{
			// Check if there is a gateway error
			if ($result['Status']['statusCode'] == "Ok")
			{
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001")
				{
					$payment->setStatus(self::STATUS_APPROVED);
					$payment->setTransactionId($result['Status']['transno']);
					$payment->setIsTransactionClosed(0);
					$payment->setTxnId($result['Status']['transno']);
					$payment->setParentTxnId($result['Status']['transno']);
					$payment->setCcTransId($result['Status']['transno']);					
					}
			}
			else
			{
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
	return $this;
	}
public function capture(Varien_Object $payment, $amount) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering capture()");
		}
		
		$this->setAmount($amount)
			->setPayment($payment);
	$ttt=$this->getReauth();
	if($this->getDebug()) { $logger->info("getReauth: $ttt"); }
	if($this->getReauth()) {
	//if(false) {
		$orderTable=Mage::getSingleton('core/resource')->getTableName('sales_flat_order'); 
		if($this->getDebug()) { $logger->info("Reauthorization Steps"); }
		$orderInvoiceTable=Mage::getSingleton('core/resource')->getTableName('sales_flat_invoice'); 
		$orderno = $payment->getOrder()->getIncrementId();
		//$this->getInvoice()->getId();  invoice number
		$sql = "SELECT * FROM $orderInvoiceTable p, $orderTable q WHERE q.increment_id ='$orderno' AND q.entity_id=p.order_id AND p.increment_id>'' ORDER BY p.entity_id desc LIMIT 1;";
		$data2 = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchRow($sql);
		$lastinvoice = $data2['increment_id'];
		if($this->getDebug()) { $logger->info("lastinvoice: $lastinvoice"); }
			if($this->getDebug()) { $logger->info("step4c"); }
		if(!$lastinvoice) {
					$result = $this->_call($payment,'capture', $amount);
				} else { 
					$result = $this->_call($payment,'captureonly', $amount);
				}
				if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
				if($result['Status']['transno']=='0') {
					$result = $this->_call($payment,'captureonly', $amount);
				} 
	} else {
			if($this->getDebug()) { $logger->info("step4b"); }
			$result = $this->_call($payment,'capture', $amount);
	}
if($this->getDebug()) { $logger->info("step5"); }
	if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
	if($result === false)
	{
		$e = $this->getError();
		if (isset($e['message'])) {
			$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
		} else {
			$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
		}
		Mage::throwException($message);
	}
	else
	{
		// Check if there is a gateway error
		if ($result['Status']['statusCode'] == "Ok")
		{
			// Check if there is an error processing the credit card
			if($result['Status']['code'] == "I00001")
			{
				//$payment->setIsTransactionClosed(1);
				//$payment->registerCaptureNotification();
				$payment->setIsTransactionClosed(0);
				$payment->setTransactionId($result['Status']['transno']);
				$payment->setLastTransId($result['Status']['transno'])
						->setCcApproval($result['Status']['code'])
						->setCcTransId($result['Status']['transno'])
						->setCcAvsStatus($result['Status']['statusCode'])
						->setCcCidStatus($result['Status']['statusCode']);
			}
		}
		else
		{
			Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
		}
	}
	return $this;
}
public function refund(Varien_Object $payment, $amount) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering refund()");
		}
		$this->setAmount($amount)
			->setPayment($payment);
			
		$result = $this->_call($payment,'refund', $amount);
		if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
		if($result === false)
		{
			$e = $this->getError();
			if (isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		}
		else
		{
			// Check if there is a gateway error
			if ($result['Status']['statusCode'] == "Ok")
			{
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001")
				{
					//$payment->registerRefundNotification();
					$payment->setCcApproval($result['Status']['code'])
						->setTransactionId($result['Status']['transno'])
						->setCcTransId($result['Status']['transno'])
						->setCcAvsStatus($result['Status']['statusCode'])
						->setCcCidStatus($result['Status']['statusCode']);
				}
			}
			else
			{
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
		return $this;
	}
public function void(Varien_Object $payment) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("entering void()");
		}
		//$this->setAmount($amount)
		//	->setPayment($payment);
		$result = $this->_call($payment,'void', 99999);
		if($this->getDebug()) { $logger->info(var_export($result, TRUE)); }
		if($result === false)
		{
			$e = $this->getError();
			if (isset($e['message'])) {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment.') . $e['message'];
			} else {
				$message = Mage::helper('authorizeCIM')->__('There has been an error processing your payment. Please try later or contact us for help.');
			}
			Mage::throwException($message);
		}
		else
		{
			// Check if there is a gateway error
			if ($result['Status']['statusCode'] == "Ok")
			{
				// Check if there is an error processing the credit card
				if($result['Status']['code'] == "I00001")
				{
					$cancelorder=true;
					if ($cancelorder) { $payment->getOrder()->setState('canceled', true, 'Canceled/Voided');
					} else { $payment->registerVoidNotification(); }
					$payment->setCcApproval($result['Status']['code'])
						->setTransactionId($result['Status']['transno'])
						->setCcTransId($result['Status']['transno'])
						->setCcAvsStatus($result['Status']['statusCode'])
						->setCcCidStatus($result['Status']['statusCode']);
					$payment->setStatus(self::STATUS_VOIDED);
				}
			}
			else
			{
				Mage::throwException("Gateway error code " . $result['Status']['code'] . ": " . $result['Status']['statusDescription']);
			}
		}
		return $this;
	}
protected function _call(Varien_Object $payment,$callby='', $amountcalled) {
		if($this->getDebug())
		{
			$writer = new Zend_Log_Writer_Stream($this->getLogPath());
			$logger = new Zend_Log($writer);
			$logger->info("paymentAction: ".$this->getPaymentAction());
			$storeId = $payment->getOrder()->getStoreId();
			$logger->info("Storeid: ".$storeId);
		}
		//print "<pre>"; print_r($payment); print "</pre>"; exit;
		$ExpirationDate = $payment->getCcExpYear() .'-'. str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT);
		$invoiceno = $payment->getOrder()->getIncrementId();
		$CustomerEmail = $payment->getOrder()->getCustomerEmail();
							if($this->getDebug()) { $logger->info("CustomerEmail1: ".$CustomerEmail); }
		if(!$CustomerEmail>"") { $CustomerEmail = $payment->getOrder()->getBillingAddress()->getEmail();  }
							if($this->getDebug()) { $logger->info("CustomerEmail2: ".$CustomerEmail); }
		$CustomerId = $payment->getOrder()->getCustomerId();
							if($this->getDebug()) { $logger->info("CustomerId1: ".$CustomerId); }
		if(!$CustomerId>0) { $CustomerId = Mage::getSingleton('customer/session')->getCustomerId();  }
							if($this->getDebug()) { $logger->info("CustomerId2: ".$CustomerId); }
		$storeId = $payment->getOrder()->getStoreId();

		$billToWhoA = $payment->getOrder()->getBillingAddress();
		$billToWho['firstname']=$billToWhoA->getFirstname();
		$billToWho['lastname']=$billToWhoA->getLastname();
		
		$billToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $billToWhoA->getStreet(1)." ".$billToWhoA->getStreet(2));	
		//$billToWho['company']=$billToWhoA->getCompany();
		$billToWho['company']=preg_replace('/[^A-Za-z0-9_]/', ' ', $billToWhoA->getCompany());
		$billToWho['telephone']=$billToWhoA->getTelephone();
		$billToWho['fax']=$billToWhoA->getFax();
		//$billToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $payment->getStreet(1)." ".$payment->getStreet(2));	
		$billToWho['city']=$billToWhoA->getCity();
		$billToWho['region']=$billToWhoA->getPostcode();
		$billToWho['postcode']=$billToWhoA->getPostcode();
		$billToWho['country_id']=$billToWhoA->getCountryId();

		
		$shipToWhoA = $payment->getOrder()->getShippingAddress();
		if((isset($shipToWho['lastname'])) and ($shipToWho['lastname']>'')) {		
			$shipToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $shipToWhoA->getStreet(1)." ".$shipToWhoA->getStreet(2));	
			$shipToWho['firstname']=$shipToWhoA->getFirstname();
			$shipToWho['lastname']=$shipToWhoA->getLastname();
			//$shipToWho['company']=$billToWhoA->getCompany();
			$shipToWho['company']=preg_replace('/[^A-Za-z0-9_]/', ' ', $shipToWhoA->getCompany());
			$shipToWho['telephone']=$shipToWhoA->getTelephone();
			$shipToWho['fax']=$shipToWhoA->getFax();
	//		$shipToWho['address']=preg_replace('/[^A-Za-z0-9_]/', ' ', $payment->getStreet(1)." ".$payment->getStreet(2));	
			$shipToWho['city']=$shipToWhoA->getCity();
			$shipToWho['region']=$shipToWhoA->getRegion();
			$shipToWho['postcode']=$shipToWhoA->getPostcode();
			$shipToWho['country_id']=$shipToWhoA->getCountryId();
		}
		$tax = $payment->getOrder()->getTaxAmount();
		$cvv = $payment->getCcCid();
		if($this->getStrictCVV()) { if(!$cvv) {$cvv="111"; } }

		$ccnum = $payment->getCcNumber();
		$ponum = $payment->getPoNumber();
		$last4 =  $payment->getCcLast4();
		$origponum=$ponum;
		$fullcarddata=false;
		if($ccnum=='') {
			$ccnum="tkn-$ponum-$last4";
		}

		$showfrontendcim=$payment->getCcSsStartMonth();
		if($this->getDebug()) { $logger->info("CcNumber, PoNumber: $ccnum, $ponum SaveCimCC: $showfrontendcim\n"); }
		if ($amountcalled<1) { $amountcalled = $this->getAmount(); }
		$amountcalled=number_format((float)$amountcalled, 2, '.', '');
		$url = $this->getGatewayUrl();
		$CustomerProfileID = 0;
		$PaymentProfileID = 0;
		$Approval = 0;
		$ShippingProfileID = 0; 
		
		if($this->getDebug()) { $logger->info("FullCardData: $fullcarddata USING ccnum: $ccnum USING ponum: $ponum"); }
		//$tkn=strpos($ccnum,'tkn')+10;
		
		$tag=substr($ccnum, 0, 4);
		//$tag=$last4;
		//$tag=substr($ccnum, -4, 4);
		if($this->getDebug()) { $logger->info("tag-: $tag"); }
		
		if($tag=="tkn-") {
			$fullcarddata=false;
			if($ccnum!="") {
				$fields = preg_split('/-/',$ccnum);
				$CustomerProfileID = (INT) $fields[1];
				if((isset($fields[2])) and ($callby!='authorize')) { 
					$PaymentProfileID = (INT) $fields[2]; 
				} else {
					$PaymentProfileID = 0; 
				}
				if(isset($fields[3])) { 
					$Approval = $fields[3];
				} else {
					$Approval = 0; 
				}
			}
			if($ponum!="") {
				$fields = preg_split('/-/',$ponum);
				if(isset($fields[3])) { 
					if(isset($fields[3]) and strlen($fields[3]!=4)) { $ShippingProfileID=$fields[3]; }
				} else {
					$ShippingProfileID = 0; 
				}
		}
			//$ccnum=substr($ccnum,-4);
			$ccnum='XXXX'.substr($ccnum, -4, 4);
			if($this->getDebug()) { $logger->info("FullCardData: NO TKN-: $fullcarddata USING ccnum: $ccnum USING ponum: $ponum"); }			
		} else {
			$fullcarddata=true;
			if($this->getDebug()) { $logger->info("FullCardData: YES $fullcarddata USING ccnum: $ccnum USING ponum: $ponum"); }
			$fields = preg_split('/-/',$ccnum);
			if(isset($fields[0]))
				{ $CustomerProfileID = (INT) $fields[0]; } else { $CustomerProfileID = 0; }
			if(isset($fields[1]))
				{ $PaymentProfileID = (INT) $fields[1];  } else { $PaymentProfileID =  0; }
			if(isset($fields[2])) { 
				$Approval = $fields[2];
			} else {
				$Approval = 0; 
			}
			if(isset($fields[3]) and strlen($fields[3]!=4)) { 
				$ShippingProfileID = (INT) $fields[3];
			} else {
				$ShippingProfileID = 0; 
			}
		}
		if($fullcarddata){
			//$CustomerProfileID = 0;
			$PaymentProfileID = 0;
			$Approval = 0;
			$ShippingProfileID = 0; 
			$fields2 = preg_split('/-/',$origponum);
			if(isset($fields2[0]))
				{ $CustomerProfileID = (INT) $fields2[0]; } else { $CustomerProfileID = 0; }
			//if(isset($fields2[1]))
				//{ $PaymentProfileID = (INT) $fields2[1];  } else { $PaymentProfileID =  0; }
		}
		if($this->getDebug()) { $logger->info("Account Values1-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
		$authtransID = $payment->getOrder()->getTransactionId();
		if($authtransID<1) { $authtransID=$payment->getParentTransactionId();	}
		if($authtransID<1) { $authtransID=$payment->getCcTransId();	}
		$authtrans2 = preg_split ('/-/',$authtransID);
		$authtransID = $authtrans2[0];
		if($PaymentProfileID == '0') { $forceupdate=true; } else { $forceupdate=false; }
//////////////////////////////////////
		if($CustomerProfileID > 0) { $ExistingCustProfile=$CustomerProfileID; } else { $ExistingCustProfile=0; }
		//if($CustomerProfileID==0) {
		if($fullcarddata) {
			// First try to create a Customer Profile
			$CustProfileXML = $this->createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $CustomerId, $billToWho, $cvv, $storeId);
			$response = $this->processRequest($url, $CustProfileXML);
			$resultErrorCode = $this->parseXML('<code>','</code>',$response);
			// Get Customer and Payment Profile ID
			$CustomerProfileID = (int) $this->parseXML('<customerProfileId>','</customerProfileId>', $response);
			$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileIdList><numericString>','</numericString></customerPaymentProfileIdList>', $response);

			$resultText = $this->parseXML('<text>','</text>',$response);
			$resultCode = $this->parseXML('<resultCode>','</resultCode>',$response);
			if($resultErrorCode == 'E00039') {
					 if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A CUST PROFILE \n\n"); }
				$split = preg_split('/ /',$resultText);
				$ExistingCustProfile = $split[5];
				if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) 
				{ $ExistingCustProfile = $CustomerProfileID; } else { $CustomerProfileID = $ExistingCustProfile; }
				if($this->getDebug()) { $logger->info("Account Values2-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
				if($this->getDebug()) {		$logger->info("\n\n Problem with CustomerProfileID \n\n"); }
				$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
				
				//<customerPaymentProfileIdList><numericString>
				
				$response = $this->processRequest($url, $addPaymentProfileXML);
				$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				if(substr($resultErrorCode,0,1) == 'E') {
				if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD \n\n"); }
					//Get Correct PaymentProfileID
				}
			}
		}
		else {
			// First try to create a Customer Profile
			$CustProfileXML = $this->createCustomerXML($ccnum, $CustomerEmail, $ExpirationDate, $CustomerId, $billToWho, $cvv, $storeId);
			$response = $this->processRequest($url, $CustProfileXML);
			$resultErrorCode = $this->parseXML('<code>','</code>',$response);
			// Get Customer and Payment Profile ID
			$CustomerProfileID = (int) $this->parseXML('<customerProfileId>','</customerProfileId>', $response);
			$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileIdList><numericString>','</numericString></customerPaymentProfileIdList>', $response);
			$resultText = $this->parseXML('<text>','</text>',$response);
			$resultCode = $this->parseXML('<resultCode>','</resultCode>',$response);
			if($resultErrorCode == 'E00039') {
					 if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A CUST PROFILE \n\n"); }
				$split = preg_split('/ /',$resultText);
				$ExistingCustProfile = $split[5];
				if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) 
				{ $ExistingCustProfile = $CustomerProfileID; } else { $CustomerProfileID = $ExistingCustProfile; }
				if($this->getDebug()) { $logger->info("Account Values2-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
				if($this->getDebug()) {		$logger->info("\n\n Problem with CustomerProfileID \n\n"); }
				$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
				$response = $this->processRequest($url, $addPaymentProfileXML);
				$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				if(substr($resultErrorCode,0,1) == 'E') {
				if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD \n\n"); }
					//Get Correct PaymentProfileID
				}
			}
		}
			if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) 
				{ $ExistingCustProfile = $CustomerProfileID; } else { $CustomerProfileID = $ExistingCustProfile; }
			if($PaymentProfileID=='0') {
				if($this->getDebug()) {	$logger->info("\n\n PAYMENT PROFILE NOT SET 1\n\n"); }
				$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
				$responseGET = $this->processRequest($url, $getCustXML);
				$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, false);
			}
			if($this->getDebug()) {	$logger->info("\n\n fullcarddata1: $fullcarddata  \n\n"); 
									if($fullcarddata) { $logger->info("\n\n fullcarddata2: TRUE  \n\n"); }
									if(!$fullcarddata) { $logger->info("\n\n fullcarddata3: FALSE  \n\n"); }}
			if(($ExistingCustProfile==0) AND ($CustomerProfileID>0)) 
				{ $ExistingCustProfile = $CustomerProfileID; } else { $CustomerProfileID = $ExistingCustProfile; }
		if($PaymentProfileID == '0') {
			if($this->getDebug()) {	$logger->info("\n\n PAYMENT PROFILE NOT SET 2\n\n"); }
			if(!$fullcarddata) {
				$addPaymentProfileXML = $this->addPaymentProfileXML($ExistingCustProfile, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
				$response = $this->processRequest($url, $addPaymentProfileXML);
				$PaymentProfileID = (int) $this->parseXML('<customerPaymentProfileId>','</customerPaymentProfileId>', $response);
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				 if(substr($resultErrorCode,0,1) == 'E') {
				 if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A PAYMENT PROFILE WITH THE CARD \n\n"); }
					//Get Correct PaymentProfileID
					$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
					$responseGET = $this->processRequest($url, $getCustXML);
					$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, true);
			}}}
			if($fullcarddata) {
				$updatePaymentProfileXML = $this->updatePaymentProfileXML($ExistingCustProfile, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
				$response = $this->processRequest($url, $updatePaymentProfileXML);
				 if($this->getDebug()) {		$logger->info("\n\n UPDATED PAYMENT PROFILE1 $PaymentProfileID \n\n"); }

				$resultErrorCode = $this->parseXML('<resultCode>','</resultCode>',$response);
				 if($resultErrorCode != 'Ok') { // Using an existing card already
				 if($this->getDebug()) {		$logger->info("\n\n UPDATING PAYMENT PROFILE1a $PaymentProfileID\n\n"); }
					//Get Correct PaymentProfileID
					$getCustXML = $this->getProfileXML($ExistingCustProfile, $storeId);
					$responseGET = $this->processRequest($url, $getCustXML);
					$PaymentProfileIDBack = $PaymentProfileID;
					$PaymentProfileID = $this->parseMultiXML($responseGET, $ccnum, true);
					if($PaymentProfileID=='0') { $PaymentProfileID = $PaymentProfileIDBack; }
					if($forceupdate) {
						$updatePaymentProfileXML = $this->updatePaymentProfileXML($ExistingCustProfile, $PaymentProfileID, $ccnum, $ExpirationDate, $billToWho, $cvv, $storeId);
						$response = $this->processRequest($url, $updatePaymentProfileXML);
					if($PaymentProfileID=='0') { $PaymentProfileID = $PaymentProfileIDBack; }
						 if($this->getDebug()) {		$logger->info("\n\n UPDATED PAYMENT PROFILE2 $PaymentProfileID \n\n"); }
					}}

				if($this->getDebug()) {		$logger->info("\n\n UPDATED PAYMENT PROFILE3 $PaymentProfileID \n\n"); }
				if($this->getDebug()) { $logger->info("Account Values4-> customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }
				}
		//}

		if((isset($shipToWho['lastname'])) and ($shipToWho['lastname']>'')) {
			if($this->getDebug()) {		$logger->info("\n\n Shipping address detected \n\n"); }
			if($this->getDebug()) {		$logger->info("\n\n last: ".$shipToWho['lastname']." first: ".$shipToWho['lastname']."\n\n"); }
			//Get Correct ShippingProfileID
			//$ExistingCustProfile=$CustomerProfileID;
			$getXML = $this->getProfileXML($CustomerProfileID, $storeId);
			$responseGET = $this->processRequest($url, $getXML);
			if($this->getDebug()) {		$logger->info("\n\n responseGET $responseGET \n\n"); }
			if($ShippingProfileID==0) {
				$ShippingProfileID = (INT) $this->parseMultiShippingXML($responseGET, $shipToWho, FALSE);
			}
			if($ShippingProfileID==0) {
				$createCustomerShippingAddressXML=$this->createCustomerShippingAddressXML($CustomerProfileID, $shipToWho, $storeId);
				$response = $this->processRequest($url, $createCustomerShippingAddressXML);
				if($this->getDebug()) {		$logger->info("\n\n Shipping Address Response: $response\n\n"); }
				$resultErrorCode = $this->parseXML('<code>','</code>',$response);
				
				if(substr($resultErrorCode,0,1) == 'E') { // Using an existing card already					
				//if($resultErrorCode == 'E00039') { // Using an existing card already
					if($this->getDebug()) {		$logger->info("\n\n ALREADY HAVE A SHIPPING PROFILE WITH THE CARD \n\n responseGET: $responseGET"); }
					$ShippingProfileID = (INT) $this->parseMultiShippingXML($responseGET, $shipToWho, TRUE);
				}
			}
		}

				
		if($this->getDebug()) { $logger->info("\nUSING customer: $CustomerProfileID - payment: $PaymentProfileID - shipping: $ShippingProfileID"); }

		$TxRqXML = $this->createTransXML($amountcalled, $tax, $CustomerProfileID, $PaymentProfileID, $ShippingProfileID, $callby, $invoiceno, $authtransID, $Approval, $cvv, $storeId);
		$TxRqResponse = $this->processRequest($url, $TxRqXML);

		$resultText = $this->parseXML('<text>','</text>',$TxRqResponse);
		$resultCode = $this->parseXML('<resultCode>','</resultCode>',$TxRqResponse);
		$resultErrorCode = $this->parseXML('<code>','</code>',$TxRqResponse);
		$transauthidar = $this->parseXML('<directResponse>','</directResponse>',$TxRqResponse);
	 	$fieldsAU = preg_split('/,/',$transauthidar);
		$responsecode  = $fieldsAU[0];
		if(!$responsecode=="1") { $resultCode="No";  }
		if(isset($fieldsAU[4])) { 
					$approval = $fieldsAU[4];
				} else {
					$approval = 0; 
				}
		if (strlen($approval)<6) { $approval=$Approval; }
		if(isset($fieldsAU[6])) { 
					$transno = $fieldsAU[6];
				} else {
					$transno = 0; 
				}
		if(isset($fieldsAU[5])) { 
				$avscode = $fieldsAU[5];
		} else {
				$avscode = "Y";
		}
		if($this->getDebug()) { $logger->info("TransID = $transno \n"); }
		if($this->getDebug()) { $logger->info("Approval Code = $approval \n"); }
		$paymentInfo = $this->getInfoInstance();
		if (($CustomerProfileID>'0') AND ($PaymentProfileID>'0')) {
			$token="$CustomerProfileID-$PaymentProfileID-$approval-$ShippingProfileID";
			$paymentInfo->setCybersourceToken($token);
			$paymentInfo->setPoNumber($token);
			$paymentInfo->getOrder()->setTransactionId($transno);
			if($paymentInfo->getCcSsStartMonth()=="on") {	
				$paymentInfo->setCcSsStartMonth('1'); 
			} else {
				if($paymentInfo->getCcSsStartMonth()!="1") {	
					$paymentInfo->setCcSsStartMonth('0'); }
		}}
		
		$paymentInfo->setCcAvsStatus($avscode);
		
		$result['Status']['transno'] = $transno;
		$result['Status']['approval'] = $approval;
		$result['Status']['CustomerProfileID'] = $CustomerProfileID;
		$result['Status']['PaymentProfileID'] = $PaymentProfileID;
		$result['Status']['ShippingProfileID'] = $ShippingProfileID;
		$result['Status']['statusCode'] = $resultCode;
		$result['Status']['code'] = $resultErrorCode;
		$result['Status']['statusDescription'] = $resultText;
		if($this->getDebug()) { $logger->info("STATUS CODE = $resultErrorCode - $resultCode - $resultText"); }
		return $result;
	}
}