<?php
/**
 * Productattachments extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Productattachments
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('productattachments')};
CREATE TABLE {$this->getTable('productattachments')} (
  `productattachments_id` int(11) unsigned NOT NULL auto_increment,  
  `title` varchar(255) NOT NULL default '',                          
  `filename` varchar(255) NOT NULL default '',                       
  `file_icon` text,                                                  
  `file_type` varchar(255) default '',                               
  `file_size` varchar(255) default '',                               
  `download_link` text,                                              
  `downloads` int(11) default '0',                                   
  `content` text NOT NULL,                                           
  `status` smallint(6) NOT NULL default '0',                         
  `cmspage_id` text,                                                 
  `created_time` datetime default NULL,                              
  `update_time` datetime default NULL,                               
  PRIMARY KEY  (`productattachments_id`)                             
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('productattachments_store')};
CREATE TABLE {$this->getTable('productattachments_store')} (
`productattachments_id` int(11) unsigned NOT NULL,                       
`store_id` smallint(5) unsigned NOT NULL,                                
PRIMARY KEY  (`productattachments_id`,`store_id`),                       
KEY `FK_PRODUCTATTACHMENTS_STORE_STORE` (`store_id`)                     
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Attachments Stores';

DROP TABLE IF EXISTS {$this->getTable('productattachments_products')};
CREATE TABLE {$this->getTable('productattachments_products')} (
  `product_related_id` int(11) NOT NULL auto_increment,   
   `productattachments_id` int(11) default NULL,           
   `product_id` int(11) default NULL,                      
   UNIQUE KEY `product_related_id` (`product_related_id`)  
 ) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
");

$installer->setConfigData('productattachments/productattachments/enabled','1');
$installer->setConfigData('productattachments/productattachments/product_attachment_heading','Downloads');

$installer->setConfigData('productattachments/cmspagesattachments/enabled','1');
$installer->setConfigData('productattachments/cmspagesattachments/cms_page_attachment_heading','Downloads');


$installer->endSetup(); 