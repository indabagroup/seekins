<?php
/**
 * Productattachments extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Productattachments
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

class FME_Productattachments_Helper_Data extends Mage_Core_Helper_Abstract
{
	const XML_PATH_LIST_PRODUCT_PAGE_ATTACHMENT_HEADING	= 'productattachments/productattachments/product_attachment_heading';
	const XML_PATH_LIST_CMS_PAGE_ATTACHMENT_HEADING	= 'productattachments/cmspagesattachments/cms_page_attachment_heading';
	
	//const XML_PATH_LIST_ALLOWED_FILE_EXTENSIONS	= 'productattachments/productattachments/allowed_file_extensions';




	public function getProductPageAttachmentHeading()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_PRODUCT_PAGE_ATTACHMENT_HEADING);
	}
	
	public function getCMSPageAttachmentHeading()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_CMS_PAGE_ATTACHMENT_HEADING);
	}
	
	/*public function getAllowedFileExtensions()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_ALLOWED_FILE_EXTENSIONS);
	}*/
	
	

}