<?php
/**
 * Productattachments extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Productattachments
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */
 
class FME_Productattachments_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout();     
		$this->renderLayout();
    }
	
	public function downloadAction(){	
	
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('productattachments/productattachments')->load($id);
		
		//Update Download Counter
		Mage::getModel('productattachments/productattachments')->updateCounter($id);
		
		$fileconfig = Mage::getModel('productattachments/image_fileicon');
		$filePath = Mage::getBaseDir('media'). DS . $model['filename'];
		$fileconfig->Fileicon($filePath);
		$fileName = $model['filename'];
		
		$fileType = $fileconfig->getType();
		$fileSize = $fileconfig->getSize();
	
		if(isset($_SERVER['HTTP_USER_AGENT']) && preg_match("/MSIE/", $_SERVER['HTTP_USER_AGENT'])) {
		   ini_set( 'zlib.output_compression','Off' );
   		}
		header("Content-Type: $fileType");
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Disposition: attachment; filename=$fileName");
		header("Content-Transfer-Encoding: binary");
		header("Content-length: " . filesize($filePath));
		// read file
		readfile($filePath);
		exit();		
	}
}
