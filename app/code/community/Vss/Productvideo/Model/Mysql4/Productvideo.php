<?php

class Vss_Productvideo_Model_Mysql4_Productvideo extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('productvideo/productvideo', 'video_id');
        $this->_isPkAutoIncrement = true;
    }
}