<?php

class Vss_Productvideo_Model_Productvideo extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('productvideo/productvideo');
    }
    
    public function deleteGridRow($id)
    {
        $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write'); 
        $where = array($write_connection ->quoteInto('video_id =?', $id));
        $write_connection->delete('productvideo',$where);
    }
}

