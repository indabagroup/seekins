<?php

$installer = $this;

$installer->startSetup();

$prefix = Mage::getConfig()->getTablePrefix();

$installer->run("
CREATE TABLE IF NOT EXISTS `".$prefix."productvideo` (
`video_id` int(10) NOT NULL AUTO_INCREMENT,
  `video_name` Varchar(200),
  `video_type` ENUM('YouTube', 'Vimeo', 'Dailymotion') NOT NULL,
  `video_path` Varchar(400),
  `video_status` ENUM('Active', 'Inactive') NOT NULL,
  `product_id` int(9),
  `main_video` ENUM('No', 'Yes') NOT NULL,
  `date_added` timestamp,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
");
 
$installer->endSetup();
