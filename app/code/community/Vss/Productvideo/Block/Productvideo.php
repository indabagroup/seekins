<?php

class Vss_Productvideo_Block_Productvideo extends Mage_Core_Block_Template
{
    public function __construct()
    {
        if ($this->hasData('template')) {
            $this->setTemplate($this->getData('template'));
        }
    }
    
    public function getMainVideo($product_id)
    {
        $collection = Mage::getModel('productvideo/productvideo')->getCollection()
                ->addFieldToFilter('product_id', $product_id)
                ->addFieldToFilter('video_status', "Active")
                ->addFieldToFilter('main_video', "Yes");
        
        $mainVideo = array();
        
        foreach($collection as $item)
        {
            $mainVideo['video_type'] = $item['video_type'];
            $mainVideo['video_path'] = $item['video_path'];
        }
        
        return $mainVideo;
    }
    
    public function getVideoDataByProductId($product_id)
    {
        $collection = Mage::getModel('productvideo/productvideo')->getCollection()
                ->addFieldToFilter('product_id', $product_id)
                ->addFieldToFilter('video_status', "Active")
                ->addFieldToFilter('main_video', "No");
        
        $productVideos = array();
        
        // Getting only useful data. Video Type and Video Path
        foreach($collection as $item)
        {
            $productVideos['video_type'][] = $item['video_type'];
            $productVideos['video_path'][] = $item['video_path'];
        }
        
        return $productVideos;
    }
    
    public function getSettings()
    {
        $savedSettings = Mage::getStoreConfig('vss/productvideo/settings');
        return unserialize($savedSettings);
    }
    
    public function getVideoAttributes()
    {
        $savedSettings = unserialize(Mage::getStoreConfig('vss/productvideo/settings'));
        
        if(isset($savedSettings['y_autoplay']))
        {
            $y_autoplay = 1;
        }
        else
        {
            $y_autoplay = 0;
        }
        if(isset($savedSettings['y_display_controls']))
        {
            $y_display_controls = 1;
        }
        else
        {
            $y_display_controls = 0;
        }
        if(isset($savedSettings['y_display_frame_border']))
        {
            $y_display_frame_border = 1;
        }
        else
        {
            $y_display_frame_border = 0;
        }
        if(isset($savedSettings['v_autoplay']))
        {
            $v_autoplay = 1;
        }
        else
        {
            $v_autoplay = 0;
        }
        if(isset($savedSettings['v_display_controls']))
        {
            $v_display_controls = 1;
        }
        else
        {
            $v_display_controls = 0;
        }
        if(isset($savedSettings['v_display_frame_border']))
        {
            $v_display_frame_border = 1;
        }
        else
        {
            $v_display_frame_border = 0;
        }
        if(isset($savedSettings['d_autoplay']))
        {
            $d_autoplay = 1;
        }
        else
        {
            $d_autoplay = 0;
        }
        if(isset($savedSettings['d_display_controls']))
        {
            $d_display_controls = 1;
        }
        else
        {
            $d_display_controls = 0;
        }
        if(isset($savedSettings['d_display_frame_border']))
        {
            $d_display_frame_border = 1;
        }
        else
        {
            $d_display_frame_border = 0;
        }
        
        $videoAttributes = array();
        $videoAttributes['y_autoplay'] = $y_autoplay;
        $videoAttributes['y_display_controls'] = $y_display_controls;
        $videoAttributes['y_display_frame_border'] = $y_display_frame_border;
        $videoAttributes['v_autoplay'] = $v_autoplay;
        $videoAttributes['v_display_controls'] = $v_display_controls;
        $videoAttributes['v_display_frame_border'] = $v_display_frame_border;
        $videoAttributes['d_autoplay'] = $d_autoplay;
        $videoAttributes['d_display_controls'] = $d_display_controls;
        $videoAttributes['d_display_frame_border'] = $d_display_frame_border;
        
        return $videoAttributes;
    }
    
}