<?php
class Vss_Productvideo_Block_Adminhtml_Productvideo_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        $this->setUseAjax(true);

        parent::__construct();
        $this->setId('productvideo_grid');
        // This is the primary key of the database
        $this->setDefaultSort('video_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
     
    protected function _getCollectionClass()
    {
        // This is the model we are using for the grid
        return 'vss_productvideo/productvideo_collection';
    }
     
    protected function _prepareCollection()
    {
        // Get and set our collection for the grid
        $collection = Mage::getModel('productvideo/productvideo')->getCollection();
        $this->setCollection($collection);
         
        return parent::_prepareCollection();
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('massaction_id');
        // Adding Item in Massaction selectbox
        $this->getMassactionBlock()->addItem('delete', array(
                    'label'     => Mage::helper('colors')->__('Delete'),
                    'url'       => $this->getUrl('*/*/massDelete'),
                    'confirm'   => Mage::helper('colors')->__('Are you sure?'),
                ));

        return $this;
    }
     
    protected function _prepareColumns()
    {
        // Add the columns that should appear in the grid
        
        $this->addColumn('video_id',
            array(
                'header'=> $this->__('ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'video_id'
            )
        );
         
        $this->addColumn('video_name',
            array(
                'header'=> $this->__('Name'),
                'index' => 'video_name'
            )
        );
        
        $this->addColumn('video_path',
            array(
                'header'=> $this->__('Video Path'),
                'index' => 'video_path'
            )
        );
        
        $this->addColumn('video_status',
            array(
                'header'=> $this->__('Status'),
                'index' => 'video_status'
            )
        );
        
        $this->addColumn('product_id',
            array(
                'header'=> $this->__('Product ID'),
                'index' => 'product_id'
            )
        );
        
        $this->addColumn('main_video',
            array(
                'header'=> $this->__('Default Video'),
                'index' => 'main_video'
            )
        );
        
        $this->addColumn('date_added',
            array(
                'header'=> $this->__('Date Added'),
                'index' => 'date_added',
                'type'  => 'datetime'
            )
        );
        
        $this->addColumn('action',
            array(
            'header' => $this->__('Action'),
            'align' => 'center',
            'renderer' =>  'Vss_Productvideo_Block_Adminhtml_Productvideo_Renderer_Action',
            'filter'    => false,
            'sortable'  => false
            )
        );
        
        return parent::_prepareColumns();
    }
     
    public function getRowUrl($row)
    {
//         This is where our row data will link to
//        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}