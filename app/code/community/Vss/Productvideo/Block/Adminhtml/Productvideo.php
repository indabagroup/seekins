<?php

class Vss_Productvideo_Block_Adminhtml_Productvideo extends Mage_Adminhtml_Block_System_Config_Switcher
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_productvideo';
        $this->_blockGroup = 'productvideo';
        parent::__construct();
        
    }
    
    public function getSettings()
    {
        $savedSettings = Mage::getStoreConfig('vss/productvideo/settings');
        return unserialize($savedSettings);
    }
    
    // Function to check if the module is running first time on the device
    public function getFirstTimeChecker()
    {
        $activeExists = Mage::getStoreConfig('vss/productvideo/active');
        if(isset($activeExists))
        {
            return 1;
        }
        else
        {
            return 0;
        }
        
    }
    
    public function getAllVideoDataByProductId($product_id)
    {
        $collection = Mage::getModel('productvideo/productvideo')->getCollection()
                ->addFieldToFilter('product_id', $product_id);
        return $collection;
    }
    
    public function getVideoDataById($video_id)
    {
        $collection = Mage::getModel('productvideo/productvideo')->getCollection()
            ->addFieldToFilter('video_id', $video_id);
        
        $video = array();
        foreach($collection as $item)
        {
            $video['video_id'] = $item['video_id'];
            $video['video_name'] = $item['video_name'];
            $video['video_type'] = $item['video_type'];
            $video['video_path'] = $item['video_path'];
            $video['video_status'] = $item['video_status'];
            $video['product_id'] = $item['product_id'];
            $video['main_video'] = $item['main_video'];
        }
        return $video;
    }
}
