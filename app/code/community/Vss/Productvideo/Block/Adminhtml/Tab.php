<?php
class Vss_Productvideo_Block_Adminhtml_Tab extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function canShowTab()
    {
        $savedSettings = Mage::getStoreConfig('vss/productvideo/settings');
        $savedSettings = unserialize($savedSettings);

        // Shows the product video tab only when the module is on
        if(isset($savedSettings['enable']))
        {
            return true;
        }
        return false;
    }
    public function getTabLabel()
    {
        return $this->__('Product Video');
    }
    public function getTabTitle()
    {
        return $this->__('Product Video');
    }
    public function isHidden()
    {
        return false;
    }
    public function getTabUrl()
    {
        // Getting the url including key
        // http://localhost/Magento/magento/index.php/productvideo/adminhtml_index/videoGrid/key/2570c11fc8d4d9810591fb12b1a9f3f0/
        // Getting the current product id and passing it to our module grid
        $id = $this->getRequest()->getParam('id');
        $url = Mage::helper("adminhtml")->getUrl("*/productvideo/videoGrid", array('product_id' => $id));
        return $url;
    }
    public function getTabClass()
    {
        return 'ajax';
    }
}