<?php
class Vss_Productvideo_Adminhtml_ProductvideoController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('Product Video'));
        $this->renderLayout();
    }

    public function configAction()
    {
        $post_data = $this->getRequest()->getPost();

        // Convert html special chars so that they are not processed as tags
        foreach($post_data as $key=>$value)
        {
            $post_data[$key] = htmlspecialchars($value);
        }

        if(isset($post_data['store_switcher']))
        {
            $scope = $post_data['store_switcher'];
        }

        $coreModel = Mage::getModel("core/config");
        if (!empty($post_data))
        {
            $coreModel->saveConfig('vss/productvideo/settings', serialize($post_data), $scope);

            if (isset($post_data['enable']))
            {
                $coreModel->saveConfig('vss/productvideo/active', 1, $scope);
            }
            else
            {
                $coreModel->saveConfig('vss/productvideo/active', 0, $scope);
            }

            if (isset($post_data['jquery']))
            {
                $coreModel->saveConfig('vss/productvideo/jquery', 1, $scope);
            }
            else
            {
                $coreModel->saveConfig('vss/productvideo/jquery', 0, $scope);
            }

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__("Settings saved successfully."));
            Mage::app()->getCacheInstance()->cleanType('config');
        }
        $this->_redirectReferer();
    }

    public function editAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('Edit - Product Video'));
        $this->renderLayout();
    }

    public function addAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('Add - Product Video'));
        $this->renderLayout();
    }

    public function videoGridAction()
    {
        echo  Mage::app()->getLayout()
        ->createBlock('productvideo/adminhtml_productvideo')
        ->setTemplate('vss_productvideo/show_all_videos.phtml')
        ->toHtml();

    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('video_id');

        $model = Mage::getModel('productvideo/productvideo');
        $model->setId($id)->delete();

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Deleted Successfully.'));
        $this->_redirectReferer();
    }

    public function massDeleteAction()
    {
        $dataString = $_GET['data'];
        $ids = json_decode($dataString);
        $arraySize = sizeof($ids);
        if($arraySize < 1)
        {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select video(s) first to perform this action. Can not perform action on blank values.'));
            $this->_redirectReferer();
        }
        else
        {
            $model = Mage::getModel('productvideo/productvideo');
            foreach ($ids as $id)
            {
                $model->setId($id)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Deleted Successfully.'));
            $this->_redirectReferer();
        }
    }

    public function submitEditFormAction()
    {
        $post = $this->getRequest()->getPost();

        // Getting the date added so that it doesn't change on edit
        $collection = Mage::getModel('productvideo/productvideo')->getCollection()
            ->addFieldToFilter('video_id', $post['video_id']);

        foreach($collection as $item)
        {
            $date_added = $item['date_added'];
        }

        // Convert html special chars so that they are not processed as tags
        foreach($post as $key=>$value)
        {
            $post[$key] = htmlspecialchars($value);
        }

        if(isset($post['video_status']))
        {
            $post['video_status'] = "Active";
        }
        else
        {
            $post['video_status'] = "Inactive";
        }

        if(isset($post['main_video']))
        {
            $post['main_video'] = "Yes";
        }
        else
        {
            $post['main_video'] = "No";
        }

        // Converting the full video url into video code
        if($post['video_type'] == "YouTube")
        {
            if(strpos($post['video_path'], 'https://www.youtube.com/watch?v=') !== false)
            {
                $post['video_path'] = str_replace("https://www.youtube.com/watch?v=","",$post['video_path']);
            }
        }
        if($post['video_type'] == "Vimeo")
        {
            if(strpos($post['video_path'], 'https://vimeo.com/') !== false)
            {
                $post['video_path'] = str_replace("https://vimeo.com/","",$post['video_path']);
            }
        }
        if($post['video_type'] == "Dailymotion")
        {
            if(strpos($post['video_path'], 'http://www.dailymotion.com/') !== false)
            {
                $post['video_path'] = str_replace("http://www.dailymotion.com/","",$post['video_path']);
            }
        }
        // If the new video is selected as default video, then set all other videos as not default.
        if($post['main_video'] == "Yes")
        {
            $collection = Mage::getModel('productvideo/productvideo')->getCollection()
                ->addFieldToFilter('product_id', $post['product_id']);
            foreach($collection as $item)
            {
                $video_id = $item['video_id'];
                $change_data_model = Mage::getModel('productvideo/productvideo');
                $change_data_model->setVideoId($video_id);
                $change_data_model->setMainVideo("No");
                $change_data_model->save();
            }
        }

        $video_id = $post['video_id'];
        $video_name = $post['video_name'];
        $video_type = $post['video_type'];
        $video_path = $post['video_path'];
        $video_status = $post['video_status'];
        $product_id = $post['product_id'];
        $main_video = $post['main_video'];

        //Loading model
        $model = Mage::getModel('productvideo/productvideo');
        // Set values into the table
        $model->setVideoId($video_id);
        $model->setVideoName($video_name);
        $model->setVideoType($video_type);
        $model->setVideoPath($video_path);
        $model->setVideoStatus($video_status);
        $model->setProductId($product_id);
        $model->setMainVideo($main_video);
        $model->setDateAdded($date_added);
        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Details edited successfully.'));

        // Redirecting to the product edit page
        $this->_redirect('adminhtml/catalog_product/edit', array('id' => $product_id));

    }

    public function submitAddFormAction()
    {
        $post = $this->getRequest()->getPost();

        // Convert html special chars so that they are not processed as tags
        foreach($post as $key=>$value)
        {
            $post[$key] = htmlspecialchars($value);
        }

        if(isset($post['video_status']))
        {
            $post['video_status'] = "Active";
        }
        else
        {
            $post['video_status'] = "Inactive";
        }

        if(isset($post['main_video']))
        {
            $post['main_video'] = "Yes";
        }
        else
        {
            $post['main_video'] = "No";
        }

        // Converting the full video url into video code
        if($post['video_type'] == "YouTube")
        {
            if(strpos($post['video_path'], 'https://www.youtube.com/watch?v=') !== false)
            {
                $post['video_path'] = str_replace("https://www.youtube.com/watch?v=","",$post['video_path']);
            }
        }
        if($post['video_type'] == "Vimeo")
        {
            if(strpos($post['video_path'], 'https://vimeo.com/') !== false)
            {
                $post['video_path'] = str_replace("https://vimeo.com/","",$post['video_path']);
            }
        }
        if($post['video_type'] == "Dailymotion")
        {
            if(strpos($post['video_path'], 'http://www.dailymotion.com/') !== false)
            {
                $post['video_path'] = str_replace("http://www.dailymotion.com/","",$post['video_path']);
            }
        }

        // If the new video is selected as default video, then set all other videos of that product as not default.
        if($main_video = $post['main_video'] == "Yes")
        {
            $collection = Mage::getModel('productvideo/productvideo')->getCollection()
                ->addFieldToFilter('product_id', $post['product_id']);
            foreach($collection as $item)
            {
                $video_id = $item['video_id'];
                $change_data_model = Mage::getModel('productvideo/productvideo');
                $change_data_model->setVideoId($video_id);
                $change_data_model->setMainVideo("No");
                $change_data_model->save();
            }
        }

        $video_name = $post['video_name'];
        $video_type = $post['video_type'];
        $video_path = $post['video_path'];
        $video_status = $post['video_status'];
        $product_id = $post['product_id'];
        $main_video = $post['main_video'];

        //Loading model
        $model = Mage::getModel('productvideo/productvideo');
        // Set values into the table
        $model->setVideoName($video_name);
        $model->setVideoType($video_type);
        $model->setVideoPath($video_path);
        $model->setVideoStatus($video_status);
        $model->setProductId($product_id);
        $model->setMainVideo($main_video);
//        $model->setDateAdded(strtotime('now'));
        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Video added successfully.'));

//        $this->_redirectReferer();
        $this->_redirect('adminhtml/catalog_product/edit', array('id' => $product_id));

    }
}