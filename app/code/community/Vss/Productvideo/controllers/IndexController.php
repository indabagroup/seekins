<?php
class Vss_Productvideo_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $active = Mage::getStoreConfig('vss/productvideo/active');
        if($active == 1)
        {
            $this->loadLayout();  
            $this->_initLayoutMessages('customer/session');
            $this->getLayout()->getBlock('head')->setTitle($this->__('Product Video'));
            $this->renderLayout();
        }
        else
        {
            Mage::app()->getResponse()->setRedirect(Mage::getBaseUrl());
        }

    }
    
}

