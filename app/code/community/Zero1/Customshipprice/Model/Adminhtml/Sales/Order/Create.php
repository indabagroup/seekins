<?php
class Zero1_Customshipprice_Model_Adminhtml_Sales_Order_Create extends Mage_Adminhtml_Model_Sales_Order_Create
//class Zero1_Customshipprice_Model_Adminhtml_Sales_Order_Create extends BinaryAnvil_Authorize_Model_Sales_Order_Create
{

    const CANCELED_ADMIN_VIEW_ONLY = 'canceled_hidden';

    /**
     * Parse data retrieved from request
     *
     * @param   array $data
     * @return  Mage_Adminhtml_Model_Sales_Order_Create
     */
    public function importPostData($data)
    {
        if (is_array($data)) {
            $this->addData($data);
        } else {
            return $this;
        }

        if (isset($data['account'])) {
            $this->setAccountData($data['account']);
        }

        if (isset($data['comment'])) {
            $this->getQuote()->addData($data['comment']);
            if (empty($data['comment']['customer_note_notify'])) {
                $this->getQuote()->setCustomerNoteNotify(false);
            } else {
                $this->getQuote()->setCustomerNoteNotify(true);
            }
        }

        if (isset($data['billing_address'])) {
            $this->setBillingAddress($data['billing_address']);
        }

        if (isset($data['shipping_address'])) {
            $this->setShippingAddress($data['shipping_address']);
        }

        if (isset($data['shipping_method'])) {
            $this->setShippingMethod($data['shipping_method']);
        }

        if (isset($data['payment_method'])) {
            $this->setPaymentMethod($data['payment_method']);
        }
        
        $reinit_rates = false;
        
        if (isset($data['shipping_amount'])) {            	
            $shippingPrice = $this->_parseShippingPrice($data['shipping_amount']);
            //$this->getQuote()->getShippingAddress()->setShippingAmount($shippingPrice);
        	Mage::getSingleton('core/session')->setCustomshippriceAmount($shippingPrice);
        	$reinit_rates = true;
        }

        if (isset($data['base_shipping_amount'])) {
            $baseShippingPrice = $this->_parseShippingPrice($data['base_shipping_amount']);
            //$this->getQuote()->getShippingAddress()->setBaseShippingAmount($baseShippingPrice, true);
        	Mage::getSingleton('core/session')->setCustomshippriceBaseAmount($baseShippingPrice);
        	$reinit_rates = true;
        }

        if (isset($data['shipping_description'])) {
            //$this->getQuote()->getShippingAddress()->setShippingDescription($data['shipping_description']);
        	Mage::getSingleton('core/session')->setCustomshippriceDescription($data['shipping_description']);
        	$reinit_rates = true;
        }

        if (isset($data['coupon']['code'])) {
            $this->applyCoupon($data['coupon']['code']);
            $reinit_rates = true;
        }
        
        if($reinit_rates)
        {
        	//$this->collectShippingRates();
        	//$this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        	//$this->collectRates();
        	//$this->getQuote()->collectTotals();
        }
        
        return $this;
    }

    protected function _parseShippingPrice($price)
    {
        $price = Mage::app()->getLocale()->getNumber($price);
        $price = $price>0 ? $price : 0;
        return $price;
    }

    public function createOrder()
    {
        $this->_prepareCustomer();
        $this->_validate();
        $quote = $this->getQuote();
        $this->_prepareQuoteItems();
        
        $service = Mage::getModel('sales/service_quote', $quote);
        if ($this->getSession()->getOrder()->getId()) {
            $oldOrder = $this->getSession()->getOrder();
            $originalId = $oldOrder->getOriginalIncrementId();
            if (!$originalId) {
                $originalId = $oldOrder->getIncrementId();
            }
            $orderData = array(
                'original_increment_id'     => $originalId,
                'relation_parent_id'        => $oldOrder->getId(),
                'relation_parent_real_id'   => $oldOrder->getIncrementId(),
                'edit_increment'            => $oldOrder->getEditIncrement()+1,
                'increment_id'              => $originalId.'-'.($oldOrder->getEditIncrement()+1)
            );
            $quote->setReservedOrderId($orderData['increment_id']);
            $service->setOrderData($orderData);
        }

        $order = $service->submit();

        //start add code by eric - copy po_number from sales_quote_payment to sales_order_payment
        $paymentPoNumber = Mage::getModel( 'sales/quote_payment' )->load($quote->getPayment()->getId())->getPoNumber();
        $orderPayment = Mage::getModel( 'sales/order_payment' )->getCollection()
            ->addFieldToFilter( 'parent_id', $order->getId() )->load();
        foreach ( $orderPayment as $paymentItem ) {
            $payment = Mage::getModel( 'sales/order_payment' )->load( $paymentItem->getEntityId() );
            $payment->setCustomPoNumber( $paymentPoNumber )->save();
        }
        //end add code by eric - copy po_number from sales_quote_payment to sales_order_payment
        
        if ((!$quote->getCustomer()->getId() || !$quote->getCustomer()->isInStore($this->getSession()->getStore()))
            && !$quote->getCustomerIsGuest()
        ) {
            $quote->getCustomer()->setCreatedAt($order->getCreatedAt());
            $quote->getCustomer()
                ->save()
                ->sendNewAccountEmail('registered', '', $quote->getStoreId());;
        }
        if ($this->getSession()->getOrder()->getId()) {
            $oldOrder = $this->getSession()->getOrder();

            $this->getSession()->getOrder()->setRelationChildId($order->getId());
            $this->getSession()->getOrder()->setRelationChildRealId($order->getIncrementId());
            $this->getSession()->getOrder()->setDisplayOriginalOrder($quote->getDisplayOriginalOrder());
            $this->getSession()->getOrder()->cancel();
            $currentState = $this->getSession()->getOrder()->getState();

            if($quote->getDisplayOriginalOrder() && $currentState == 'canceled'){
                $this->getSession()->getOrder()->setStatus(self::CANCELED_ADMIN_VIEW_ONLY);
            }

            $this->getSession()->getOrder()->save();

            $order->save();
        }
        if ($this->getSendConfirmation()) {
            $order->sendNewOrderEmail();
        }

        Mage::dispatchEvent('checkout_submit_all_after', array('order' => $order, 'quote' => $quote));

        return $order;
    }

}
