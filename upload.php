<?php

require_once('app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

    $type = 'Filedata';
    if(isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
        try {
            $uploader = new Varien_File_Uploader($type);
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $path = Mage::getBaseDir('media') . DS . 'customer_ffl' . DS;
            /*if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }*/
            $path_parts = pathinfo($_FILES[$type]['name']);
            $_filename = $path_parts['filename']. '_' . date('Ymd_His'). '.' .$path_parts['extension'];

            $uploader->save($path, $_filename );
            $filename = $uploader->getUploadedFileName();
            echo $filename;
        } catch (Exception $e) {
            Mage::logException($e);
        }

    }

