<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'seekinsp_magev11302');

/** MySQL database username */
define('DB_USER', 'seekinsp_usr01');

/** MySQL database password */
define('DB_PASSWORD', 'T3FeFBtUzuzvDnaQwtA3bf');

/** MySQL hostname */
define('DB_HOST', 'db01');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/* FORCE SSL ADMIN */
define('FORCE_SSL_ADMIN', true); 

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K+Q2!3rhBSNJ!lK-`?dQjb ~hd+Jw!U$Up_yd3K af{yz<1G R p5@-,e,Y8:0m<');
define('SECURE_AUTH_KEY',  'i8h~qqW.j/z:Z<Cen}w-4j|%cCC^~u.f;E0FP-LN6*{mx@0)v52!*w1$oNu]FSr-');
define('LOGGED_IN_KEY',    '!OW[&5FE*ip|]0dgD(mL7Z%^7~rChD:N=H?|P(,Nn^7{dK$Y<vf`FKtZ<n+fwSr4');
define('NONCE_KEY',        'ufLi2o$0lIZ-4XfQdR;7V.C`6)lIDrg5ePAS+60=^+y~p2R|v#!^%m=/8:6PW.~0');
define('AUTH_SALT',        '_TVnm-{ W NH)-mDPp$BmY+FVt[-@0NR08_G0A0M,<5?S|[+uU~,_-2=&k6= oj-');
define('SECURE_AUTH_SALT', 'L=[$<|S~0s|)|O`^f-Kk{)+`UOOEGaUt|] cYCMpf*>|x.OdoYI 0ob%=H&l;xC`');
define('LOGGED_IN_SALT',   'e?8wHn[Y~B!1,HOUm.Q{m<-`#V3p}Ximig3|D +f@[snXRs`mc4QCN.8 DDP!ya3');
define('NONCE_SALT',       '_+]!jM=P&/|%Z/;5dLPe~Kf,y:+5wxjVPpL[d0m^/fem;JFs&2u>)0c|.L(xZF4w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpress_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
