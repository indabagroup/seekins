/**
 * Package: BinaryAnvil
 * Author: calabanzas
 * Date: 12/6/13
 * Time: 4:09 PM
 */
jQuery(function($){
    $(document).ready(function () {
        var numberPattern = /[0-9]+/g;

            $('#sidebar').children('#sidebar_tab').css('display', 'none');

            var $sidebar   = $("#sidebar"),
                $sidebarReferenceElement = $("#sidebar-container"),
                $footer = $('footer'),
                $bottom = $('#bottom_wrap'),
                $window    = $(window),
                sidebarOffset     = $sidebar.offset(),
                sidebarReferenceOffset = $sidebarReferenceElement.position(),
                sidebarReference = sidebarReferenceOffset.top,
                bottomWrapHeight = $bottom.height(),
                topPadding = 0;
            $window = $(window);
            $productShop = $('.product-shop');
            var productShopOffset = $productShop.offset();


            $window.scroll(function () {
                if($productShop.length){
                    if ($window.scrollTop() > productShopOffset.top) {
                        $('#sidebar').children('#sidebar_tab').css('display', 'block');
                    } else {
                        $('#sidebar').children('#sidebar_tab').css('display', 'none');
                    }
                }
            });//$window.scroll(function () {


    }); //$(document).ready(function () {
    $(window).load(function () {
        var $sidebar   = $("#sidebar"),
            $sidebarReferenceElement = $("#sidebar-container"),
            $footer = $('footer'),
            $bottom = $('#bottom_wrap'),
            $window    = $(window),
            sidebarOffset     = $sidebar.offset(),
            sidebarReferenceOffset = $sidebarReferenceElement.position(),
            sidebarReference = sidebarReferenceOffset.top,
            bottomWrapHeight = $bottom.height(),
            topPadding = 0;
        $window = $(window);

        $window.scroll(function () {
            if ($window.scrollTop() > sidebarReference) {
                var scrollAmount = $window.scrollTop() + $sidebar.height() + bottomWrapHeight + 30;
                var footerPosition = $footer.position();
                var footerOffsetTop = footerPosition.top;
                if (scrollAmount < footerOffsetTop){
                    if ($window.scrollTop() - (sidebarOffset.top) > 0)
                    $sidebar.stop().animate({
                        top: $window.scrollTop() - (sidebarOffset.top) + topPadding
                    });
                }

            } else {
                $sidebar.stop().animate({
                    top: 0
                });
            }
        });//$window.scroll(function () {

    });//$(window).load(function () {
});


