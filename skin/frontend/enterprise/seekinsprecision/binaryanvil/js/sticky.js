/**
 * Package: BinaryAnvil
 * Author: calabanzas
 * Date: 12/6/13
 * Time: 4:09 PM
 */
jQuery(function($){
    $(document).ready(function () {
        var numberPattern = /[0-9]+/g;
        if (!$('#content').hasClass('grouped')) {
            $('#sidebar').children('.add_to_box').css('display', 'none');
            $('#sidebar').children('#sidebar_tab').css('display', 'none');
            /*$sidebar = $("#sidebar");

            $bottom = $('#bottom_wrap');
            $reference = $('#sidebar-container');
            var referenceOffset = $reference.offset();
            var referenceOffsetTop = referenceOffset.top;
            $footer = $('footer');
            var bottomWrapHeight = $bottom.height();
            var sidebarOffset = $sidebar.offset();
            var bottomWrapOffset = $bottom.offset();
            var numberPattern = /[0-9]+/g;*/
            var $sidebar   = $("#sidebar"),
                $sidebarReferenceElement = $("#sidebar-container"),
                $footer = $('footer'),
                $bottom = $('#bottom_wrap'),
                $window    = $(window),
                sidebarOffset     = $sidebar.offset(),
                sidebarReferenceOffset = $sidebarReferenceElement.position(),
                sidebarReference = sidebarReferenceOffset.top,
                bottomWrapHeight = $bottom.height(),
                topPadding = 0;
            $window = $(window);
            $productShop = $('.product-shop');
            var productShopOffset = $productShop.offset();


            $window.scroll(function () {
                /*if ($window.scrollTop() > sidebarReference) {
                    var scrollAmount = $window.scrollTop() + $sidebar.height() + bottomWrapHeight + 30;
                    var footerPosition = $footer.position();
                    var footerOffsetTop = footerPosition.top;
                    if (scrollAmount < footerOffsetTop){
                        $sidebar.stop().animate({
                            top: $window.scrollTop() - (sidebarOffset.top) + topPadding
                        });
                    }

                } else {
                    $sidebar.stop().animate({
                        top: 0
                    });
                }*/
                /*if ($window.scrollTop() > referenceOffsetTop) {

                    var scrollAmount = $window.scrollTop() + $sidebar.height() + bottomWrapHeight + 30;
                    var footerOffset = $footer.offset();
                    var footerOffsetTop = footerOffset.top;
                    //console.log(scrollAmount+ "---"+(footerOffsetTop));
                    if (scrollAmount >= footerOffsetTop){

                    } else {
                        $sidebar.stop().animate({
                            //top: sidebarOffset.top - $window.scrollTop()
                            top: $window.scrollTop() - sidebarOffset.top
                        });
                    }
                } else {
                    $sidebar.stop().animate({
                        top: 0
                    });
                }*/

                if ($window.scrollTop() > productShopOffset.top) {
                    $('#sidebar').children('.add_to_box').css('display', 'block');
                    $('#sidebar').children('#sidebar_tab').css('display', 'block');
                    /*if ($('#product-options-wrapper').length > 0) {
                        if ($('#sidebar').children('.add_to_box').find('.configure').children('#product-options-wrapper').length < 1) {
                            $('#sidebar').children('.add_to_box').find('.configure').append($('#content').find('#product-options-wrapper').clone());
                            $sidebarSelects = $('#sidebar').find('#product-options-wrapper').find('select.super-attribute-select');
                            if ($sidebarSelects.length == 1) {
                                var attributeId = $sidebarSelects.attr('id').match(numberPattern);
                                $sidebarSelects.attr('id','select-'+attributeId);
                                $sidebarSelects.removeClass('super-attribute-select');
                                $sidebarSelects.addClass('select-clone');
                                $sidebarSelects.attr('onchange','cloneUpdateValues(this)');
                                if (jQuery('.color-swatch-wrapper').length > 0) {
                                    $('#sidebar').find('#product-options-wrapper').find('.color-swatch-wrapper').remove();
                                    //$('#sidebar').find('#product-options-wrapper').find('.color-swatch-wrapper').find('ul').attr('id','ul-'+attributeId);
                                    //$('#sidebar').find('#product-options-wrapper').find('color-swatch-wrapper').find('input').attr('id','hidden-'+attributeId);
                                }
                            } else {
                                $sidebarSelects.each(function() {
                                    var attributeId = $(this).attr('id').match(numberPattern);
                                    $(this).attr('id','select-'+attributeId);
                                    $(this).removeClass('super-attribute-select');
                                    $(this).addClass('select-clone');
                                    $(this).attr('onchange','cloneUpdateValues(this)');
                                    if (jQuery('.color-swatch-wrapper').length > 0) {
                                        $(this).parent().siblings('.color-swatch-wrapper').remove();
                                        //$(this).parent().siblings('.color-swatch-wrapper').find('ul').attr('id','ul-'+attributeId);
                                        //$(this).parent().siblings('.color-swatch-wrapper').find('input').attr('id','hidden-'+attributeId);
                                    }
                                });
                            }
                        }
                    }*/
                } else {
                    $('#sidebar').children('.add_to_box').css('display', 'none');
                    $('#sidebar').children('#sidebar_tab').css('display', 'none');
                }
            });//$window.scroll(function () {

            if ($('#product-options-wrapper').length > 0) {
                if ($('#sidebar').children('.add_to_box').find('.configure').children('#product-options-wrapper').length < 1) {
                    $('#sidebar').children('.add_to_box').find('.configure').append($('#content').find('#product-options-wrapper').clone());
                    $sidebarSelects = $('#sidebar').find('#product-options-wrapper').find('select.super-attribute-select');
                    if ($sidebarSelects.length == 1) {
                        var attributeId = $sidebarSelects.attr('id').match(numberPattern);
                        $sidebarSelects.attr('id','select-'+attributeId);
                        $sidebarSelects.removeClass('super-attribute-select');
                        $sidebarSelects.addClass('select-clone');
                        $sidebarSelects.attr('onchange','cloneUpdateValues(this)');
                        if (jQuery('.color-swatch-wrapper').length > 0) {
                            $('#sidebar').find('#product-options-wrapper').find('.color-swatch-wrapper').remove();
                            //$('#sidebar').find('#product-options-wrapper').find('.color-swatch-wrapper').find('ul').attr('id','ul-'+attributeId);
                            //$('#sidebar').find('#product-options-wrapper').find('color-swatch-wrapper').find('input').attr('id','hidden-'+attributeId);
                        }
                    } else {
                        $sidebarSelects.each(function() {
                            var attributeId = $(this).attr('id').match(numberPattern);
                            $(this).attr('id','select-'+attributeId);
                            $(this).removeClass('super-attribute-select');
                            $(this).addClass('select-clone');
                            $(this).attr('onchange','cloneUpdateValues(this)');
                            if (jQuery('.color-swatch-wrapper').length > 0) {
                                $(this).parent().siblings('.color-swatch-wrapper').remove();
                                //$(this).parent().siblings('.color-swatch-wrapper').find('ul').attr('id','ul-'+attributeId);
                                //$(this).parent().siblings('.color-swatch-wrapper').find('input').attr('id','hidden-'+attributeId);
                            }
                        });
                    }
                }
            }

        }// end of if (!$('#content').hasClass('grouped')) {

        // start - qty clones
        $('#qty-clone').on("change", function() {
            $('#qty').val($(this).val());
        });
        $('#qty').on("change", function() {
            $('#qty-clone').val($(this).val());
        });
        // end - qty clones

    }); //$(document).ready(function () {
    $(window).load(function () {
        if (!$('#content').hasClass('grouped')) {
        var $sidebar   = $("#sidebar"),
            $sidebarReferenceElement = $("#sidebar-container"),
            $footer = $('footer'),
            $bottom = $('#bottom_wrap'),
            $window    = $(window),
            sidebarOffset     = $sidebar.offset(),
            sidebarReferenceOffset = $sidebarReferenceElement.position(),
            sidebarReference = sidebarReferenceOffset.top,
            bottomWrapHeight = $bottom.height(),
            topPadding = 0;
        $window = $(window);

        $window.scroll(function () {
            if ($window.scrollTop() > sidebarReference) {
                var scrollAmount = $window.scrollTop() + $sidebar.height() + bottomWrapHeight + 30;
                var footerPosition = $footer.position();
                var footerOffsetTop = footerPosition.top;
                if (scrollAmount < footerOffsetTop){
                    if ($window.scrollTop() - (sidebarOffset.top) > 0)
                    $sidebar.stop().animate({
                        top: $window.scrollTop() - (sidebarOffset.top) + topPadding
                    });
                }

            } else {
                $sidebar.stop().animate({
                    top: 0
                });
            }
        });//$window.scroll(function () {
        }

        var top_value = $('#collateral-tabs').height();
        var initZIndexVal = $('#prodReviewTab').css('zIndex');
        $('#collateral-tabs').find('dt.tab-not-tab').on("click", function() {
            if($('#prodReviewTab').hasClass('active')) {
                $('#collateral-tabs').find("dt").removeClass("active");
                $(this).addClass('active');
                $('#prodReviewTab').siblings('dd.tab-container').css('display', 'none');
                $('#prodReviewTab').siblings('dd.tab-container').hide();
                var zIndexVal = $('#prodReviewTab').css('zIndex');
                $('#prodReviewTab').removeClass('active');
                $('#prodReviewTab').css('z-index', initZIndexVal);
                $('dt.tab.first').css('z-index', zIndexVal);
                $('dt.tab.first').siblings('dd.tab-container').not("dd.review_tab_container").css('display', 'block');
                $('#collateral-tabs').css('height', top_value+'px');
                $('dt.tab.first').trigger('click');
                $('#collateral-tabs').find("dt.tab").removeClass("active");
                $(this).addClass('active');

                var classEq = $(this).attr('class').split(' ')[1];
                $('.my_tabs').find('li a').each(function() {
                    $(this).removeClass("active");
                });
                $('.my_tabs').find('a.'+classEq).addClass('active');
                if ($(this).find('a').attr('href') != '#') {
                    var scrollToVal = $(this).find('a').attr('href');
                    $('html,body').animate({
                        scrollTop: $(scrollToVal).offset().top
                    });
                }
            } else {
                $('#collateral-tabs').find("dt").removeClass("active");
                $(this).addClass('active');
                var classEq = $(this).attr('class').split(' ')[1];
                $('.my_tabs').find('li a').each(function() {
                    $(this).removeClass("active");
                });
                $('.my_tabs').find('a.'+classEq).addClass('active');
            }
        });
        $('#collateral-tabs').find('dt.tab').on("click", function() {
            $('#collateral-tabs').find("dt").removeClass("active");
            var classEq = $(this).attr('class').split(' ')[1];
            $('.my_tabs').find('li a').each(function() {
                $(this).removeClass("active");
            });
            $('.my_tabs').find('a.'+classEq).addClass('active');
            if($(this).hasClass('active') == false) {
                $(this).addClass('active');
            }
            if (classEq == "details") {
                $('html,body').animate({
                    scrollTop: $('#details').offset().top
                });
            }
        });
        var top_value = $('#collateral-tabs').height();
        $('.my_tabs').find('a').on("click", function() {
            if(!$(this).hasClass('active')) {
                var tabClass = $(this).attr('class').split(' ')[0];
                var initZIndexVal = $('dt.active').css('zIndex');
                if ($(this).attr('href') != '#') {
                    if($('#prodReviewTab').hasClass('active')) {
                        $('dt.tab').siblings('dd.tab-container').each(function(){
                            $(this).css('display', 'none');
                            $(this).hide();
                        });
                        var zIndexVal = $('#prodReviewTab').css('zIndex');
                        $('#prodReviewTab').removeClass('active');
                        $('#prodReviewTab').css('z-index', initZIndexVal);
                        $('dt.tab.first').css('z-index', zIndexVal);
                        $('dt.tab.first').siblings('dd.tab-container').not("dd.review_tab_container").css('display', 'block');
                        top_value = $('dd#descriptionheader').height() + 15;
                        $('#collateral-tabs').css('height', top_value+'px');
                        $('#collateral-tabs').find("dt").each(function(){
                            $(this).removeClass("active");
                            $(this).css('zIndex',2);
                        });
                        $('.my_tabs').find('li a').each(function() {
                            $(this).removeClass("active");
                        });
                        $('dt.tab-not-tab.'+tabClass).addClass('active');
                        $('dt.tab.'+tabClass).addClass('active');
                        $(this).addClass("active");
                        if ($(this).attr('href') != '#') {
                            var scrollToVal = $(this).attr('href');
                            $('html,body').animate({
                                scrollTop: $(scrollToVal).offset().top
                            });
                        }
                    } else {
                        $('#collateral-tabs').find("dt").each(function(){
                            $(this).removeClass("active");
                            $(this).css('zIndex',2);
                        });
                        $('.my_tabs').find('li a').each(function() {
                            $(this).removeClass("active");
                        });
                        $('dt.tab-not-tab.'+tabClass).addClass('active');
                        $('dt.tab.'+tabClass).addClass('active');
                        $(this).addClass("active");
                        top_value = $('dd.description').height() + 15;
                        $('#collateral-tabs').css('height', top_value+'px');
                        if (tabClass == 'review-tab') {
                            $('dt.tab').siblings('dd.tab-container').each(function(){
                                $(this).css('display', 'none');
                                $(this).hide();
                            });
                            top_value = $('dd.'+tabClass).height() + 15;
                            $('dt.tab.'+tabClass).siblings('dd.'+tabClass).css('display', 'block');
                            $('#collateral-tabs').css('height', top_value+'px');
                            if ($(this).attr('href') != '#') {
                                var scrollToVal = $(this).attr('href');
                                $('html,body').animate({
                                    scrollTop: $(scrollToVal).offset().top
                                });
                            }
                        } //if (tabClass == 'review-tab') {
                    }
                } else {
                    $('dt.tab').siblings('dd.tab-container').each(function(){
                        $(this).css('display', 'none');
                        $(this).hide();
                    });
                    $('#collateral-tabs').find("dt").each(function(){
                        $(this).removeClass("active");
                        $(this).css('zIndex',2);
                    });
                    $('.my_tabs').find('li a').each(function() {
                        $(this).removeClass("active");
                    });
                    $('dt.tab.'+tabClass).css('z-index', initZIndexVal);
                    $('dt.tab.'+tabClass).siblings('dd.'+tabClass).css('display', 'block');
                    $('dt.tab.'+tabClass).addClass('active');
                    $(this).addClass("active");
                    var dtHeight = $('dt.tab.'+tabClass).height();
                    var ddHeight = $('dd.tab-container.'+tabClass).height();
                    var collateralHeight = dtHeight + ddHeight;
                    $('#collateral-tabs').css('height', collateralHeight+'px');
                }

            }
            return false;
        });
    });//$(window).load(function () {
});

function proceedAddToCart(){
    jQuery( "#add-button" ).trigger( "click" );
}
/*function proceedAddToCart(){
    jQuery( "#add-button" ).trigger( "click" );
}
function updateFormData(){
    var pattern = /[0-9]+/g;
    if (jQuery('#sidebar').find('#product-options-wrapper').find('select').length == 1) {
        var matches = jQuery('#sidebar').find('#product-options-wrapper').find('select').attr('id').match(pattern);
        jQuery('#attribute'+matches).val(jQuery('#sidebar').find('#product-options-wrapper').find('select').val());
    } else {
        jQuery('#sidebar').find('#product-options-wrapper').find('select').each(function() {
            var matches = jQuery(this).attr('id').match(pattern);
            jQuery('#attribute'+matches).val(jQuery(this).val());
        });
    }
    if (productAddToCartForm.validator && productAddToCartForm.validator.validate()) {
        jQuery( "#add-button" ).trigger( "click" );
    } else {
        if (jQuery('#sidebar').find('#product-options-wrapper').find('select').length == 1) {
            jQuery('#sidebar').find('#product-options-wrapper').find('select').addClass('validation-failed');
            jQuery('#sidebar').find('#product-options-wrapper').find('select').parent().append('<div class="validation-advice" style="display:block">This is a required field.</div>');
            jQuery('#sidebar').find('#product-options-wrapper').find('select').parent().parent('.input-box').addClass('validation-error');
        } else {
            jQuery('#sidebar').find('#product-options-wrapper').find('select').each(function() {
                if (jQuery(this).val() === "") {
                    jQuery(this).addClass('validation-failed');
                    jQuery(this).parent().append('<div class="validation-advice" style="">This is a required field.</div>');
                    jQuery(this).parent().parent('.input-box').addClass('validation-error');
                } else {
                    jQuery(this).removeClass('validation-failed');
                    jQuery(this).siblings('div.validation-advice').remove();
                    jQuery(this).parent().parent('.input-box').removeClass('validation-error');
                }
            });
        }
    }
}*/

/*function cloneValues(ths)
{
    var pattern = /[0-9]+/g;
    var matches = jQuery(ths).attr('id').match(pattern);
    var attributeId = matches;
    jQuery('#attribute'+matches).val(ths.value);
    jQuery('#select-'+matches).val(ths.value);
    jQuery('#attribute'+matches).trigger("change");
    if (jQuery('.color-swatch-wrapper').length > 0) {
        var selectVal = ths.value;
        var selectText = jQuery(ths).find("option:selected").text();
        var htmlLabel = jQuery('#attribute'+matches).parent('div').parent('dd').siblings('dt').children('label').html();
        if (jQuery('#attribute'+matches).parent('div').parent('dd').siblings('dt').children('label').children('span.option-name').length > 0) {
            jQuery('#attribute'+matches).parent('div').parent('dd').siblings('dt').children('label').children('span.option-name').html(selectText);
        } else {
            jQuery('#attribute'+matches).parent('div').parent('dd').siblings('dt').children('label').html(htmlLabel+"<span class='option-name'>"+selectText+"</span>");
        }
        jQuery('#color-swatch-attribute-'+attributeId).children('li').each(function() {
            jQuery(this).removeClass('active');
        });
        jQuery('.color-swatch-'+matches+'-'+selectVal).addClass('active');
        jQuery('.color-swatch-'+matches+'-'+selectVal).children('img').trigger('click');
        jQuery('.color-swatch-'+matches+'-'+selectVal).children('span').trigger('click');
        jQuery('#hidden-attribute-'+matches).val(selectVal);
        jQuery('#hidden-'+matches).val(selectVal);
    }
    if (jQuery('#sidebar').find('#product-options-wrapper').find('select').length > 1) {
        jQuery('#sidebar').find('#product-options-wrapper').find('select').each(function() {
            var matches = parseInt(jQuery(this).attr('id').match(pattern));
            var selectedId = parseInt(attributeId);
            //&& jQuery('#select-'+matches).is(':disabled')
            if (selectedId != matches) {
                setTimeout(function() {
                    var originalValues = jQuery.map(jQuery('#attribute'+matches+' option'), function(e) { return e.value; });
                    var cloneValues = jQuery.map(jQuery('#select-'+matches+' option'), function(e) { return e.value; });
                    //console.log(originalValues);
                    //console.log(cloneValues);
                    if(JSON.stringify(originalValues) != JSON.stringify(cloneValues)){
                        if (jQuery('#select-'+matches).length > 0) {
                            jQuery('#select-'+matches).parent().html(jQuery('#attribute'+matches).parent().html());
                            jQuery('#sidebar').find('#product-options-wrapper').find('#attribute'+matches).addClass('select-clone');
                            jQuery('#sidebar').find('#product-options-wrapper').find('#attribute'+matches).attr('onchange','cloneValues(this)');
                            jQuery('#sidebar').find('#product-options-wrapper').find('#attribute'+matches).attr('id','select-'+matches);

                        }
                    }*//*else{
                     alert('Collections are equal');
                     }*//*
                }, 500);
            } //end if (selectedId != matches) {
        });
    }
}*/

