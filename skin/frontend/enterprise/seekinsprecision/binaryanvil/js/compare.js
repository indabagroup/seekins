jQuery.noConflict();
jQuery(document).ready(function() { //document ready start
    var martop20MarginTop2 = jQuery('.category-description').css('marginTop');

    compare = {//compare start
        addItem: function(thisItem, removeUrl, imgUrlPH, cmprItemImageUrl, prodUrl) {
            var url = jQuery(thisItem).attr("href");
            var check_product_if_cmp = jQuery(thisItem).parent("li.compared").length;
            var count_empty_cmp_box = jQuery("#compare-items").children("li.empty_cmp_box").length;
            var cat_page_cmp_id = jQuery(thisItem).parent("li").attr("id");
            var productID = cat_page_cmp_id.substring(cat_page_cmp_id.lastIndexOf('_') + 1);
//            var product_name = jQuery(thisItem).attr("alt");

            var product_name = jQuery(thisItem).parent().parent().parent().siblings("h2.product-name").children("a").text();

            var html = "<li class=\"item\" id=\"" + cat_page_cmp_id + "_s\">" +
                    "<input type=\"hidden\" class=\"compare-item-id\" value=\"" + productID + "\" />" +
                    "<a href=\"" + removeUrl + "\" title=\Remove\" class=\"btn-remove\" onclick=\"compare.sidebarRemoveItem(this,'" + url + "', '" + imgUrlPH + "', '" + cmprItemImageUrl + "'); return false;\">Remove</a>" +
                    "<p class=\"product-name\">" +
                    "<a href=\"" + prodUrl + "\">" +
                    "<img src=\"" + cmprItemImageUrl + "\" alt=\"" + product_name + "\" title=\"" + product_name + "\"/>" +
                    "</a>" +
                    "</p>" +
                    "</li>";
            /*class=\"thumb-img-"+cat_page_cmp_id+"\"*/

            var htmlhref = "" +
                    "<a href=\"" + removeUrl + "\" title=\"Remove\" alt=\"" + product_name + "\" rel=\"" + prodUrl + "\" onclick=\"compare.removeItem(this, '" + url + "', '" + imgUrlPH + "', '" + cmprItemImageUrl + "'); return false;\" class=\"link-compare\">- Remove</a>";

            if (check_product_if_cmp == 0 && count_empty_cmp_box > 0) {
                jQuery("#compare-items").children("li.empty_cmp_box:first").before(html);
                jQuery("#compare-items").children("li.empty_cmp_box:first").remove();
                jQuery(thisItem).parent("li").addClass("compared");
                jQuery(thisItem).parent("li").html(htmlhref);
                //					return false;
                new Ajax.Request(url, {
                    parameters: {
                        isAjax: 1,
                        method: 'POST'
                    },
                    onLoading: function() {//$('compare-list-please-wait').show();      	
                        jQuery('body').append('<div class="m-overlay" style="left:0px; top: 0px;width: 100%;height:100%; position: fixed; z-index: 22; background: #000; "></div>');
                        jQuery('.m-overlay').css({
                            opacity: '.3'
                        });

                        if (jQuery('#m-wait').length > 0) {
                            jQuery('#m-wait').css({'display': 'block'});
                        } else {
                            jQuery('body').append('<div id="m-wait">Please wait...</div>');
                        }
                    },
                    onSuccess: function(transport) {
                        setTimeout(function() {
                            if (jQuery('.m-overlay').length > 0) {
                                jQuery('.m-overlay').remove();
                                jQuery('#m-wait').css({'display': 'none'});
                                jQuery('.thumb-img-'+cat_page_cmp_id).css({visibility: 'visible'});
                            }
                        }, 3000);

                        //	                        $('compare-list-please-wait').hide();
                        //window.location.reload();
                        //window.opener.location.reload();
                    }
                });
            } else {
                if (check_product_if_cmp != 0) {
                    alert("The product " + product_name + "is already added to comparison list.");
                } else {
                    alert("You have reached the maximum allowed compare items.");
                }
            }
            if (jQuery(".compare-item-id").length > 0) {
                jQuery(".compare-items-action").css("display", "block");
                jQuery(".block-compare").css("display", "block");
                jQuery(".compare-title").css("display", "block");
            } else {
                jQuery(".block-compare").css("display", "none");
                jQuery(".compare-items-action").css("display", "none");
                jQuery(".compare-title").css("display", "none");
            }
            return false;

        },
        removeItem: function(thisItem, cmpUrl, imgUrlPH, cmprItemImageUrl) {
            var url = jQuery(thisItem).attr("href");
            var product_name = jQuery(thisItem).attr("alt");
            var cat_page_cmp_id = jQuery(thisItem).parent("li").attr("id");
            var productID = cat_page_cmp_id.substring(cat_page_cmp_id.lastIndexOf('_') + 1);
            var prodUrl = jQuery(thisItem).attr("rel");
            var html = "<li class=\"add-to-compare item empty_cmp_box\">" +
                  //             	"<img alt=\"\" src=\""+imgUrlPH+"\">"+
                  "<p>add item here</p></li>";

            var htmlhref = "" +
                    "<a href=\"" + cmpUrl + "\" alt=\"" + product_name + "\" rel=\"" + prodUrl + "\" title=\"Add to compare\" onclick=\"compare.addItem(this, '" + url + "', '" + imgUrlPH + "','" + cmprItemImageUrl + "','" + prodUrl + "'); return false;\" class=\"link-compare\">+ Compare</a>";

            jQuery("li#" + cat_page_cmp_id + "_s").replaceWith(html);
            //            	jQuery("li#"+cat_page_cmp_id+"_s").remove();
            jQuery(thisItem).parent("li").removeClass("compared");
            jQuery(thisItem).parent("li").html(htmlhref);
            //            	jQuery("#compare-items").append(html);
            if (jQuery(".compare-item-id").length > 0) {
                jQuery(".compare-items-action").css("display", "block");
                jQuery(".block-compare").css("display", "block");
                jQuery(".compare-title").css("display", "block");
            } else {
                jQuery(".block-compare").css("display", "none");
                jQuery(".compare-items-action").css("display", "none");
                jQuery(".compare-title").css("display", "none");

                var newScrollTop = jQuery(window).scrollTop() - (parseInt(jQuery('.block-compare').css('marginTop')) + jQuery('.block-compare').outerHeight() + parseInt(jQuery('.category-description').css('marginTop')));

                jQuery('.category-description').animate({
                    marginTop: martop20MarginTop2
                }, 100, function() {
//                    if(newScrollTop > 0){
//                        jQuery('html,body').animate({scrollTop:newScrollTop}, 100);
//                    }else{
//                        jQuery('html,body').animate({scrollTop:0}, 100);
//                    }
                });

            }

            new Ajax.Request(url, {
                parameters: {
                    isAjax: 1,
                    method: 'POST'
                },
                onLoading: function() {
                    //                    	$('compare-list-please-wait').show();
                },
                onSuccess: function(transport) {
                    //                        $('compare-list-please-wait').hide();
                    //window.location.reload();
                    //window.opener.location.reload();
                }
            });
            return false;
        },
        sidebarRemoveItem: function(thisItem, cmpUrl, imgUrlPH, cmprItemImageUrl) {
            var url = jQuery(thisItem).attr("href");
            var cat_page_cmp_id_s = jQuery(thisItem).parent("li").attr("id");
            var cat_page_cmp_id = cat_page_cmp_id_s.substring(0, cat_page_cmp_id_s.indexOf("_s"))
            var product_name = jQuery(thisItem).siblings("p").children().children("img").attr("alt");
            var prodUrl = jQuery(thisItem).siblings("p").children("a").attr("href");
            var html = "<li class=\"add-to-compare item empty_cmp_box\">" +
                    //            	"<img alt=\"\" src=\""+imgUrlPH+"\">"+
                    "</li>";

            var htmlhref = "" +
                    "<a href=\"" + cmpUrl + "\" alt=\"" + product_name + "\" rel=\"" + prodUrl + "\" title=\"Add to compare\" onclick=\"compare.addItem(this, '" + url + "', '" + imgUrlPH + "','" + cmprItemImageUrl + "','" + prodUrl + "'); return false;\" class=\"link-compare\">+ Compare</a>";

            jQuery(thisItem).parent("li").replaceWith(html);
            jQuery("li#" + cat_page_cmp_id).removeClass("compared");
            jQuery("li#" + cat_page_cmp_id).html(htmlhref);

            if (jQuery(".compare-item-id").length > 0) {
                jQuery(".compare-items-action").css("display", "block");
                jQuery(".block-compare").css("display", "block");
                jQuery(".compare-title").css("display", "block");
            } else {
                jQuery(".block-compare").css("display", "none");
                jQuery(".compare-items-action").css("display", "none");
                jQuery(".compare-title").css("display", "none");

                var newScrollTop = jQuery(window).scrollTop() - (parseInt(jQuery('.block-compare').css('marginTop')) + jQuery('.block-compare').outerHeight() + parseInt(jQuery('.category-description').css('marginTop')));

                jQuery('.category-description').animate({
                    marginTop: martop20MarginTop2
                }, 100, function() {
//                    if(newScrollTop > 0){
//                        jQuery('html,body').animate({scrollTop:newScrollTop}, 100);
//                    }else{
//                        jQuery('html,body').animate({scrollTop:0}, 100);
//                    }
                });

            }
            new Ajax.Request(url, {
                parameters: {
                    isAjax: 1,
                    method: 'POST'
                },
                onLoading: function() {
                    //                    	$('compare-list-please-wait').show();
                },
                onSuccess: function(transport) {
                    //                        $('compare-list-please-wait').hide();
                    //window.location.reload();
                    //window.opener.location.reload();
                }
            });
            return false;
        },
        compareItem: function(url) {
            if (jQuery(".compare-item-id").length > 1) {
                window.open(url, 'compare');
                //            		popWin(url,'compare','top:0,left:0,width=820,height=600,resizable=yes,scrollbars=yes');
            } else {
                alert('Select at least 2 items to compare');
            }
        },
        remove_all_compare: function(url, imgUrlPH) {

            var html = "<li class=\"item empty_cmp_box\">" +
                    "</li>";
            jQuery("#compare-items li").each(function() {
                jQuery(this).replaceWith(html);
                var cat_page_cmp_id_s = jQuery(this).attr("id");
                if (cat_page_cmp_id_s) {
                    var cat_page_cmp_id = cat_page_cmp_id_s.substring(0, cat_page_cmp_id_s.indexOf("_s"));
                    var removeurl = jQuery(this).children("a").attr("href");
                    var product_name = jQuery(this).children("p").children("a").children("img").attr("alt");
                    var cmpUrl = url.replace("remove", "add");
                    var cmprItemImageUrl = jQuery(this).find("img").attr("src");
                    var htmlhref = "" +
                            "<a href=\"" + cmpUrl + "\" alt=\"" + product_name + "\" title=\"" + product_name + "\" onclick=\"compare.addItem(this, '" + removeurl + "', '" + imgUrlPH + "','" + cmprItemImageUrl + "'); return false;\" class=\"link-compare\"><small>+</small><span>Compare</span></a>";
                    jQuery("li#" + cat_page_cmp_id).removeClass("compared");
                    jQuery("li#" + cat_page_cmp_id).html(htmlhref);
                    jQuery(this).replaceWith(html);
                }
            });
            if (jQuery(".compare-item-id").length > 0) {
                jQuery(".compare-items-action").css("display", "block");
                jQuery(".block-compare").css("display", "block");
                jQuery(".compare-title").css("display", "block");
            } else {
                jQuery(".block-compare").css("display", "none");
                jQuery(".compare-items-action").css("display", "none");
                jQuery(".compare-title").css("display", "none");
            }
            //             return false;
            new Ajax.Request(url, {
                parameters: {
                    isAjax: 1,
                    method: 'POST'
                },
                //                  onLoading: function(){$('compare-list-please-wait').show();},
                onSuccess: function(transport) {
                    //                      $('compare-list-please-wait').hide();
                }
            });
        }
    } //end compare
}); //document ready end