jQuery(document).ready(function ($) {
    $(".content ul.blocks li.block_wrap.video-youtube .video-wrap .play-vid").click(function () {
        $(".content ul.blocks li.block_wrap.video-youtube .video-wrap").hide();
        $(".content ul.blocks li.block_wrap.video-youtube iframe")[0].src += "&autoplay=1";
        setTimeout(function () {
            $(".content ul.blocks li.block_wrap.video-youtube iframe").show();
        }, 200);
    });
});