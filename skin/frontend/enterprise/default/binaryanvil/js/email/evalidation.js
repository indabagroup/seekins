Validation.addAllThese([
	['validate-cemail', 'Email addresses do not match.', function(v) {
		var conf = $('confirmation') ? $('confirmation') : $$('.validate-cemail')[0];
        var pass = false;
        var confirm;
        if ($('email_address')) {
        	pass = $('email_address');
        }
        confirm =conf.value;
        if(!confirm && $('confirm_email'))
        	confirm = $('confirm_email').value;
        
        return (pass.value == confirm);
	}],
	['validate-contacts-email', 'Email addresses do not match.', function(v) {
		var conf = $('confirmation') ? $('confirmation') : $$('.validate-contacts-email')[0];
        var pass = false;
        var confirm;
        if ($('email')) {
        	pass = $('email');
        }
        confirm =conf.value;
        if(!confirm && $('confirm_email'))
        	confirm = $('confirm_email').value;
        
        return (pass.value == confirm);
	}],
	['validate-nemail', 'Email addresses do not match.', function(v) {
		var conf = $('confirmation') ? $('confirmation') : $$('.validate-nemail')[0];
        var pass = false;
        var confirm;
        if ($('newsletter')) {
        	pass = $('newsletter');
        }
        confirm =conf.value;
        if(!confirm && $('confirm_email'))
        	confirm = $('confirm_email').value;
        
        return (pass.value == confirm);
	}] 
	
]); 