var JQ= jQuery.noConflict();
JQ(document).ready(function($){
	var location = window.location.href;
	if(location.indexOf('checkout/cart') != -1){
		jQuery('#BA_QC .block-title').addClass("qc_disabled");
	}else{
		jQuery('#BA_QC .block-title').removeClass("qc_disabled");
	}	
//	$("#ba_quickcart").fancybox({
//		'titleShow' : false, 
//		'transitionIn' : 'none',
//		'width' : 600,
//		'margin' : 10,
//		'height' : 500,
//		'autoDimensions': false,
//		'transitionOut' : 'none',
//		'overlayColor' : '#000',
//		'overlayOpacity' : 0.5,
//		'centerOnScroll' : true,
//		'showCloseButton' : true,
//		'hideOnOverlayClick' : true,
//		'scrolling': 'auto',
//		'onStart' : function(){
//			var flag = false;
//			var location = window.location.href;
//			if(location.indexOf('checkout/cart') != -1){
//				return false;
////				$.fancybox.cancel();
//			}
//		},
//		'onComplete' : function() {
//		var windowWidth = document.documentElement.clientWidth;
//		var windowHeight = document.documentElement.clientHeight;
//		var popupHeight = $("#fancybox-wrap").height();
//		var popupWidth = $("#fancybox-wrap").width();
//			$("#fancybox-wrap").css({
//			"position": "absolute",
//			"top": ((windowHeight/2) - (popupHeight/2))
//			});
//		}
//	});
	
	
	$("#ba_quickcartcontent .btn-act .button").live('click', function() {
		var location = window.location.href;
		
		if(location.indexOf('checkout/onepage') != -1){
			
			jQuery('#ba_quickcartcontent').html("<div class='redirect'><h1>Redirecting to shopping cart</h1></div>");
//        	jQuery('#fancybox-loading').show();
        	jQuery.fancybox.showActivity();
        	jQuery('#fancybox-content').height(jQuery(".redirect").height()+100);
			jQuery.fancybox.resize();
        	redirect = location.replace("checkout/onepage","checkout/cart");
        	$("#fancybox-overlay").unbind();
        	$("#fancybox-close").unbind();
			window.location.replace(redirect);
//			window.location.href = redirect;
//			window.location = redirect;
//        	window.location = redirect;
        	return false;
		}else{
		
			if($(this).attr("value") == "delete"){
				var deleteUrl = $(this).attr("href");
				deleteUrl = deleteUrl.replace("checkout/cart/delete","quickcart/ajaxcart/ajaxDelete");
				try {
					jQuery.ajax( {
						url : deleteUrl,
						dataType : 'json',
						type : 'post',
						beforeSend: function(){
					        	jQuery('#ba_quickcartcontent').hide();
//					        	jQuery('#fancybox-loading').show();
					        	jQuery.fancybox.showActivity();
						},
						success : function(data) {
							if(jQuery('.qcHeader')){
					            jQuery('.qcHeader').replaceWith(data.sidebar);
					        }
					        if(jQuery('#ba_quickcartcontent')){
					            jQuery('#ba_quickcartcontent').replaceWith(data.sidebarcontent);
//					            jQuery('#fancybox-loading').hide();
					            jQuery.fancybox.hideActivity();
					        }
						}
					});
				} catch (e) {
				}
				return false;
			}else{
			
				var url = $('#BA_QC_Form').attr('action');
				url = url.replace("checkout/cart/updatePost","quickcart/ajaxcart/ajaxupdatePost");
				var qc_data = jQuery('#BA_QC_Form').serialize();
				qc_data += "&update_cart_action="+$(this).attr("value");
				try {
					jQuery.ajax( {
						url : url,
						dataType : 'json',
						type : 'post',
						data : qc_data,
						beforeSend: function(){
					        	jQuery('#ba_quickcartcontent').hide();
//					        	jQuery('#fancybox-loading').show();
					        	jQuery.fancybox.showActivity();
						},
						success : function(data) {
							if(jQuery('.qcHeader')){
					            jQuery('.qcHeader').replaceWith(data.sidebar);
					        }
					        if(jQuery('#ba_quickcartcontent')){
					            jQuery('#ba_quickcartcontent').replaceWith(data.sidebarcontent);
//					            jQuery('#fancybox-loading').hide();
					            jQuery.fancybox.hideActivity();
					        }
		//					setTimeout( function() {
		//						jQuery('#ba_quickcartcontent').show();
		//			        	jQuery('#fancybox-loading').hide();
		//					}, 1000 );
						}
					});
				} catch (e) {
				}
				return false;
			}
		}
	});
});
			