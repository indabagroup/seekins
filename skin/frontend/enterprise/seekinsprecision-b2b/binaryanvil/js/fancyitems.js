jQuery.noConflict();
        		jQuery(document).ready(function(){
        			jQuery('#product-page-feedback').fancybox(
        				{
        					'titleShow' : false, 
        					'transitionIn' : 'none',
        					'autoDimensions': false,
        					'transitionOut' : 'none',
        					'overlayColor' : '#000',
        					'overlayOpacity' : 0.5,
        					'centerOnScroll' : true,
        					'showCloseButton' : true,
        					'hideOnOverlayClick' : true,
        					onStart: function(){
        						jQuery("#fancybox-wrap").removeClass();
        						jQuery("#fancybox-wrap").addClass("productAddResult");
        						jQuery('#fancybox-content').height(jQuery("#product-view-result").height()+50);
        						jQuery.fancybox.resize();
        					},
        					onComplete: function(){	
        						jQuery('#fancybox-content').height(jQuery("#product-view-result").height()+50);
        						jQuery.fancybox.resize();
     					   	}, 
     					   	onClosed :function(){
     					   		jQuery("#fancybox-wrap").removeClass();
        					}
        					
        				}
        			);
        			jQuery('#product-page-loader').fancybox(
            				{
            					'titleShow' : false, 
            					'transitionIn' : 'none',
            					'autoDimensions': false,
            					'transitionOut' : 'none',
            					'overlayColor' : '#000',
            					'overlayOpacity' : 0.5,
            					'centerOnScroll' : true,
            					'showCloseButton' : false,
            					'hideOnOverlayClick' : false,
            					onStart: function(){
            						jQuery("#fancybox-wrap").addClass("loaderContainer");
            						jQuery.fancybox.showActivity();
            					},
            					onComplete: function(){	
            						jQuery('#fancybox-content').height(jQuery("#loader-content").height()+50);
            						jQuery.fancybox.resize();
            						jQuery.fancybox.showActivity();
//            						jQuery("#fancybox-loading").show()
         					   },
         					  onClosed :function(){
       					   		jQuery("#fancybox-wrap").removeClass();
          					}
            					
            				}
            			);
        		});