/**
 * Created with JetBrains PhpStorm.
 * User: camz
 * Date: 7/14/14
 * Time: 7:22 PM
 * To change this template use File | Settings | File Templates.
 */
var BinaryAnvil = Class.create(Checkout, {
    initialize: function($super,accordion, urls){
        this.progressUrlBin = urls.progress;
        $super(accordion, urls);
        //New Code Addded
        this.steps = ['login', 'ffl' ,'billing', 'shipping', 'shipping_method', 'payment', 'review'];
    },
    setMethod: function(){
        if ($('login:guest') && $('login:guest').checked) {
            this.method = 'guest';
            var request = new Ajax.Request(
                this.saveMethodUrl,
                {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'guest'}}
            );
            Element.hide('register-customer-password');
            if ($('login:binaryanvil').checked) {
                this.gotoSection('ffl'); //New Code Here
            } else {
                this.gotoSection('login');
            }
            //this.gotoSection('ffl'); //New Code Here
        }
        else if($('login:register') && ($('login:register').checked || $('login:register').type == 'hidden')) {
            this.method = 'register';
            var request = new Ajax.Request(
                this.saveMethodUrl,
                {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'register'}}
            );
            Element.show('register-customer-password');
            if ($('login:binaryanvil').checked) {
                this.gotoSection('ffl'); //New Code Here
            } else {
                this.gotoSection('billing');
            }

        }
        else{
            alert(Translator.translate('Please choose to register or to checkout as a guest'));
            return false;
        }
    }, newAddress: function(isNew){
        if (isNew) {
            this.resetSelectedAddress();
            Element.show('dropship-new-address-form');
        } else {
            Element.hide('dropship-new-address-form');
        }
//        shipping.setSameAsBilling(false);
    },reloadProgressBlock: function(toStep) {
        this.reloadStep(toStep);
        if (this.syncBillingShipping) {
            this.syncBillingShipping = false;
            this.reloadStep('shipping');
        }
    },

    reloadStep: function(prevStep) {
        var updater = new Ajax.Updater(prevStep + '-progress-opcheckout', this.progressUrlBin, {
            method:'get',
            onFailure:this.ajaxFailure.bind(this),
            onComplete: function(){
                this.checkout.resetPreviousSteps();
            },
            parameters:prevStep ? { prevStep:prevStep } : null
        });
    }
});

var BinaryAnvilMethod = Class.create();
BinaryAnvilMethod.prototype = {
    initialize: function(form, saveUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.save();Event.stop(event);}.bind(this));
        }
        this.saveUrl = saveUrl;
        this.validator = new Validation(this.form);
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    validate: function() {
        if(!this.validator.validate()) {
            return false;
        }
        return true;
    },

    save: function(){

        if (checkout.loadWaiting!=false) return;
        var noFflValue = document.getElementById('no_ffl').value;
        if (!Validation.get('IsEmpty').test(noFflValue) || this.validate()) {
            checkout.setLoadWaiting('ffl');
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        }
    },

    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false);
    },

    nextStep: function(transport){
        if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }

        if (response.error) {
            if (response.exist > 0) {
                var ths = this;
                var chkOut = checkout;
                Dialog.confirm("<h2>FFL already exist. Would you like to be associated to this FFL?</h2><br><address class='checkffl'>"+response.html+"</adress>",
                    {width:505, height:'auto', okLabel: "Yes",
                        buttonClass: "myButtonClass",
                        id: "Associate",
                        cancel:function(win) {
                        },
                        ok:function(win) {
                            var data = Form.serialize(ths.form) + "&associate=1&new_ffl_id="+response.new_ffl_id;
                            var noFflValue = document.getElementById('no_ffl').value;
                            //if (!Validation.get('IsEmpty').test(noFflValue)) {
                                chkOut.setLoadWaiting('ffl');
                                var request = new Ajax.Request(
                                    response.url,
                                    {
                                        method:'post',
                                        onComplete: ths.onComplete,
                                        onSuccess: ths.onSave,
                                        onFailure: chkOut.ajaxFailure.bind(chkOut),
                                        parameters: data
                                    }
                                );
                            //}
                            return true;
                        }
                    });
                /*if(confirm('FFL already exist. Would you like to be associated to this FFL with FFL Number :'+response.number+'?')) {

                }*/
                return false;
                //alert("You are now associated to FFL Number "+ response.number);
            } else {
                alert(response.message);
            }
            return false;
        }

        if (response.update_section) {
            $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
        }


        if (response.goto_section) {
            if (response.new_ffl_id > 0) {
                $('new_ffl_id').setValue(response.new_ffl_id);
            }

            if (response.associate > 0) {
                $('associate').setValue(response.associate);
            }

            checkout.gotoSection(response.goto_section);
            checkout.reloadProgressBlock('ffl');
            return;
        }

        checkout.setBilling();
    }, newAddress: function(isNew){
        if (isNew) {
            Element.show('ffl-form');
        } else {
            Element.hide('ffl-form');
        }
//        shipping.setSameAsBilling(false);
    }
}
