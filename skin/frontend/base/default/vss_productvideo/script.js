function arrowHide()
{
    var x = document.getElementsByClassName("video_frame");
    
    if(x.length == 1)
    {
      var leftArrow = document.getElementById("left_arrow");
      var rightArrow = document.getElementById("right_arrow");
      leftArrow.hide();
      rightArrow.hide();
    }
}

function arrowHideRight()
{
    var x = document.getElementsByClassName("video_frame_right");
    
    if(x.length == 1)
    {
      var leftArrow = document.getElementById("left_arrow_right");
      var rightArrow = document.getElementById("right_arrow_right");
      leftArrow.hide();
      rightArrow.hide();
    }
}

var marginTop = 0;

function plusDivs(n) {
  showDivs(slideIndex += n, n);
}

function showDivs(n, sliderChecker) {
    var i;
    var x = document.getElementsByClassName("video_frame");
    if(slideIndex == 1)
    {
        marginTop = 0;
    }
    if(slideIndex != 1)
    {
        if(sliderChecker == 1)
        {
            marginTop = marginTop - 21;
        }
        else
        {
            marginTop = marginTop + 21;
        }
    }
    if(x.length == 1)
    {
      var leftArrow = document.getElementById("left_arrow");
      var rightArrow = document.getElementById("right_arrow");
      leftArrow.hide();
      rightArrow.hide();
    }
    if (n > x.length)
    {
        slideIndex = 1;
        marginTop = 0;
    }
    if (n < 1)
    {
        slideIndex = x.length;
        marginTop = -21 * (x.length-1);
    }
    for (i = 0; i < x.length; i++)
    {
       x[i].style.width = "0px";
       x[i].style.height = "0px";
//       x[i].style.borderWidth = "0px";
       x[i].style.marginLeft = "50%";

    }
    x[slideIndex-1].style.opacity = "1";
    x[slideIndex-1].style.width = "100%";
    x[slideIndex-1].style.height = "100%";
    x[slideIndex-1].style.marginLeft = "0px";
//    x[slideIndex-1].style.borderWidth = "0px";
    x[slideIndex-1].style.marginTop = marginTop + "px";
}

var marginTopRight = 0;

function plusDivsRight(n) {
  showDivsRight(slideIndexRight += n, n);
}

function showDivsRight(n, sliderChecker) {
    var i;
    var x = document.getElementsByClassName("video_frame_right");
    if(slideIndexRight == 1)
    {
        marginTopRight = 0;
    }
    if(slideIndexRight != 1)
    {
        if(sliderChecker == 1)
        {
            marginTopRight = marginTopRight - 21;
        }
        else
        {
            marginTopRight = marginTopRight + 21;
        }
    }
    if(x.length == 1)
    {
      var leftArrow = document.getElementById("left_arrow_right");
      var rightArrow = document.getElementById("right_arrow_right");
      leftArrow.hide();
      rightArrow.hide();
    }
    if (n > x.length)
    {
        slideIndexRight = 1;
        marginTopRight = 0;
    }
    if (n < 1)
    {
        slideIndexRight = x.length;
        marginTopRight = -21 * (x.length-1);
    }
    for (i = 0; i < x.length; i++)
    {
       x[i].style.width = "0px";
       x[i].style.height = "0px";
//       x[i].style.borderWidth = "0px";
       x[i].style.marginLeft = "50%";

    }
    x[slideIndexRight-1].style.opacity = "1";
    x[slideIndexRight-1].style.width = "100%";
    x[slideIndexRight-1].style.height = "100%";
    x[slideIndexRight-1].style.marginLeft = "0px";
//    x[slideIndexRight-1].style.borderWidth = "0px";
    x[slideIndexRight-1].style.marginTop = marginTopRight + "px";
}