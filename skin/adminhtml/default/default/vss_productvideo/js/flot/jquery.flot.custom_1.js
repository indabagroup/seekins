/*
    Document    : jquery.flot.custom.js
    Author      : Raghubendra Singh
    Description : To Generate the graph(with tooltip) on the basis of passed data.
*/

function drawChart(json)
{
        if(json['error'] != undefined && json['error'] == 'blank')
        {
                jQuery("#chart_div").css("display", "none");
                jQuery("#empty_chart").css("display", "block");
        }
        else
        {
            jQuery("#empty_chart").css("display", "none");
                var i,j;
                var ticks = [];
                var graph_data = [];
                var dataset = [];
                for(i=0; i<json['view']['data'].length; i++ )
                {
                        ticks.push([i, json['view']['data'][i][0]]);
                        graph_data.push([i, json['view']['data'][i][1]]);
                }
                dataset.push({label: "No. of Views", data: graph_data, color: "#5482FF"});
                var options = {
                        series: {
                            grow: {active: true}
                        },
                        bars: {
                            show: true,
                            barWidth: 0.7,
                            fill: 1,
                            align: "center"
                        },

                        xaxis: {
                            axisLabel: json['view']['type'],
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 10,
                            ticks: ticks,
                            autoscaleMargin: 0.01
                        },
                        yaxis: {
                            axisLabel: "No. of Views",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 3,
                            tickFormatter: function(v, axis) {
                                return Math.round(v * 100) / 100;
                            }
                        },
                        legend: {
                            noColumns: 0,
                            labelBoxBorderColor: "#000000",
                            position: "ne"
                        },
                        grid: {
                            hoverable: true,
                            borderWidth: 1,
                            axisMargin: 20,
                            backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                        }
                    };
                jQuery("#chart_div").css("display", "block");
                jQuery.plot(jQuery("#chart_div"), dataset, options);
                jQuery("#chart_div").CreateVerticalGraphToolTip();
        }
}
var previousPoint = null, previousLabel = null;
jQuery.fn.CreateVerticalGraphToolTip = function () {
    jQuery(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                jQuery("#tooltip").remove();
                
                var x = item.datapoint[0];
                var y = item.datapoint[1];

                var color = item.series.color;
                OlGraph_showTooltip(item.pageX, item.pageY, color,
                            "<strong>" + item.series.xaxis.ticks[x].label + "</strong><br><strong>" + item.series.label + "</strong>"  +
                            " : <strong>" + y + "</strong> ");
            }
        } else {
            jQuery("#tooltip").remove();
            previousPoint = null;
        }
    });
};

function OlGraph_showTooltip(x, y, color, contents) {
    jQuery('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 40,
        left: x - 20,
        border: '1px solid ' + color,
        padding: '3px',
        'font-size': '11px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}
