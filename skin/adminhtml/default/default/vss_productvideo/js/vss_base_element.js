/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var decimalOnly = /^[0-9]*(?:\.\d{1,6})?$/;
document.observe('dom:loaded', function() {
    
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function (html) {
        var switchery = new Switchery(html, {color: '#496CAD'});
    });
    
//    $$('div[data-toggle="tooltip"]').each(function (element) {
//        $(element).setAttribute('title', $(element).readAttribute('data-original-title'));
//        new Tooltip(element, {backgroundColor: "#496CAD", borderColor: "#496CAD",
//            textColor: "#fff", textShadowColor: "", mouseFollow: false, opacity: .90});
//    });
    
    jQuery(".vss_custom_select").each(function(){
        jQuery(this).wrap("<span class='vss-select-wrapper'></span>");
        jQuery(this).after("<span class='vss-select-holder'></span>");
        var selectedOption = jQuery(this).find(":selected").text();
        jQuery(this).next(this).text(selectedOption);
    });
    jQuery(".vss_custom_select").change(function(){
        var selectedOption = jQuery(this).find(":selected").text();
        jQuery(this).next(".vss-select-holder").text(selectedOption);
        switchStore(this);
    });

    jQuery(".vss_custom_dropdown").each(function(){
        jQuery(this).wrap("<span class='vss-select-wrapper'></span>");
        jQuery(this).after("<span class='vss-select-holder'></span>");
        var selectedOption = jQuery(this).find(":selected").text();
        jQuery(this).next(this).text(selectedOption);
    });
    jQuery(".vss_custom_dropdown").change(function(){
        var selectedOption = jQuery(this).find(":selected").text();
        jQuery(this).next(".vss-select-holder").text(selectedOption);
    });        
});

function switchStore(e)
{
    location.href = e.options[e.selectedIndex].getAttribute('url')
}

function closeModal(modal)
{
    jQuery('#'+modal).hide();
}

function closeModalAfterReset(modal){
    jQuery('#'+modal+' input[type="text"]').attr('value', '');
    jQuery('#'+modal+' select option').removeAttr('selected');
    jQuery('#'+modal+' textarea').val('');
    jQuery('#'+modal+' .validation_error_msg').remove();
    jQuery('#'+modal).hide();
}

function redirectLocation(url){
    location.href = url;
}


function getProductReviewInfo(product_review_id, product_id){
    new Ajax.Request(getProductReviewInfoUrl, {
                method:'post',
                parameters: {
                        id: product_review_id,
                        pro_id: product_id
                },
                requestHeaders: {Accept: 'application/json'},
                onSuccess: function(transport) {
                    retjson = transport.responseText.evalJSON();
                    jQuery('#vss_product_reviewpopup_name').html(retjson['product_name']);
                    jQuery('#vss_product_reviewpopup_created_date').html(retjson['reviewData'][0]['created_at']);
                    jQuery('#vss_product_reviewpopup_customer_name').html(retjson['reviewData'][0]['nickname']);
                    jQuery('#vss_product_reviewpopup_review_content').html(retjson['reviewData'][0]['detail']);
                    jQuery('#vss_product_reviewpopup_average_rating').css('width',retjson['average_rating']+'%');
                    jQuery('#vss_product_reviewpopup_value_rating').css('width',retjson['ratings'][0]+'%');
                    jQuery('#vss_product_reviewpopup_quality_rating').css('width',retjson['ratings'][1]+'%');
                    jQuery('#vss_product_reviewpopup_price_rating').css('width',retjson['ratings'][2]+'%');
                    jQuery("#vss_product_reviewpopup_customer_name").html(retjson['customer_url']);
                    jQuery("#vss_product_reviewpopup_edit_button").attr("onclick",'window.open( "'+retjson['review_url']+'","_blank");');
                    jQuery('#vssmp_productreview-view-popup_blk').show();
                }
        });
    
}

//function deleteProductReview(product_review_id,product_id){
//    jQuery('#vssmp-reason-popup-blk').show();
//    jQuery('#vssmp_submit_popup_form').click(function(){
//        var reason = jQuery('#vssmp_reason').val();
//        new Ajax.Request(deleteProductReviewUrl, {
//                method:'post',
//                parameters: {
//                        id: product_review_id,
//                        reason: reason,
//                        product_id: product_id
//                },
//                requestHeaders: {Accept: 'application/json'},
//                onSuccess: function(transport) {
//                    closeModal('vssmp-reason-popup-blk');
//                    location.reload();
//                }
//        });
//    });
//}

function getSellerReviewInfo(url){
    jQuery('#vssmp_review_reason_content').hide();
    jQuery('#vssmp_review_rsn_content').html('');
    new Ajax.Request(url, {
            method:'post',
            requestHeaders: {Accept: 'application/json'},
            onSuccess: function(transport) {
                json = transport.responseText.evalJSON();
                if(json['found']){
                    jQuery('#vssmp_review_popup_heading').html(json['shop_title']);
                    jQuery('#vssmp_reviewer_txt').html(json['reviewer_info']);
                    jQuery('#vssmp_rating').css('width', json['rating']+'%');
                    jQuery('#vssmp_review_content').html(json['content']);
                    if(json['rsn'] != ''){
                        jQuery('#vssmp_review_rsn_content').html(json['rsn']);
                        jQuery('#vssmp_review_reason_content').show();
                    }
                    jQuery('#vssmp_popup_loader').hide();
                    jQuery('#vssmp_view_content').show();
                    jQuery('#vssmp_sellerreview-view-popup_blk').show();
                }else{
                    alert('Technical Error Occurred');
                    jQuery('#vssmp_sellerreview-view-popup_blk').hide();
                }
            }
    });
}

function deleteSellerReview(seller_review_id){
    jQuery('#vssmp-reason-popup-blk').show();
}

function approveSellerReview(seller_review_id){
    
}

function disapproveSellerReview(seller_review_id){
    jQuery('#vssmp-reason-popup-blk').show();
}

function approveSellerAccount(seller_id){
    var actionToPerform = confirm("Please confirm approval?");
    if (actionToPerform == true) {
        new Ajax.Request(approveSellerRequestUrl, {
                method:'post',
                parameters: {
                        id: seller_id 
                },
                requestHeaders: {Accept: 'application/json'},
                onSuccess: function(transport) {
                    location.reload();                    
                }
        });
        //location.href = "<?php echo Mage::getUrl('marketplace/index/register'); ?>";
    } else {
        return false;
    }
    
}

function disapproveSellerAccount(seller_id){
    jQuery('#vssmp-reason-popup-blk').show();
    jQuery('#vssmp_reason').removeClass('validation-failed');
    jQuery('#vssmp_submit_popup_form').click(function(){
        var reason = jQuery('#vssmp_reason').val();
        if(reason == '')
        {
            jQuery('#vssmp_reason').addClass('validation-failed');
            return false;
        }
        new Ajax.Request(disApproveSellerRequestUrl, {
                method:'post',
                parameters: {
                        id: seller_id,
                        reason: reason
                },
                requestHeaders: {Accept: 'application/json'},
                onSuccess: function(transport) {
                    closeModal('vssmp-reason-popup-blk');
                    location.reload();
                }
        });
    });    
}

function disapproveSellerProduct(seller_product_id){
    jQuery('#vssmp-reason-popup-blk').show();
}

function submitForm() {
        jQuery(".validation_error_msg").hide();
        var commission = jQuery("#vss-mp-seller-commission").val();
        var productLimit = jQuery("#vss-mp-product-limit").val();
        if(commission == '' || isNaN(commission) || commission > 100 || commission < 0 || !decimalOnly.test(commission))
        {
            jQuery("#vss_commission_error_field").show();
            return false;
        }
        if(isNaN(productLimit))
        {
            jQuery("#vss_product_limit_error_field").show();
//            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, 100);
            jQuery("#vss-mp-product-limit").css('border','1px solid red');
            return false;
        }
        jQuery("#vss_mp_general").submit();
}


function approveAction(url)
{
    location.href = url;
}

function takeReasonAction(id)
{
    jQuery('#vssmp-reason-form input[name="custom_id"]').val(id);
    jQuery('#vssmp-reason-popup-blk').show();
}


function vssmpGetReasonAndSubmit(){
    jQuery('#vssmp_reason').parent().find('.validation_error_msg').remove();
    var txt = jQuery('#vssmp_reason').val();
    txt = txt.replace(/^\s+|\s+$/g,'');
    if(txt == ''){
        jQuery('#vssmp_reason').parent().append('<span class="validation_error_msg">'+empty_field_error+'</span>');
    }else if(txt.length < vssmp_rsn_min_chars){
        jQuery('#vssmp_reason').parent().append('<span class="validation_error_msg">'+minimum_reason_length_error+'</span>');
    }else{
        jQuery('#vssmp_reason').attr('value', txt);
        jQuery('#vssmp-reason-form').submit();
    }
}

function deleteWithReason(id)
{
    jQuery('#vssmp-del-reason-form input[name="custom_id"]').val(id);
    jQuery('#vssmp-del-reason-popup-blk').show();
}


function vssmpGetDelReasonAndSubmit(){
    jQuery('#vssmp_del_reason').parent().find('.validation_error_msg').remove();
    var txt = jQuery('#vssmp_del_reason').val();
    txt = txt.replace(/^\s+|\s+$/g,'');
    if(txt == ''){
        jQuery('#vssmp_del_reason').parent().append('<span class="validation_error_msg">'+empty_field_error+'</span>');
    }else if(txt.length < vssmp_rsn_min_chars){
        jQuery('#vssmp_del_reason').parent().append('<span class="validation_error_msg">'+minimum_reason_length_error+'</span>');
    }else{
        jQuery('#vssmp_del_reason').attr('value', txt);
        jQuery('#vssmp-del-reason-form').submit();
    }
}

function submit_new_transaction()
{
    jQuery('#vssmp-new-transaction-form .validation_error_msg').remove();
    var error = false;
    var container = jQuery('#vssmp-new-transaction-form');
    if(jQuery('#vssmp-new-transaction-form select[name="transaction[seller_id]"]').val() == ''){
        error = true;
        jQuery('#vssmp-new-transaction-form select[name="transaction[seller_id]"]').parent().append('<span class="validation_error_msg">'+empty_field_error+'</span>');
    }
    
    if(jQuery('#vssmp-new-transaction-form input[name="[transaction_id]"]').val() == ''){
        error = true;
        jQuery('#vssmp-new-transaction-form input[name="[transaction_id]"]').parent().append('<span class="validation_error_msg">'+empty_field_error+'</span>');
    }
    
    if(jQuery('#vssmp-new-transaction-form input[name="transaction[amount]"]').val() == ''){
        error = true;
        jQuery('#vssmp-new-transaction-form input[name="transaction[amount]"]').parent().append('<span class="validation_error_msg">'+empty_field_error+'</span>');
    }else if(!decimalOnly.test(jQuery('#vssmp-new-transaction-form input[name="transaction[amount]"]').val())){
        error = true;
        jQuery('#vssmp-new-transaction-form input[name="transaction[amount]"]').parent().append('<span class="validation_error_msg">'+invalid_value_error+'</span>');
    }
    
    if(!error){
        jQuery('#vssmp-new-transaction-form').submit();
    }
}

function open_new_transaction_form()
{
    jQuery('#vssmp-new-transaction-form input[type="text"]').val('');
    jQuery('#vssmp-new-transaction-form select option').removeAttr('selected');
    jQuery('#vssmp-new-transaction-form select[name="transaction[seller_id]"]').attr('disabled', false);
    jQuery('#vssmp-new-transaction-form select textarea').val('');
    jQuery('#vssmp-new-transaction-form .validation_error_msg').remove();
    jQuery('#vssmp-new-transaction-form-modal').show();
}

function prepareNewTransactionForm(url, seller_id)
{
    new Ajax.Request(url, {
        method:'post',
        requestHeaders: {Accept: 'application/json'},
        onSuccess: function(transport) {
            json = transport.responseText.evalJSON();
            if(json['found']){
                jQuery('#vssmp-new-transaction-form select option').removeAttr('selected');
                jQuery('#vssmp-new-transaction-form select textarea').val('');
                jQuery('#vssmp-new-transaction-form .validation_error_msg').remove();
                jQuery('#vssmp-new-transaction-form input[name="transaction[amount]"]').val(json['amount']);
                jQuery('#vssmp-new-transaction-form input[name="transaction[seller_id]"]').val(seller_id);
                jQuery('#vssmp-new-transaction-form select[name="transaction[seller_id]"] option').each(function(){
                    if(jQuery(this).val() == seller_id){
                        jQuery(this).attr('selected', 'selected');
                    }
                });
                jQuery('#vssmp-new-transaction-form select[name="transaction[seller_id]"]').attr('disabled', true);
                jQuery('#vssmp-new-transaction-form-modal').show();
            }else{
                jQuery('#messages').html(json['msg']);
                jQuery('#messages').show();
                //alert('No Balance amount remaining.');
            }
        }
    });
}