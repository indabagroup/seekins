function general()
{
    document.getElementById("general").style.display = "block";
    document.getElementById("youtube").style.display = "none";
    document.getElementById("vimeo").style.display = "none";
    document.getElementById("dailymotion").style.display = "none";
    
    var divLeftGeneral = document.getElementById("div_left_general").classList;
    divLeftGeneral.add("active");
    
    var divLeftYoutube = document.getElementById("div_left_youtube").classList;
    divLeftYoutube.remove("active");
    
    var divLeftVimeo = document.getElementById("div_left_vimeo").classList;
    divLeftVimeo.remove("active");
    
    var divLeftDailymotion = document.getElementById("div_left_dailymotion").classList;
    divLeftDailymotion.remove("active");
}
function youtubeSettings()
{
    document.getElementById("general").style.display = "none";
    document.getElementById("youtube").style.display = "block";
    document.getElementById("vimeo").style.display = "none";
    document.getElementById("dailymotion").style.display = "none";
    
    var divLeftGeneral = document.getElementById("div_left_general").classList;
    divLeftGeneral.remove("active");
    
    var divLeftYoutube = document.getElementById("div_left_youtube").classList;
    divLeftYoutube.add("active");
    
    var divLeftVimeo = document.getElementById("div_left_vimeo").classList;
    divLeftVimeo.remove("active");
    
    var divLeftDailymotion = document.getElementById("div_left_dailymotion").classList;
    divLeftDailymotion.remove("active")
}

function vimeoSettings()
{
    document.getElementById("general").style.display = "none";
    document.getElementById("youtube").style.display = "none";
    document.getElementById("vimeo").style.display = "block";
    document.getElementById("dailymotion").style.display = "none";
    
    var divLeftGeneral = document.getElementById("div_left_general").classList;
    divLeftGeneral.remove("active");
    
    var divLeftYoutube = document.getElementById("div_left_youtube").classList;
    divLeftYoutube.remove("active");
    
    var divLeftVimeo = document.getElementById("div_left_vimeo").classList;
    divLeftVimeo.add("active");
    
    var divLeftDailymotion = document.getElementById("div_left_dailymotion").classList;
    divLeftDailymotion.remove("active")
}

function dailymotionSettings()
{
    document.getElementById("general").style.display = "none";
    document.getElementById("youtube").style.display = "none";
    document.getElementById("vimeo").style.display = "none";
    document.getElementById("dailymotion").style.display = "block";
    
    var divLeftGeneral = document.getElementById("div_left_general").classList;
    divLeftGeneral.remove("active");
    
    var divLeftYoutube = document.getElementById("div_left_youtube").classList;
    divLeftYoutube.remove("active");
    
    var divLeftVimeo = document.getElementById("div_left_vimeo").classList;
    divLeftVimeo.remove("active");
    
    var divLeftDailymotion = document.getElementById("div_left_dailymotion").classList;
    divLeftDailymotion.add("active")
}

// JQUERY AND ALL
function submitForm() {
    
    var canSubmit = validateForm();
    if(canSubmit == 1)
    {
        jQuery("#form_general").submit();
    }
}

function validateForm()
{

    var error = 0;
    if(error == 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

function submitEditForm()
{
    var canSubmit = validateEditForm();
    if(canSubmit == 1)
    {
        jQuery("#form_edit").submit();
    }
}

function validateEditForm()
{
    var videoName = jQuery("#video_name").val();
    var videoCode = jQuery("#video_path").val();
    var errorVideoName = 0;
    var errorVideoCode = 0;
    var error = 0;
    
    if(videoName == "")
    {
        jQuery("#error_video_name").html("Please provide name for video.");
        jQuery("#error_video_name").show();
        errorVideoName = 1;
        error = 1;
    }
    else
    {
        jQuery("#error_video_name").hide();
    }
    if(videoCode == "")
    {
        jQuery("#error_video_path").html("Please provide the video code of the video.");
        jQuery("#error_video_path").show();
        errorVideoCode = 1;
        error = 1;
    }
    else
    {
        jQuery("#error_video_path").hide();
    }
    if(error == 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

function submitAddForm()
{
    var canSubmit = validateAddForm();
    if(canSubmit == 1)
    {
        jQuery("#form_add").submit();
    }
}

function validateAddForm()
{
    var videoName = jQuery("#video_name").val();
    var videoCode = jQuery("#video_path").val();
    var errorVideoName = 0;
    var errorVideoCode = 0;
    var error = 0;
    
    if(videoName == "")
    {
        jQuery("#error_video_name").html("Please provide name for video.");
        jQuery("#error_video_name").show();
        errorVideoName = 1;
        error = 1;
    }
    else
    {
        jQuery("#error_video_name").hide();
    }
    if(videoCode == "")
    {
        jQuery("#error_video_path").html("Please provide the video code of the video.");
        jQuery("#error_video_path").show();
        errorVideoCode = 1;
        error = 1;
    }
    else
    {
        jQuery("#error_video_path").hide();
    }
    if(error == 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

function canDelete()
{
    var checkedBoxes = document.getElementsByName("mass_delete");
    var j = 0;
    var arrayId = new Array();
    for(var i=0; i < checkedBoxes.length; i++){
        if(checkedBoxes[i].checked){
            arrayId[i] = checkedBoxes[i].value;
            j++;
        }
    }

    if(j == 0)
    {
        var divToastClone = document.getElementById("tost_clone");
        divToastClone.style.opacity = 1;
        setTimeout(function () { 
            divToastClone.style.opacity = 0;
        }, 2000);
    }
    else
    {
        var canDelete = confirm("Are you sure to delete the selected video(s)?");
        if(canDelete)
        {
            var jsonString = JSON.stringify(arrayId);
            window.location = window.url+"?data="+jsonString+"";
        }
    }
}

function changeHelpDiv()
{
    var helpDivValue = document.getElementById("video_type").value;
    if(helpDivValue == "YouTube")
    {
        document.getElementById("help_youtube").style.display = "block";
        document.getElementById("help_vimeo").style.display = "none";
        document.getElementById("help_dailymotion").style.display = "none";
    }
    if(helpDivValue == "Vimeo")
    {
        document.getElementById("help_youtube").style.display = "none";
        document.getElementById("help_vimeo").style.display = "block";
        document.getElementById("help_dailymotion").style.display = "none";
    }
    if(helpDivValue == "Dailymotion")
    {
        document.getElementById("help_youtube").style.display = "none";
        document.getElementById("help_vimeo").style.display = "none";
        document.getElementById("help_dailymotion").style.display = "block";
    }
}