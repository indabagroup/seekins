/**
 * Created with JetBrains PhpStorm.
 * User: camz
 * Date: 7/30/14
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */
function saveAssociateFfl(url){

    $$('input[type="radio"]').each(function(c){
        if ($(c).checked) {
            var ffl_id = $(c).value;
            url  = url + 'ffl_id/'+ffl_id
            setLocation(url);
        }
    });

    return false;


}

function openNewTab(url){
    var win=window.open(url, '_blank');
    win.focus();
}
Validation.add('validate-ffl-number-format', 'Please enter a valid FFL number. For example 1-12-123-12-12-12345.', function(v) {
    return Validation.get('IsEmpty').test(v) || /^[a-zA-Z0-9]{1}?(-|\s)?[a-zA-Z0-9]{2}(-|\s)[a-zA-Z0-9]{3}?(-|\s)?[a-zA-Z0-9]{2}?(-|\s)?[a-zA-Z0-9]{2}?(-|\s)?[a-zA-Z0-9]{5}$/.test(v);

});